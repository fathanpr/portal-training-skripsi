$(document).ready(function() {
    $('#budgetTable').DataTable();
});
$(document).ready(function() {
    $('#example').DataTable();
    $('#external').DataTable();
    $('#tnaAtmp').DataTable();
    $('#unplannedAtmp').DataTable();
    $('#otherAtmp').DataTable();
    $('#dataRS').DataTable();
    $('#statusTna').DataTable({
        searching: true,
        columnDefs: [
            { targets: [0, 1], searchable: true }
        ]
    });
});

$(document).ready(function() {
    $('#mytable').DataTable();
});


const warning = $('.warning').data('warning')
if (warning) {
    Swal.fire({
        icon: 'error',
        title: warning,
        text: '',
    })
}

const talk = $('.talk').data('talk')
if (talk) {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: talk,
    })
}

const list = $('.success').data('success')
if (list) {
    Swal.fire({
        title: list,
        text: '',
        icon: 'success'
    })
}

$(document).ready(function() {
    $('#example2').DataTable();
});


$('.btn-delete').on('click', function(e) {
    e.preventDefault();
    const href = $(this).parents('form');
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Menghapus Data ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus Data!'
    }).then((result) => {
        if (result.isConfirmed) {
            href.submit();
        }
    })
});

//user
$(document).ready(function() {
    $('#user-table').DataTable();
});

$(document).ready(function() {
    $('#member').DataTable();
});

$(document).ready(function() {
    $('#personal-schedule').DataTable();
});

$(document).ready(function() {
    $('#History').DataTable();
});