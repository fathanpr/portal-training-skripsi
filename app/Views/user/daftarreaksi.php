<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<?php 
$page = basename($_SERVER['PHP_SELF']); ?>
<div class="talk" data-talk="<?= session()->get('talk'); ?>"></div>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title">
            Evaluasi Pelatihan
        </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Training Title</th>
                    <th>Training Category</th>
                    <th>Training Type</th>
                    <th>Training Category</th>
                    <th>Training Method</th>
                    <th>Training Goals</th>
                    <th>Training Implementation</th>
                    <th>Module</th>
                    <th>Description</th>
                </tr>
            </thead>
            <?php foreach ($evaluasi as $Atmps) : ?>
            <tr>
                <td><?= $Atmps['nama'] ?></td>
                <td><?= $Atmps['departemen'] ?></td>
                <td><?= $Atmps['training'] ?></td>
                <td>
                    <?php if($Atmps['kelompok_training'] == 'amdi_aop') : ?>
                        <span class="badge badge-info">AMDI AOP</span>
                        <?php elseif($Atmps['kelompok_training'] == 'cross_budget') :  ?>
                        <span class="badge badge-success">CROSS BUDGET</span>
                        <?php elseif($Atmps['kelompok_training'] == 'training') : ?>
                        <span class="badge badge-primary">TRAINING</span>
                        <?php else : ?>
                        <span class="badge badge-warning">UNPLANNED</span>
                    <?php endif; ?>
                </td>
                <td><?= $Atmps['jenis_training'] ?></td>
                <td><?= $Atmps['kategori_training'] ?></td>
                <td><?= $Atmps['metode_training'] ?></td>
                <td><?= $Atmps['tujuan_training'] ?></td>
                <td><?= date('d M', strtotime($Atmps['mulai_training'])) . ' - ' . date('d M Y', strtotime($Atmps['rencana_training'])) ?></td>
                <td>
                    <?= $Atmps['status_evaluasi'] == null ? '<p>Material</p>' : '<a href="'.$Atmps['material_path'].'">Material</a>' ?>
                </td>
                <td>
                    <div class="d-flex justify-content-start">
                        <?php 
                            if(($page == 'detail_evaluasi_member' || $page == 'detail_evaluasi_member_unplanned' || $page == 'detail_evaluasi_member_other') && $Atmps['status_evaluasi'] == null) {
                                echo '<h5><span class="badge badge-danger">NOT YET EVALUATED</span></h5>';
                            } else {
                                $status = $Atmps['status_evaluasi'] == null ? 'NOT YET EVALUATED' : 'ALREADY EVALUATED';
                                $color = $Atmps['status_evaluasi'] == null ? 'danger' : 'success';
                                $url = $Atmps['status_evaluasi'] == null ? 'form_evaluasi' : 'form_evaluasi_selesai';
                                if($page == 'evaluasi_reaksi_other' || $page == 'detail_evaluasi_member_other') {
                                    $url = $Atmps['status_evaluasi'] == null ? 'form_evaluasi' : 'form_evaluasi_selesai';
                                } elseif($page != 'evaluasi_reaksi' && $page != 'detail_evaluasi_member') {
                                    $url = $Atmps['status_evaluasi'] == null ? 'form_evaluasi_unplanned' : 'form_unplanned_selesai';
                                }
                                echo '<a href="'.base_url().$url.'/'.$Atmps['id_tna'].'" class="btn btn-'.$color.'">'.$status.'</a>';
                            }
                        ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
    <tbody>
    </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
<?= $this->endSection() ?>