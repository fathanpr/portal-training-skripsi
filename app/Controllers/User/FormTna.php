<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_Approval;
use App\Models\M_EvaluasiEfektifitas;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_History;
use App\Models\M_ListTraining;
use App\Models\M_Tna;
use App\Models\UserModel;
use App\Models\M_CompetencyAstra;
use App\Models\M_CompetencyTechnical;
use App\Models\M_Astra;
use App\Models\M_Soft;
use App\Models\M_Company;
use App\Models\M_TechnicalB;
use App\Models\M_Budget;
use App\Models\M_CompetencyCompany;
use App\Models\M_CompetencyExpert;
use App\Models\M_CompetencySoft;
use App\Models\M_CompetencyTechnicalB;
use App\Models\M_Nilai;
use App\Models\M_Technical;

class FormTna extends BaseController
{
    private UserModel $user;
    private M_ListTraining $training;
    private M_Tna $tna;
    private M_Approval $approval;
    private M_EvaluasiReaksi $evaluasiReaksi;
    private M_EvaluasiEfektifitas $efektivitas;
    private M_History $history;

    private M_Astra $astra;
    private M_Soft $soft;
    private M_Company $company;
    private M_CompetencyAstra $competencyAstra;
    private M_Technical $technical;
    private M_TechnicalB $technicalB;
    private M_CompetencyTechnical $competencyTechnical;

    private M_Budget $budget;

    private M_CompetencyExpert $competencyExpert;
    private M_CompetencySoft $competencySoft;
    private M_CompetencyCompany $competencyCompany;
    private M_CompetencyTechnicalB $competencyTechnicalB;
    private M_Nilai $nilai;


    public function __construct()
    {
        $this->training = new M_ListTraining();
        $this->user = new UserModel();
        $this->tna = new M_Tna();
        $this->approval = new M_Approval();
        $this->evaluasiReaksi = new M_EvaluasiReaksi();
        $this->history = new M_History();
        $this->efektivitas = new M_EvaluasiEfektifitas();
        $this->astra = new M_Astra();
        $this->soft = new M_Soft();
        $this->company = new M_Company();
        $this->technicalB = new M_TechnicalB();
        $this->competencyAstra = new M_CompetencyAstra();
        $this->technical = new M_Technical();
        $this->competencyTechnical = new M_CompetencyTechnical();
        $this->budget = new M_Budget();
        $this->competencyCompany = new M_CompetencyCompany();
        $this->competencyExpert = new M_CompetencyExpert();
        $this->competencySoft = new M_CompetencySoft();
        $this->competencyTechnicalB = new M_CompetencyTechnicalB();
        $this->nilai = new M_Nilai();
    }

    public function index()
    {
        $id = session()->get('id');
        $seksi = session()->get('seksi');
        $user = $this->user->filter($id);

        $isOperator = false;
        foreach ($user as $u) {
            if ($u->nama_jabatan == 'Operator') {
                $isOperator = true;
                break;
            }
        }
        
        if($isOperator == TRUE){
            $user_member = $this->user->filterOperator($seksi);
        } else{
            $user_member = [];
        }

        $departemen = $this->tna->getTnaFilterDistinct($id);
        $tna = $this->tna->getTnaFilter($id);
        $budget = $this->budget->getBudgetNextYear(session()->get('departemen'));

        if(empty($budget)){
            $budget = $this->budget->getBudgetCurrent(session()->get('departemen'));
        }

        $data = [
            'tittle' => 'Data Member',
            'user' => $user,
            'user_member' => $user_member,
            'tna' => $tna,
            'budget' => $budget,
            'departemen' => $departemen,
            'tnaKadept' => $this->tna,
            'budgetKadept' => $this->budget,
        ];
        return view('user/datamember', $data);
    }

    public function trainingSubmitted() {
        $id_user = $this->request->getPost('id_user');
        $npk = $this->request->getPost('npk');
        $bagian = $this->request->getPost('bagian');
        $data = $this->tna->getDataTrainingSubmitted($id_user,$npk,$bagian);
        return $this->response->setJSON($data);
    }

    public function TnaUser()
    {
        $id = $this->request->getPost('member');
        $user = $this->user->getAllUser($id);
        $npk = $user['npk'];
        $bagian = $user['bagian'];
        $trainings = $this->training->getAll();
        $tna = $this->tna->getHistoryTna($bagian,$npk);
        $tnaUser = $this->tna->getUserTna($bagian,$npk);
        $terdaftar = $this->tna->getTnaTerdaftar($bagian,$npk);

        $array = [];
        foreach ($tnaUser as $usersTna) {
            $trainingUser = $this->training->getIdTraining($usersTna['id_training']);
            // dd($trainingUser);
            if(empty($trainingUser)){
                $trainingProcess = [];
            } else{
                $trainingProcess =
                    [
                        'id_training' => $usersTna['id_training'],
                        'judul_training' => $usersTna['training'],
                        'jenis_training' => $usersTna['jenis_training'],
                        'deskripsi' => $trainingUser['deskripsi'],
                        'vendor' => $usersTna['vendor'],
                        'biaya' => $usersTna['biaya']
                    ];
                }
                array_push($array, $trainingProcess);
        }

        if (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'REGULAR') {
            //Filter Astra Competency
            $datas  = $this->competencyAstra->getProfileAstraCompetency($id, $user['npk']);
            $astra = [];
            if (!empty($datas)) {

                foreach ($datas as $data) {
                    if ($data['score_astra'] < $data['proficiency']) {
                        $competency = [
                            'id' => $data['id_competency_astra'],
                            'category' => "ALC - " . $data['astra'],
                            'competency' => $data['astra'],
                            'proficiency' => $data['proficiency'],
                            'score' => $data['score_astra'],
                            'keterangan' => 'Astra'
                        ];
                        array_push($astra, $competency);
                    }
                }
            } else {
                $astra = [];
            }
            //Filter Tecnhnical Competency
            $datas2  = $this->competencyTechnical->getProfileTechnicalCompetency($id, $user['npk']);
            $technical = [];
            //dd($datas);
            if (!empty($datas2)) {

                foreach ($datas2 as $dataTech) {
                    if ($dataTech['score_technical'] < $dataTech['proficiency']) {
                        $competencyTech = [
                            'id' => $dataTech['id_competency_technical'],
                            'category' => "Technical Comp - " . $dataTech['technical'],
                            'competency' => $dataTech['technical'],
                            'proficiency' => $dataTech['proficiency'],
                            'score' => $dataTech['score_technical'],
                            'keterangan' => 'TechnicalA'
                        ];
                        array_push($technical, $competencyTech);
                    }
                }
            } else {
                $technical = [];
            }

            $dataCompany  = $this->competencyCompany->getProfileCompanyCompetency($id,$user['npk']);
            $company = [];
            if (!empty($dataCompany)) {

                foreach ($dataCompany as $DataCompany) {
                    if ($DataCompany['score_company'] < $DataCompany['proficiency']) {
                        $competency = [
                            'id' => $DataCompany['id_competency_company'],
                            'category' => "Comp - " . $DataCompany['company'],
                            'competency' => $DataCompany['company'],
                            'proficiency' => $DataCompany['proficiency'],
                            'score' => $DataCompany['score_company'],
                            'keterangan' => 'Company'
                        ];
                        array_push($company, $competency);
                    }
                }
            } else {
                $company = [];
            }

            $dataTechnicalB  = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id,$user['npk']);
            $technicalB = [];

            if (!empty($dataTechnicalB)) {

                foreach ($dataTechnicalB as $dataTechnicalB) {
                    if ($dataTechnicalB['score'] < $dataTechnicalB['proficiency']) {
                        $competency = [
                            'id' => $dataTechnicalB['id_competency_technicalB'],
                            'category' => "TechB - " . $dataTechnicalB['technicalB'],
                            'competency' => $dataTechnicalB['technicalB'],
                            'proficiency' => $dataTechnicalB['proficiency'],
                            'score' => $dataTechnicalB['score'],
                            'keterangan' => 'TechnicalB'
                        ];
                        array_push($technicalB, $competency);
                    }
                }
            } else {
                $technicalB = [];
            }

            $dataSoft  = $this->competencySoft->getProfileSoftCompetency($id,$user['npk']);
            $soft = [];
            if (!empty($dataSoft)) {

                foreach ($dataSoft as $DataSoft) {
                    if ($DataSoft['score_soft'] < $DataSoft['proficiency']) {
                        $competency = [
                            'id' => $DataSoft['id_competency_soft'],
                            'category' => "Soft - " . $DataSoft['soft'],
                            'competency' => $DataSoft['soft'],
                            'proficiency' => $DataSoft['proficiency'],
                            'score' => $DataSoft['score_soft'],
                            'keterangan' => 'Soft'
                        ];
                        array_push($soft, $competency);
                    }
                }
            } else {
                $soft = [];
            }

            $target = array_merge($astra, $technical, $company, $technicalB, $soft);
            $data = [
                'tittle' => 'TRAINING',
                'user' => $user,
                'type_golongan' => $user['type_golongan'],
                'training' => $trainings,
                'tna' => $tna,
                'terdaftar' => $terdaftar,
                'astra' => $astra,
                'technical' => $technical,
                'target' => $target,
                'company' => $company,
                'technicalB' => $technicalB,
                'soft' => $soft,
                'validation' => \Config\Services::validation(),
            ];

        } elseif (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'EXPERT') {
            
            //Filter Expert Competency
            $dataExpert  = $this->competencyExpert->getProfileExpertCompetency($id,$user['npk']);
            $Expert = [];
            if (!empty($dataExpert)) {

                foreach ($dataExpert as $DataExpert) {
                    if ($DataExpert['score_expert'] < $DataExpert['proficiency']) {
                        $competency = [
                            'id' => $DataExpert['id_competency_expert'],
                            'category' => "Exp - " . $DataExpert['expert'],
                            'competency' => $DataExpert['expert'],
                            'proficiency' => $DataExpert['proficiency'],
                            'score' => $DataExpert['score_expert'],
                            'keterangan' => 'Expert'
                        ];
                        array_push($Expert, $competency);
                    }
                }
            } else {
                $Expert = [];
            }


            //Filter Tecnhnical Competency
            $datas2  = $this->competencyTechnical->getProfileTechnicalExpertCompetency($id,$user['npk']);
            $technical = [];
            //dd($datas);
            if (!empty($datas2)) {

                foreach ($datas2 as $dataTech) {
                    if ($dataTech['score_technical'] < $dataTech['proficiency']) {
                        $competencyTech = [
                            'id' => $dataTech['id_competency_technical'],
                            'category' => "Technical Comp - " . $dataTech['technical'],
                            'competency' => $dataTech['technical'],
                            'proficiency' => $dataTech['proficiency'],
                            'score' => $dataTech['score_technical'],
                            'keterangan' => 'TechnicalA'
                        ];
                        array_push($technical, $competencyTech);
                    }
                }
            } else {
                $technical = [];
            }

            $target = array_merge($Expert, $technical);
            $data = [
                'tittle' => 'TRAINING',
                'user' => $user,
                'training' => $trainings,
                'type_golongan' => $user['type_golongan'],
                'tna' => $tna,
                'tnaModel' => $this->tna,
                'terdaftar' => $terdaftar,
                'expert' => $Expert,
                'technical' => $technical,
                'target' => $target,
                'validation' => \Config\Services::validation(),
            ];
            // dd($data);

        } else {
            $dataCompany  = $this->competencyCompany->getProfileCompanyCompetency($id,$user['npk']);
            $Company = [];
            if (!empty($dataCompany)) {

                foreach ($dataCompany as $DataCompany) {
                    if ($DataCompany['score_company'] < $DataCompany['proficiency']) {
                        $competency = [
                            'id' => $DataCompany['id_competency_company'],
                            'category' => "Comp - " . $DataCompany['company'],
                            'competency' => $DataCompany['company'],
                            'proficiency' => $DataCompany['proficiency'],
                            'score' => $DataCompany['score_company'],
                            'keterangan' => 'Company'
                        ];
                        array_push($Company, $competency);
                    }
                }
            } else {
                $Company = [];
            }

            $dataSoft  = $this->competencySoft->getProfileSoftCompetency($id,$user['npk']);
            $Soft = [];
            if (!empty($dataSoft)) {
                foreach ($dataSoft as $DataSoft) {
                    if ($DataSoft['score_soft'] < $DataSoft['proficiency']) {
                        $competency = [
                            'id' => $DataSoft['id_competency_soft'],
                            'category' => "Soft - " . $DataSoft['soft'],
                            'competency' => $DataSoft['soft'],
                            'proficiency' => $DataSoft['proficiency'],
                            'score' => $DataSoft['score_soft'],
                            'keterangan' => 'Soft'
                        ];
                        array_push($Soft, $competency);
                    }
                }
            } else {
                $Soft = [];
            }

            $dataTechnicalB  = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id,$user['npk']);
            $TechnicalB = [];

            if (!empty($dataTechnicalB)) {
                foreach ($dataTechnicalB as $dataTechnicalB) {
                    if ($dataTechnicalB['score'] < $dataTechnicalB['proficiency']) {
                        $competency = [
                            'id' => $dataTechnicalB['id_competency_technicalB'],
                            'category' => "TechB - " . $dataTechnicalB['technicalB'],
                            'competency' => $dataTechnicalB['technicalB'],
                            'proficiency' => $dataTechnicalB['proficiency'],
                            'score' => $dataTechnicalB['score'],
                            'keterangan' => 'TechnicalB'
                        ];
                        array_push($TechnicalB, $competency);
                    }
                }
            } else {
                $TechnicalB = [];
            }
            $target = array_merge($Soft, $Company, $TechnicalB);
            $data = [
                'tittle' => 'TRAINING',
                'user' => $user,
                'training' => $trainings,
                'type_golongan' => $user['type_golongan'],
                'tna' => $tna,
                'tnaModel' => $this->tna,
                'terdaftar' => $terdaftar,
                'soft' => $Soft,
                'technicalB' => $TechnicalB,
                'company' => $Company,
                'target' => $target,
                'validation' => \Config\Services::validation(),
            ];
        }

        return view('user/formtna', $data);
    }


    //function untuk status tna yang sudah dikirim ke admin
    public function status()
    {
        $id =  session()->get('id');
        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');
        $seksi = session()->get('seksi');
        $npk = session()->get('npk');

        if ($bagian == 'BOD') {
            $status =  $this->tna->getStatusWaitUser($bagian, $dic);
        } elseif ($bagian == 'KADIV') {
            $status =  $this->tna->getStatusWaitUser($bagian, $divisi);
        } elseif ($bagian == 'KADEPT') {
            $status = $this->tna->getStatusWaitUser($bagian, $departemen);
        } else {
            $status =  $this->tna->getStatusWaitUser($bagian, $seksi);
        }
        $data = [
            'tittle' => 'Status TNA',
            'status' => $status,
        ];

        // dd($status);
        return view('user/statustna', $data);
    }

    public function statusFilter($year)
    {
        $id =  session()->get('id');
        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');
        $seksi = session()->get('seksi');

        // dd($id);

        if ($bagian == 'BOD') {
            $status =  $this->tna->getStatusWaitUserFilter($bagian, $dic ,$year);
        } elseif ($bagian == 'KADIV') {
            $status =  $this->tna->getStatusWaitUserFilter($bagian, $divisi ,$year);
        } elseif ($bagian == 'KADEPT') {
            $status = $this->tna->getStatusWaitUserFilter($bagian, $departemen ,$year);
        } else {
            $status =  $this->tna->getStatusWaitUserFilter($bagian, $seksi ,$year);
        }
        // dd($status);
        $data = [
            'tittle' => 'Status TNA',
            'status' => $status,
        ];

        // dd($status);
        return view('user/statustna', $data);
    }

    public function statusPersonal()
    {
        $id =  session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');
        $status = $this->tna->getStatusPersonal($id,$npk,$bagian);
        $data = [
            'tittle' => 'Status TNA',
            'status' => $status,
        ];
        return view('user/statustnapersonal', $data);
    }

    public function statusPersonalFilter($year)
    {
        $id =  session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');
        $status = $this->tna->getStatusPersonalFilter($id,$year,$npk,$bagian);
        $data = [
            'tittle' => 'Status TNA',
            'status' => $status,
        ];
        return view('user/statustnapersonal', $data);
    }
    public function statusPersonalUnplanned()
    {
        $id =  session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');
        $status = $this->tna->getStatusPersonalUnplanned($id,$npk,$bagian);
        $data = [
            'tittle' => 'Status Unplanned',
            'status' => $status,
        ];
        return view('user/statusunplannedpersonal', $data);
    }

    // public function statusMember()
    // {
    //     $id = session()->get('id');
    //     $data = $this->tna->getStatusWaitMember($id);
    //     dd($data);
    // }

    public function approve()
    {
        $data = [
            'tittle' => 'Approve TNA',
        ];
        return view('user/approvetna', $data);
    }


    //function untuk mengupdate status tna ke wait

    public function TnaSend()
    {
        $bagian = session()->get('bagian');
        $temporary_budget = $this->request->getPost('temporary_budget');
        
        $page = basename($_SERVER['PHP_SELF']);

        if ($bagian == 'BOD') {
            if (empty($_POST['training'])) {
                return redirect()->to('/data_member');
            } else {
                $data = $_POST['training'];
            }

            for ($i = 0; $i < count($data); $i++) {
                $update = $this->tna->getAllTna($data[$i]);
                $tna = [
                    'id_tna' => $update[0]->id_tna,
                    'status' => 'wait',
                ];
                $approvals = $this->approval->getIdApproval($update[0]->id_tna);

                $dataApproval = [
                    'id_approval' => $approvals['id_approval'],
                    'status_approval_1' => 'accept',
                ];
                $this->tna->save($tna);
                $this->approval->save($dataApproval);
            }
            if ($page == 'send') {
                return redirect()->to('/data_member');
            } else {
                return redirect()->to('/data_member_unplanned');
            }
        } else {
            if (empty($_POST['training'])) {
                return redirect()->to('/data_member');
            } else {
                $data = $_POST['training'];
            }

            for ($i = 0; $i < count($data); $i++) {
                $update = $this->tna->getAllTna($data[$i]);
                $tna = [
                    'id_tna' => $update[0]->id_tna,
                    'status' => 'wait',

                ];
                $this->tna->save($tna);
            }
            if ($page == 'send') {
                return redirect()->to('/data_member');
            } else {
                return redirect()->to('/data_member_unplanned');
            }
        }
    }

    public function AjaxTna()
    {
        $id_training = $this->request->getPost('id_training');
        $jenis_trainng = $this->training->getIdTraining($id_training);

        echo json_encode($jenis_trainng);
    }

    //function untuk save tna
    public function TnaForm()
    {
        $competency = $this->request->getPost('kompetensi');
        $id_training = $this->request->getPost('training');

        if ($competency !== null) {
            $type_kompetensi = explode("," , $competency);
            $deadline = $this->request->getVar('deadline');
            if ($deadline == 0) {
                $kelompok = 'training';
            } else {
                $kelompok = 'unplanned';
            }
    
            $id_user = $this->request->getPost('id_user');
            $npk = $this->request->getPost('npk');
            $bagian = $this->request->getPost('bagian');
            $id = $this->request->getPost('trainingunplanned');

            if ($id == null) {
                $id_training = $_POST['training'];
            } else {
                $id_training = $id;
            }
            $user = $this->user->get_all_user_with_npk($id_user,$npk);
            //dd($user);
            $jenis_trainng = $this->training->getIdTraining($id_training);
    
            $budget = $this->budget->getBudgetNextYear($user['departemen']);
            if(empty($budget)){
                $budget = $this->budget->getBudgetCurrent(session()->get('departemen'));
            }
    
            if (empty($budget['available_budget'])) {
                session()->setFlashdata('warning', 'Budget Tidak Mencukupi!');
                return redirect()->to('/data_member');
            } else {
                if ($jenis_trainng['biaya'] > $budget['available_budget']) {
                    session()->setFlashdata('warning', 'Budget Melebihi yang Tersedia!');
                    return redirect()->to('/data_member');
                }
            }

            $data = [
                'id_user' => $id_user,
                'npk' => $npk,
                'bagian'=> $bagian,
                'id_training' => $id_training,
                'id_budget' => $budget['id_budget'],
                'nama' => $user['nama'],
                'golongan' => null,
                'jenis_training' => $jenis_trainng['category'],
                'kategori_training' => $this->request->getVar('kategori'),
                'training' => $jenis_trainng['judul_training'],
                'metode_training' => $this->request->getVar('metode'),
                'request_training' => $this->request->getVar('request'),
                'tujuan_training' => $this->request->getVar('tujuan'),
                'notes' => $this->request->getVar('notes'),
                'biaya' => $jenis_trainng['biaya'],
                'status' => 'save',
                'kelompok_training' => $kelompok
            ];
            
            $this->tna->save($data);
    
            $id  = $this->tna->getIdTna();
    
            $data2 = [
                'id_tna' => $id->id_tna,
                'id_user' => $id->id_user,
                'npk' => $id->npk,
                'bagian' => $id->bagian,
            ];
            $data3 = [
                'id_tna' => $id->id_tna,
            ];
            $data4 = [
                'id_tna' => $id->id_tna
            ];
            $data5 = [
                'id_tna' => $id->id_tna,
                'id_user' => $id_user,
                'npk' => $npk,
                'bagian' => $bagian,
            ];
            $nilai = [
                'id_tna' => $id->id_tna,
                'id_competency1' => $type_kompetensi[0],
                'type_competency1' => $type_kompetensi[1]
            ];
    
            // dd($this->tna->save($data));
    
    
            $this->nilai->save($nilai);
            $this->approval->save($data2);
            $this->evaluasiReaksi->save($data3);
            $this->history->save($data4);
            $this->efektivitas->save($data5);
    
            session()->setFlashdata('success', 'TNA Berhasil Disimpan');
            if ($kelompok == 'training') {
                return redirect()->to('/data_member');
            } else {
                return redirect()->to('/data_member_unplanned');
            }
        } else {
            session()->setFlashdata('warning', 'Form tidak boleh kosong!');
            return redirect()->to('/data_member');
        }
    }

    public function cekTraining(){
        $id_user = $this->request->getVar('id_user');
        $npk = $this->request->getVar('npk');
        $bagian = $this->request->getVar('bagian');
        $id_training = $this->request->getVar('id_training');

        $trained = $this->tna->getTnaByIdTraining($bagian,$id_training,$npk);
        return $this->response->setJSON($trained);
    }


    public function requestTna()
    {
        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');

        if ($bagian == 'BOD') {
            $distinct = $this->tna->getRequestTnaDisntinct($bagian, $dic);
        } elseif ($bagian == 'KADIV') {
            $distinct = $this->tna->getRequestTnaDisntinct($bagian, $divisi);
        } elseif ($bagian == 'KADEPT') {
            $distinct = $this->tna->getRequestTnaDisntinct($bagian, $departemen);
        }
        //dd($distinct);
        $data = [
            'tittle' => 'Request TNA',
            'departemen' => $distinct,
            'status' => $this->tna,
            'budget' => $this->budget
        ];

        if ($bagian == 'KADEPT' || $bagian == 'KADIV') {
            return view('user/requestuser', $data);
        } else {
            return view('user/requestuserbod', $data);
        }
    }


    //function accept kadiv
    public function acceptKadiv()
    {
        $bagian = session()->get('bagian');
        $approve = $this->approval->getIdApproval($this->request->getPost('id_tna'));
        $biaya_actual = $this->request->getPost('biaya_actual');
        // KADIV APPROVAL
        if ($bagian == 'KADIV') {
            if($biaya_actual <= 2000000){
                $data = [
                    'id_approval' => $approve['id_approval'],
                    'id_tna' => $this->request->getPost('id_tna'),
                    'status_approval_1' => 'accept',
                    'status_approval_2' => 'accept'
                ];
            }else{
                $data = [
                    'id_approval' => $approve['id_approval'],
                    'id_tna' => $this->request->getPost('id_tna'),
                    'status_approval_1' => 'accept',
                ];
            }
            $this->approval->save($data);
        } else {
            // KADEPT APPROVAL
            $data = [
                'id_approval' => $approve['id_approval'],
                'id_tna' => $this->request->getPost('id_tna'),
                'status_approval_0' => 'accept'
            ];
            $this->approval->save($data);
        }
        // dd($biaya_actual);

        echo json_encode('SUCCESS');
    }
    public function rejectKadiv()
    {
        $bagian = session()->get('bagian');
        $approve = $this->approval->getIdApproval($this->request->getPost('id_tna'));
        $training = $this->tna->getAllTna($this->request->getPost('id_tna'));
        $budgetData = $this->budget->getBudgetById($training[0]->id_budget);
        // $this->subtraction($training[0]->biaya_actual, $training[0]->departemen);

        if ($bagian == 'KADIV') {
            $data = [
                'id_approval' => $approve['id_approval'],
                'alasan' => $this->request->getPost('alasan'),
                'status_approval_1' => 'reject',
                'status_approval_2' => 'reject',
                'status_approval_3' => 'reject'
            ];

            $data_budget = [
                'id_budget' => $training[0]->id_budget,
                'temporary_calculation' => $budgetData['temporary_calculation'] - $training[0]->biaya_actual
            ];
            $this->budget->save($data_budget);
            $this->approval->save($data);
        } else {
            $data = [
                'id_approval' => $approve['id_approval'],
                'alasan' => $this->request->getPost('alasan'),
                'status_approval_0' => 'reject',
                'status_approval_1' => 'reject',
                'status_approval_2' => 'reject',
                'status_approval_3' => 'reject'
            ];

            $data_budget = [
                'id_budget' => $training[0]->id_budget,
                'temporary_calculation' => $budgetData['temporary_calculation'] - $training[0]->biaya_actual
            ];

            $this->budget->save($data_budget);
            $this->approval->save($data);
        }
        echo json_encode('SUCCESS');
    }

    public function acceptBod()
    {

        $approve = $this->approval->getIdApproval($this->request->getPost('id_tna'));
        $tna =  $this->tna->getAllTna($this->request->getPost('id_tna'));
        $budget = $this->budget->getDataBudgetById($tna[0]->id_budget);
        $dataBudget = [
            'id_budget' => $budget['id_budget'],
            'used_budget' => $budget['used_budget'] + $tna[0]->biaya_actual,
            'available_budget' => $budget['available_budget'] - $tna[0]->biaya_actual
        ];
        $this->budget->save($dataBudget);
        $data = [
            'id_approval' => $approve['id_approval'],
            'id_tna' => $this->request->getPost('id_tna'),
            'status_approval_2' => 'accept'
        ];
        $this->approval->save($data);
        echo json_encode('SUCCESS');
    }

    public function rejectBod()
    {
        $approve = $this->approval->getIdApproval($this->request->getPost('id_tna'));
        $training = $this->tna->getAllTna($this->request->getPost('id_tna'));
        $budgetData = $this->budget->getBudgetById($training[0]->id_budget);
        // $this->subtraction($training[0]->biaya_actual, $training[0]->departemen);

        $data_budget = [
            'id_budget' => $training[0]->id_budget,
            'temporary_calculation' => $budgetData['temporary_calculation'] - $training[0]->biaya_actual
        ];
        $this->budget->save($data_budget);

        $data = [
            'id_approval' => $approve['id_approval'],
            'id_tna' => $this->request->getPost('id_tna'),
            'alasan' => $this->request->getPost('alasan'),
            'status_approval_2' => 'reject',
            'status_approval_3' => 'reject'
        ];
        $this->approval->save($data);

        echo json_encode($training);
    }

    public function subtraction($money, $departemen)
    {
        $budgets = $this->budget->getBudgetNextYear($departemen);
        if(empty($budget)){
            $budget = $this->budget->getBudgetCurrent(session()->get('departemen'));
        }

        $budgetData = [
            'id_budget' => $budgets['id_budget'],
            'temporary_calculation' => $budgets['temporary_calculation'] - $money
        ];
        // return $budgetData;
        $this->budget->save($budgetData);
    }

    public function DeleteTrainingUser()
    {
        $id = $this->request->getVar('id');
        $url = $this->request->getVar('url');
        $this->tna->delete($id);
        if ($url == 0) {
            return redirect()->to('/data_member');
        } else {
            return redirect()->to('/data_member_unplanned');
        }
    }

    public function CrossBudget()
    {
        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');
        // dd($departemen);

        if ($bagian == 'BOD') {
            $new_dept = [
                "departemen" => $this->user->getDepartemenForKadivBod($bagian,$dic)
            ];

            $new_depts = [];

            foreach($new_dept as $item){
                $total_index_departemen = count($item);
                for ($i = 0; $i < $total_index_departemen; $i++){
                    $departemens[] = $item[$i]['departemen'];
                }
            }

            $array1 = [];
            foreach($departemens as $item){
                $id_budget = $this->budget->getIdBudgetByDepartment($item);
                $array1 = array_merge($array1,$id_budget);
            }

            $array2 = [];
            foreach($array1 as $item){
                $cross_budget = $this->tna->getRequestCrossBudgetBod($bagian,$item['id_budget']);
                if(!empty($cross_budget)){
                   $array2 = array_merge($array2,$cross_budget);
                } 
            }

            $total_index = count($array2);

            $budgetData = [];
            $addedBudgetIds = [];
    
            for ($i = 0; $i < $total_index; $i++) {
                $currentIdBudget = $array2[$i]['id_budget'];
    
                if (!in_array($currentIdBudget, $addedBudgetIds)) {
                    $budgetData[] = $this->budget->getBudgetById($currentIdBudget);
                    $addedBudgetIds[] = $currentIdBudget;
                }
            }

            $data = [
                'tittle' => 'Request Cross Budget',
                'data_cross_budget' => $array2,
                'budget' => $budgetData,
                'user' => $this->user
            ];
    
            return view('user/requestcrossbudgetuser', $data);

        }elseif ($bagian == 'KADIV') {
            $new_dept = [
                "departemen" => $this->user->getDepartemenForKadivBod($bagian,$divisi)
            ];
            $new_depts = [];

            foreach($new_dept as $item){
                $total_index_departemen = count($item);
                for ($i = 0; $i < $total_index_departemen; $i++){
                    $departemens[] = $item[$i]['departemen'];
                }
            }

            $array1 = [];
            foreach($departemens as $item){
                $id_budget = $this->budget->getIdBudgetByDepartment($item);
                $array1 = array_merge($array1,$id_budget);
            }

            $array2 = [];
            foreach($array1 as $item){
                $cross_budget = $this->tna->getRequestCrossBudgetKadiv($bagian,$item['id_budget']);
                if(!empty($cross_budget)){
                   $array2 = array_merge($array2,$cross_budget);
                } 
            }

            $total_index = count($array2);

            $budgetData = [];
            $addedBudgetIds = [];
    
            for ($i = 0; $i < $total_index; $i++) {
                $currentIdBudget = $array2[$i]['id_budget'];
    
                if (!in_array($currentIdBudget, $addedBudgetIds)) {
                    $budgetData[] = $this->budget->getBudgetById($currentIdBudget);
                    $addedBudgetIds[] = $currentIdBudget;
                }
            }

            $data = [
                'tittle' => 'Request Cross Budget',
                'data_cross_budget' => $array2,
                'budget' => $budgetData,
                'user' => $this->user
            ];
    
            return view('user/requestcrossbudgetuser', $data);
                
        }elseif($bagian == 'KADEPT'){
            $id_budget = $this->budget->getIdBudgetByDepartment($departemen);
            $array_id_budget = [];
    
            
            foreach($id_budget as $item){
                $array_id_budget[] = $item['id_budget'];
            }
    
            $list_cross_budget = [];
            foreach ($array_id_budget as $item){
                $cross_budget = $this->tna->getRequestCrossBudget($bagian,$departemen,$item);
                if(!empty($cross_budget)){
                   $list_cross_budget = array_merge($list_cross_budget,$cross_budget);
                } 
            }
    
            $total_index = count($list_cross_budget);
    
            $budgetData = [];
            $addedBudgetIds = [];
    
            for ($i = 0; $i < $total_index; $i++) {
                $currentIdBudget = $list_cross_budget[$i]['id_budget'];
    
                if (!in_array($currentIdBudget, $addedBudgetIds)) {
                    $budgetData[] = $this->budget->getBudgetById($currentIdBudget);
                    $addedBudgetIds[] = $currentIdBudget;
                }
            }
    
            $mergeCrossBudget = [];
            foreach ($list_cross_budget as $crossBudgetItem) {
                $idBudget = $crossBudgetItem['id_budget'];
                $index = array_search($idBudget, array_column($budgetData, 'id_budget'));
                if ($index !== false) {
                    $mergeCrossBudget[] = array_merge($crossBudgetItem, $budgetData[$index]);
                }
            }
    
            $cb_with_databudget = [];
            foreach ($mergeCrossBudget as $item){
                $idBudget = $item['id_budget'];
                $get_data_budget = $this->budget->getDataBudgetById($item['id_budget']);
                $mergedData = array_merge($item, $get_data_budget);
    
                if ($mergedData['department'] == session()->get('departemen')) {
                    // Tambahkan data ke dalam array jika kondisinya terpenuhi
                    $mergedData['dic'] = session()->get('dic');
                    $mergedData['divisi'] = session()->get('divisi');
                    $cb_with_databudget[] = $mergedData;
                }
            }
    
            // dd($budgetData);
            $data = [
                'tittle' => 'Request Cross Budget',
                'data_cross_budget' => $cb_with_databudget,
                'budget' => $budgetData,
                'user' => $this->user
            ];
    
            return view('user/requestcrossbudgetuser', $data);
        }
    }

    public function MultiTna()
    {
        $checked_user = $this->request->getPost('user');
        $trainings = $this->training->getAll();

        $users = [];
        foreach ($checked_user as $index => $value) {
            $explodedValues = explode(',', $value);
            $users[] = [
                'npk' => $explodedValues[0],
                'id_user' => $explodedValues[1],
                'bagian' => $explodedValues[2],
                'nama' => $explodedValues[3]
            ];
        }
        
        $array = [];
        $dataCompany  = $this->competencyCompany->getProfileCompanyCompetency($users[0]['id_user'],$users[0]['npk']);
        $Company = [];
        if (!empty($dataCompany)) {
            foreach ($dataCompany as $DataCompany) {
                if ($DataCompany['score_company'] < $DataCompany['proficiency']) {
                    $competency = [
                        'id' => $DataCompany['id_competency_company'],
                        'category' => "Comp - " . $DataCompany['company'],
                        'competency' => $DataCompany['company'],
                        'proficiency' => $DataCompany['proficiency'],
                        'score' => $DataCompany['score_company'],
                        'keterangan' => 'Company'
                    ];
                    array_push($Company, $competency);
                }}
            } else {
                $Company = [];
            }
            $dataSoft  = $this->competencySoft->getProfileSoftCompetency($users[0]['id_user'],$users[0]['npk']);
            $Soft = [];
            if (!empty($dataSoft)) {
                foreach ($dataSoft as $DataSoft) {
                    if ($DataSoft['score_soft'] < $DataSoft['proficiency']) {
                        $competency = [
                            'id' => $DataSoft['id_competency_soft'],
                            'category' => "Soft - " . $DataSoft['soft'],
                            'competency' => $DataSoft['soft'],
                            'proficiency' => $DataSoft['proficiency'],
                            'score' => $DataSoft['score_soft'],
                            'keterangan' => 'Soft'
                        ];
                        array_push($Soft, $competency);
                    }
                }
            } else {
                $Soft = [];
            }
            $dataTechnicalB  = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($users[0]['id_user'],$users[0]['npk']);
            $TechnicalB = [];

            if (!empty($dataTechnicalB)) {
                foreach ($dataTechnicalB as $dataTechnicalB) {
                    if ($dataTechnicalB['score'] < $dataTechnicalB['proficiency']) {
                        $competency = [
                            'id' => $dataTechnicalB['id_competency_technicalB'],
                            'category' => "TechB - " . $dataTechnicalB['technicalB'],
                            'competency' => $dataTechnicalB['technicalB'],
                            'proficiency' => $dataTechnicalB['proficiency'],
                            'score' => $dataTechnicalB['score'],
                            'keterangan' => 'TechnicalB'
                        ];
                        array_push($TechnicalB, $competency);
                    }
                }
            } else {
                $TechnicalB = [];
            }
            
            $target = array_merge($Soft, $Company, $TechnicalB);
            $data = [
                'tittle' => 'TRAINING',
                'user' => $users,
                'training' => $trainings,
                'target' => $target,
                'validation' => \Config\Services::validation(),
            ];
        return view('user/formmultitna',$data);
    }

    public function MultiUnplanned()
    {
        $checked_user = $this->request->getPost('user');
        $trainings = $this->training->getAll();

        $users = [];
        foreach ($checked_user as $index => $value) {
            $explodedValues = explode(',', $value);
            $users[] = [
                'npk' => $explodedValues[0],
                'id_user' => $explodedValues[1],
                'bagian' => $explodedValues[2],
                'nama' => $explodedValues[3]
            ];
        }
        
        $array = [];
        $dataCompany  = $this->competencyCompany->getProfileCompanyCompetency($users[0]['id_user'],$users[0]['npk']);
        $Company = [];
        if (!empty($dataCompany)) {
            foreach ($dataCompany as $DataCompany) {
                if ($DataCompany['score_company'] < $DataCompany['proficiency']) {
                    $competency = [
                        'id' => $DataCompany['id_competency_company'],
                        'category' => "Comp - " . $DataCompany['company'],
                        'competency' => $DataCompany['company'],
                        'proficiency' => $DataCompany['proficiency'],
                        'score' => $DataCompany['score_company'],
                        'keterangan' => 'Company'
                    ];
                    array_push($Company, $competency);
                }}
            } else {
                $Company = [];
            }
            $dataSoft  = $this->competencySoft->getProfileSoftCompetency($users[0]['id_user'],$users[0]['npk']);
            $Soft = [];
            if (!empty($dataSoft)) {
                foreach ($dataSoft as $DataSoft) {
                    if ($DataSoft['score_soft'] < $DataSoft['proficiency']) {
                        $competency = [
                            'id' => $DataSoft['id_competency_soft'],
                            'category' => "Soft - " . $DataSoft['soft'],
                            'competency' => $DataSoft['soft'],
                            'proficiency' => $DataSoft['proficiency'],
                            'score' => $DataSoft['score_soft'],
                            'keterangan' => 'Soft'
                        ];
                        array_push($Soft, $competency);
                    }
                }
            } else {
                $Soft = [];
            }
            $dataTechnicalB  = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($users[0]['id_user'],$users[0]['npk']);
            $TechnicalB = [];

            if (!empty($dataTechnicalB)) {
                foreach ($dataTechnicalB as $dataTechnicalB) {
                    if ($dataTechnicalB['score'] < $dataTechnicalB['proficiency']) {
                        $competency = [
                            'id' => $dataTechnicalB['id_competency_technicalB'],
                            'category' => "TechB - " . $dataTechnicalB['technicalB'],
                            'competency' => $dataTechnicalB['technicalB'],
                            'proficiency' => $dataTechnicalB['proficiency'],
                            'score' => $dataTechnicalB['score'],
                            'keterangan' => 'TechnicalB'
                        ];
                        array_push($TechnicalB, $competency);
                    }
                }
            } else {
                $TechnicalB = [];
            }
            
            $target = array_merge($Soft, $Company, $TechnicalB);
            $data = [
                'tittle' => 'Add Multi Unplanned',
                'user' => $users,
                'training' => $trainings,
                'target' => $target,
                'validation' => \Config\Services::validation(),
            ];
        return view('user/formmultiunplanned',$data);
    }

    public function TnaFormMulti()
    {
        $users = $this->request->getPost('user');

        $users_array = [];
        foreach ($users as $index => $value) {
            $explodedValues = explode(',', $value);
            $users_array[] = [
                'id_user' => $explodedValues[0],
                'npk' => $explodedValues[1],
                'nama' => $explodedValues[2],
                'bagian' => $explodedValues[3]
            ];
        }

        $competency = $this->request->getPost('kompetensi');
        $id_training = $this->request->getPost('training');

        if ($competency !== null) {
            $type_kompetensi = explode("," , $competency);
            $deadline = $this->request->getVar('deadline');
            if ($deadline == 0) {
                $kelompok = 'training';
            } else {
                $kelompok = 'unplanned';
            }
            
            $id = $this->request->getPost('trainingunplanned');
            
            if ($id == null) {
                $id_training = $_POST['training'];
            } else {
                $id_training = $id;
            }
            
            foreach ($users_array as $users) {
                $user = $this->user->get_all_user_with_npk($users['id_user'], $users['npk']);

                $jenis_trainng = $this->training->getIdTraining($id_training);

                $budget = $this->budget->getBudgetNextYear($user['departemen']);
                if(empty($budget)){
                    $budget = $this->budget->getBudgetCurrent(session()->get('departemen'));
                }

                if (empty($budget['available_budget'])) {
                    session()->setFlashdata('warning', 'Budget Tidak Mencukupi!');
                    return redirect()->to('/data_member');
                } else {
                    if ($jenis_trainng['biaya'] > $budget['available_budget']) {
                        session()->setFlashdata('warning', 'Budget Melebihi yang Tersedia!');
                        return redirect()->to('/data_member');
                    }
                }

                $data = [
                    'id_user' => $users['id_user'],
                    'npk' => $users['npk'],
                    'bagian' => $users['bagian'],
                    'id_training' => $id_training,
                    'id_budget' => $budget['id_budget'],
                    'nama' => $user['nama'],
                    'golongan' => null,
                    'jenis_training' => $jenis_trainng['category'],
                    'kategori_training' => $this->request->getVar('kategori'),
                    'training' => $jenis_trainng['judul_training'],
                    'metode_training' => $this->request->getVar('metode'),
                    'request_training' => $this->request->getVar('request'),
                    'tujuan_training' => $this->request->getVar('tujuan'),
                    'notes' => $this->request->getVar('notes'),
                    'biaya' => $jenis_trainng['biaya'],
                    'status' => 'save',
                    'kelompok_training' => $kelompok
                ];

                $this->tna->save($data);

                $id  = $this->tna->getIdTna();

                $data2 = [
                    'id_tna' => $id->id_tna,
                    'id_user' => $id->id_user,
                    'npk' => $id->npk,
                    'bagian' => $id->bagian,
                ];
                $data3 = [
                    'id_tna' => $id->id_tna,
                ];
                $data4 = [
                    'id_tna' => $id->id_tna
                ];
                $data5 = [
                    'id_tna' => $id->id_tna,
                    'id_user' => $users['id_user'],
                    'npk' => $users['npk'],
                    'bagian' => $users['npk'],
                ];
                $nilai = [
                    'id_tna' => $id->id_tna,
                    'id_competency1' => $type_kompetensi[0],
                    'type_competency1' => $type_kompetensi[1]
                ];

                // dd($this->tna->save($data));
                $this->nilai->save($nilai);
                $this->approval->save($data2);
                $this->evaluasiReaksi->save($data3);
                $this->history->save($data4);
                $this->efektivitas->save($data5);
            }
    
            session()->setFlashdata('success', 'TNA Berhasil Disimpan');
            if ($kelompok == 'training') {
                return redirect()->to('/data_member');
            } else {
                return redirect()->to('/data_member_unplanned');
            }
        } else {
            session()->setFlashdata('warning', 'Form tidak boleh kosong!');
            return redirect()->to('/data_member');
        }
    }
}