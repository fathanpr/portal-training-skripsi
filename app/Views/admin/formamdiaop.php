<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <div class="row d-flex justify-content-between">
            <div class="col">
                <h3 class="card-title">Form AMDI AOP</h3>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <form action="<?= base_url() ?>save_form_cross_budget" method="POST">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                <!-- <div class="row">
                        <div class="col">
                        <label>Kelompok Training<span style="color:red;">*</span></label>
                            <select class="custom-select" name="kelompok_training" id="kelompok_training">
                                <option value="" selected>Choose</option>
                                <option value="amdi_aop">AMDI AOP</option>
                                <option value="cross_budget">Cross Budget</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="row mt-3">
                        <div class="col">
                        <label>Budget<span style="color:red;">*</span></label>
                        <select class="form-control" name="id_budget" id="biaya" style="width:100%;">
                            <option value="" selected disabled>Choose</option>
                            <?php foreach ($budget as $b) : ?>
                            <option value="<?= $b['id_budget'] ?>" data-available-budget="<?= $b['available_budget'] ?>" data-department-budget="<?= $b['department'] ?>" data-used-budget="<?= $b['used_budget'] ?>">
                            Rp<?= " " . number_format($b['available_budget'], 0, ',', '.')  ?> - <?= $b['department'] ?> - <?= $b['year'] ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                        <div class="row mt-3">
                        <div class="col">
                        <label>Nama<span style="color:red;">*</span></label>
                            <select name="id_user" id="id_user" class="form-control" style="width:100%;">
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Training<span style="color:red;">*</span></label>
                        <select class="form-control" name="training" id="training" style="width:100%;">
                            <?php foreach ($training as $trainings) : ?>
                            <option value="<?= $trainings['id_training'] ?>">
                                <?= $trainings['judul_training'] ?> 
                            </option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label for="jenis_training">Training Type<span style="color:red;">*</span></label>
                        <input class="form-control" name="jenis_training" id="jenis_training" style="width:100%;" readonly></input>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Training Category<span style="color:red;">*</span></label>
                            <select class="form-control" name="kategori_training" id="kategori">
                                <option value="" disabled selected>Choose</option>
                                <option value="Internal">Internal</option>
                                <option value="External">External</option>
                                <option value="Inhouse">Inhouse</option>
                                <option value="Blended">Blended</option>    
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Method<span style="color:red;">*</span></label>
                            <select class="custom-select" name="metode_training" id="metode">
                                <option value="" disabled selected>Choose</option>
                                <option value="Online">Online</option>
                                <option value="Offline">Offline</option>
                                <option value="Keduanya">Antara Keduanya</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <label>Mulai Training<span style="color:red;">*</span></label>
                                    <input class="form-control" type="date" name="mulai_training" id="mulai_training">
                                </div>
                                <div class="col">
                                    <label>Selesai Training<span style="color:red;">*</span></label>
                                    <input class="form-control" type="date" name="rencana_training" id="rencana_training">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <label>Vendor<span style="color:red;">*</span></label>
                                    <input class="form-control" type="text" name="vendor" id="vendor">
                                </div>
                                <div class="col">
                                    <label>Tempat<span style="color:red;">*</span></label>
                                    <input class="form-control" type="text" name="tempat" id="tempat">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Tujuan<span style="color:red;">*</span></label>
                            <textarea
                                class="form-control <?= ($validation->hasError('tujuan')) ? 'is-invalid' : ''; ?> "
                                id="validationTextarea" placeholder="Masukkan tujuan dan target training"
                                name="tujuan_training"></textarea>
                            <div class="invalid-feedback">
                                <?= $validation->getError('tujuan'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Notes</label>
                        <textarea class="form-control" name="notes" placeholder="Permintaan Khusus"></textarea></textarea>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                        <label>Budget Actual<span style="color:red;">*</span></label>
                        <input style="width:100%;" class="form-control" type="text" id="biaya_actual" name="biaya_actual" placeholder="Rp. 0" onkeyup="rupiah('biaya_actual')">
                        </div>
                    </div>
                <div class="row mt-3">
                        <div class="col d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Submit Training</button>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </form>
    <!-- /.card-body -->
</div>

<script>
    $("#datepicker").datepicker({
        format: "M-yyyy",
        startView: "months",
        minViewMode: "months"
    });


    $('#training').select2();

    $('#id_user').select2({
        ajax:{
            url : '<?= base_url() ?>get_user_data',
            dataType : 'json',
            delay: 250,
            data: function (params) {
                console.log(params)
                return{
                    q:params.term
                };
            },
            processResults: function (data) {
                var options = data.map(function (user) {
                    return{
                        id: user.id_user,
                        text: user.nama
                    };
                });

                return {
                    results: options
                };
            },
            cache:true
        },
        placeholder: 'Pilih User',
        minimumInputLenght:1,
        maximumSelectionLength:10
    });


    $("#training").on('change', function() {
    var id_training = this.value;
    console.log(id_training);

    $.ajax({
        type: 'post',
        url: "<?= base_url() ?>User/FormTna",
        async: true,
        dataType: "json",
        data: {
            id_training: id_training
        },
        success: function(data) {

            console.log(data.jenis_training);
            $("#jenis_training").val(data.category)
        }

    })
})


document.getElementById('biaya_actual').addEventListener('input', function () {
    var value = this.value.replace(/\D/g, '');
    value = value.replace(/^0+/, ''); 

    if (value === '') {
        value = '0';
    } 

    let formattedValue = 'Rp. ' + formatRupiah(value);
    this.value = formattedValue;
});

function formatRupiah(angka) {
    var reverse = angka.toString().split('').reverse().join('');
    var ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    return ribuan;
}

function rupiah(id) {

var rupiah = document.getElementById(id);
rupiah.addEventListener("keyup", function(e) {
rupiah.value = formatRupiah(this.value, "Rp. ");
});

function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}

}

$(document).ready(function() {
    $('form').submit(function(e) {
        let id_user = $('#id_user').val()
        let training = $('#training').val()
        let jenis_training  = $('#jenis_training').val()
        let vendor  = $('#vendor').val()
        let tempat  = $('#tempat').val()
        let mulai_training = $('#mulai_training').val()
        let rencana_training = $('#rencana_training').val()
        let biaya_actual = $('#biaya_actual').val()
        let budget = $('#biaya').val()
        biaya_actual = parseInt(biaya_actual.replace(/\D/g, ''), 10);
        let available_budget = $('#biaya option:selected').data('available-budget');
        let used_budget = $('#biaya option:selected').data('used-budget');

        if (rencana_training < mulai_training){
            Swal.fire({
            icon: 'error',
            title: 'Gagal Mendaftar',
            text: 'Rencana training harus setelah tanggal mulai training.',
            });
            return false;
        } else if (available_budget < biaya_actual){
            Swal.fire({
            icon: 'error',
            title: 'Gagal Mendaftar',
            text: 'Biaya Aktual Melebihi Budget Tersedia Departemen Tersebut.',
            });
            return false;
        } else if (!id_user || !training || !jenis_training || !mulai_training || !rencana_training || !tempat || !vendor || !biaya_actual || !budget){
            Swal.fire({
            icon: 'error',
            title: 'Data Tidak Boleh Kosong!',
            text: 'Periksa dan lengkapi kembali form pendaftaran.',
            });
            return false;
        } else{
            Swal.fire({
            icon: 'success',
            title: 'Registration Complete!',
            text: 'Training AMDI AOP telah berhasil didaftarkan.',
            }).then(function () {
                location.reload();
            });

            e.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({ name: 'used_budget', value: used_budget });

            $.ajax({
                url: '<?= base_url() ?>save_form_amdi_aop',
                type: 'POST',
                data: formData,
                success: function(response) {
                    console.log(formData);
                    console.log('Form berhasil terkirim');
                },
                error: function(error) {
                    console.error('Terjadi kesalahan saat mengirim formulir:', error);
                }
            });
        }
    });
});

</script>
<?= $this->endSection() ?>