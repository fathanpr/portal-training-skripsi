<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h3 class="card-title"><?= $tittle ?></h3>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="<?php base_url() ?>download_trainer_report" class="btn btn-success"><i class="fa-solid fa-file-arrow-down fa-xs"></i> Export</a>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <!-- INTERNAL TRAINER -->
        <div class="row">
            <div class="col">
            <h3>Internal Trainer</h3>
            <table class="table table-hover" id="example">
            <thead class='bg-primary'>
                <tr>
                    <th>Name</th>
                    <th>Vendor</th>
                    <th>Trainer Since</th>
                    <th>Total Hours</th>
                    <th>Training</th>
                    <th>Average Score</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $combinedData = [];
            foreach ($trainer as $item) {
                if ($item['vendor'] == NULL) {
                    $key = $item['nama'] . '-' . $item['judul_training'];
                    if (isset($combinedData[$key])) {
                        $combinedData[$key]['score'] += $item['score'];
                        $combinedData[$key]['count']++;
                    } else {
                        $combinedData[$key] = [
                            'id_trainer' => $item['id_trainer'],
                            'nama' => $item['nama'],
                            'vendor' => 'CBI',
                            'trainer_since' => ($item['trainer_since'] === NULL) ? '-' : $item['trainer_since'],
                            'clock' => $item['clock'] . ' Hours',
                            'judul_training' => $item['judul_training'],
                            'score' => $item['score'],
                            'count' => 1,
                        ];
                    }
                }
            }

            foreach ($combinedData as $key => $data) {
                $averageScore = $data['score'] / $data['count'];
                echo '<tr>';
                if ($data['count'] > 1) {
                    echo '<td><a href="#" class="detail-score" data-toggle="modal" data-target="#detailTrainer" onclick="showDetailScore(' . $data['id_trainer'] . ', \'' . $data['judul_training'] . '\')">'.$data['nama'].'</a></td>';

                } else {
                    echo '<td><a href="#" class="detail-score" data-toggle="modal" data-target="#detailTrainer" onclick="showDetailScore(' . $data['id_trainer'] . ', \'' . $data['judul_training'] . '\')">'.$data['nama'].'</a></td>';
                }
                echo '<td>' . $data['vendor'] . '</td>';
                echo '<td>' . $data['trainer_since'] . '</td>';
                echo '<td>' . $data['clock'] . '</td>';
                echo '<td>' . $data['judul_training'] . '</td>';
                echo '<td>';
                
                if ($averageScore >= 3.5) {
                    echo '<h4><span class="badge badge-pill badge-success">' . $averageScore . '</span></h4>';
                } elseif ($averageScore < 3.5 && $averageScore >= 2) {
                    echo '<h4><span class="badge badge-pill badge-primary">' . $averageScore . '</span></h4>';
                } else {
                    echo '<h4><span class="badge badge-pill badge-danger">' . $averageScore . '</span></h4>';
                }
                
                echo '</td>';
                echo '</tr>';
            }
            
            ?>
            </tbody>
        </table>
        </div>
        </div>
        
        <!-- EXTERNAL TRAINER -->
        <div class="row mt-5">
            <div class="col">
            <h3>External Trainer</h3>
            <table class="table table-hover" id="external">
            <thead class='bg-primary'>
                <tr>
                    <th>Name</th>
                    <th>Vendor</th>
                    <th>Trainer Since</th>
                    <th>Total Hours</th>
                    <th>Training</th>
                    <th>Average Score</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($trainer as $item) : ?>
                    <?php if($item['vendor'] !== NULL) : ?>
                        <tr>
                        <td style="text-transform:uppercase;"><a href="#" class="detail-score" data-toggle="modal" data-target="#detailTrainer" onclick="showDetailScore(<?php echo $item['id_trainer']; ?>, '<?php echo $item['judul_training']; ?>')">
        <?php echo $item['nama']; ?>
    </a></td>
                        <td><?= ($item['vendor'] == NULL) ? 'CBI' : $item['vendor'] ?></td>
                        <td><?= ($item['trainer_since'] === NULL) ? '-' : $item['trainer_since'] ?></td>
                        <td><?= $item['clock'] ?> Hours</td>
                        <td><?= $item['judul_training'] ?></td>
                        <td>
                            <?php if($item['score'] >= 3.5) : ?>
                                <h4><span class="badge badge-pill badge-success"><?= $item['score'] ?></span></h4>
                            <?php elseif($item['score'] < 3.5 && $item['score'] >= 2) : ?>
                                <h4><span class="badge badge-pill badge-primary"><?= $item['score'] ?></span></h4>
                                <?php else:  ?>
                            <h4><span class="badge badge-pill badge-danger"><?= $item['score'] ?></span></h4>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
            </div>
    </div>
    <!-- /.card-body -->
</div>

<!-- MODAL DETAIL TRAINER -->
<div class="modal fade bd-example-modal-lg" id="detailTrainer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Detail Score</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                <table class="table table-hover" id="detailScore">
                    <thead class="bg-primary">
                        <tr>
                            <td>Training</td>
                            <td>Pengetahuan</td> 
                            <td>Kemampuan</td>
                            <td>Kemampuan Melibatkan</td>
                            <td>Kemampuan Menanggapi</td>
                            <td>Kemampuan Mengendalikan</td>
                            <td>Average Score</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

function tampilkanBadge(score) {
  if (score >= 3.5) {
    return '<span class="badge badge-pill badge-success">' + score + '</span>';
  } else if (score > 2) {
    return '<span class="badge badge-pill badge-primary">' + score + '</span>';
  } else {
    return '<span class="badge badge-pill badge-danger">' + score + '</span>';
  }
}

// function showDetailScore(idTrainer,judulTraining) {
//     var id_trainer = idTrainer;
//     var judul_training = judulTraining;
//     console.log(id_trainer)
//     console.log(judul_training)

//     $.ajax({
//         type: 'POST',
//         url: '<?= base_url() ?>trainer_detail_score',
//         data: {
//             id_trainer: id_trainer,
//             judul_training : judul_training
//         },
//         success: function(response) {
//             console.log(response);
//             $('#detailScore tbody').empty();
//             var detailScoreTable = $('#detailScore tbody');
//             $.each(response, function(index, data) {
//                 detailScoreTable.append(
//                     '<tr>' +
//                     '<td>' + data.judul_training + '</td>' +
//                     '<td>' + tampilkanBadge(data.pengetahuan) + '</td>' +
//                     '<td>' + tampilkanBadge(data.kemampuan) + '</td>' +
//                     '<td>' + tampilkanBadge(data.kemampuan_melibatkan) + '</td>' +
//                     '<td>' + tampilkanBadge(data.kemampuan_menanggapi) + '</td>' +
//                     '<td>' + tampilkanBadge(data.kemampuan_mengendalikan) + '</td>' +
//                     '<td><h4>' + tampilkanBadge((data.pengetahuan+data.kemampuan+data.kemampuan_melibatkan+data.kemampuan_menanggapi+data.kemampuan_mengendalikan)/5) + '</h4></td>' +
//                     '</tr>'
//                 );
//             });
//         }
//     });
//     }
function showDetailScore(idTrainer, judulTraining) {
    var id_trainer = idTrainer;
    var judul_training = judulTraining;

    $('#detailScore tbody').html('<tr><td colspan="7" class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>Loading...</td></tr>');

    $.ajax({
        type: 'POST',
        url: '<?= base_url() ?>trainer_detail_score',
        data: {
            id_trainer: id_trainer,
            judul_training: judul_training
        },
        success: function (response) {
            console.log(response);
            $('#detailScore tbody').empty();
            var detailScoreTable = $('#detailScore tbody');
            $.each(response, function (index, data) {
                detailScoreTable.append(
                    '<tr>' +
                    '<td>' + data.judul_training + '</td>' +
                    '<td>' + tampilkanBadge(data.pengetahuan) + '</td>' +
                    '<td>' + tampilkanBadge(data.kemampuan) + '</td>' +
                    '<td>' + tampilkanBadge(data.kemampuan_melibatkan) + '</td>' +
                    '<td>' + tampilkanBadge(data.kemampuan_menanggapi) + '</td>' +
                    '<td>' + tampilkanBadge(data.kemampuan_mengendalikan) + '</td>' +
                    '<td><h4>' + tampilkanBadge((data.pengetahuan + data.kemampuan + data.kemampuan_melibatkan + data.kemampuan_menanggapi + data.kemampuan_mengendalikan) / 5) + '</h4></td>' +
                    '</tr>'
                );
            });
        },
        error: function () {
            $('#detailScore tbody').html('<tr><td colspan="7" class="text-center">Failed to load data</td></tr>');
        },
        complete: function () {
        }
    });
}

</script>
<?= $this->endSection() ?>