<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_History;
use App\Models\M_Tna;
use App\Models\UserModel;
use App\Models\M_Approval;

class C_History extends BaseController
{
    private M_Tna $tna;
    private UserModel $user;
    private M_Approval $approval;
    private M_History $history;


    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
        $this->history = new M_History();
        $this->approval = new M_Approval();
    }

    public function index()
    {

        $user = $this->user->getAllUser();
        $DataHistory = [];

        foreach ($user as $users) {
            $training = $this->tna->getTnaUserHistory($users->npk,$users->bagian);
            $history = [
                'id' => $users->id_user,
                'nama' => $users->nama,
                'npk' => $users->npk,
                'bagian' => $users->bagian,
                'jumlah_training' => $training[0]['npk']
            ];
            array_push($DataHistory, $history);
        }

        $data = [
            'tittle' => 'History Training',
            'user' => $DataHistory,
        ];
        return view('admin/history', $data);
    }

    public function DetailHistory()
    {
        $id = $this->request->getPost('history');
        $npk = $this->request->getPost('npk');
        $bagian = $this->request->getPost('bagian');

        $history = $this->tna->getDetailHistory($npk,$bagian);


        $data = [
            'tittle' => 'History Training',
            'history' => $history,
            'id' => $id
        ];
        return view('admin/detailhistory', $data);
    }

    public function SertifikatUpload()
    {

        $id_tna = $this->request->getVar('history');

        $keterangan = $this->request->getVar('keterangan');
        $file = $this->request->getFile('file');
        $id  = $this->history->getIdHistory($id_tna);


        if (!empty($file->getName())) {
            $file->getClientExtension();
            $newName = $file->getRandomName();
            $file->move("../public/sertifikat", $newName);
            $filepath = "/sertifikat/" . $newName;

            $data = [
                'id_history' => $id[0]['id_history'],
                'sertifikat' => $filepath,
                'keterangan' => $keterangan
            ];
        } else {
            $data = [
                'id_history' => $id[0]['id_history'],
                'keterangan' => $keterangan
            ];
        }
        $this->history->save($data);
        return redirect()->to('/history');
    }

    public function GetDataEditHistory()
    {
        $id = $this->request->getPost('id_history');
        $History = $this->history->getIdHistory($id);
        echo json_encode($History);
    }

    // public function UploadHistory()
    // {
    //     $file = $this->request->getFile('file');
    //     $id = $this->request->getVar('id_user');
    //     $user = $this->user->getAllUser($id);
    //     //dd($user);
    //     if ($file === "") {
    //         return redirect()->to('/history');
    //     }
    //     $ext = $file->getClientExtension();
    //     if ($ext == 'xls') {
    //         $render = new
    //             \PhpOffice\PhpSpreadsheet\Reader\Xls();
    //     } else {
    //         $render = new
    //             \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //     }

    //     $spreadsheet = $render->load($file);
    //     $sheet = $spreadsheet->getActiveSheet()->toArray();
    //     // var_dump($sheet).die();
    //     for ($i = 1; $i < count($sheet); $i++) {
    //         if ($sheet[$i][1] != null) {
    //             $training = $sheet[$i][1];
    //             $mulai = strtotime($sheet[$i][2]);
    //             $selesai = strtotime($sheet[$i][3]);
    //             $vendor = $sheet[$i][4];
    //             $tempat = $sheet[$i][5];
    //             $data = [
    //                 'id_user' => $user['id_user'],
    //                 'npk' => $user['npk'],
    //                 'bagian' => $user['bagian'],
    //                 'nama' =>  $user['nama'],
    //                 'training' => $training,
    //                 'mulai_training' => date('Y-m-d', $mulai),
    //                 'rencana_training' => date('Y-m-d', $selesai),
    //                 'vendor' => $vendor,
    //                 'tempat' => $tempat,
    //                 'kelompok_training' => 'training'
    //             ];
    //             //   dd($data);
    //             $this->tna->save($data);
    //             var_dump($data).die();
    //             $id  = $this->tna->getIdTnaHistory($user['npk'],$user['bagian']);
    //             $data2 = [
    //                 'id_tna' => $id->id_tna,
    //             ];
    //             $this->history->save($data2);
    //         }
    //     }
    //     // session()->setFlashdata('success', 'Data Berhasil Di Import');
    //     return redirect()->to('/history');
    // }
    public function UploadHistory()
    {
        $file = $_FILES['file'];
        $id = $this->request->getVar('id_user');
        $user = $this->user->getAllUser($id);

        if (empty($file['name'])) {
            return redirect()->to('/history');
        }

        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        if ($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $render->load($file['tmp_name']);
        $sheet = $spreadsheet->getActiveSheet()->toArray();

        for ($i = 1; $i < count($sheet); $i++) {
            if ($sheet[$i][0] != null) {
                $training = $sheet[$i][0];
                $mulai = strtotime($sheet[$i][1]);
                $selesai = strtotime($sheet[$i][2]);
                $vendor = $sheet[$i][3];
                $tempat = $sheet[$i][4];
                $data = [
                    'id_user' => $user['id_user'],
                    'npk' => $user['npk'],
                    'bagian' => $user['bagian'],
                    'nama' =>  $user['nama'],
                    'training' => $training,
                    'mulai_training' => date('Y-m-d', $mulai),
                    'rencana_training' => date('Y-m-d', $selesai),
                    'vendor' => $vendor,
                    'tempat' => $tempat,
                    'kelompok_training' => 'training'
                ];
                $this->tna->save($data);

                $id  = $this->tna->getIdTnaHistory($user['npk'], $user['bagian']);

                $data_approval = [
                    'id_tna' => $id->id_tna,
                    'id_user' => $id->id_user,
                    'npk' => $id->npk,
                    'bagian' => $id->bagian,
                    'status_training' => 1,
                ];
                $this->approval->save($data_approval);

                $data2 = [
                    'id_tna' => $id->id_tna,
                ];
                $this->history->save($data2);
            }
        }
        
        return redirect()->to('/history');
    }

}
