<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<style>
.my-custom-scrollbar {
    position: relative;
    height: 200px;
    overflow: auto;
}

.table-wrapper-scroll-y {
    display: block;
}
</style>
<div class="card">
    <div class="card-header h6 d-flex justify-content-between">
        <div class="col-6">
            <h3 class="card-title">Unplanned Training</h3>
        </div>
        <div class="col-6">
            <input type="text" id="trainingFilter" class="form-control" placeholder="Filter by Training">
        </div>
    </div>
    <!-- /.card-header -->
    <?php $i = 0; foreach ($dept as $d) : ?>
        <div class="card-body table-responsive p-0 table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-hover table-striped mb-0 overflow-auto" id="tableTna<?= $i ?>">
                <thead style="position: sticky; top: 0;background-color:" class="bg-primary">
                    <tr>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Training</th>
                        <th>Training Request</th>
                        <th>Training Start</th>
                        <th>Training Finished</th>
                        <th>Budget Planning</th>
                        <th>Budget Actual</th>
                        <th>Vendor</th>
                        <th>Place</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="tna-admin">
                    <?php
                        $deptTna = $tna->getStatusWaitAdminUnplanned($d->departemen);
                        foreach ($deptTna as $tnas) : ?>
                    <tr>
                        <td><?= $tnas->nama ?></td>
                        <td><?= $tnas->departemen ?></td>
                        <td><?= $tnas->training ?></td>
                        <td>
                            <div style="width:60px;"><?= $tnas->request_training ?></div>
                        </td>
                        <?php $currentYear=date('Y'); ?>
                        <td><input type="date" class="form-control" required value="<?= $tnas->mulai_training ?>" name="mulai-training<?= $i ?>"
                                id="mulai-training<?= $i ?>" min="<?= $currentYear ?>-01-01" max="<?= $currentYear ?>-12-31"></td>
                        <td><input class="form-control" type="date" required value="<?= $tnas->rencana_training ?>" name="rencana-training<?= $i ?>"
                                id="rencana-training<?= $i ?>" min="<?= $currentYear ?>-01-01" max="<?= $currentYear ?>-12-31"></td>
                        <td>
                            <h6 style="width:90px;">
                                Rp<span><?= " " . number_format($tnas->biaya, 0, ',', '.') ?></span></h6>
                        </td>
                        <td>
                            <div class="d-flex flex-row">
                                <input type="text" class="form-control" step="1" required id="biaya<?= $i ?>" name="biaya<?= $i ?>" onkeyup="rupiah('biaya<?= $i ?>')">
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-row">
                                <input class="form-control" type="text" required id="vendor<?= $i ?>" name="vendor<?= $i ?>"
                                    value="<?= ($tnas->biaya == null) ? '' : $tnas->vendor ?>">
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-row">
                                <input type="text" class="form-control" required id="tempat<?= $i ?>" name="tempat<?= $i ?>"
                                    value="<?= ($tnas->tempat == null) ? '' : $tnas->tempat ?>">
                            </div>
                        </td>
                        <?php $budgets = $budget->getBudgetCurrent($d->departemen); ?>
                        <td>
                            <div class="row">
                                <div class="col">
                                    <a id="accept<?= $i ?>" href="javascript:;" class="btn btn-success m-1"
                                            style="width:100px;color:white;" data-accept="<?= $tnas->id_tna ?>" data-available-budget="<?= $budgets['available_budget'] ?>" onclick="Accept(<?= $i ?>)"><i class=" fa fa-fw fa-check"></i>Accept</a>
                                        <!-- <a id="reject<?= $i ?>" href="javascript:;" class="btn btn-danger btn-sm mt-3"
                                            style="width:100px;color:white;" data-reject="<?= $tnas->id_tna ?>"
                                            onclick="Reject(<?= $i ?>)"><i class=" fa fa-fw fa-close"></i>Reject</a> -->
                                        <a href="javascript:;" class="item-edit btn btn-primary m-1"
                                            style="width:100px;color:white;" data-id="<?= $tnas->id_tna ?>" data-i="<?= $i ?>"><i
                                                class=" fa fa-fw fa-file-text-o"></i>Detail</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $i++;
                        endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php $budgets = $budget->getBudgetCurrent($d->departemen); ?>
        <div class="d-flex justify-content-around my-3">
            <div><strong>Alocated Budget : </strong>
                <?= "Rp " . number_format($budgets['alocated_budget'], 0, ',', '.') ?>
            </div>
            <div><strong>Available
                    Budget : </strong><?= "Rp " . number_format($budgets['available_budget'], 0, ',', '.') ?></div>
            <div><strong>Used Budget : </strong><?= "Rp " . number_format($budgets['used_budget'], 0, ',', '.') ?></div>
            <div><strong>Temporary Calculation :
                </strong><?= "Rp " . number_format($budgets['temporary_calculation'], 0, ',', '.') ?>
                <input type="text" style="display:none;" id='biaya_tersedia' data-biaya="<?= $budgets['available_budget'] ?>" value='<?php $budgets['available_budget'] ?>'>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <!-- /.card-body -->
    <div class=" modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Training Need Analysis</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex flex-column">
                        <label for="nama">Name</label>
                        <input id="nama" class="nama" name="nama" readonly></input>
                        <label for="dic">DIC</label>
                        <input id="dic" class="dic" name="dic" readonly></input>
                        <label for="divisi">Divisi</label>
                        <input id="divisi" class="divisi" name="divisi" readonly></input>
                        <label for="departemen">Department</label>
                        <input id="departemen" class="departemen" name="departemen" readonly></input>
                        <label for="training">Training</label>
                        <input id="training" class="mt-1" name="training" readonly></input>
                        <label for="jenis-training">Training Type</label>
                        <input id="jenis-training" class="mt-1" name="jenis-training" readonly></input>
                        <label for="kategori-training">Training Category</label>
                        <input id="kategori-training" class="mt-1" name="kategori-training" readonly></input>
                        <label for="metode-training">Training Method</label>
                        <input id="metode-training" class="mt-1" name="metode-training" readonly></input>
                        <label for="mulai-training">Training Start</label>
                        <input id="mulai-training" class="mt-1" name="mulai-training" readonly></input>
                        <label for="Selesai-training">Training Finished</label>
                        <input id="Selesai-training" class="mt-1" name="rencana-training" readonly></input>
                        <label for="tujuan-training">Training Goals</label>
                        <input id="tujuan-training" class="mt-1" name="tujuan-training" readonly></input>
                        <label for="notes">Note</label>
                        <textarea id="notes" class="mt-1" name="notes" readonly></textarea>
                        <label for="budget">Budget</label>
                        <input id="budget" class="mt-1" name="budget" readonly></input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<script>
function rupiah(id) {

    var rupiah = document.getElementById(id);
    rupiah.addEventListener("input", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

}

$('[name^="mulai-training"], [name^="rencana-training"], [name^="biaya"]').on('input', function() {
    var i = $(this).attr('name').match(/\d+/)[0]; 
    var mulaiTrainingValue = $('[name="mulai-training' + i + '"]').val();
    var rencanaTrainingValue = $('[name="rencana-training' + i + '"]').val();
    var biayaTrainingValue = $('[name="biaya' + i + '"]').val().replace(/[^0-9]/g, "");;

    // var biayaAngka = parseInt(biayaTrainingValue);

    var formattedBiayaTrainingValue = parseFloat(biayaTrainingValue).toLocaleString('id-ID', {
    style: 'currency',
    currency: 'IDR'
    });
    console.log(formattedBiayaTrainingValue);
    $('[name = "rencana-training"]').val(rencanaTrainingValue)
    $('[name = "mulai-training"]').val(mulaiTrainingValue)
    $('[name = "budget"]').val(formattedBiayaTrainingValue)
    

    $('.item-edit').on('click', function() {
    var id_tna = $(this).attr('data-id');
    // var i = $(this).attr('data-i');
    $.ajax({
        type: 'post',
        url: "<?= base_url() ?>detail_tna",
        async: true,
        dataType: "json",
        data: {
            id_tna: id_tna
        },
        success: function(data) {
            console.log(data)
            jQuery.noConflict()
            $("#exampleModal").modal("show");
            $('[name = "nama"]').val(data[0].nama)
            $('[name = "dic"]').val(data[0].dic)
            $('[name = "divisi"]').val(data[0].divisi)
            $('[name = "departemen"]').val(data[0].departemen)
            $('[name = "training"]').val(data[0].training)
            $('[name = "jenis-training"]').val(data[0].jenis_training)
            $('[name = "kategori-training"]').val(data[0].kategori_training)
            $('[name = "metode-training"]').val(data[0].metode_training)
            $('[name = "tujuan-training"]').val(data[0].tujuan_training)
            $('[name = "notes"]').val(data[0].notes)
            
        }

    })
})
});



function Accept(i) {
    var id_tna = $('#accept' + i).attr('data-accept');
    var biaya_actual = $('#biaya' + i).val();
    var mulai_training = $('#mulai-training' + i).val();
    var rencana_training = $('#rencana-training' + i).val();
    var vendor = $('#vendor' + i).val();
    var tempat = $('#tempat' + i).val();
    var biaya_tersedia = $('#accept' + i).attr('data-available-budget');
    var biaya_actual_number = parseFloat(biaya_actual.replace(/\D/g, ''));

    // Validasi data kosong
    if (!biaya_actual || !mulai_training || !rencana_training || !vendor || !tempat) {
        Swal.fire({
            icon: 'error',
            title: 'Data Tidak Boleh Kosong',
            text: 'Pastikan semua data terisi sebelum melanjutkan.',
        });
    } else if (biaya_actual_number > biaya_tersedia) {
        Swal.fire({
            icon: 'error',
            title: 'Biaya Aktual Melebihi Biaya Tersedia',
            text: 'Silahkan ubah dan turunkan budget untuk melanjutkan.',
        });
    } else if (rencana_training < mulai_training){
        Swal.fire({
            icon: 'error',
            title: 'Gagal Menginput TNA',
            text: 'Rencana Training Finished berada pada tanggal sebelum Training Start',
        });
    } else {
        Swal.fire({
            icon: 'success',
            title: 'Training Disetujui',
            text: 'Menunggu persetujuan Kadept, Kadiv dan BOD.',
        });

        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>accept_admin",
            async: true,
            dataType: "json",
            data: {
                id_tna: id_tna,
                biaya_actual: biaya_actual,
                mulai_training: mulai_training,
                rencana_training: rencana_training,
                vendor: vendor,
                tempat: tempat
            },
            success: function(data) {
                $("#accept" + i).closest('tr').addClass('accepted');
                $("#accept" + i).remove();
                $("#detail" + i).remove();
                $("#biaya" + i).prop('disabled', true);
                $("#mulai-training" + i).prop('disabled', true);
                $("#rencana-training" + i).prop('disabled', true);
                $("#vendor" + i).prop('disabled', true);
                $("#tempat" + i).prop('disabled', true);
            }
        });
    }
}

$(document).ready(function() {
    $("#trainingFilter").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        localStorage.setItem('trainingFilter', value);
        $("#tna-admin tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    var savedFilterValue = localStorage.getItem('trainingFilter');
    if (savedFilterValue) {
        $("#trainingFilter").val(savedFilterValue).trigger('keyup');
    }
    
    // $("table[id^='tableTna']").each(function() {
    //     $(this).DataTable();
    // });
});

</script>
<?= $this->endSection() ?>