<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<div class="talk" data-talk="<?= session()->get('talk'); ?>"></div>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title"><?= $tittle ?></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Training Title</th>
                    <th>Training Type</th>
                    <th>Training Category</th>
                    <th>Training Method</th>
                    <th>Training Goals</th>
                    <th>Training Implementation</th>
                    <th>Module</th>
                    <th>Description</th>
                </tr>
            </thead>
            <?php foreach ($evaluasi as $Atmps) : ?>
            <tr>
                <td><?= $Atmps['nama'] ?></td>
                <td><?= $Atmps['departemen'] ?></td>
                <td><?= $Atmps['training'] ?></td>
                <td><?= $Atmps['jenis_training'] ?></td>
                <td><?= $Atmps['kategori_training'] ?></td>
                <td><?= $Atmps['metode_training'] ?></td>
                <td><?= $Atmps['tujuan_training'] ?></td>
                <td><?= date('d M', strtotime($Atmps['mulai_training'])) . ' - ' . date('d M Y', strtotime($Atmps['rencana_training'])) ?>
                </td>
                <td> <?php if ($Atmps['status_evaluasi'] == null) : ?>
                    <p>Module</p>
                    <?php else : ?>
                    <a href="<?= $Atmps['material_path'] ?>">Module</a>
                    <?php endif; ?>
                </td>
                <?php if ($Atmps['status_evaluasi'] == null) : ?>
                <td>
                    <div class="d-flex justify-content-start">
                        <a href="<?= base_url() ?>form_evaluasi_unplanned/<?= $Atmps['id_tna'] ?>"
                            class="btn btn-danger">Not Evaluated</a>
                    </div>
                </td>
                <?php else : ?>
                <td>
                    <div class="d-flex justify-content-start">
                        <a href="<?= base_url() ?>form_unplanned_selesai/<?= $Atmps['id_tna'] ?>"
                            class="btn btn-success">Evaluated</a>
                    </div>
                </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<?= $this->endSection() ?>