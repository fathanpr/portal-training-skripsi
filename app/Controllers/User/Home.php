<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_Tna;
use App\Models\UserModel;

class Home extends BaseController
{

    private M_Tna $tna;
    private UserModel $user;
    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
    }
    public function index()
    {
        $id = session()->get('id');
        $npk = session()->get('npk');
        $user = $this->user->filter($id);
        $data = [
            'tittle' => 'Portal Training & Development',
            'user' => $user,
        ];
        return view('user/homeuser', $data);
    }

    // public function DataHome()
    // {

    //     $home =  $this->tna->getAllDataHome();
    //     // var_dump($home);
    //     $json = [];
    //     foreach ($home as $row) {
    //         $date = date('Y-m-d');
    //         $dateTraining = $row['rencana_training'];
    //         if (strtotime($dateTraining) > strtotime($date)) {
    //             $data = [
    //                 'title' => $row['kategori_training'],
    //                 'start' => $row['mulai_training'],
    //                 'end' => date('Y-m-d', strtotime('+1 days', strtotime($row['rencana_training']))),
    //                 'color' => '#FFD700',
    //                 // 'url' => base_url('/jadwal/' . $row['rencana_training'])
    //             ];
    //             array_push($json, $data);
    //         } else {
    //             $data = [
    //                 'title' => $row['kategori_training'],
    //                 'start' => $row['mulai_training'],
    //                 'end' => date('Y-m-d', strtotime('+1 days', strtotime($row['rencana_training']))),
    //                 'color' => 'green',
    //                 // 'url' => base_url('/jadwal/' . $row['rencana_training'])
    //             ];
    //             array_push($json, $data);
    //         }
    //     }
    //     echo json_encode($json);
    // }
    public function DataHome()
    {
        $home =  $this->tna->getAllDataHome();
        $json = []; 

        foreach ($home as $row) {
            $date = date('Y-m-d');
            $dateTraining = $row['rencana_training'];

            if ($row['kategori_training'] == 'External') {
                $color = 'green';
            } elseif ($row['kategori_training'] == 'Internal') {
                $color = 'orange';
            } elseif ($row['kategori_training'] == 'Inhouse') {
                $color = 'purple';
            }
            
            $data = [
                'title' => $row['kategori_training'],
                'start' => $row['mulai_training'],
                'end' => date('Y-m-d', strtotime('+1 days', strtotime($row['rencana_training']))),
                'color' => $color,
                // 'url' => base_url('/jadwal/' . $row['rencana_training'])
            ];


            array_push($json, $data);
        }

        echo json_encode($json);
    }

    public function getRegistrans($start,$end,$kategori){
        $test = $this->tna->getIdUserJadwalHome($start, $end, $kategori);

        foreach ($test as $tests) {
            $nama[] = $this->user->getNama($tests['id_user']);
        }
    }

    public function JadwalHome()
    {
        $date = $this->request->getPost('start');
        $date_end = $this->request->getPost('end');
        $kategori_training = $this->request->getPost('kategori_training');
        $time = strtotime(current(explode("(", $date)));
        $time_end = strtotime(current(explode("(", $date_end)));
        $dates = date('Y-m-d', $time);
        $time_end_minus_1_day = $time_end - (24 * 60 * 60);
        $dates_end = date('Y-m-d', $time_end_minus_1_day);
        $dataFix = [];
        // $jadwal = $this->tna->getDataJadwalHome($dates);
        $jadwal_1 = $this->tna->getNewDataJadwalHome($dates, $dates_end, $kategori_training);
        $jadwals_1 = $this->tna->getNewDataJadwalHomes($dates, $dates_end, $kategori_training);
        $jadwal = [];

        $combinedArray = array_merge($jadwal_1, $jadwals_1);

        foreach ($combinedArray as $row) {
            $key = $row['id_training'].$row['Training'].$row['kategori_training'].$row['jenis_training'].$row['mulai_training'].$row['rencana_training'];
            if (!isset($jadwal[$key])) {
                $jadwal[$key] = $row;
                $jadwal[$key]['npk'] = [];
            }
            $jadwal[$key]['npk'][] = $this->user->getNamaByNpk($row['npk']);
        }
        // var_dump($jadwal);
        
        foreach ($jadwal as &$item) {
            $item['npk'] = array_unique($item['npk']);
            $item['Pendaftar'] = count($item['npk']);
        }

        // var_dump($jadwal);
        
        $jadwal = array_values($jadwal);

        // var_dump($jadwal);

        // foreach ($jadwal as $item){
        //     $data_user = $this->user->getNama($item['id_user']);
        //     $jadwals = array_push($data_user,$jadwal);
        // }
        // var_dump($data_user);
        // $jadwals = $this->tna->getNewDataJadwalHomes($dates,$dates_end, $kategori_training);
        // var_dump($jadwals);
        // var_dump($jadwals);
        // foreach ($jadwal as $jadwals) {
        //     if ($jadwals['kategori_training'] == $kategori_training) {
        //         $data = [
        //             'id_training' => $jadwals['id_training'],
        //             'training' => $jadwals['Training'],
        //             'pendaftar' => $jadwals['Pendaftar'],
        //             'tanggal_start' => $dates,
        //             'tanggal_ahir' => $jadwals['rencana_training'],
        //             'kategori' => $jadwals['kategori_training'],
        //         ];
        //         array_push($dataFix, $data);
        //     }
        // }
        // foreach ($jadwal as $jadwals) {
        //     $data = [
        //         'id_training' => $jadwals['id_training'],
        //         'training' => $jadwals['Training'],
        //         'pendaftar' => $jadwals['Pendaftar'],
        //         'tanggal_start' => $dates,
        //         'tanggal_ahir' => $jadwals['rencana_training'],
        //         'kategori' => $jadwals['kategori_training'],
        //     ];

        //     array_push($dataFix, $data);
        // }

        echo json_encode($jadwal);
    }


    // public function MemberModal()
    // {
    //     //$training = $this->request->getPost('training');
    //     $id = session()->get('id');
    //     $users = $this->user->Filter($id);
    //     echo json_encode($users);
    // }
}