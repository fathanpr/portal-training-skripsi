<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Tna extends Model
{
    protected $table      = 'tna';
    protected $primaryKey = 'id_tna';
    // protected $useAutoIncrement = true;
    protected $allowedFields = [
        'id_user', 'npk', 'bagian', 'id_training', 'dic', 'divisi', 'id_handler_report',
        'departemen', 'nama', 'jabatan', 'golongan', 'seksi', 'jenis_training',
        'kategori_training', 'training', 'vendor', 'tempat', 'metode_training', 'request_training', 'mulai_training', 'rencana_training',
        'tujuan_training', 'notes', 'biaya', 'biaya_actual', 'status', 'kelompok_training', 'id_budget', 'id_competency', 'type_competency', 'year', 'man_hour'
    ];


    private UserModel $user;

    public function __construct()
    {
        $this->user = new UserModel();
    }
    // public function getTnaByid($id)
    // {
    //     $this->select()->where('id_tna', $id);
    //     return $this->get()->getResultArray();
    // }

    public function getTna(){
        $this->select();
        return $this->get()->getResultArray();
    }

    public function getDataTna(){
        $this->select()->join('approval','approval.id_tna = tna.id_tna','left')
            ->where('status_approval_0 IS NULL')
            ->orWhere('status_approval_0 !=','reject')
            ->orWhere('status_approval_1 IS NULL')
            ->orWhere('status_approval_1 !=','reject')
            ->orWhere('status_approval_2 IS NULL')
            ->orWhere('status_approval_2 !=','reject')
            ->orWhere('status_approval_3 IS NULL')
            ->orWhere('status_approval_3 !=','reject');
        return $this->get()->getResultArray();
    }

    public function getTnaById($id){
        $this->select()->where('id_tna',$id);
        return $this->get()->getResultArray();
    }

    public function getDataTnaById($id){
        $this->select()->where('tna.id_tna',$id)->join('approval','approval.id_tna = tna.id_tna','left');
        return $this->get()->getRowArray();
    }

    public function getTnaUser($bagian, $npk)
    {
        $this->selectCount('id_user')->where('bagian', $bagian)->where('npk',$npk)->where('kelompok_training', 'training');
        return $this->get()->getResultArray();
    }

    // public function getTnaUserHistory($id_user)
    // {
    //     $this->selectCount('id_user')->where('id_user', $id_user)->where('kelompok_training', 'training');
    //     return $this->get()->getResultArray();
    // }

    public function getTnaByJudul($judul){
        $this->select('id_tna')->where('training',$judul);
        return $this->get()->getResultArray();
    }

    public function getTnaUserHistory($npk,$bagian)
    {
        $this->selectCount('tna.npk')->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','unplanned','amdi_aop','cross_budget'])
        ->join('approval', 'approval.id_tna = tna.id_tna')->where('approval.status_training',1);
        return $this->get()->getResultArray();
    }

    public function getDetailHistory($npk,$bagian)
    {
        $this->select()->where('tna.npk', $npk)->where('tna.bagian', $bagian);
        $this->join('history', 'history.id_tna = tna.id_tna');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('approval.status_training',1);
        return $this->get()->getResultArray();
    }

    public function getAllTna($id = false,$bagian = false)
    {
        if ($id == false && $bagian == false) {
            $this->select('tna.*,user.*');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select('tna.*,user.*')->where('id_tna', $id);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian',$bagian);
            return $this->get()->getResult();
        }
    }

    public function getTnaForRole($id)
    {
        $this->select()->where('id_tna', $id);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    // public function getAllSave($id = false)
    // {
    //     if ($id == false) {
    //         $this->select()->where('status', 'save');
    //         return $this->get()->getResult();
    //     }
    //     return $this->where(['id_tna' => $id])->get()->getResult();
    // }

    public function getUserTna($bagian,$npk)
    {
        $this->select()->where(['bagian' => $bagian])->where(['npk' => $npk])->where('kelompok_training', 'training');
        return $this->get()->getResultArray();
    }

    public function getTrainingRequest($id_user)
    {
        $status = 'save';
        $query = "SELECT COUNT(*) as row_count
                FROM tna
                WHERE id_user = ? AND status = ? AND kelompok_training = 'training'";
    
        $result = $this->db->query($query, [$id_user, $status])->getRow();
        
        if ($result) {
            return $result->row_count;
        }
    
        return 0;
    }

    public function getTrainingSubmitted($id_user, $npk, $bagian)
    {
        $statusArray = ['accept', 'wait'];
        $query = "SELECT COUNT(*) as row_count
            FROM tna
            WHERE (id_user = ? AND npk = ? AND status IN (" . implode(',', array_fill(0, count($statusArray), '?')) . ")
            AND kelompok_training IN ('training','amdi_aop','cross_budget'))
            OR (bagian = ? AND npk = ? AND status IN (" . implode(',', array_fill(0, count($statusArray), '?')) . ")
            AND kelompok_training IN ('training','amdi_aop','cross_budget'))";
        $result = $this->db->query($query, array_merge([$id_user, $npk], $statusArray, [$bagian, $npk], $statusArray))->getRow();
        
        if ($result) {
            return $result->row_count;
        }

        return 0;
    }

    public function getDataTrainingSubmitted($id_user, $npk, $bagian)
    {
        $nextYear = date('Y') + 1;
        $statusArray = ['accept', 'wait'];

        $this->select()
            ->groupStart()
                ->where('id_user', $id_user)
                ->where('npk', $npk)
                ->whereIn('status', $statusArray)
                ->whereIn('kelompok_training', ['training', 'amdi_aop', 'cross_budget'])
            ->groupEnd()
            ->orGroupStart()
                ->where('bagian', $bagian)
                ->where('npk', $npk)
                ->whereIn('status', $statusArray)
                ->whereIn('kelompok_training', ['training', 'amdi_aop', 'cross_budget'])
            ->groupEnd();

        return $this->get()->getResult();
    }

    public function getTrainingCurrentYearById($id_user)
    {
        // UBAH date(Y) menjadi date(Y)+1
        $currentYear = date('Y');

        $query = "SELECT *
        FROM tna
        WHERE id_user = ?
          AND request_training LIKE ?
          AND kelompok_training = 'training'
          AND (status = 'wait' OR status = 'accept')";

        $results = $this->db->query($query, [$id_user, '%' . $currentYear . '%'])->getResult();

        return $results;
    }

    public function getTnaByIdTraining($bagian,$id_training,$npk){
        $status = ['accept', 'wait'];
        $this->select()->where('bagian',$bagian)->where('npk',$npk)->where('id_training', $id_training)->whereIn('status',$status);
        return $this->get()->getResult();
    }


    public function getTnaFilterDistinct($id)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select('user.departemen')->where('user.dic', $user['dic'])->where('tna.status', 'save')->where('kelompok_training', 'training')->distinct()->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select('user.departemen')->where('user.divisi', $user['divisi'])->where('tna.status', 'save')->where('kelompok_training', 'training')->distinct()->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select('user.departemen')->where('user.departemen', $user['departemen'])->where('tna.status', 'save')->where('kelompok_training', 'training')->distinct()->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select('user.departemen')->where('user.seksi', $user['seksi'])->where('tna.status', 'save')->where('kelompok_training', 'training')->distinct()->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select('user.departemen')->where('tna.npk', $user['npk'])->where('tna.bagian',$user['bagian'])->where('tna.status', 'save')->where('kelompok_training', 'training')->distinct();
            return $this->get()->getResult();
        }
    }

    public function getTnaFilterKadept($id, $departemen = false)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select()->where('user.dic', $user['dic'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'training')->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select()->where('user.divisi', $user['divisi'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'training')->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select()->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'training')->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select()->where('user.seksi', $user['seksi'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'training')->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select()->where('tna.npk', $user['npk'])->where('tna.bagian',$user['bagian'])->where('departemen', $departemen)->where('status', 'save')->where('kelompok_training', 'training');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        }
    }

    public function getTnaFilter($id)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select()->where('user.dic', $user['dic'])->where('tna.status', 'save')->where('kelompok_training', 'training')->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select()->where('user.divisi', $user['divisi'])->where('tna.status', 'save')->where('kelompok_training', 'training')->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select()->where('user.departemen', $user['departemen'])->where('tna.status','save')->where('kelompok_training', 'training')->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select()->where('user.seksi', $user['seksi'])->where('tna.status', 'save')->where('kelompok_training', 'training')->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select()->where('tna.npk', $user['npk'])->where('tna.bagian', $user['bagian'])->where('tna.status', 'save')->where('kelompok_training', 'training');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        }
    }

    // public function getTnaFilterUnplanned($id)
    // {
    //     $user = $this->user->getAllUser($id);

    //     if ($user['bagian'] == 'BOD') {
    //         $this->select()->where('dic', $user['dic'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } elseif ($user['bagian'] == 'KADIV') {
    //         $this->select()->where('divisi', $user['divisi'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } elseif ($user['bagian'] == 'KADEPT') {
    //         $this->select()->where('departemen', $user['departemen'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } else {
    //         $this->select()->where('id_user', $user['id_user'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     }
    // }

    public function getStatusWaitAdminDepartemen()
    {
        $this->select('user.departemen')->where('tna.status', 'wait')->where('kelompok_training', 'training');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->groupBy('user.departemen');
        return $this->get()->getResult();
    }

    public function countStatusWaitAdmin()
    {
        $this->select('COUNT(*) as count')->where('tna.status', 'wait')->where('kelompok_training', 'training');
        
        return $this->countAllResults();
    }

    public function getStatusWaitAdmin($departemen)
    {
        $this->select('tna.*,user.*,training.*')->where('tna.status', 'wait')->where('kelompok_training', 'training')->where('user.departemen', $departemen);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('training','training.id_training = tna.id_training');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResult();
    }

    public function getStatusPersonal($id,$npk,$bagian)
    {
        $this->select('tna.*,approval.*')->groupStart()->where('tna.id_user', $id)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget'])->groupEnd();
        $this->OrGroupStart()->where('tna.npk', $npk)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget'])->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getStatusPersonalFilter($id,$year,$npk,$bagian)
    {
        $this->select('tna.*,approval.*')->groupStart()->where('tna.id_user', $id)->where('YEAR(mulai_training)',$year)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget'])->groupEnd();
        $this->OrGroupStart()->where('tna.npk', $npk)->where('tna.bagian', $bagian)->where('YEAR(mulai_training)',$year)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget'])->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getStatusPersonalUnplanned($id,$npk,$bagian)
    {
        $this->select('tna.*,approval.*')->groupStart()->where('tna.id_user', $id)->where('tna.bagian',$bagian)->where('kelompok_training', 'unplanned')->groupEnd();
        $this->OrGroupStart()->where('tna.npk', $npk)->where('tna.bagian', $bagian)->where('kelompok_training', 'unplanned')->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getStatusWaitUser($bagian, $member, $id = null)
    {
        $status = ['wait', 'accept'];
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.bagian')->where('user.dic', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $jabatan = ['KADIV'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.divisi', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['STAFF 4UP', 'KASIE', 'EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.departemen', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KASIE') {
            $jabatan = ['STAFF 4UP', 'KASIE','EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.seksi', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian')->whereNotIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } else {
            $this->select('tna.*,approval.*,user.bagian')->where('tna.id_user', $id)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }
    }

    public function getStatusWaitUserFilter($bagian, $member,$year, $id = null)
    {
        $status = ['wait', 'accept'];
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.bagian')->where('user.dic', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('YEAR(mulai_training)',$year);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $jabatan = ['KADIV'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.divisi', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('YEAR(mulai_training)',$year);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['STAFF', 'STAFF 4UP', 'KASIE', 'EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.departemen', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('YEAR(mulai_training)',$year);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KASIE') {
            $jabatan = ['STAFF', 'STAFF 4UP', 'KASIE','EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.seksi', $member)->whereIn('tna.status', $status)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('YEAR(mulai_training)',$year);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereNotIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } else {
            $this->select('tna.*,approval.*,user.bagian')->where('tna.id_user', $id)->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('YEAR(mulai_training)',$year);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }
    }

    //function untuk menampilkan data tna yang sudah di accept
    public function getKadivStatus()
    {
        $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    }

    public function getKadeptStatus()
    {
        $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    }

    public function getKadivStatusFilter($year)
    {
        $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned'])
            ->like('mulai_training', $year, 'after');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    
    }
    public function getKadeptStatusFilter($year)
    {
        $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned'])
            ->like('mulai_training', $year, 'after');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    }

    public function countKadivStatusWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->where('approval.status_approval_1', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function countKadeptStatusWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->where('approval.status_approval_0', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function countBodStatusWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->where('approval.status_approval_1', 'accept');
        $this->where('approval.status_approval_2', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function getBodStatus()
    {
        $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->where('biaya_actual > 200000');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('status_approval_1','accept');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->orWhere('approval.bagian','KADIV'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    }

    public function getBodStatusFilter($year)
    {
        $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')->whereIn('kelompok_training',['training','amdi_aop','cross_budget','unplanned'])->like('mulai_training',$year,'after')->where('biaya_actual > 200000');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('status_approval_1','accept');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->orWhere('approval.bagian','KADIV'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        return $this->get()->getResultArray();
    }

    //function get Request Tna
    public function getRequestTna($bagian, $member, $depertemen)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,user.*,approval.*')->where('user.dic', $member)->where('user.departemen', $depertemen)->where('tna.status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null)->where('kelompok_training', 'training');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,user.*,approval.*')->where('user.divisi', $member)->where('user.departemen', $depertemen)->where('tna.status', 'accept')->where('kelompok_training', 'training');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')->where('kelompok_training', 'training');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.departemen', $member)->where('user.departemen', $depertemen);
            return $this->get()->getResultArray();
        } else {
            return  $status  =  array();
        }
    }
    public function getRequestTnaDisntinct($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('user.departemen')->where('user.dic', $member)->where('tna.status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null)->where('kelompok_training', 'training')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('user.departemen')->where('user.divisi', $member)->where('tna.status', 'accept')->where('kelompok_training', 'training')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $this->select('user.departemen')->where('user.departemen', $member)->where('tna.status', 'accept')->where('kelompok_training', 'training')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } else {
            return  $status  =  array();
        }
    }
    public function countRequestTna($bagian, $member)
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept')->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');

        if ($bagian == 'BOD') {
            $this->where('user.dic', $member);
            $this->where('status_approval_0', 'accept');
            $this->where('status_approval_1', 'accept');
            $this->where('status_approval_2', null);
            $this->where('status_approval_3', null);
        } elseif ($bagian == 'KADIV') {
            $this->where('user.divisi', $member);
            $this->where('status_approval_0', 'accept');
            $this->where('status_approval_1', null);
        } elseif ($bagian == 'KADEPT') {
            $this->where('user.departemen', $member);
            $this->where('status_approval_0', null);
        } else {
            return 0;
        }

        return $this->countAllResults();
    }

    public function countRequestCrossBudget($bagian, $member, $id_budget)
    {
        $this->select('tna.id_tna');

        if ($bagian == 'BOD') {
            $this->where('tna.status', 'accept')
                ->where('status_approval_0', 'accept')
                ->where('status_approval_1', 'accept')
                ->where('status_approval_2', null)
                ->where('status_approval_3', null)
                ->where('kelompok_training', 'cross_budget')
                ->whereIn('id_budget', [$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        } elseif ($bagian == 'KADIV') {
            $this->where('tna.status', 'accept')
                ->where('kelompok_training', 'cross_budget')
                ->whereIn('id_budget', [$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        } elseif ($bagian == 'KADEPT') {
            $this->where('tna.status', 'accept')
                ->where('kelompok_training', 'cross_budget')
                ->whereIn('id_budget', [$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereNotIn('user.departemen', [$member]);
        } else {
            return 0;
        }

        return $this->countAllResults();
    }

    public function getKadivAccept($date, $year, $departemen)
    {
        $this->select('tna.*,approval.*,user.departemen')->where('month(mulai_training)', $date)->where('YEAR(mulai_training)', $year)->where('user.departemen', $departemen)->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian', 'left');
        return $this->get()->getResultArray();
    }

    public function getKadivAcceptForExport($date)
    {
        $year = date('Y')+1;
        $current = date('Y');
        $this->select('tna.*,user.*,approval.*,accommodation.*')->where('month(mulai_training)', $date)->whereIn('YEAR(mulai_training)', [$year,$current])->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('accommodation', 'tna.id_tna = accommodation.id_tna', 'left');
        // $this->join('accommodation','accommodation.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getKadivAcceptForExportOther($date,$year)
    {
        $this->select('tna.*,user.*,approval.*,accommodation.*')->where('month(mulai_training)', $date)->where('YEAR(mulai_training)', $year)->whereIn('kelompok_training', ['cross_budget','amdi_aop']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('accommodation', 'tna.id_tna = accommodation.id_tna', 'left');
        // $this->join('accommodation','accommodation.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function countTrainingMonthly()
    {
        $this->select('COUNT(*) as total')->where('kelompok_training', 'training')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->countAllResults();
    }

    public function countOtherTrainingMonthly()
    {
        $this->select('COUNT(*) as total')->whereIn('kelompok_training', ['cross_budget','amdi_aop'])->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->countAllResults();
    }

    public function getKadivAcceptDistinct($date, $year)
    {
        $this->select('user.departemen')->where("Month(mulai_training)", $date)->where("YEAR(mulai_training)", $year)->where('kelompok_training', 'training')->distinct();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2','accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDataTrainingMonthly($date, $year)
    {
        $this->select()->where("Month(mulai_training)", $date)->where("YEAR(mulai_training)", $year)->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getIdTna()
    {
        $this->select();
        return $this->get()->getLastRow();
    }

    public function getIdTnaHistory($npk,$bagian)
    {
        $this->select()->where('npk', $npk)->where('bagian',$bagian);
        return $this->get()->getLastRow();
    }

    public function getDetailReject($id)
    {
        $this->select('tna.*,approval.*,user.bagian')->where('tna.id_tna', $id);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDateTraining()
    {
        $this->select('tna.rencana_training,tna.training,approval.status_approval_1,approval.status_approval_2,approval.status_approval_3')->distinct();
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResult();
    }



    public function getTrainingMonthly($date)
    {
        // $sql = $this->query("select MONTH(tna.mulai_training) as 'Planing Training', count(distinct tna.training) as 'Jumlah Training',count(distinct approval.status_approval_2) as 'Admin Approval',count(distinct approval.status_approval_3) as 'BOD Approval'
        //     from tna join approval on approval.id_tna = tna.id_tna where tna.kelompok_training = 'training' and approval.status_approval_1 = 'accept' group by MONTH(tna.mulai_training)")->getResultArray();
        $sql = $this->query(
            "SELECT
        MONTH(tna.mulai_training) AS 'Planing Training',
        YEAR(tna.mulai_training) AS 'Year Training',
        COUNT(tna.training) AS 'Jumlah Training',
        COUNT(CASE WHEN approval.status_approval_1 = 'accept' THEN 1 ELSE NULL END) AS 'Admin Approval',
        COUNT(CASE WHEN approval.status_approval_2 = 'accept' THEN 1 ELSE NULL END) AS 'BOD Approval',
        SUM(CASE WHEN approval.status_approval_2 = 'reject' THEN 1
            WHEN approval.status_approval_3 = 'reject' THEN 1 ELSE 0 END) AS 'Reject'
        FROM
            tna
        JOIN
            approval ON approval.id_tna = tna.id_tna
        WHERE
            tna.kelompok_training = 'training'
            AND status_approval_0 = 'accept'
            AND status_approval_1 = 'accept'
            AND status_approval_2 = 'accept'
            AND YEAR(mulai_training) = $date
        GROUP BY
            YEAR(tna.mulai_training), MONTH(tna.mulai_training)"
            )->getResultArray();

            return $sql;
    }

    public function getOtherMonthly()
    {
        $now = date('Y');
        $next_year = date('Y')+1;
        $sql = $this->query(
            "SELECT
        MONTH(tna.mulai_training) AS 'Planing Training',
        YEAR(tna.mulai_training) AS 'Year Training',
        COUNT(CASE WHEN tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1 ELSE NULL END) AS 'Jumlah Training',
        COUNT(CASE WHEN approval.status_approval_1 = 'accept' AND tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1 ELSE NULL END) AS 'Admin Approval',
        COUNT(CASE WHEN approval.status_approval_2 = 'accept' AND tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1 ELSE NULL END) AS 'BOD Approval',
        SUM(CASE WHEN approval.status_approval_2 = 'reject' AND tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1    
            WHEN approval.status_approval_3 = 'reject' AND tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1 ELSE 0 END) AS 'Reject'
        FROM
            tna
        JOIN
            approval ON approval.id_tna = tna.id_tna
        WHERE
            tna.kelompok_training IN ('amdi_aop', 'cross_budget')
            AND status_approval_0 = 'accept'
            AND status_approval_1 = 'accept'
            AND status_approval_2 = 'accept'
            AND YEAR(mulai_training) BETWEEN $now AND $next_year
        GROUP BY
            YEAR(tna.mulai_training), MONTH(tna.mulai_training)
        HAVING
        COUNT(CASE WHEN tna.kelompok_training IN ('amdi_aop', 'cross_budget') THEN 1 ELSE NULL END) > 0")->getResultArray();
            return $sql;
    }

    public function getKadivAcceptOtherByDate($date, $year)
    {
        $this->select()->where("Month(mulai_training)", $date)->where('YEAR(mulai_training)', $year)->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = user.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getKadivAcceptOtherDistinct($date, $year)
    {
        $this->select('user.departemen')->where("Month(mulai_training)", $date)->where('YEAR(mulai_training)', $year)->whereIn('kelompok_training', ['amdi_aop','cross_budget'])->distinct();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = user.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDataOtherTrainingMonthly($date, $year)
    {
        $this->select()->where("Month(mulai_training)", $date)->where("YEAR(mulai_training)", $year)->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getTrainingDitolak()
    {
        $this->select();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'reject')->orWhere('status_approval_2', 'reject')->orWhere('status_approval_3', 'reject');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getAtmp()
    {
        $now = date('Y');
        $next_year = date('Y')+1;
        $then = date('Y')-1;
        $this->select('tna.*,approval.*,accommodation.*');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('accommodation','accommodation.id_tna = tna.id_tna');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')
        $this->groupStart()
         ->like('tna.mulai_training', $now)
         ->orLike('tna.mulai_training', $next_year)
         ->orLike('tna.mulai_training', $then)
         ->groupEnd();
        return $this->get()->getResultArray();
    }

    public function getAtmpFilter($year)
    {
        $this->select('tna.*,approval.*,accommodation.*');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('accommodation','accommodation.id_tna = tna.id_tna');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')
        $this->groupStart()
         ->like('tna.mulai_training', $year)
         ->groupEnd();
        return $this->get()->getResultArray();
    }

    public function getAtmpSubmit()
    {
        $then = date('Y')-1;
        $now = date('Y');
        $next_year = date('Y')+1;
        $this->select('tna.*,approval.*,training.vendor as vendor_suggest');
        $this->join('approval','approval.id_tna = tna.id_tna')->whereIn('status', ['wait','accept'])->orWhere('status_approval_0','reject')->orWhere('status_approval_1','reject')->orWhere('status_approval_2','reject')->orWhere('status_approval_3','reject');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')
        $this->join('training','training.id_training = tna.id_training');
        $this->groupStart()
         ->like('tna.mulai_training', $now)
         ->orLike('tna.mulai_training', $next_year)
         ->orLike('tna.mulai_training', $then)
         ->groupEnd();
        return $this->get()->getResultArray();  
    }



    public function getMemberSchedule($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.*')->where('user.dic', $member)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,approval.*,user.*')->where('user.divisi', $member)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE'];
            $this->select('tna.*,approval.*,user.*')->where('user.departemen', $member)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } else {
            $this->select('tna.*,approval.*,user.*')->where('user.seksi', $member)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }
    }

    public function getMemberHistory($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.*')->where('user.dic', $member);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,approval.*,user.bagian')->where('user.divisi', $member);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.departemen', $member);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        }elseif ($bagian == 'KASIE') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE']; 
            $this->select('tna.*,approval.*,user.bagian')->where('user.seksi', $member);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } else{
            return  $status  =  array();
        }
    }

    public function getMemberAllHistory($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,user.*')->where('user.dic', $member);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,user.bagian')->where('user.divisi', $member);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE'];
            $this->select('tna.*,user.bagian')->where('user.departemen', $member);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('bagian', $jabatan);
            return $this->get()->getResultArray();
        }elseif ($bagian == 'KASIE') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE']; 
            $this->select('tna.*,user.bagian')->where('user.seksi', $member);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } else{
            return  $status  =  array();
        }
    }

    public function getPersonalSchedule($id, $npk, $bagian)
    {
        $this->select('tna.*,approval.*,user.*')
        ->groupStart()->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned'])->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        return $this->get()->getResultArray();
    }

    public function getDataHome()
    {
        $this->select('tna.mulai_training,tna.rencana_training,tna.kategori_training')->distinct()->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getAllDataHome()
    {
        $this->select('tna.mulai_training,tna.rencana_training,tna.kategori_training')->distinct()->whereIn('kelompok_training', ['training','unplanned','amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDataJadwalHome($date)
    {

        $sql = $this->query("select id_training,metode_training,jenis_training, training as Training,COUNT(training) as Pendaftar,kategori_training,mulai_training,rencana_training,biaya_actual from tna where mulai_training = '$date'  group by id_training,training,kategori_training,metode_training,jenis_training,mulai_training,rencana_training,biaya_actual");
        return  $sql->getResultArray();
    }

    public function getNewDataJadwalHome($start,$end,$kategori_training)
    {
        $sql = $this->query("select DISTINCT id_training,jenis_training,metode_training,id_user,npk,bagian,biaya,training as Training,COUNT(training) as Pendaftar,kategori_training,mulai_training,rencana_training from tna where mulai_training = '$start' AND rencana_training = '$end'  AND kategori_training = '$kategori_training' group by id_training,training,kategori_training,jenis_training,mulai_training,rencana_training,id_user,npk,bagian,metode_training,biaya");
        return  $sql->getResultArray();
    }

    public function getNewDataJadwalHomes($start,$end,$kategori_training)
    {
        $sql = $this->query("select DISTINCT id_training,jenis_training,metode_training,id_user,npk,bagian,biaya, training as Training,COUNT(training) as Pendaftar,kategori_training,mulai_training,rencana_training from tna where mulai_training = '$start' AND rencana_training = '$end'  AND kategori_training = '$kategori_training' group by id_training,training,kategori_training,jenis_training,mulai_training,rencana_training,id_user,npk,bagian,metode_training,biaya");
        return $result = $sql->getResultArray();
    }
    
    public function getJadwalHomeVer($date)
    {
        $this->select()->where('mulai_training', $date)->where('kelompok_training', 'training');
        return $this->get()->getResultArray();
    }


    public function getSchedule()
    {
        $this->select('tna.*,approval.*')->where('tna.kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getOtherSchedule()
    {
        $this->select('tna.*,approval.*')->whereIn('tna.kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function countSchedule()
    {
        $this->select('COUNT(*) as total_schedules')->where('tna.kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        // $this->groupBy('user.id_user'); 

        return $this->countAllResults();
        // return $this->get()->getRow()->total_schedules;
    }

    public function countScheduleOther()
    {
        $this->select('COUNT(*) as total_schedules')->whereIn('tna.kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        // $this->groupBy('user.id_user'); 

        return $this->countAllResults();
        // return $this->get()->getRow()->total_schedules;
    }


    public function getEvaluasiReaksi($npk,$bagian)
    {
        $this->select('tna.*,approval.*,user.*,evaluasi_reaksi.*,training.material_path')->where('tna.npk', $npk)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->get()->getResultArray();
    }

    public function getAllEvaluasiReaksi($npk,$bagian)
    {
        $this->select('tna.*,approval.*,user.*,evaluasi_reaksi.*,training.material_path')->where('tna.npk', $npk)->where('tna.bagian', $bagian);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->get()->getResultArray();
    }

    public function countAllEvaluasiReaksi($npk,$bagian)
    {
        $this->select('COUNT(*) as total')->where('tna.npk', $npk)->where('tna.bagian',$bagian);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->countAllResults();
    }

    public function countTotalEvaluasiReaksi()
    {
        $this->select('COUNT(*) as total');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->countAllResults();
    }

    public function countTotalEvaluasiEfektivitas()
    {
        $this->select('COUNT(*) as total');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna')->where('status_efektivitas', null);
        $this->join('training', 'training.id_training = tna.id_training');

        $currentYear = date('Y');
        $this->where("GETDATE() >= DATEADD(DAY, 90, tna.mulai_training) AND YEAR(tna.mulai_training) = '{$currentYear}'");

        return $this->countAllResults();
    }

    public function countTotalEvaluasiEfektivitasByNpk($npk)
    {
        $this->select('COUNT(*) as total');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna')->where('status_efektivitas', null);
        $this->join('user', 'user.npk = tna.npk')->where('npk', $npk);
        $this->join('training', 'training.id_training = tna.id_training');

        $currentYear = date('Y');
        $this->where("GETDATE() >= DATEADD(DAY, 90, tna.mulai_training) AND YEAR(tna.mulai_training) = '{$currentYear}'");

        return $this->countAllResults();
    }

    public function countEvaluasiReaksi($npk,$bagian)
    {
        $this->select('COUNT(*) as total')->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','amdi_aop','cross_budget','unplanned']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->countAllResults();
    }

    public function getEvaluasiReaksiOther($npk,$bagian)
    {
        $this->select('tna.*,approval.*,user.*,evaluasi_reaksi.*,training.material_path')->where('tna.npk', $npk)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->get()->getResultArray();
    }

    public function countEvaluasiReaksiOther($npk,$bagian)
    {
        $this->select('COUNT(*) as total')->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        $this->join('training', 'training.id_training = tna.id_training');

        return $this->countAllResults();
    }

    public function getDataForEvaluation($id,$npk,$bagian)
    {
        $this->select('tna.*,approval.*,user.*,nilai.*,evaluasi_efektivitas.*,evaluasi_reaksi.*')->where('tna.id_tna', $id);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('nilai', 'nilai.id_tna = tna.id_tna');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }
    
    public function getDataForEvaluationTraining($id,$npk,$bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas');
        $this->OrGroupStart()->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','cross_budget','unplanned'])->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function countDataForEvaluationTraining($npk,$bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('tna.npk', $npk)->where('tna.bagian',$bagian)->whereIn('kelompok_training', ['training','cross_budget','unplanned']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training',1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna')->where('status_efektivitas', NULL);
        return $this->get()->getResultArray();
    }

    public function getDataForEvaluationOther($id,$npk,$bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->groupStart()->where('tna.id_user', $id)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['amdi_aop','cross_budget'])->groupEnd();
        $this->OrGroupStart()->where('tna.npk', $npk)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['amdi_aop','cross_budget'])->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function countDataForEvaluationOther($npk,$bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('tna.npk', $npk)->where('tna.bagian', $bagian)->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training',1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna')->where('status_efektivitas', NULL);
        return $this->get()->getResultArray();
    }
    

    public function getDataForEvaluationUnplanned($id, $npk, $bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')
        ->groupStart()->where('tna.npk', $npk)->where('tna.bagian', $bagian)->where('kelompok_training', 'unplanned')->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training',1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function countDataForEvaluationUnplanned($npk,$bagian)
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('tna.npk', $npk)->where('tna.bagian', $bagian)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training',1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna')->where('status_efektivitas',NULL);
        return $this->get()->getResultArray();
    }

    // public function getDataHistory($id)
    // {
    //     $this->select()->where('tna.id_tna', $id);
    //     $this->join('history', 'history.id_tna = tna.id_tna');
    //     return $this->get()->getResultArray();
    // }

    // public function getDataHistory($id)
    // {
    //     $this->select('tna.*,approval.*,user.bagian,user.id_user,user.npk')->where('tna.id_tna', $id);
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    // }


    public function getDetailEvaluasiReaksi($id,$npk,$bagian)
    {
        $this->select('tna.*,user.bagian,user.id_user,user.npk,user.departemen,evaluasi_reaksi.*')->where(['tna.id_tna' => $id]);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }


    public function getDataEfektivitas($id)
    {
        $user = $this->user->getAllUser($id);
        $status = [0];

        if ($user['bagian'] == "BOD") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['training','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV')->where('user.dic', $user['dic']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian'] == "KADIV") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['training','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT')->where('user.divisi', $user['divisi']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['training','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $bagian)->where('user.departemen', $user['departemen'])->where('user.level', 'USER');
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KASIE" || $user['bagian']  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE','EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['training','cross_budget','unplanned']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.seksi', $user['seksi'])->where('user.level', 'USER')->whereNotIn('user.bagian', $bagian);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } else {
            return 'E';
        }
    }

    public function getDataEfektivitasOther($id)
    {
        $user = $this->user->getAllUser($id);
        $status = [0];

        if ($user['bagian'] == "BOD") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV')->where('user.dic', $user['dic']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian'] == "KADIV") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT')->where('user.divisi', $user['divisi']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $bagian)->where('user.departemen', $user['departemen'])->where('user.level', 'USER');
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KASIE" || $user['bagian']  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE', 'EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->whereIn('kelompok_training', ['amdi_aop','cross_budget']);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.seksi', $user['seksi'])->where('user.level', 'USER')->whereNotIn('user.bagian', $bagian);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } else {
            return 'E';
        }
    }


    public function getNotifEmailTraining()
    {
        $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    // public function getMemberHistory($bagian, $member)
    // {
    //     if ($bagian == 'BOD') {
    //         $this->select('tna.*,approval.*,user.bagian')->where('tna.dic', $member)->where('kelompok_training', 'training');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //         $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('bagian', 'KADIV');
    //         return $this->get()->getResultArray();
    //     } elseif ($bagian == 'KADIV') {
    //         $this->select('tna.*,approval.*,user.bagian')->where('tna.divisi', $member)->where('kelompok_training', 'training');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //         $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('bagian', 'KADEPT');
    //         return $this->get()->getResultArray();
    //     } elseif ($bagian == 'KADEPT') {
    //         $jabatan = ['STAFF', 'STAFF 4UP', 'KASIE'];
    //         $this->select('tna.*,approval.*,user.bagian')->select('tna.id_user,tna.nama')->where('tna.departemen', $member)->where('kelompok_training', 'training');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //         $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('bagian', $jabatan);
    //         return $this->get()->getResult();
    //     } else {
    //         return  $status  =  array();
    //     }
    // }


    public function getHistoryTna($bagian,$npk)
    {
        $this->select()->where('tna.bagian', $bagian)->where('tna.npk',$npk)->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        // $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', '1');
        return $this->get()->getResult();
    }

    public function getTnaTerdaftar($bagian,$npk)
    {
        $this->select()->where('tna.bagian', $bagian)->where('tna.npk', $npk)->where('kelompok_training', 'training');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        return $this->get()->getResult();
    }

    //Dashboard Function Model


    public function getCountTrainingByYear($year)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getCountTrainingByYearDepartment($year,$department)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResultArray();
    }

    public function getCountTrainingByYearDivisi($year,$divisi)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }

    public function getJenisTraining($year)
    {
        $this->select('tna.jenis_training')->Distinct()->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');

        return $this->get()->getResult();
    }
    
    public function getJenisTrainingDepartment($year, $department)
    {
        $this->select('tna.jenis_training')->Distinct()->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResult();
    }

    public function CountJenisTrainingDepartment($jenis, $date, $department)
    {
        $this->selectCount('tna.jenis_training')->where('tna.jenis_training', $jenis)->where('YEAR(mulai_training)', $date);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResult();
    }

    public function CountJenisTraining($jenis, $date)
    {
        $this->selectCount('tna.jenis_training')->where('tna.jenis_training', $jenis)->where('YEAR(mulai_training)', $date);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', 1);
        return $this->get()->getResult();
    }

    public function getCategory($year)
    {
        $this->select('tna.kategori_training')->Distinct()->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');
        return $this->get()->getResult();
    }

    public function getCategoryDepartment($year, $department)
    {
        $this->select('tna.kategori_training')->distinct()->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResult();
    }


    public function CountCategory($category, $date)
    {
        $this->selectCount('tna.kategori_training')->where('tna.kategori_training', $category)->where('YEAR(mulai_training)', $date);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');
        return $this->get()->getResult();
    }

    public function CountCategoryDepartment($category, $date, $department)
{
    $this->selectCount('tna.kategori_training')->where('tna.kategori_training', $category)->where('YEAR(mulai_training)', $date);
    $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_training', '1');
    $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
    return $this->get()->getResult();
}



    public function DashboardTraining($date)
    {
        // $sql =  $this->query("Select count(mulai_training) as counted_leads,mulai_training as count_date from tna group by mulai_training")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna where status_approval_3 = 'accept' and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();
        return $sql;
    }

    public function DashboardTrainingSeksi($date,$seksi)
    {
        $sql = $this->query("SELECT
            MONTH(mulai_training) as Bulan,
            COUNT(mulai_training) as total
            FROM
                tna
            JOIN
                approval ON approval.id_tna = tna.id_tna
            JOIN
                [dbo].[user] ON [dbo].[user].id_user = tna.id_user
            WHERE
                status_approval_3 = 'accept'
                AND seksi = ?
                AND YEAR(mulai_training) = ?
            GROUP BY
                MONTH(mulai_training)
            ", [$seksi, $date])->getResultArray();

        return $sql;
    }

    public function DashboardTrainingDivisi($date,$divisi)
    {
        $sql = $this->query("SELECT
            MONTH(mulai_training) as Bulan,
            COUNT(mulai_training) as total
            FROM
                tna
            JOIN
                approval ON approval.id_tna = tna.id_tna
            JOIN
                [dbo].[user] ON [dbo].[user].id_user = tna.id_user
            WHERE
                status_approval_3 = 'accept'
                AND divisi = ?
                AND YEAR(mulai_training) = ?
            GROUP BY
                MONTH(mulai_training)
            ", [$divisi, $date])->getResultArray();

        return $sql;
    }

    public function dashboard_training_department($date, $department)
    {
        $sql = $this->query("SELECT
            MONTH(mulai_training) as Bulan,
            COUNT(mulai_training) as total
            FROM
                tna
            JOIN
                approval ON approval.id_tna = tna.id_tna
            JOIN
                [dbo].[user] ON [dbo].[user].id_user = tna.id_user
            WHERE
                status_approval_3 = 'accept'
                AND departemen = ?
                AND YEAR(mulai_training) = ?
            GROUP BY
                MONTH(mulai_training)
            ", [$department, $date])->getResultArray();

        return $sql;
    }

    public function DashboardTrainingLine($date)
    {
        // $sql =  $this->query("Select count(mulai_training) as counted_leads,mulai_training as count_date from tna group by mulai_training")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna where status_approval_3 = 'accept'and status_training = 1 and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }

    public function DashboardTrainingLineDivisi($date, $divisi)
    {
        // $sql =  $this->query("Select count(mulai_training) as counted_leads,mulai_training as count_date from tna group by mulai_training")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [dbo].[user] on [dbo].[user].id_user = tna.id_user where status_approval_3 = 'accept' and divisi = '$divisi' and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }
    
    //Departemen Dashboard
    public function dashboard_training_line_department($date, $department)
    {
        // $sql =  $this->query("Select count(mulai_training) as counted_leads,mulai_training as count_date from tna group by mulai_training")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [dbo].[user] on [dbo].[user].id_user = tna.id_user where status_approval_3 = 'accept' and departemen = '$department' and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }

    public function DashboardTrainingDepartment($date, $department)
    {
        // $sql =  $this->query("Select count(mulai_training) as counted_leads,mulai_training as count_date from tna group by mulai_training")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [dbo].[user] on [dbo].[user].id_user = tna.id_user where status_approval_3 = 'accept' and departemen = '$department' and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }


    public function DashboardTrainingLineDepartment($date, $department)
    {
        //$sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [user] on [user].id_user = tna.id_user where status_approval_3 = 'accept' and departemen = $department and status_training = 1 and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [dbo].[user] on [dbo].[user].id_user = tna.id_user where status_approval_3 = 'accept' and departemen = '$department' and status_training = 1 and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }

    public function DashboardTrainingLineSeksi($date, $seksi)
    {
        //$sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [user] on [user].id_user = tna.id_user where status_approval_3 = 'accept' and departemen = $department and status_training = 1 and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan,COUNT(mulai_training) as total  FROM  tna join approval on approval.id_tna = tna.id_tna join [dbo].[user] on [dbo].[user].id_user = tna.id_user where status_approval_3 = 'accept' and seksi = '$seksi' and status_training = 1 and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();

        return $sql;
    }


    public function getDashboardTrainingReject($year)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'reject')->orWhere('status_approval_2', 'reject')->orWhere('status_approval_3', 'reject');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDashboardTrainingRejectDepartment($year, $department)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].departemen = ?
        AND (
            (approval.status_approval_1 = 'reject' OR approval.status_approval_2 = 'reject' OR approval.status_approval_3 = 'reject')
            OR approval.status_approval_1 = 'reject');
        ",[$year, $department])->getResultArray();
        return $sql;
    }

    public function getDashboardTrainingRejectDivisi($year, $divisi)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].divisi = ?
        AND (
            (approval.status_approval_1 = 'reject' OR approval.status_approval_2 = 'reject' OR approval.status_approval_3 = 'reject')
            OR approval.status_approval_1 = 'reject');
        ",[$year, $divisi])->getResultArray();
        return $sql;
    }


    public function getDashboardTrainingApproved($year)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getDashboardTrainingApprovedDepartment($year, $department)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].departemen = ?
        AND approval.status_approval_3 = 'accept';
        ",[$year, $department])->getResultArray();
        return $sql;
    }

    public function getDashboardTrainingApprovedDivisi($year, $divisi)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].divisi = ?
        AND approval.status_approval_3 = 'accept';
        ",[$year, $divisi])->getResultArray();
        return $sql;
    }

    public function getDashboardTrainingNeedApproval($year)
    {
        $this->selectCount('tna.id_tna')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', NULL);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getDashboardTrainingNeedApprovalDepartment($year,$department)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].departemen = ?
        AND approval.status_approval_3 IS NULL;
        ",[$year, $department])->getResultArray();
        return $sql;
    }

    public function getDashboardTrainingNeedApprovalDivisi($year,$divisi)
    {
        $sql = $this->query("SELECT COUNT(*) AS id_tna
        FROM [dbo].[tna] AS tna
        JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
        LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
        WHERE YEAR(tna.mulai_training) = ?
        AND [user].divisi = ?
        AND approval.status_approval_3 IS NULL;
        ",[$year, $divisi])->getResultArray();
        return $sql;
    }

    public function CountAllTraining($year)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function CountAllTrainingDepartment($year,$department)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResultArray();
    }

    public function CountAllTrainingDivisi($year,$divisi)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }


    public function CountTrainingImplemented($year)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        return $this->get()->getResultArray();
    }

    public function CountTrainingImplementedDivisi($year,$divisi)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }

    public function CountTrainingImplementedDepartment($year,$department)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('departemen', $department);
        return $this->get()->getResultArray();
    }


    public function CountTrainingNotImplemented($year)
    {
        $this->selectCount('training')->where('YEAR(mulai_training)', $year);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', NULL)->orWhereIn('status_training', [null, 0]);
        return $this->get()->getResultArray();
    }
    
    public function CountTrainingNotImplementedDepartment($year, $department)
    {
        $query = $this->db->query("SELECT COUNT(*) AS training
                                FROM [dbo].[tna] AS tna
                                JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
                                LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
                                WHERE YEAR(tna.mulai_training) = ?
                                AND [user].departemen = ?
                                AND (
                                    (approval.status_training = 0 OR approval.status_training IS NULL)
                                    OR approval.status_training = 0
                                );", [$year, $department]);
        return $query->getResultArray();
    }

    public function CountTrainingNotImplementedDivisi($year, $divisi)
    {
        $query = $this->db->query("SELECT COUNT(*) AS training
                                FROM [dbo].[tna] AS tna
                                JOIN [dbo].[user] AS [user] ON [user].id_user = tna.id_user
                                LEFT JOIN [dbo].[approval] AS approval ON approval.id_tna = tna.id_tna
                                WHERE YEAR(tna.mulai_training) = ?
                                AND [user].divisi = ?
                                AND (
                                    (approval.status_training = 0 OR approval.status_training IS NULL)
                                    OR approval.status_training = 0
                                );", [$year, $divisi]);
        return $query->getResultArray();
    }

    public function getTrainingNotImplemented()
    {
        $this->select()->where('status_training', 0);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function DistinctTraining(){
        $this->select()->distinct();
        return $this->get()->getResultArray();
    }

    public function getYear()
    {
        $this->select('DISTINCT(YEAR(mulai_training)) as Years')->where('YEAR(mulai_training) !=', null);
        $this->orderBy('YEAR(mulai_training)', 'DESC');
        return $this->get()->getResultArray();
    }

    public function TnaWillBeEvaluated($year)
    {
        $sql = $this->query("SELECT DISTINCT MONTH(tna.mulai_training) as 'bulan', YEAR(tna.mulai_training) as 'tahun'
    FROM tna
    JOIN approval ON approval.id_tna = tna.id_tna 
    WHERE YEAR(mulai_training) = $year AND status_training = 1
    GROUP BY MONTH(tna.mulai_training), YEAR(tna.mulai_training)")->getResultArray();
return $sql;

    }


//     public function TnaWillBeEvaluated($year)
//     {

//         $sql = $this->query("select	MONTH(tna.mulai_training) as 'bulan'
// from	tna  join	approval on approval.id_tna = tna.id_tna 
// where YEAR(mulai_training) = $year and status_training = 1
// group by MONTH(tna.mulai_training)")->getResultArray();
//         return $sql;
//     }
    // public function getDasboardTrainingNeedApproved($year)
    // {
    //     $this->selectCount('tna.id_tna')->where('year', $year);
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', NULL)->orWhere('status_approval_2', NULL)->orWhere('status_approval_3', NULL);
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    //}


    //function For Add Id Handler in Unplanned Training
    public function GetIdHnadlerByTna($date, $id_training)
    {
        $this->select()->where('mulai_training', $date)->where('id_training', $id_training);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getLastRow();
    }

    public function getTnaForTrainer($month, $year)
    {
        $sql = $this->query("select DISTINCT tna.training,tna.id_training,tna.mulai_training,tna.rencana_training,tna.id_handler_report,instruktur_1,instruktur_2,instruktur_3,instruktur_4,instruktur_5,duration from tna  join evaluasi_reaksi on evaluasi_reaksi.id_tna = tna.id_tna join approval on approval.id_tna = tna.id_tna  where MONTH(tna.mulai_training) = $month and YEAR(tna.mulai_training) = $year and status_training = 1")->getResultArray();
        return $sql;
    }

    public function getIntegrateTrainer($id_training, $date_start_old)
    {
        $this->select()->where('id_training', $id_training)->where('mulai_training', $date_start_old);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getIntegrateHasTrainer($id_handler_report)
    {
        $this->select()->where('id_handler_report', $id_handler_report);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function CheckedTrainingNullStatus($id, $start, $finish)
    {
        $this->select()->where('id_training', $id)->where('mulai_training', $start)->where('rencana_training', $finish);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getDataScoreForTrainerDone($id_training, $date)
    {
        $this->select()->where('id_training', $id_training)->where('mulai_training', $date);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', 1);
        return $this->get()->getResultArray();
    }

    public function CountDataScoreForTrainer($id_training, $date)
    {
        $this->selectCount('id_training')->where('id_training', $id_training)->where('mulai_training', $date);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function GetParticipants($id_training, $date)
    {
        $this->select('user.nama')->where('id_training', $id_training)->where('mulai_training', $date);
        $this->join('user', 'user.id_user=tna.id_user');
        return $this->get()->getResultArray();
    }

    public function GetTrainingScoreReport($id_training, $date)
    {
        $this->select()->where('id_training', $id_training)->where('mulai_training', $date);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna=tna.id_tna')->where('status_evaluasi', 1);
        return $this->get()->getResultArray();
    }

    public function checkedApproval($date, $id_training)
    {
        $this->select('tna.*,approval.*')->where('mulai_training', $date)->where('id_training', $id_training);
        $this->join('approval', 'approval.id_tna=tna.id_tna')->where('status_approval_3', NULL);
        return $this->get()->getResultArray();
    }

    // public function GetKesesuaianIsiProgram($id_training, $date)
    // {
    //     $this->select('sum(program) as kesesuaian')->where('id_training', $id_training)->where('mulai_training', $date);
    //     $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna=tna.id_tna')->where('status_evaluasi', 1);
    //     return $this->get()->getResultArray();
    // }

    public function getRequestCrossBudget($bagian, $member, $id_budget)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,user.*,approval.*')->where('tna.status', 'accept')
            ->where('status_approval_0', 'accept')
            ->where('status_approval_1', 'accept')->where('status_approval_2', null)
            ->where('status_approval_3', null)->where('kelompok_training', 'cross_budget')
            ->whereIn('id_budget',[$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->where('kelompok_training', 'cross_budget')
            ->whereIn('id_budget',[$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->where('kelompok_training', 'cross_budget')
            ->whereIn('id_budget',[$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereNotIn('user.departemen',[$member]);
            return $this->get()->getResultArray();
        } else {
            return  $status  =  array();
        }
    }

    public function getRequestCrossBudgetKadiv($bagian, $id_budget)
    {
        if ($bagian == 'KADIV') {
            $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->where('kelompok_training', 'cross_budget')
            ->whereIn('id_budget',[$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }  else {
            return  $status  =  array();
        }
    }

    public function getRequestCrossBudgetBod($bagian, $id_budget)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,user.*,approval.*')
            ->where('tna.status', 'accept')
            ->where('kelompok_training', 'cross_budget')
            ->whereIn('id_budget',[$id_budget]);
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }  else {
            return  $status  =  array();
        }
    }

    public function getTnaByDateIdTraining($date_start,$date_finish, $id_training)
    {
        $this->select()->where('mulai_training', $date_start)->where('rencana_training',$date_finish)->where('id_training', $id_training);
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_training', 1);
        return $this->get()->getResultArray();
    }

    public function getManHour($npk,$bagian){
        $current_year = date('Y');
        $query = $this->db->query("SELECT SUM(CONVERT(INT, man_hour)) as man_hour FROM tna WHERE npk = ? AND bagian = ? AND YEAR(mulai_training) = ?", [$npk,$bagian, $current_year]);
        $result = $query->getRow();
        return (int) $result->man_hour;
    }

    public function getTotalManHour($year){
        $query = $this->db->query("SELECT SUM(CONVERT(INT, man_hour)) as man_hour FROM tna WHERE YEAR(mulai_training) = ?", [$year]);
        $result = $query->getRow();
        return (int) $result->man_hour;
    }

    public function getTotalManHourDivisi($year, $divisi){
        $query = $this->db->query("SELECT SUM(CONVERT(INT, CONVERT(FLOAT, t.man_hour))) as man_hour 
                                   FROM tna t 
                                   LEFT JOIN [user] u ON t.npk = u.npk AND t.bagian = u.bagian
                                   WHERE YEAR(t.mulai_training) = ? AND u.divisi = ?", [$year, $divisi]);
        $result = $query->getRow();
        return (int) $result->man_hour;
    }

    public function getTotalManHourDepartment($year, $department){
        $query = $this->db->query("SELECT SUM(CONVERT(INT, CONVERT(FLOAT, t.man_hour))) as man_hour 
                                   FROM tna t 
                                   LEFT JOIN [user] u ON t.npk = u.npk AND t.bagian = u.bagian
                                   WHERE YEAR(t.mulai_training) = ? AND u.departemen = ?", [$year, $department]);
        $result = $query->getRow();
        return (int) $result->man_hour;
    }

    public function getManHourDepartemen($year){
        $query = $this->db->query("SELECT
        u.departemen AS nama_departemen,
        SUM(CONVERT(INT, CONVERT(FLOAT, t.man_hour))) AS total_man_hour
        FROM
            tna t
        LEFT JOIN
            [user] u ON t.npk = u.npk AND t.bagian = u.bagian
        WHERE
            YEAR(t.mulai_training) = ?
        GROUP BY
            u.departemen
        ORDER BY
            departemen;",[$year]);

        return $query->getResultArray();
    }

    public function getManHourDivisi($year, $divisi){
        $query = $this->db->query("SELECT
        u.departemen AS nama_departemen,
        SUM(CONVERT(INT, CONVERT(FLOAT, t.man_hour))) AS total_man_hour
        FROM
            tna t
        LEFT JOIN
            [user] u ON t.npk = u.npk AND t.bagian = u.bagian
        WHERE
            YEAR(t.mulai_training) = ?
        AND 
            u.divisi = ?
        GROUP BY
            u.departemen
        ORDER BY
            departemen;",[$year,$divisi]);
    
        return $query->getResultArray();
    }

    public function getManHourSection($year,$departemen){
        $query = $this->db->query("SELECT
        u.seksi AS nama_seksi,
        SUM(CONVERT(INT, CONVERT(FLOAT, t.man_hour))) AS total_man_hour
        FROM
            tna t
        LEFT JOIN 
            [user] u ON t.npk = u.npk AND t.bagian = u.bagian
        WHERE
            YEAR(t.mulai_training) = ?
        AND 
            u.departemen = ?
        GROUP BY
            u.seksi
        ORDER BY
            seksi;",[$year,$departemen]);
    
        return $query->getResultArray();
    }

    public function DashboardManHourDepartment($date,$department){
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan, SUM(CONVERT(INT, CONVERT(FLOAT, man_hour))) AS total_man_hour FROM tna LEFT JOIN [dbo].[user] ON tna.npk = [dbo].[user].npk AND tna.bagian = [dbo].[user].bagian WHERE departemen = '$department' AND YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();
        return $sql;
    }

    public function DashboardManHourSeksi($date,$seksi){
        $sql =  $this->query("SELECT MONTH(mulai_training) as Bulan, SUM(CONVERT(INT, CONVERT(FLOAT, man_hour))) AS total_man_hour FROM tna LEFT JOIN [dbo].[user] ON tna.npk = [dbo].[user].npk AND tna.bagian = [dbo].[user].bagian where seksi = '$seksi' and YEAR(mulai_training) = $date GROUP BY  MONTH(mulai_training)")->getResultArray();
        return $sql;
    }
}
