<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>


<div class="success" data-success="<?= session()->get('success'); ?>"></div>
<div class="card m-1 overflow-auto">
    <div class="card-header d-flex justify-content-center">
        <h3 class="card-title">Form Multi Unplanned</h3>

    </div>
    <!-- form -->
    <div class="card card-primary m-3">
        <div class="m-3">
        <input type="text" class="form-control mb-2" value="Dept &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;: <?= session()->get('departemen') ?>" disabled>
            <input type="text" class="form-control mb-2" value="Seksi &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;: <?= session()->get('seksi') ?>" disabled>
            <input type="text" class="form-control mb-2" value="Jabatan &ensp;&ensp;&ensp;&ensp;: Operator" disabled>
            <ul class="list-group">
            <?php foreach($user as $item) : ?>
                <li class="list-group-item"><?= $item['nama'] ?> (<?= $item['npk'] ?>)</li>
            <?php endforeach; ?>
            </ul>
        </div>
                        <!-- form start -->
                        <form role="form" action="<?= base_url() ?>save_form_multi" method="post" id="form-tna">
                        <?php foreach($user as $currentUser) : ?>
                            <input type="hidden" name="user[]" value="<?= $currentUser['id_user'] . ',' . $currentUser['npk'] . ',' . $currentUser['nama']. ',' . $currentUser['bagian'] ?>">
                            <input type="hidden" name="trainingunplanned" id="trainingunplanned">
                        <?php endforeach; ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="hidden" id="role" name="role">
                                    <label>Target Competency<span style="color:red;">*</span></label>
                                    <select class=" js-example-basic-single form-control" name="kompetensi"
                                        id="kompetensi" required>
                                        <option value="" disabled selected>Choose</option>
                                        <?php foreach ($target as $competency) : ?>
                                        <option value="<?= $competency['id'] ?>,<?= $competency['keterangan'] ?>">
                                            <?= $competency['category'] ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" value="<?= 1 ?>" name="deadline">
                                    <label>Training<span style="color:red;">*</span></label>
                                    <select class="js-example-basic-single form-control" name="training" id="training" required>
                                        <option value="" disabled selected>Choose</option>
                                        <?php foreach ($training as $trainings) : ?>
                                        <option value="<?= $trainings['id_training'] ?>">
                                            <?= $trainings['judul_training'] ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_training">Training Type<span style="color:red;">*</span></label>
                                    <div style="display: flex; align-items: center;">
                                        <input class="form-control" id="jenis_training" readonly required>
                                        <div id="loading-spinner" class="spinner-border text-primary" role="status" style="display: none; margin-left: 10px; width: 1rem; height: 1rem;">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Training Category<span style="color:red;">*</span></label>
                                    <select class="form-control" name="kategori" id="kategori" required>
                                        <option value="" disabled selected>Choose</option>
                                        <option value="Internal">Internal</option>
                                        <option value="External">External</option>
                                        <option value="Inhouse">Inhouse</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Method<span style="color:red;">*</span></label>
                                    <select class="custom-select" name="metode" id="metode" required>
                                        <option value="" disabled selected>Choose</option>
                                        <option value="Online">Online</option>
                                        <option value="Offline">Offline</option>
                                        <option value="Blended">Blended</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                <label for="datepicker">Request Training<span style="color:red;">*</span></label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" class="form-control" name="request" required>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-white">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                                <div class="form-group">
                                <label>Tujuan<span style="color:red;">*</span></label>
                                <textarea
                                    class="form-control <?= ($validation->hasError('tujuan')) ? 'is-invalid' : ''; ?> "
                                    id="validationTextarea" placeholder="Masukkan tujuan dan target training" required
                                    name="tujuan"></textarea>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('tujuan'); ?>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea class="form-control" name=" notes" placeholder="Permintaan Khusus"></textarea></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="biaya">Budget Estimation<span style="color:red;">*</span></label>
                                    <div style="display: flex; align-items: center;">
                                        <input class="form-control" id="biaya" readonly>
                                        <div id="loading-spinners" class="spinner-border text-primary" role="status" style="display: none; margin-left: 10px; width: 1rem; height: 1rem;">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary"  id="save-button"><i class="fa-solid fa-floppy-disk"></i>
                                    Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- endform -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('.js-example-basic-single').select2();

$("#datepicker").datepicker({
    format: "M-yyyy",
    startView: "months",
    minViewMode: "months"
});

$('#kompetensi').on('change', function() {
    var competency = this.value;
    console.log(competency);
})

$("#training").on('change', function() {
    var id_training = this.value;
    console.log(id_training);

    // Show loading spinner
    $("#loading-spinner").show();
    $("#loading-spinners").show();

    $.ajax({
        type: 'post',
        url: "<?= base_url(); ?>User/FormTna",
        async: true,
        dataType: "json",
        data: {
            id_training: id_training
        },
        success: function(data) {
            // Hide loading spinner
            $("#loading-spinner").hide();
            $("#loading-spinners").hide();

            console.log(data.jenis_training);
            const format = data.biaya.toString().split('').reverse().join('');
            const convert = format.match(/\d{1,3}/g);
            const rupiah = 'Rp ' + convert.join('.').split('').reverse().join('')
            console.log(rupiah)
            $("#training").val(data.id_training).prop('readonly', true)
            $("#trainingunplanned").val(data.id_training)
            $("#jenis_training").val(data.category)
            $("#biaya").val(rupiah)
        },
        error: function() {
            // Hide loading spinner
            $("#loading-spinner").hide();
            $("#loading-spinners").hide();
        }
    });
})

// let data = $('#data').val()
// if (data != null) {
//     $.ajax({
//         type: 'post',
//         url: "<?= base_url() ?>User/FormTna",
//         async: true,
//         dataType: "json",
//         data: {
//             id_training: data
//         },
//         success: function(data) {


//             console.log(data);

//             const format = data.biaya.toString().split('').reverse().join('');
//             const convert = format.match(/\d{1,3}/g);
//             const rupiah = 'Rp ' + convert.join('.').split('').reverse().join('')
//             $("#training").val(data.id_training).prop(
//                 'readonly', true)
//             $("#trainingunplanned").val(data.id_training)
//             $("#jenis_training").val(data.jenis_training)
//             $("#biaya").val(rupiah)
//         }

//     })

// }
</script>
<?= $this->endSection() ?>