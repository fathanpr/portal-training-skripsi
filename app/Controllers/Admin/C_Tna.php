<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Controllers\User\FormTna;
use App\Models\M_Approval;
use App\Models\M_Budget;
use App\Models\M_Deadline;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\M_Astra;
use App\Models\M_Soft;
use App\Models\M_Company;
use App\Models\M_History;
use App\Models\M_TechnicalB;
use App\Models\M_Technical;
use App\Models\M_Expert;
use App\Models\UserModel;
use App\Models\M_Akomodasi;
use App\Models\M_Report;
use App\Models\M_Trainer;
use App\Models\M_ListTraining;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Nilai;
use App\Models\M_EvaluasiEfektifitas;

use App\Models\M_CompetencyCompany;
use App\Models\M_CompetencyExpert;
use App\Models\M_CompetencySoft;
use App\Models\M_CompetencyTechnicalB;
use App\Models\M_CompetencyAstra;
use App\Models\M_CompetencyTechnical;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\FilterType;

class C_Tna extends BaseController
{
    private M_Tna $tna;
    
    private UserModel $user;
    private M_Akomodasi $akomodasi;
    private M_Approval $approval;

    //Competency
    private M_Astra $astra;
    private M_Company $company;
    private M_Soft $soft;
    private M_TechnicalB $technicalb;
    private M_Technical $technical;
    private M_Expert $expert;

    private M_Budget $budget;
    private C_Budget $budgetC;
    private M_History $history;

    private M_Nilai $nilai;

    private M_EvaluasiReaksi $evaluasiReaksi;
    private M_EvaluasiEfektifitas $efektifitas;

    private M_TnaUnplanned $unplanned;
    private M_Report $report;
    private M_Trainer $trainer;
    private M_ListTraining $training;

    private M_CompetencyExpert $competencyExpert;
    private M_CompetencySoft $competencySoft;
    private M_CompetencyCompany $competencyCompany;
    private M_CompetencyTechnicalB $competencyTechnicalB;
    private M_CompetencyTechnical $competencyTechnical;
    private M_CompetencyAstra $competencyAstra;

    private FormTna $UserTna;

    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
        $this->approval = new M_Approval();
        $this->unplanned = new M_TnaUnplanned();
        $this->budget = new M_Budget();
        $this->budgetC = new C_Budget();
        $this->UserTna = new FormTna();
        $this->akomodasi = new M_Akomodasi();
        $this->report = new M_Report();
        $this->trainer = new M_Trainer();
        $this->evaluasiReaksi = new M_EvaluasiReaksi();
        $this->astra = new M_Astra();
        $this->soft = new M_Soft();
        $this->company = new M_Company();
        $this->technicalb = new M_TechnicalB();
        $this->technical = new M_Technical();
        $this->expert = new M_Expert();
        $this->training = new M_ListTraining();
        $this->history = new M_History();
        $this->reaksi = new M_EvaluasiReaksi();
        $this->efektifitas = new M_EvaluasiEfektifitas();
        $this->nilai = new M_Nilai();

        $this->competencyCompany = new M_CompetencyCompany();
        $this->competencyExpert = new M_CompetencyExpert();
        $this->competencySoft = new M_CompetencySoft();
        $this->competencyTechnicalB = new M_CompetencyTechnicalB();
        $this->competencyTechnical = new M_CompetencyTechnical();
        $this->competencyAstra = new M_CompetencyAstra();
    }
    public function index()
    {
        $departemen = $this->tna->getStatusWaitAdminDepartemen();
        // dd('test');
        $data = [
            'tittle' => 'Form TNA',
            'tna' => $this->tna,
            'dept' => $departemen,
            'budget' => $this->budget
        ];

        // dd($data);
        return view('admin/tna', $data);
    }

    // public function change()
    // {
    //     $id = $this->request->getVar('id_deadline');
    //     $deadline = $this->request->getVar('deadline');

    //     if ($deadline == 0) {
    //         $data = [
    //             'id_deadline' => $id,
    //             'deadline' => 1
    //         ];
    //         $this->deadline->save($data);
    //         return redirect()->to('/tna');
    //     } else {
    //         $data = [
    //             'id_deadline' => $id,
    //             'deadline' => 0
    //         ];
    //         $this->deadline->save($data);
    //         return redirect()->to('/tna');
    //     }
    // }

    //moderate improvement
    public function trainingMonthly()
    {
        // UBAH date(Y) menjadi date(Y)+1
        
        // $TrainingMonthly = $this->tna->getTrainingMonthly(date('Y')+1);
        $TrainingMonthly = $this->tna->getTrainingMonthly(date('Y'));
        $TrainingMothlyName = [];

        foreach ($TrainingMonthly as $Month) {
            $MONTH = [
                'Year Training' => $Month['Year Training'],
                'Month Training' => $Month['Planing Training'],
                'Planning Training' => date("F", mktime(0, 0, 0, $Month['Planing Training'], 10)),
                'Jumlah Training' => $Month['Jumlah Training'],
                'Admin Approval' => $Month['Admin Approval'],
                'BOD Approval' => $Month['BOD Approval'],
                'Reject' => $Month['Reject']
            ];
            array_push($TrainingMothlyName, $MONTH);
        }
        
        $data = [
            'tittle' => 'Training Monthly',
            'training' => $TrainingMothlyName,
            'tna' => $this->tna
        ];
        return view('admin/trainingmonthly', $data);
    }

    public function otherMonthly()
    {

        $TrainingMonthly = $this->tna->getOtherMonthly();
        // dd($TrainingMonthly);
        $TrainingMothlyName = [];

        // dd($TrainingMonthly);

        foreach ($TrainingMonthly as $Month) {
            $MONTH = [
                'Year Training' => $Month['Year Training'],
                'Month Training' => $Month['Planing Training'],
                'Planning Training' => date("F", mktime(0, 0, 0, $Month['Planing Training'], 10)),
                'Jumlah Training' => $Month['Jumlah Training'],
                'Admin Approval' => $Month['Admin Approval'],
                'BOD Approval' => $Month['BOD Approval'],
                'Reject' => $Month['Reject']
            ];
            array_push($TrainingMothlyName, $MONTH);
        }

        // dd($TrainingMothlyName);
        
        $data = [
            'tittle' => 'Other Training Monthly',
            'training' => $TrainingMothlyName,
            'other' => $this->tna
        ];
        // dd($data);
        return view('admin/trainingmonthly', $data);
    }

    public function accept()
    {
        $id = $this->request->getPost('id_tna');

        $tna = $this->tna->getTnaForRole($id);
        $angka = $this->request->getPost('biaya_actual');
        $rupiah = $this->budgetC->strFilter($angka);
        
        $budgets = $this->budget->getBudgetNextYear($tna[0]['departemen']);
        
        if(empty($budgets)){
            $budgets = $this->budget->getBudgetCurrent($tna[0]['departemen']);
        }

        $budgetData = [
            'id_budget' => $budgets['id_budget'],
            'temporary_calculation' => $budgets['temporary_calculation'] + $rupiah
        ];

        $this->budget->save($budgetData);
        
        if (trim($tna[0]['type_golongan']) == 'A') {

            $data = [
                'id_tna' => $id,
                'biaya_actual' => $rupiah,
                'mulai_training' => $this->request->getPost('mulai_training'),
                'rencana_training' => $this->request->getPost('rencana_training'),
                'vendor' => $this->request->getPost('vendor'),
                'tempat' => $this->request->getPost('tempat'),
                'status' => 'accept'
            ];
            $id_approval = $this->approval->getIdApproval($id);
            $approval = [
                'id_approval' => $id_approval['id_approval'],
                'status_approval_0' => 'accept'
            ];
            $this->tna->save($data);
            $this->approval->save($approval);
        } else {
            $data = [
                'id_tna' => $id,
                'biaya_actual' => $rupiah,
                'mulai_training' => $this->request->getPost('mulai_training'),
                'rencana_training' => $this->request->getPost('rencana_training'),
                'vendor' => $this->request->getPost('vendor'),
                'tempat' => $this->request->getPost('tempat'),
                'status' => 'accept'
            ];
            $this->tna->save($data);
        }
        echo json_encode($rupiah);
    }

    public function reject()
    {

        $data = [
            'id_tna' =>  $this->request->getPost('id_tna'),
            'biaya_actual' => $this->request->getPost('biaya_actual'),
            'rencana_training' => $this->request->getPost('rencana_training'),
            'status' => 'reject'
        ];
        // $this->tna->save($data);
        echo json_encode($data);
    }


    public function kadivStatus()
    {
        $tna = $this->tna->getKadivStatus();
        $tahun = [];

        foreach ($tna as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $year = [date('Y'),date('Y')+1];
        }else{
            $year = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadiv Status Approval',
            'tna' => $tna,
            'year' => $year,
            'user' => $this->user,
        ];
        return view('admin/tnakadiv', $data);
    }

    public function kadeptStatus()
    {
        $tna = $this->tna->getKadeptStatus();
        $tahun = [];

        foreach ($tna as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $year = [date('Y'),date('Y')+1];
        }else{
            $year = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadept Status Approval',
            'tna' => $tna,
            'year' => $year,
            'user' => $this->user,
        ];
        return view('admin/tnakadept', $data);
    }

    public function kadeptStatusFilter($year)
    {
        $tna = $this->tna->getKadeptStatusFilter($year);
        $tna_year = $this->tna->getKadeptStatus();
        $tahun = [];

        foreach ($tna_year as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadept Status Approval',
            'tna' => $tna,
            'year' => $years,
            'user' => $this->user,
        ];
        return view('admin/tnakadept', $data);
    }

    public function kadivStatusFilter($year)
    {
        $tna = $this->tna->getKadivStatusFilter($year);
        $tna_year = $this->tna->getKadivStatus();
        $tahun = [];

        foreach ($tna_year as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadiv Status Approval',
            'tna' => $tna,
            'year' => $years,
            'user' => $this->user,
        ];
        return view('admin/tnakadiv', $data);
    }

    public function bodStatus()
    {
        $tna = $this->tna->getBodStatus();
        $filteredTna = [];
        $tahun = [];

        // dd($tna);
    
        foreach ($tna as $item) {
            if ($item['biaya_actual'] > 2000000 || $item['bagian'] == 'KADIV') {
                $filteredTna[] = $item;
            }
        }
        foreach ($filteredTna as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'BOD Status Approval',
            'tna' => $filteredTna,
            'year' => $years,
            'user' => $this->user,
        ];
        return view('admin/tnabod', $data);
    }

    public function bodStatusFilter($year)
    {
        $tna = $this->tna->getBodStatusFilter($year);
        $tna_year = $this->tna->getBodStatus();
        $tahun = [];

        $filteredYear = [];
        $filteredTna = [];
    
        foreach ($tna as $item) {
            if ($item['biaya_actual'] > 2000000 || $item['bagian'] == 'KADIV') {
                $filteredTna[] = $item;
            }
        }

        foreach ($tna_year as $item) {
            if ($item['biaya_actual'] > 2000000 || $item['bagian'] == 'KADIV') {
                $filteredYear[] = $item;
            }
        }

        foreach ($filteredYear as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'BOD Status Approval',
            'tna' => $filteredTna,
            'year' => $years,
            'user' => $this->user,
        ];
        return view('admin/tnabod', $data);
    }


    public function kadivAccept($Month)
    {
        $date = date_parse($Month);
        // dd($date);

        // UBAH date(Y) menjadi date(Y)+1
        $departemen = $this->tna->getKadivAcceptDistinct($date['month'], date('Y'));

        $data = [
            'tittle' => 'Training Realization',
            'departemen' => $departemen,
            'stat' => $this->tna,
            'date' => $date['month'],
            'year' => date('Y'),
            'budget' => $this->budget,
            'akomodasi' => $this->akomodasi,
            'user' => $this->user
        ];
        return view('admin/kadivaccept', $data);
    }

    public function exportTrainingMonthly($month){
       $data = $this->tna->getKadivAcceptForExport($month);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getActiveSheet()->getStyle('A1:N1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFFFF00');

        $sheet->getStyle('B')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('J')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(8);
        $sheet->getColumnDimension('C')->setWidth(40);
        $sheet->getColumnDimension('D')->setWidth(45);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(40);
        $sheet->getColumnDimension('H')->setWidth(18);
        $sheet->getColumnDimension('I')->setWidth(18);
        $sheet->getColumnDimension('J')->setWidth(17);
        $sheet->getColumnDimension('K')->setWidth(25);
        $sheet->getColumnDimension('L')->setWidth(25);
        $sheet->getColumnDimension('M')->setWidth(25);
        $sheet->getColumnDimension('N')->setWidth(20);

        $sheet->setCellValue('A1', 'Name');
        $sheet->setCellValue('B1', 'NPK');
        $sheet->setCellValue('C1', 'Department');
        $sheet->setCellValue('D1', 'Training Title');
        $sheet->setCellValue('E1', 'Training Type');
        $sheet->setCellValue('F1', 'Training Category');
        $sheet->setCellValue('G1', 'Training Goals');
        $sheet->setCellValue('H1', 'Training Start');
        $sheet->setCellValue('I1', 'Training Finished');
        $sheet->setCellValue('J1', 'Actual Budget');
        $sheet->setCellValue('K1', 'Accomodation Budget');
        $sheet->setCellValue('L1', 'Total Budget');
        $sheet->setCellValue('M1', 'Training Group');
        $sheet->setCellValue('N1', 'Status');

        $row = 2;
        foreach ($data as $item) {

            if($item['status_training'] == NULL){
                $statusTraining = 'Not Yet';
            }elseif($item['status_training'] == '1'){
                $statusTraining = 'Implemented';
            }elseif($item['status_training'] == NULL && ($item['status_approval_0'] == 'reject' || $item['status_approval_1'] == 'reject' || $item['status_approval_2'] == 'reject' || $item['status_approval_3'] == 'reject')){
                $statusTraining = 'Reject';
            }

            if(empty($item['biaya_akomodasi'])){
                $biaya_akomodasi = 0;
            }else{
                $biaya_akomodasi = intval(preg_replace("/[^0-9]/", "", $item['biaya_akomodasi']));
            }

            $new_biaya_actual = intval($item['biaya_actual'])-$biaya_akomodasi;
            $biayaRupiah = 'Rp. ' . number_format($new_biaya_actual, 0, ',', '.');
            $biaya_akomodasi = $item['biaya_akomodasi'] === NULL ? 'Biaya Akomodasi Belum Diinput' : $item['biaya_akomodasi'];
            
            $sheet->setCellValue('A' . $row, $item['nama']);
            $sheet->setCellValue('B' . $row, $item['npk']);
            $sheet->setCellValue('C' . $row, $item['departemen']);
            $sheet->setCellValue('D' . $row, $item['training']);
            $sheet->setCellValue('E' . $row, $item['jenis_training']);
            $sheet->setCellValue('F' . $row, $item['kategori_training']);
            $sheet->setCellValue('G' . $row, $item['tujuan_training']);
            $sheet->setCellValue('H' . $row, $item['mulai_training']);
            $sheet->setCellValue('I' . $row, $item['rencana_training']);
            $sheet->setCellValue('J' . $row, $biayaRupiah);
            $sheet->setCellValue('K' . $row, $biaya_akomodasi);
            $sheet->setCellValue('L' . $row, 'Rp. ' . number_format($item['biaya_actual'], 0, ',', '.'));
            $sheet->setCellValue('M' . $row, $item['kelompok_training']);
            $sheet->setCellValue('N' . $row, $statusTraining);
            $row++;
        }

        $spreadsheet->getActiveSheet()->setAutoFilter("A1:N1");

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His'). '-Training-Monthly.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function exportTrainingMonthlyOther($month,$year){
        $data = $this->tna->getKadivAcceptForExportOther($month,$year);
 
         $spreadsheet = new Spreadsheet();
         $sheet = $spreadsheet->getActiveSheet();
 
         $spreadsheet->getActiveSheet()->getStyle('A1:N1')->getFill()
         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
         ->getStartColor()->setARGB('FFFFFF00');
 
         $sheet->getStyle('B')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
         $sheet->getStyle('J')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
         $sheet->getColumnDimension('A')->setWidth(30);
         $sheet->getColumnDimension('B')->setWidth(8);
         $sheet->getColumnDimension('C')->setWidth(40);
         $sheet->getColumnDimension('D')->setWidth(45);
         $sheet->getColumnDimension('E')->setWidth(30);
         $sheet->getColumnDimension('F')->setWidth(20);
         $sheet->getColumnDimension('G')->setWidth(40);
         $sheet->getColumnDimension('H')->setWidth(18);
         $sheet->getColumnDimension('I')->setWidth(18);
         $sheet->getColumnDimension('J')->setWidth(17);
         $sheet->getColumnDimension('K')->setWidth(25);
         $sheet->getColumnDimension('L')->setWidth(25);
         $sheet->getColumnDimension('M')->setWidth(25);
         $sheet->getColumnDimension('N')->setWidth(20);
 
         $sheet->setCellValue('A1', 'Name');
         $sheet->setCellValue('B1', 'NPK');
         $sheet->setCellValue('C1', 'Department');
         $sheet->setCellValue('D1', 'Training Title');
         $sheet->setCellValue('E1', 'Training Type');
         $sheet->setCellValue('F1', 'Training Category');
         $sheet->setCellValue('G1', 'Training Goals');
         $sheet->setCellValue('H1', 'Training Start');
         $sheet->setCellValue('I1', 'Training Finished');
         $sheet->setCellValue('J1', 'Actual Budget');
         $sheet->setCellValue('K1', 'Accomodation Budget');
         $sheet->setCellValue('L1', 'Total Budget');
         $sheet->setCellValue('M1', 'Training Group');
         $sheet->setCellValue('N1', 'Status');
 
         $row = 2;
         foreach ($data as $item) {
 
             if($item['status_training'] == NULL){
                 $statusTraining = 'Not Yet';
             }elseif($item['status_training'] == '1'){
                 $statusTraining = 'Implemented';
             }elseif($item['status_training'] == NULL && ($item['status_approval_0'] == 'reject' || $item['status_approval_1'] == 'reject' || $item['status_approval_2'] == 'reject' || $item['status_approval_3'] == 'reject')){
                 $statusTraining = 'Reject';
             }
 
             if(empty($item['biaya_akomodasi'])){
                 $biaya_akomodasi = 0;
             }else{
                 $biaya_akomodasi = intval(preg_replace("/[^0-9]/", "", $item['biaya_akomodasi']));
             }
 
             $new_biaya_actual = intval($item['biaya_actual'])-$biaya_akomodasi;
             $biayaRupiah = 'Rp. ' . number_format($new_biaya_actual, 0, ',', '.');
             $biaya_akomodasi = $item['biaya_akomodasi'] === NULL ? 'Biaya Akomodasi Belum Diinput' : $item['biaya_akomodasi'];
             
             $sheet->setCellValue('A' . $row, $item['nama']);
             $sheet->setCellValue('B' . $row, $item['npk']);
             $sheet->setCellValue('C' . $row, $item['departemen']);
             $sheet->setCellValue('D' . $row, $item['training']);
             $sheet->setCellValue('E' . $row, $item['jenis_training']);
             $sheet->setCellValue('F' . $row, $item['kategori_training']);
             $sheet->setCellValue('G' . $row, $item['tujuan_training']);
             $sheet->setCellValue('H' . $row, $item['mulai_training']);
             $sheet->setCellValue('I' . $row, $item['rencana_training']);
             $sheet->setCellValue('J' . $row, $biayaRupiah);
             $sheet->setCellValue('K' . $row, $biaya_akomodasi);
             $sheet->setCellValue('L' . $row, 'Rp. ' . number_format($item['biaya_actual'], 0, ',', '.'));
             $sheet->setCellValue('M' . $row, $item['kelompok_training']);
             $sheet->setCellValue('N' . $row, $statusTraining);
             $row++;
         }
 
         $spreadsheet->getActiveSheet()->setAutoFilter("A1:N1");
 
         $writer = new Xlsx($spreadsheet);
         $tempDir = WRITEPATH . 'spreadsheet/';
         $filename = date('Y-m-d-His'). '-Training-Monthly-Other.xlsx';
         $writer->save($tempDir . $filename);
 
         return $this->response->download($tempDir . $filename, null);
     }

    public function kadivAcceptOther($Month,$Year)
    {
        $date = date_parse($Month);

        $data_other_training = $this->tna->getKadivAcceptOtherByDate($date['month'], $Year);
        $total_index = count($data_other_training);

        $budgetData = [];
        $addedBudgetIds = [];

        for ($i = 0; $i < $total_index; $i++) {
            $currentIdBudget = $data_other_training[$i]['id_budget'];

            if (!in_array($currentIdBudget, $addedBudgetIds)) {
                $budgetData[] = $this->budget->getBudgetById($currentIdBudget);
                $addedBudgetIds[] = $currentIdBudget;
            }
        }
        

        $departemen =  $this->tna->getKadivAcceptOtherDistinct($date['month'], $Year);

        $data = [
            'tittle' => 'KADIV Other Training Accepted',
            'budgetData' => $budgetData,
            'status' => $data_other_training,
            'date' => $date['month'],
            'year' => $Year,
            'budget' => $this->budget,
            'akomodasi' => $this->akomodasi
        ];

        // dd($data);
        return view('admin/kadivacceptother', $data);
    }

    public function acceptAdmin()
    {
        $approve = $this->approval->getIdApproval($this->request->getPost('id_tna'));
        $biaya_actual = $this->request->getPost('biaya_actual');
        $biaya = $this->request->getPost('biaya');

        $Rupiah  = $this->budgetC->strFilter($biaya_actual);
        $tna =  $this->tna->getAllTna($this->request->getPost('id_tna'));
        $budget = $this->budget->getDataBudgetById($tna[0]->id_budget);

        $dataBudget = [
            'id_budget' => $budget['id_budget'],
            'used_budget' => $budget['used_budget'] + $Rupiah,
            'available_budget' => $budget['available_budget'] - $Rupiah,
            'temporary_calculation' => $budget['temporary_calculation'] - $biaya,
        ];

        $this->budget->save($dataBudget);

        $data = [
            'id_approval' => $approve['id_approval'],
            'status_approval_3' => 'accept',
        ];

        $data1 = [
            'id_tna' => $this->request->getPost('id_tna'),
            'mulai_training' => $this->request->getPost('mulai_training'),
            'rencana_training' => $this->request->getPost('rencana_training'),
            'biaya_actual' => $Rupiah,
            'vendor' => $this->request->getPost('vendor')
        ];

        //UPLOAD AKOMODASI
        $file = $this->request->getFile('attachment');
        $biaya_akomodasi = $this->request->getPost('akomodasi');

        if ($file->isValid() && !$file->hasMoved()) {
            $file->getName();
            $newName = $file->getRandomName();
            $file->move(ROOTPATH . 'public/attachment', $newName);
            $filepath = "/attachment/" . $newName;

            $dataAkomodasi =[
                'id_tna' => $this->request->getPost('id_tna'),
                'biaya_akomodasi' => $biaya_akomodasi,
                'attachment'=> $filepath
            ];

            $this->akomodasi->save($dataAkomodasi);
        }

        $this->tna->save($data1);
        $this->approval->save($data);
        echo json_encode($data);
    }

    public function rejectAdmin()
    {
        $id = $this->request->getPost('id_tna');
        $approve = $this->approval->getIdApproval($id);
        $biaya_actual = $this->request->getPost('biaya_actual');
        $biaya = $this->request->getPost('biaya');
        $Rupiah  = $this->budgetC->strFilter($biaya_actual);
        $training = $this->tna->getAllTna($id);
        $budgetData = $this->budget->getBudgetById($training[0]->id_budget);

        // var_dump($this->request->getPost('alasan')).die();
        
        // $this->UserTna->subtraction($Rupiah, $training[0]->departemen);
        $data1 = [
            'id_tna' => $this->request->getPost('id_tna'),
            'biaya_actual' => $Rupiah,
        ];

        $data_budget = [
            'id_budget' => $training[0]->id_budget,
            'temporary_calculation' => $budgetData['temporary_calculation'] - $training[0]->biaya_actual
        ];

        $data = [
            'id_approval' => $approve['id_approval'],
            'alasan' => $this->request->getPost('alasan'),
            'status_approval_2' => 'reject',
            'status_approval_3' => 'reject'
        ];

        $this->budget->save($data_budget);
        $this->tna->save($data1);
        $this->approval->save($data);
        echo json_encode($data);
    }

    public function rejectAdminFirst()
    {
        $id = $this->request->getPost('id_tna');
        $approve = $this->approval->getIdApproval($id);
        $biaya_actual = $this->request->getPost('biaya_actual');
        $biaya = $this->request->getPost('biaya');
        $Rupiah  = $this->budgetC->strFilter($biaya_actual);
        $training = $this->tna->getAllTna($id);
        $budgetData = $this->budget->getBudgetById($training[0]->id_budget);

        // $this->UserTna->subtraction($Rupiah, $training[0]->departemen);
        $data1 = [
            'id_tna' => $this->request->getPost('id_tna'),
            'status' => 'reject',
        ];

        // $data_budget = [
        //     'id_budget' => $training[0]->id_budget,
        //     'temporary_calculation' => $budgetData['temporary_calculation'] - $training[0]->biaya_actual
        // ];

        $data = [
            'id_approval' => $approve['id_approval'],
            'alasan' => $this->request->getPost('alasan'),
            'status_approval_0' => 'reject',
            'status_approval_1' => 'reject',
            'status_approval_2' => 'reject',
            'status_approval_3' => 'reject'
        ];

        // $this->budget->save($data_budget);
        $this->tna->save($data1);
        $this->approval->save($data);
        echo json_encode($data);
    }

    public function detailTna()
    {
        $id_tna = $this->request->getPost('id_tna');
        $data = $this->tna->getAllTna($id_tna);
        echo json_encode($data);
    }

    public function detailReject()
    {
        $id = $this->request->getPost('id_tna');
        $data = $this->approval->getIdApproval($id);
        echo json_encode($data);
    }

    public function TrainingDitolak()
    {
        $reject = $this->tna->getTrainingDitolak();

        $data = [
            'tittle' => 'Training Rejected',
            'training' => $reject
        ];
        return view('admin/trainingreject', $data);
    }


    public function AtmpReport()
    {
        $atmp = $this->tna->getAtmp();
        $data = [
            'tittle' => 'ATMP Report',
            'Atmp' => $atmp,
            'user' => $this->user
        ];

        return view('admin/atmpreport', $data);
    }

    public function DownloadAtmpReport()
    {
    
        $data = $this->tna->getAtmp();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFFF00');

        $sheet->getStyle('B')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('K')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('L')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('M')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('P')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(8);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(40);
        $sheet->getColumnDimension('E')->setWidth(45);
        $sheet->getColumnDimension('F')->setWidth(30);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(40);
        $sheet->getColumnDimension('I')->setWidth(18);
        $sheet->getColumnDimension('J')->setWidth(18);
        $sheet->getColumnDimension('K')->setWidth(17);
        $sheet->getColumnDimension('L')->setWidth(25);
        $sheet->getColumnDimension('M')->setWidth(25);
        $sheet->getColumnDimension('N')->setWidth(25);
        $sheet->getColumnDimension('O')->setWidth(20);
        $sheet->getColumnDimension('P')->setWidth(20);
        $sheet->getColumnDimension('Q')->setWidth(20);
        $sheet->getColumnDimension('R')->setWidth(20);

        $sheet->setCellValue('A1', 'Name');
        $sheet->setCellValue('B1', 'NPK');
        $sheet->setCellValue('C1', 'Golongan');
        $sheet->setCellValue('D1', 'Department');
        $sheet->setCellValue('E1', 'Training Title');
        $sheet->setCellValue('F1', 'Vendor');
        $sheet->setCellValue('G1', 'Training Type');
        $sheet->setCellValue('H1', 'Training Category');
        $sheet->setCellValue('I1', 'Training Goals');
        $sheet->setCellValue('J1', 'Request Training');
        $sheet->setCellValue('K1', 'Training Start');
        $sheet->setCellValue('L1', 'Training Finished');
        $sheet->setCellValue('M1', 'Actual Budget');
        $sheet->setCellValue('N1', 'Accomodation Budget');
        $sheet->setCellValue('O1', 'Total Budget');
        $sheet->setCellValue('P1', 'Training Group');
        $sheet->setCellValue('Q1', 'Status');
        $sheet->setCellValue('R1', 'Hour');

        $row = 2;
        foreach ($data as $item) {

            if ($item['status_training'] == NULL) {
                $statusTraining = 'Not Yet';
            } elseif ($item['status_training'] == '1') {
                $statusTraining = 'Implemented';
            } elseif ($item['status_training'] == NULL && ($item['status_approval_0'] == 'reject' || $item['status_approval_1'] == 'reject' || $item['status_approval_2'] == 'reject' || $item['status_approval_3'] == 'reject')) {
                $statusTraining = 'Reject';
            }

            if (empty($item['biaya_akomodasi'])) {
                $biaya_akomodasi = 0;
            } else {
                $biaya_akomodasi = intval(preg_replace("/[^0-9]/", "", $item['biaya_akomodasi']));
            }

            $dpt = $this->user->getDepartemenByNpk($item['npk']);
            $departemen = $dpt->departemen;
            $type_golongan = $dpt->type_golongan;

            $new_biaya_actual = intval($item['biaya_actual']) - $biaya_akomodasi;
            $biayaRupiah = 'Rp. ' . number_format($new_biaya_actual, 0, ',', '.');

            $sheet->setCellValue('A' . $row, $item['nama']);
            $sheet->setCellValue('B' . $row, $item['npk']);
            $sheet->setCellValue('C' . $row, $type_golongan);
            $sheet->setCellValue('D' . $row, $departemen);
            $sheet->setCellValue('E' . $row, $item['training']);
            $sheet->setCellValue('F' . $row, $item['vendor']);
            $sheet->setCellValue('G' . $row, $item['jenis_training']);
            $sheet->setCellValue('H' . $row, $item['kategori_training']);
            $sheet->setCellValue('I' . $row, $item['tujuan_training']);
            $sheet->setCellValue('J' . $row, $item['request_training']);
            $sheet->setCellValue('K' . $row, $item['mulai_training']);
            $sheet->setCellValue('L' . $row, $item['rencana_training']);
            $sheet->setCellValue('M' . $row, $new_biaya_actual);
            $sheet->setCellValue('N' . $row, $biaya_akomodasi);
            $sheet->setCellValue('O' . $row, $item['biaya_actual']);
            $sheet->setCellValue('P' . $row, $item['kelompok_training']);
            $sheet->setCellValue('Q' . $row, $statusTraining);
            $sheet->setCellValue('R' . $row, $item['man_hour']);
            $row++;
        }

        $spreadsheet->getActiveSheet()->setAutoFilter("A1:R1");

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His'). '-Atmp-Report.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function AtmpSubmit(){
        $atmpSubmit = $this->tna->getAtmpSubmit();
        
        $data = [
            'tittle' => 'ATMP Submit',
            'atmpSubmit' => $atmpSubmit,
            'user' => $this->user
        ];

        return view('admin/atmpsubmit',$data);
    }

    public function DownloadAtmpSubmit()
    {
    
        $data = $this->tna->getAtmpSubmit();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFFF00');

        $sheet->getStyle('B')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('J')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(8);
        $sheet->getColumnDimension('C')->setWidth(40);
        $sheet->getColumnDimension('D')->setWidth(45);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(40);
        $sheet->getColumnDimension('H')->setWidth(18);
        $sheet->getColumnDimension('I')->setWidth(18);
        $sheet->getColumnDimension('J')->setWidth(17);
        $sheet->getColumnDimension('K')->setWidth(17);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(30);

        $sheet->setCellValue('A1', 'Name');
        $sheet->setCellValue('B1', 'NPK');
        $sheet->setCellValue('C1', 'Department');
        $sheet->setCellValue('D1', 'Training Title');
        $sheet->setCellValue('E1', 'Vendor Suggest');
        $sheet->setCellValue('F1', 'Training Type');
        $sheet->setCellValue('G1', 'Training Category');
        $sheet->setCellValue('H1', 'Training Goals');
        $sheet->setCellValue('I1', 'Request Training');
        $sheet->setCellValue('J1', 'Training Start');
        $sheet->setCellValue('K1', 'Training Finished');
        $sheet->setCellValue('L1', 'Actual Budget');
        $sheet->setCellValue('M1', 'Training Group');

        $row = 2;
        foreach ($data as $item) {
            $biayaRupiah = 'Rp' . number_format($item['biaya_actual'], 0, ',', '.');
            $dpt = $this->user->getDepartemenByNpk($item['npk']);
            $departemen = $dpt->departemen;

            $sheet->setCellValue('A' . $row, $item['nama']);
            $sheet->setCellValue('B' . $row, $item['npk']);
            $sheet->setCellValue('C' . $row, $departemen);
            $sheet->setCellValue('D' . $row, $item['training']);
            $sheet->setCellValue('E' . $row, $item['vendor_suggest']);
            $sheet->setCellValue('F' . $row, $item['jenis_training']);
            $sheet->setCellValue('G' . $row, $item['kategori_training']);
            $sheet->setCellValue('H' . $row, $item['tujuan_training']);
            $sheet->setCellValue('I' . $row, $item['request_training']);
            $sheet->setCellValue('J' . $row, $item['mulai_training']);
            $sheet->setCellValue('K' . $row, $item['rencana_training']);
            $sheet->setCellValue('L' . $row, $biayaRupiah);
            $sheet->setCellValue('M' . $row, $item['kelompok_training']);
            $row++;
        }

        $spreadsheet->getActiveSheet()->setAutoFilter("A1:M1");

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His'). '-Atmp-Submit.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function TrainerReport()
    {

        $trainerScore = $this->trainer->getTrainerWithScore(); 
        // dd($trainerScore);

        $data = [
            'tittle' => 'Trainer Report',
            'trainer' => $trainerScore
        ];

        return view('admin/trainerreport',$data);
    }

    public function DownloadTrainerReport(){
        $tn_score = $this->trainer->getTrainerWithScore();

        $dataTrainer = $this->tna->getAllEvaluasiReaksi();
        $arrayTrainer = [];
        foreach($dataTrainer as $data){
            for ($i = 1; $i <= 5; $i++) {
                $instrukturKey = "instruktur_$i";
                if (isset($data[$instrukturKey])) {
                    $arrayTrainer[] = $data[$instrukturKey];
                }
            }
        }

        $id_trainers = array_unique($arrayTrainer);
        
        $trainerArray = [];
        $reportTrainer = [];
        foreach($id_trainers as $id){
            $trainer_report_score = $this->report->getReportTrainer($id);
            $nama_trainer = $this->trainer->getTrainerName($id);
            $reportTrainer = array_merge($reportTrainer, $trainer_report_score);
            $trainerArray = array_merge($trainerArray,$nama_trainer);
        }

        $tnaByJudul = [];
        foreach($reportTrainer as $item){
            $tna_by_judul = $this->tna->getTnaByJudul($item['judul_training']);
            $tnaByJudul = array_merge($tnaByJudul,$tna_by_judul);
        }

        $id_tna_by_judul = [];
        foreach($tnaByJudul as $item){
            $array_1[] = $item['id_tna'];
            $id_tna_by_judul = array_merge($id_tna_by_judul, $array_1);
        }

        $distinct_id_tna_judul = array_unique($id_tna_by_judul);
        $trainer_with_score = [];

        foreach($distinct_id_tna_judul as $item){
            $data_evaluasi_reaksi = $this->evaluasiReaksi->getDataEvaluasiReaksiById($item);
            $trainer_with_score = array_merge($trainer_with_score,$data_evaluasi_reaksi);
        }

        $instruktur1 = [];
        $instruktur2 = [];
        $instruktur3 = [];
        $instruktur4 = [];
        $instruktur5 = [];

        foreach ($trainer_with_score as $item) {
            //INSTRUKTUR 1
            $id_tna = $item['id_tna'];
            $id_instruktur1 = $item['instruktur_1'];
            $pengetahuan1 = $item["pengetahuan1"];
            $kemampuan1 = $item["kemampuan1"];
            $kemampuan_melibatkan1 = $item["kemampuan_melibatkan1"];
            $kemampuan_menanggapi1 = $item["kemampuan_menanggapi1"];
            $kemampuan_mengendalikan1 = $item["kemampuan_mengendalikan1"];
            $judul_training = $item["training"];

            //INSTRUKTUR 2
            $id_tna = $item['id_tna'];
            $id_instruktur2 = $item['instruktur_2'];
            $pengetahuan2 = $item["pengetahuan2"];
            $kemampuan2 = $item["kemampuan2"];
            $kemampuan_melibatkan2 = $item["kemampuan_melibatkan2"];
            $kemampuan_menanggapi2 = $item["kemampuan_menanggapi2"];
            $kemampuan_mengendalikan2 = $item["kemampuan_mengendalikan2"];
            $judul_training = $item["training"];

            // INSTRUKTUR 3
            $id_tna = $item['id_tna'];
            $id_instruktur3 = $item['instruktur_3'];
            $pengetahuan3 = $item["pengetahuan3"];
            $kemampuan3 = $item["kemampuan3"];
            $kemampuan_melibatkan3 = $item["kemampuan_melibatkan3"];
            $kemampuan_menanggapi3 = $item["kemampuan_menanggapi3"];
            $kemampuan_mengendalikan3 = $item["kemampuan_mengendalikan3"];
            $judul_training = $item["training"];

            // INSTRUKTUR 4
            $id_tna = $item['id_tna'];
            $id_instruktur4 = $item['instruktur_4'];
            $pengetahuan4 = $item["pengetahuan4"];
            $kemampuan4 = $item["kemampuan4"];
            $kemampuan_melibatkan4 = $item["kemampuan_melibatkan4"];
            $kemampuan_menanggapi4 = $item["kemampuan_menanggapi4"];
            $kemampuan_mengendalikan4 = $item["kemampuan_mengendalikan4"];
            $judul_training = $item["training"];

            // INSTRUKTUR 4
            $id_tna = $item['id_tna'];
            $id_instruktur5 = $item['instruktur_5'];
            $pengetahuan5 = $item["pengetahuan5"];
            $kemampuan5 = $item["kemampuan5"];
            $kemampuan_melibatkan5 = $item["kemampuan_melibatkan5"];
            $kemampuan_menanggapi5 = $item["kemampuan_menanggapi5"];
            $kemampuan_mengendalikan5 = $item["kemampuan_mengendalikan5"];
            $judul_training = $item["training"];

            
            $instruktur1[] = [
                "id_tna" => $id_tna,
                "id_instruktur" => $id_instruktur1,
                "pengetahuan" => $pengetahuan1,
                "kemampuan" => $kemampuan1,
                "kemampuan_melibatkan" => $kemampuan_melibatkan1,
                "kemampuan_menanggapi" => $kemampuan_menanggapi1,
                "kemampuan_mengendalikan" => $kemampuan_mengendalikan1,
                "judul_training" => $judul_training
            ];

            
            if ($id_instruktur2 !== null) {
                $instruktur2[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur2,
                    "pengetahuan" => $pengetahuan2,
                    "kemampuan" => $kemampuan2,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan2,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi2,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan2,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur3 !== null) {
                $instruktur3[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur3,
                    "pengetahuan" => $pengetahuan3,
                    "kemampuan" => $kemampuan3,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan3,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi3,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan3,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur4 !== null) {
                $instruktur4[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur4,
                    "pengetahuan" => $pengetahuan4,
                    "kemampuan" => $kemampuan4,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan4,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi4,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan4,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur5 !== null) {
                $instruktur5[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur5,
                    "pengetahuan" => $pengetahuan5,
                    "kemampuan" => $kemampuan5,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan5,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi5,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan5,
                    "judul_training" => $judul_training
                ];
            }
        }

        $instruktur_with_detail_score = array_merge($instruktur1,$instruktur2,$instruktur3,$instruktur4,$instruktur5);

        $trainer_name = $this->trainer->getAllTrainer();

        $trainerNameMap = [];
        foreach ($trainer_name as $trainer) {
            $trainerNameMap[$trainer['id_trainer']] = $trainer;
        }

        $finalArrayTrainer = [];

        foreach ($instruktur_with_detail_score as $instruktur) {
            $id_instruktur = $instruktur['id_instruktur'];
            if (isset($trainerNameMap[$id_instruktur])) {
                $finalArrayTrainer[] = array_merge($instruktur, $trainerNameMap[$id_instruktur]);
            }
        }
        

        $spreadsheet = new Spreadsheet();
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Report Trainer');

        $sheet2 = $spreadsheet->createSheet();
        $sheet2->setTitle('Detail Trainer Score');

        $sheet1->getStyle('A1:G1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFFFF00');

        $sheet2->getStyle('A1:H1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFFFF00');

        $sheet1->getColumnDimension('A')->setWidth(30);
        $sheet1->getColumnDimension('B')->setWidth(30);
        $sheet1->getColumnDimension('C')->setWidth(30);
        $sheet1->getColumnDimension('D')->setWidth(26);
        $sheet1->getColumnDimension('E')->setWidth(45);
        $sheet1->getColumnDimension('F')->setWidth(15);
        $sheet1->getColumnDimension('G')->setWidth(40);

        $sheet2->getColumnDimension('A')->setWidth(30);
        $sheet2->getColumnDimension('B')->setWidth(30);
        $sheet2->getColumnDimension('C')->setWidth(25);
        $sheet2->getColumnDimension('D')->setWidth(25);
        $sheet2->getColumnDimension('E')->setWidth(25);
        $sheet2->getColumnDimension('F')->setWidth(25);
        $sheet2->getColumnDimension('G')->setWidth(25);
        $sheet2->getColumnDimension('H')->setWidth(25);

        $sheet1->setCellValue('A1', 'Name');
        $sheet1->setCellValue('B1', 'Trainer Since');
        $sheet1->setCellValue('C1', 'Vendor');
        $sheet1->setCellValue('D1', 'Training Duration (Hour)');
        $sheet1->setCellValue('E1', 'Training');
        $sheet1->setCellValue('F1', 'Avg Score');
        $sheet1->setCellValue('G1', 'Predicate');

        $sheet2->setCellValue('A1', 'Nama');
        $sheet2->setCellValue('B1', 'Training');
        $sheet2->setCellValue('C1', 'Pengetahuan');
        $sheet2->setCellValue('D1', 'Kemampuan');
        $sheet2->setCellValue('E1', 'Kemampuan Melibatkan');
        $sheet2->setCellValue('F1', 'Kemampuan Menanggapi');
        $sheet2->setCellValue('G1', 'Kemampuan Mengendalikan');
        $sheet2->setCellValue('H1', 'Skor AVG');

        $row1 = 2;
        foreach ($tn_score as $item) {
            if($item['score'] >= 3){
                $predicate = 'Sangat Baik';
            } elseif($item['score'] >= 2){
                $predicate = 'Baik';
            } else{
                $predicate = 'Kurang Baik';
            }

            if($item['vendor'] == NULL){
                $vd = 'PT Century Batteries Indonesia';
            } else{
                $vd = $item['vendor'];
            }

            $sheet1->setCellValue('A' . $row1, $item['nama']);
            $sheet1->setCellValue('B' . $row1, $item['trainer_since']);
            $sheet1->setCellValue('C' . $row1, $vd);
            $sheet1->setCellValue('D' . $row1, $item['clock']);
            $sheet1->setCellValue('E' . $row1, $item['judul_training']);
            $sheet1->setCellValue('F' . $row1, $item['score']);
            $sheet1->setCellValue('G' . $row1, $predicate);
            $row1++;
        }

        $row2 = 2;
        foreach ($finalArrayTrainer as $item) {
            $skor = ($item['pengetahuan']+$item['kemampuan']+$item['kemampuan_melibatkan']+$item['kemampuan_menanggapi']+$item['kemampuan_mengendalikan'])/5;
            $sheet2->setCellValue('A' . $row2, $item['nama']);
            $sheet2->setCellValue('B' . $row2, $item['judul_training']);
            $sheet2->setCellValue('C' . $row2, $item['pengetahuan']);
            $sheet2->setCellValue('D' . $row2, $item['kemampuan']);
            $sheet2->setCellValue('E' . $row2, $item['kemampuan_melibatkan']);
            $sheet2->setCellValue('F' . $row2, $item['kemampuan_menanggapi']);
            $sheet2->setCellValue('G' . $row2, $item['kemampuan_mengendalikan']);
            $sheet2->setCellValue('H' . $row2, $skor);
            $row2++;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His'). '-Trainer-Report.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function TrainerDetailScore(){
        $id_trainer = $this->request->getPost('id_trainer');
        $judul = $this->request->getPost('judul_training');
        
        $dataTrainer = $this->tna->getAllEvaluasiReaksi();
        $arrayTrainer = [];
        foreach($dataTrainer as $data){
            for ($i = 1; $i <= 5; $i++) {
                $instrukturKey = "instruktur_$i";
                if (isset($data[$instrukturKey])) {
                    $arrayTrainer[] = $data[$instrukturKey];
                }
            }
        }

        $id_trainers = array_unique($arrayTrainer);
        
        $trainerArray = [];
        $reportTrainer = [];
        foreach($id_trainers as $id){
            $trainer_report_score = $this->report->getReportTrainer($id);
            $nama_trainer = $this->trainer->getTrainerName($id);
            $reportTrainer = array_merge($reportTrainer, $trainer_report_score);
            $trainerArray = array_merge($trainerArray,$nama_trainer);
        }

        $tnaByJudul = [];
        foreach($reportTrainer as $item){
            $tna_by_judul = $this->tna->getTnaByJudul($item['judul_training']);
            $tnaByJudul = array_merge($tnaByJudul,$tna_by_judul);
        }

        $id_tna_by_judul = [];
        foreach($tnaByJudul as $item){
            $array_1[] = $item['id_tna'];
            $id_tna_by_judul = array_merge($id_tna_by_judul, $array_1);
        }

        $distinct_id_tna_judul = array_unique($id_tna_by_judul);
        $trainer_with_score = [];

        foreach($distinct_id_tna_judul as $item){
            $data_evaluasi_reaksi = $this->evaluasiReaksi->getDataEvaluasiReaksiById($item);
            $trainer_with_score = array_merge($trainer_with_score,$data_evaluasi_reaksi);
        }

        // dd($trainer_with_score);

        $instruktur1 = [];
        $instruktur2 = [];
        $instruktur3 = [];
        $instruktur4 = [];
        $instruktur5 = [];

        foreach ($trainer_with_score as $item) {
            //INSTRUKTUR 1
            $id_tna = $item['id_tna'];
            $id_instruktur1 = $item['instruktur_1'];
            $pengetahuan1 = $item["pengetahuan1"];
            $kemampuan1 = $item["kemampuan1"];
            $kemampuan_melibatkan1 = $item["kemampuan_melibatkan1"];
            $kemampuan_menanggapi1 = $item["kemampuan_menanggapi1"];
            $kemampuan_mengendalikan1 = $item["kemampuan_mengendalikan1"];
            $judul_training = $item["training"];

            //INSTRUKTUR 2
            $id_tna = $item['id_tna'];
            $id_instruktur2 = $item['instruktur_2'];
            $pengetahuan2 = $item["pengetahuan2"];
            $kemampuan2 = $item["kemampuan2"];
            $kemampuan_melibatkan2 = $item["kemampuan_melibatkan2"];
            $kemampuan_menanggapi2 = $item["kemampuan_menanggapi2"];
            $kemampuan_mengendalikan2 = $item["kemampuan_mengendalikan2"];
            $judul_training = $item["training"];

            // INSTRUKTUR 3
            $id_tna = $item['id_tna'];
            $id_instruktur3 = $item['instruktur_3'];
            $pengetahuan3 = $item["pengetahuan3"];
            $kemampuan3 = $item["kemampuan3"];
            $kemampuan_melibatkan3 = $item["kemampuan_melibatkan3"];
            $kemampuan_menanggapi3 = $item["kemampuan_menanggapi3"];
            $kemampuan_mengendalikan3 = $item["kemampuan_mengendalikan3"];
            $judul_training = $item["training"];

            // INSTRUKTUR 4
            $id_tna = $item['id_tna'];
            $id_instruktur4 = $item['instruktur_4'];
            $pengetahuan4 = $item["pengetahuan4"];
            $kemampuan4 = $item["kemampuan4"];
            $kemampuan_melibatkan4 = $item["kemampuan_melibatkan4"];
            $kemampuan_menanggapi4 = $item["kemampuan_menanggapi4"];
            $kemampuan_mengendalikan4 = $item["kemampuan_mengendalikan4"];
            $judul_training = $item["training"];

            // INSTRUKTUR 4
            $id_tna = $item['id_tna'];
            $id_instruktur5 = $item['instruktur_5'];
            $pengetahuan5 = $item["pengetahuan5"];
            $kemampuan5 = $item["kemampuan5"];
            $kemampuan_melibatkan5 = $item["kemampuan_melibatkan5"];
            $kemampuan_menanggapi5 = $item["kemampuan_menanggapi5"];
            $kemampuan_mengendalikan5 = $item["kemampuan_mengendalikan5"];
            $judul_training = $item["training"];

            
            $instruktur1[] = [
                "id_tna" => $id_tna,
                "id_instruktur" => $id_instruktur1,
                "pengetahuan" => $pengetahuan1,
                "kemampuan" => $kemampuan1,
                "kemampuan_melibatkan" => $kemampuan_melibatkan1,
                "kemampuan_menanggapi" => $kemampuan_menanggapi1,
                "kemampuan_mengendalikan" => $kemampuan_mengendalikan1,
                "judul_training" => $judul_training
            ];

            
            if ($id_instruktur2 !== null) {
                $instruktur2[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur2,
                    "pengetahuan" => $pengetahuan2,
                    "kemampuan" => $kemampuan2,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan2,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi2,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan2,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur3 !== null) {
                $instruktur3[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur3,
                    "pengetahuan" => $pengetahuan3,
                    "kemampuan" => $kemampuan3,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan3,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi3,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan3,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur4 !== null) {
                $instruktur4[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur4,
                    "pengetahuan" => $pengetahuan4,
                    "kemampuan" => $kemampuan4,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan4,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi4,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan4,
                    "judul_training" => $judul_training
                ];
            }
            
            if ($id_instruktur5 !== null) {
                $instruktur5[] = [
                    "id_tna" => $id_tna,
                    "id_instruktur" => $id_instruktur5,
                    "pengetahuan" => $pengetahuan5,
                    "kemampuan" => $kemampuan5,
                    "kemampuan_melibatkan" => $kemampuan_melibatkan5,
                    "kemampuan_menanggapi" => $kemampuan_menanggapi5,
                    "kemampuan_mengendalikan" => $kemampuan_mengendalikan5,
                    "judul_training" => $judul_training
                ];
            }
        }

        $instruktur_with_detail_score = array_merge($instruktur1,$instruktur2,$instruktur3,$instruktur4,$instruktur5);


        $data = array_filter($instruktur_with_detail_score, function ($item) use ($id_trainer, $judul) {
            return $item['id_instruktur'] == $id_trainer && $item['judul_training'] == $judul ;
        });

        // var_dump($data);

        return $this->response->setJSON($data);

        // dd($instruktur_with_detail_score);

    }

    public function DeleteTrainingReject()
    {
        $id = $this->request->getVar('id');
        $this->tna->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Di Hapus');
        return redirect()->to('/training_ditolak');
    }


    public function TrainingNotImplemented()
    {
        $training = $this->tna->getTrainingNotImplemented();
        //dd($training);

        $data = [
            'tittle' => 'Training Not Implemented',
            'training' => $training
        ];
        return view('admin/trainingnotimplemented', $data);
    }

    public function ViewAttachment(){
        $id_tna = $this->request->getPost('id_tna');
        $data = $this->akomodasi->getAkomodasiByTna($id_tna);
        return $this->response->setJSON($data);
    }

    public function FormUnplannedAdmin(){
        $training = $this->training->getAll();
        $userData = $this->user->getAllUser();

        $data = [
            'tittle' => 'Form Unplanned (Admin)',
            'training'=> $training,
            'validation' => \Config\Services::validation(),
        ];
        return view('admin/formunplannedadmin',$data);
    }

    public function FormCrossBudget(){
        $training = $this->training->getAll();
        $budget = $this->budget->getCrossBudget();
        $userData = $this->user->getAllUser();

        $data = [
            'tittle' => 'Form Cross Budget',
            'training'=> $training,
            'validation' => \Config\Services::validation(),
            'budget' => $budget,
        ];
        return view('admin/formcrossbudget',$data);
    }

    public function FormAmdiAop(){
        $training = $this->training->getAll();
        $budget = $this->budget->getAmdiAOPBudget();
        $userData = $this->user->getAllUser();

        $data = [
            'tittle' => 'Form AMDI AOP',
            'training'=> $training,
            'validation' => \Config\Services::validation(),
            'budget' => $budget,
        ];
        return view('admin/formamdiaop',$data);
    }

    public function getUserBySearch(){
        $user = $this->user->getAllNamaUser();
        $searchTerm = $this->request->getGet('q');
        
        $result = array_filter($user, function($item) use ($searchTerm) {
            if (is_object($item)) {
                if (property_exists($item, 'nama') && is_string($item->nama)) {
                    return stripos($item->nama, $searchTerm) !== false;
                }
            }
            return false;
        });
        
        $results = array_slice($result,0,10);
        echo json_encode($results);
    }

    public function getCompetencyByIdUser(){
        $id = $this->request->getPost('id_user');
        $user = $this->user->getAllUser($id);
        // var_dump($user).die();
        if (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'REGULAR') {
            //Filter Astra Competency
            $datas  = $this->competencyAstra->getProfileAstraCompetency($id,$user['npk']);
            $astra = [];
            if (!empty($datas)) {
                foreach ($datas as $data) {
                    if ($data['score_astra'] < $data['proficiency']) {
                        $competency = [
                            'id' => $data['id_competency_astra'],
                            'category' => "ALC - " . $data['astra'],
                            'competency' => $data['astra'],
                            'proficiency' => $data['proficiency'],
                            'score' => $data['score_astra'],
                            'keterangan' => 'Astra'
                        ];
                        array_push($astra, $competency);
                    }
                }
            } else {
                $astra = [];
            }

            //Filter Tecnhnical Competency
            $datas2  = $this->competencyTechnical->getProfileTechnicalCompetency($id,$user['npk']);
            $technical = [];
            //dd($datas);
            if (!empty($datas2)) {
                foreach ($datas2 as $dataTech) {
                    if ($dataTech['score_technical'] < $dataTech['proficiency']) {
                        $competencyTech = [
                            'id' => $dataTech['id_competency_technical'],
                            'category' => "Technical Comp - " . $dataTech['technical'],
                            'competency' => $dataTech['technical'],
                            'proficiency' => $dataTech['proficiency'],
                            'score' => $dataTech['score_technical'],
                            'keterangan' => 'TechnicalA'
                        ];
                        array_push($technical, $competencyTech);
                    }
                }
            } else {
                $technical = [];
            }
            $data = array_merge($astra, $technical);
            echo json_encode($data);

        } elseif (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'EXPERT') {
            
            //Filter Expert Competency
            $dataExpert  = $this->competencyExpert->getProfileExpertCompetency($id,$user['npk']);
            $Expert = [];
            if (!empty($dataExpert)) {

                foreach ($dataExpert as $DataExpert) {
                    if ($DataExpert['score_expert'] < $DataExpert['proficiency']) {
                        $competency = [
                            'id' => $DataExpert['id_competency_expert'],
                            'category' => "Exp - " . $DataExpert['expert'],
                            'competency' => $DataExpert['expert'],
                            'proficiency' => $DataExpert['proficiency'],
                            'score' => $DataExpert['score_expert'],
                            'keterangan' => 'Expert'
                        ];
                        array_push($Expert, $competency);
                    }
                }
            } else {
                $Expert = [];
            }

            //Filter Tecnhnical Competency
            $datas2  = $this->competencyTechnical->getProfileTechnicalCompetency($id,$user['npk']);
            $technical = [];
            //dd($datas);
            if (!empty($datas2)) {

                foreach ($datas2 as $dataTech) {
                    if ($dataTech['score_technical'] < $dataTech['proficiency']) {
                        $competencyTech = [
                            'id' => $dataTech['id_competency_technical'],
                            'category' => "Technical Comp - " . $dataTech['technical'],
                            'competency' => $dataTech['technical'],
                            'proficiency' => $dataTech['proficiency'],
                            'score' => $dataTech['score_technical'],
                            'keterangan' => 'TechnicalA'
                        ];
                        array_push($technical, $competencyTech);
                    }
                }
            } else {
                $technical = [];
            }

            $data = array_merge($Expert, $technical);
            echo json_encode($data);
        } else {
            $dataCompany  = $this->competencyCompany->getProfileCompanyCompetency($id,$user['npk']);
            $Company = [];
            if (!empty($dataCompany)) {

                foreach ($dataCompany as $DataCompany) {
                    if ($DataCompany['score_company'] < $DataCompany['proficiency']) {
                        $competency = [
                            'id' => $DataCompany['id_competency_company'],
                            'category' => "Comp - " . $DataCompany['company'],
                            'competency' => $DataCompany['company'],
                            'proficiency' => $DataCompany['proficiency'],
                            'score' => $DataCompany['score_company'],
                            'keterangan' => 'Company'
                        ];
                        array_push($Company, $competency);
                    }
                }
            } else {
                $Company = [];
            }

            $dataSoft  = $this->competencySoft->getProfileSoftCompetency($id,$user['npk']);
            $Soft = [];
            if (!empty($dataSoft)) {
                foreach ($dataSoft as $DataSoft) {
                    if ($DataSoft['score_soft'] < $DataSoft['proficiency']) {
                        $competency = [
                            'id' => $DataSoft['id_competency_soft'],
                            'category' => "Soft - " . $DataSoft['soft'],
                            'competency' => $DataSoft['soft'],
                            'proficiency' => $DataSoft['proficiency'],
                            'score' => $DataSoft['score_soft'],
                            'keterangan' => 'Soft'
                        ];
                        array_push($Soft, $competency);
                    }
                }
            } else {
                $Soft = [];
            }

            $dataTechnicalB  = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id,$user['npk']);
            $TechnicalB = [];

            if (!empty($dataTechnicalB)) {
                foreach ($dataTechnicalB as $dataTechnicalB) {
                    if ($dataTechnicalB['score'] < $dataTechnicalB['proficiency']) {
                        $competency = [
                            'id' => $dataTechnicalB['id_competency_technicalB'],
                            'category' => "TechB - " . $dataTechnicalB['technicalB'],
                            'competency' => $dataTechnicalB['technicalB'],
                            'proficiency' => $dataTechnicalB['proficiency'],
                            'score' => $dataTechnicalB['score'],
                            'keterangan' => 'TechnicalB'
                        ];
                        array_push($TechnicalB, $competency);
                    }
                }
            } else {
                $TechnicalB = [];
            }
            $data = array_merge($Soft, $Company, $TechnicalB);
            echo json_encode($data);
        }
    }

    public function getCompetencyBySearch(){

        $searchTerm = $this->request->getGet('q');
        $astra = [];
        $data_astra = $this->astra->getDataAstra();
        foreach($data_astra as $data){
            $AstraComp = [
                'id' => $data['id_astra'],
                'category' => "ALC - " . $data['astra'],
                'competency' => $data['astra'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Astra'
            ];
            array_push($astra, $AstraComp);
        }

        //TECHNICAL A
        $technical = [];
        $data_technical = $this->technical->getDataTechnical();
        foreach($data_technical as $data){
            $TechnicalComp = [
                'id' => $data['id_technical'],
                'category' => "Technical Comp - " . $data['technical'],
                'competency' => $data['technical'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'TechnicalA'
            ];
            array_push($technical, $TechnicalComp);
        }

        //TECHNICAL B
        $technicalb = [];
        $data_technicalb = $this->technicalb->getDataTechnical();
        foreach($data_technicalb as $data){
            $TechnicalbComp = [
                'id' => $data['id_technicalB'],
                'category' => "TechB - " . $data['technicalB'],
                'competency' => $data['technicalB'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'TechnicalB'
            ];
            array_push($technicalb, $TechnicalbComp);
        }

        //COMPANY
        $company = [];
        $data_company = $this->company->getDataCompany();
        foreach($data_company as $data){
            $CompanyComp = [
                'id' => $data['id_company'],
                'category' => "Comp - " . $data['company'],
                'competency' => $data['company'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Company'
            ];
            array_push($company, $CompanyComp);
        }

        //SOFT
        $soft = [];
        $data_soft = $this->soft->getDataSoft();
        foreach($data_soft as $data){
            $SoftComp = [
                'id' => $data['id_soft'],
                'category' => "Soft - " . $data['soft'],
                'competency' => $data['soft'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Soft'
            ];
            array_push($soft, $SoftComp);
        }

        //Expert
        $expert = [];
        $data_expert = $this->expert->getDataExpert();
        foreach($data_expert as $data){
            $ExpertComp = [
                'id' => $data['id_expert'],
                'category' => "Exp - " . $data['expert'],
                'competency' => $data['expert'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Expert'
            ];
            array_push($expert, $ExpertComp);
        }

        $target = array_merge($astra,$technical,$technicalb,$company,$soft,$expert);

        $result = array_filter($target, function($item) use ($searchTerm) {
            if (is_array($item)) {
                if (array_key_exists('category', $item) && is_string($item['category'])) {
                    return stripos($item['category'], $searchTerm) !== false;
                }
            }
            return false;
        });

        $results = array_slice($result,0,10);
        echo json_encode($results);
    }

    public function getCompetencyById($id_competency,$keterangan){
        $astra = [];
        $data_astra = $this->astra->getDataAstra();
        foreach($data_astra as $data){
            $AstraComp = [
                'id' => $data['id_astra'],
                'category' => "ALC - " . $data['astra'],
                'competency' => $data['astra'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Astra'
            ];
            array_push($astra, $AstraComp);
        }

        //TECHNICAL A
        $technical = [];
        $data_technical = $this->technical->getDataTechnical();
        foreach($data_technical as $data){
            $TechnicalComp = [
                'id' => $data['id_technical'],
                'category' => "Technical Comp - " . $data['technical'],
                'competency' => $data['technical'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'TechnicalA'
            ];
            array_push($technical, $TechnicalComp);
        }

        //TECHNICAL B
        $technicalb = [];
        $data_technicalb = $this->technicalb->getDataTechnical();
        foreach($data_technicalb as $data){
            $TechnicalbComp = [
                'id' => $data['id_technicalB'],
                'category' => "TechB - " . $data['technicalB']." - ". $data['department']." - ".$data['nama_jabatan'],
                'competency' => $data['technicalB'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'TechnicalB'
            ];
            array_push($technicalb, $TechnicalbComp);
        }

        //COMPANY
        $company = [];
        $data_company = $this->company->getDataCompany();
        foreach($data_company as $data){
            $CompanyComp = [
                'id' => $data['id_company'],
                'category' => "Comp - " . $data['company']." - ". $data['divisi'],
                'competency' => $data['company'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Company'
            ];
            array_push($company, $CompanyComp);
        }

        //SOFT
        $soft = [];
        $data_soft = $this->soft->getDataSoft();
        foreach($data_soft as $data){
            $SoftComp = [
                'id' => $data['id_soft'],
                'category' => "Soft - " . $data['soft'],
                'competency' => $data['soft'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Soft'
            ];
            array_push($soft, $SoftComp);
        }

        //Expert
        $expert = [];
        $data_expert = $this->expert->getDataExpert();
        foreach($data_expert as $data){
            $ExpertComp = [
                'id' => $data['id_expert'],
                'category' => "Exp - " . $data['expert'],
                'competency' => $data['expert'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Expert'
            ];
            array_push($expert, $ExpertComp);
        }

        $target = array_merge($astra,$technical,$technicalb,$company,$soft,$expert);

        $filteredArray = array_filter($target, function ($item) use ($id_competency, $keterangan) {
            return $item['id'] == $id_competency && $item['keterangan'] == $keterangan;
        });

        return $filteredArray;
    }

    public function UnplannedAdminFormSave(){
        $id_training = $this->request->getPost('training');
        $id_user = $this->request->getPost('id_user');
        $jenis_training = $this->request->getPost('jenis_training');
        $kategori_training = $this->request->getPost('kategori_training');
        $metode_training = $this->request->getPost('metode_training');
        $mulai_training = $this->request->getPost('mulai_training');
        $rencana_training = $this->request->getPost('rencana_training');
        $vendor = $this->request->getPost('vendor');
        $tempat = $this->request->getPost('tempat');
        $tujuan_training = $this->request->getPost('tujuan_training');
        $notes = $this->request->getPost('notes');
        $biaya = $this->training->getIdTraining($id_training);
        $biaya_actual = $this->request->getPost('biaya_actual');
        $biaya_actual = str_replace(["Rp.", "."," "], "", $biaya_actual);
        $nama = $this->user->getNamaById($id_user);
        $user = $this->user->getAllUser($id_user);
        $training = $this->training->getJudulById($id_training);
        $dateTime = new \CodeIgniter\I18n\Time($mulai_training);
        $request_training = $dateTime->format('M-Y');
        $kelompok_training = $this->request->getPost('kelompok_training');
        $kompetensi = $this->request->getPost('kompetensi');
        $arrayKompetensi = explode(",", $this->request->getPost('kompetensi'));
        $id_kompetensi = $arrayKompetensi[0];
        $keterangan_kompetensi = $arrayKompetensi[1];

        $budget = $this->budget->getIdBudgetByDepartmentAndYear($user['departemen'], date('Y'));
        $id_budget = $budget['id_budget'];

            //TNA
            $data_tna = [
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian'],
                'id_training' => $id_training,
                'id_budget' => $id_budget,
                'nama' => $nama['nama'],
                'jenis_training' => $jenis_training,
                'kategori_training' => $kategori_training,
                'training' => $training,
                'vendor' => $vendor,
                'tempat' => $tempat,
                'metode_training' => $metode_training,
                'request_training' => $request_training,
                'mulai_training' => $mulai_training,
                'rencana_training' => $rencana_training,
                'tujuan_training' => $tujuan_training,
                'notes' => $notes,
                'biaya' => $biaya['biaya'],
                'biaya_actual' => $biaya_actual,
                'status' => 'accept',
                'kelompok_training' => 'unplanned'
            ];
            $this->tna->save($data_tna);

            //BUDGET
            $budgetData = $this->budget->getBudgetById($id_budget);
            $data_budget = [
                'id_budget' => $id_budget,
                'temporary_calculation' => $budgetData['temporary_calculation'] + $biaya_actual
            ];

            //APPROVAL 
            $id_tna = $this->tna->getIdTna();
            $data_approval = [
                'id_tna' => $id_tna->id_tna,
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian']
            ];

            //NILAI
            $data_nilai = [
                'id_tna' => $id_tna->id_tna,
                'id_competency1' => $id_kompetensi,
                'type_competency1' => $keterangan_kompetensi
            ];

            //HISTORY
            $data_history = [
                'id_tna' => $id_tna->id_tna
            ];

            //REAKSI
            $data_reaksi = [
                'id_tna' => $id_tna->id_tna
            ];

            //EFEKTIFITAS
            $data_efektifitas = [
                'id_tna' => $id_tna->id_tna,
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian']
            ];

            $this->budget->save($data_budget);
            $this->nilai->save($data_nilai);
            $this->approval->save($data_approval);
            $this->evaluasiReaksi->save($data_reaksi);
            $this->history->save($data_history);
            $this->efektifitas->save($data_efektifitas);

            // $data_tna = [
            //     'id_user' => $id_user,
            //     'id_training' => $id_training,
            //     'id_budget' => $id_budget,
            //     'nama' => $nama['nama'],
            //     'jenis_training' => $jenis_training,
            //     'kategori_training' => $kategori_training,
            //     'training' => $training,
            //     'vendor' => $vendor,
            //     'tempat' => $tempat,
            //     'metode_training' => $metode_training,
            //     'request_training' => $request_training,
            //     'mulai_training' => $mulai_training,
            //     'rencana_training' => $rencana_training,
            //     'tujuan_training' => $tujuan_training,
            //     'notes' => $notes,
            //     'biaya' => $biaya['biaya'],
            //     'biaya_actual' => $biaya_actual,
            //     'status' => 'accept',
            //     'kelompok_training' => $kelompok_training 
            // ];
            // // $this->tna->save($data_tna);

            // //BUDGET
            // $budgetData = $this->budget->getBudgetById($id_budget);
            // $data_budget = [
            //     'id_budget' => $id_budget,
            //     'available_budget' => $budgetData['available_budget'] - $biaya_actual,
            //     'used_budget' => $budgetData['used_budget'] + $biaya_actual
            // ];

            // //APPROVAL 
            // $id_tna = $this->tna->getIdTna();
            // $data_approval = [
            //     'id_tna' => $id_tna->id_tna,
            //     'id_user' => $id_user,
            //     'status_approval_0' => 'accept',
            //     'status_approval_1' => 'accept',
            //     'status_approval_2' => 'accept',
            //     'status_approval_3' => 'accept',
            // ];

            // //HISTORY
            // $data_history = [
            //     'id_tna' => $id_tna->id_tna
            // ];

            // $this->history->save($data_history);
            // $this->approval->save($data_approval);
            // $this->budget->save($data_budget);
    }

    public function CrossBudgetFormSave(){
        $id_training = $this->request->getPost('training');
        $id_user = $this->request->getPost('id_user');
        $jenis_training = $this->request->getPost('jenis_training');
        $kategori_training = $this->request->getPost('kategori_training');
        $metode_training = $this->request->getPost('metode_training');
        $mulai_training = $this->request->getPost('mulai_training');
        $rencana_training = $this->request->getPost('rencana_training');
        $vendor = $this->request->getPost('vendor');
        $tempat = $this->request->getPost('tempat');
        $tujuan_training = $this->request->getPost('tujuan_training');
        $id_budget = $this->request->getPost('id_budget');
        $notes = $this->request->getPost('notes');
        $biaya = $this->training->getIdTraining($id_training);
        $biaya_actual = $this->request->getPost('biaya_actual');
        $biaya_actual = str_replace(["Rp.", "."," "], "", $biaya_actual);
        $nama = $this->user->getNamaById($id_user);
        $user = $this->user->getAllUser($id_user);
        $training = $this->training->getJudulById($id_training);
        $dateTime = new \CodeIgniter\I18n\Time($mulai_training);
        $request_training = $dateTime->format('M-Y');
        $kelompok_training = $this->request->getPost('kelompok_training');
        $kompetensi = $this->request->getPost('kompetensi');
        $arrayKompetensi = explode(",", $this->request->getPost('kompetensi'));
        $id_kompetensi = $arrayKompetensi[0];
        $keterangan_kompetensi = $arrayKompetensi[1];

            //TNA
            $data_tna = [
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian'],
                'id_training' => $id_training,
                'id_budget' => $id_budget,
                'nama' => $nama['nama'],
                'jenis_training' => $jenis_training,
                'kategori_training' => $kategori_training,
                'training' => $training,
                'vendor' => $vendor,
                'tempat' => $tempat,
                'metode_training' => $metode_training,
                'request_training' => $request_training,
                'mulai_training' => $mulai_training,
                'rencana_training' => $rencana_training,
                'tujuan_training' => $tujuan_training,
                'notes' => $notes,
                'biaya' => $biaya['biaya'],
                'biaya_actual' => $biaya_actual,
                'status' => 'accept',
                'kelompok_training' => 'cross_budget'
            ];
            $this->tna->save($data_tna);

            //BUDGET
            $budgetData = $this->budget->getBudgetById($id_budget);
            $data_budget = [
                'id_budget' => $id_budget,
                'temporary_calculation' => $budgetData['temporary_calculation'] + $biaya_actual
            ];

            //APPROVAL 
            $id_tna = $this->tna->getIdTna();
            $data_approval = [
                'id_tna' => $id_tna->id_tna,
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian']
            ];

            //NILAI
            $data_nilai = [
                'id_tna' => $id_tna->id_tna,
                'id_competency1' => $id_kompetensi,
                'type_competency1' => $keterangan_kompetensi
            ];

            //HISTORY
            $data_history = [
                'id_tna' => $id_tna->id_tna
            ];

            //REAKSI
            $data_reaksi = [
                'id_tna' => $id_tna->id_tna
            ];

            //EFEKTIFITAS
            $data_efektifitas = [
                'id_tna' => $id_tna->id_tna,
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian']
            ];

            $this->budget->save($data_budget);
            $this->nilai->save($data_nilai);
            $this->approval->save($data_approval);
            $this->evaluasiReaksi->save($data_reaksi);
            $this->history->save($data_history);
            $this->efektifitas->save($data_efektifitas);
    }

    public function AmdiAopFormSave(){
        $id_training = $this->request->getPost('training');
        $id_user = $this->request->getPost('id_user');
        $jenis_training = $this->request->getPost('jenis_training');
        $kategori_training = $this->request->getPost('kategori_training');
        $metode_training = $this->request->getPost('metode_training');
        $mulai_training = $this->request->getPost('mulai_training');
        $rencana_training = $this->request->getPost('rencana_training');
        $vendor = $this->request->getPost('vendor');
        $tempat = $this->request->getPost('tempat');
        $tujuan_training = $this->request->getPost('tujuan_training');
        $id_budget = $this->request->getPost('id_budget');
        $notes = $this->request->getPost('notes');
        $biaya = $this->training->getIdTraining($id_training);
        $biaya_actual = intval(preg_replace("/[^0-9]/", "", $this->request->getPost('biaya_actual')));
        $nama = $this->user->getNamaById($id_user);
        $user = $this->user->getAllUser($id_user);
        $training = $this->training->getJudulById($id_training);
        $dateTime = new \CodeIgniter\I18n\Time($mulai_training);
        $request_training = $dateTime->format('M-Y');
        $used_budget = $this->request->getPost('used_budget');

            //TNA
            $data_tna = [
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian'],
                'id_training' => $id_training,
                'id_budget' => $id_budget,
                'nama' => $nama['nama'],
                'jenis_training' => $jenis_training,
                'kategori_training' => $kategori_training,
                'training' => $training,
                'vendor' => $vendor,
                'tempat' => $tempat,
                'metode_training' => $metode_training,
                'request_training' => $request_training,
                'mulai_training' => $mulai_training,
                'rencana_training' => $rencana_training,
                'tujuan_training' => $tujuan_training,
                'notes' => $notes,
                'biaya' => $biaya['biaya'],
                'biaya_actual' => $biaya_actual,
                'status' => 'accept',
                'kelompok_training' => 'amdi_aop'
            ];
            $this->tna->save($data_tna);

            //BUDGET
            $budgetData = $this->budget->getBudgetById($id_budget);
            $data_budget = [
                'id_budget' => $id_budget,
                'available_budget' => $budgetData['available_budget'] - $biaya_actual,
                'used_budget' => $budgetData['used_budget'] + $biaya_actual
            ];

            //APPROVAL 
            $id_tna = $this->tna->getIdTna();
            $data_approval = [
                'id_tna' => $id_tna->id_tna,
                'id_user' => $id_user,
                'npk' => $user['npk'],
                'bagian' => $user['bagian'],
                'status_approval_0' => 'accept',
                'status_approval_1' => 'accept',
                'status_approval_2' => 'accept'
            ];

            //HISTORY
            $data_history = [
                'id_tna' => $id_tna->id_tna
            ];

            $this->approval->save($data_approval);
            $this->history->save($data_history);
    }

    public function DataTraining(){
        $tna = $this->tna->getDataTna();
        $data = [
            'tna'=>$tna,
            'user'=>$this->user,
            'tittle'=>'Data Training'
        ];
        return view('admin/data_tna', $data);
    }

    public function DeleteDataTraining(){
        $id_tna = $this->request->getPost('id_tna');
        $data_tna = $this->tna->getDataTnaById($id_tna);
        $budget = $this->budget->getDataBudgetById($data_tna['id_budget']);

        if ($data_tna['id_budget'] === NULL && $data_tna['id_training'] === NULL && $data_tna['status'] === NULL){
            $this->tna->delete($id_tna);
            $this->history->where('id_tna', $id_tna)->delete();
            $this->approval->where('id_tna', $id_tna)->delete();
        }
        if($data_tna['status_approval_3'] == NULL && $data_tna['status'] == 'accept'){
            if($data_tna['status_approval_0'] != 'reject' || $data_tna['status_approval_1'] != 'reject' || $data_tna['status_approval_2'] != 'reject'){
                $data_budget = [
                    'id_budget'=>$data_tna['id_budget'],
                    'temporary_calculation' => $budget['temporary_calculation'] - $data_tna['biaya_actual']
                ];
                $this->budget->save($data_budget);
                $this->tna->delete($id_tna);
                $this->reaksi->delete($id_tna);
                $this->efektifitas->delete($id_tna);
                $this->nilai->delete($id_tna);
                $this->history->delete($id_tna);
                $this->approval->where('id_tna', $id_tna)->delete();
            }
        } else {
            $akomodasi = $this->akomodasi->getAkomodasiByTna($id_tna);
            $numericString = preg_replace("/[^0-9]/", "", $akomodasi['biaya_akomodasi']);
            $numericValue = (int)$numericString;

            $data_budget = [
                'id_budget'=> $data_tna['id_budget'],
                'used_budget' => $budget['used_budget'] - ($data_tna['biaya_actual'] + $numericValue),
                'available_budget' => $budget['available_budget'] + ($data_tna['biaya_actual'] + $numericValue)
            ];
            $this->budget->save($data_budget);
            $this->tna->delete($id_tna);
            $this->reaksi->delete($id_tna);
            $this->efektifitas->delete($id_tna);
            $this->nilai->delete($id_tna);
            $this->history->delete($id_tna);
            $this->approval->where('id_tna', $id_tna)->delete();
        }

        $akomodasi = $this->akomodasi->getAkomodasiByTna($id_tna);

        if(!empty($akomodasi)){
            $this->akomodasi->delete($id_tna);
        };

        echo json_encode($data_budget);
    }
}