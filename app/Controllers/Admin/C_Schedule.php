<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Approval;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\UserModel;

class C_Schedule extends BaseController
{
    private M_Tna $tna;
    private M_Approval $approval;

    private M_EvaluasiReaksi $reaksi;

    private M_TnaUnplanned $unplanned;
    private UserModel $user;

    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->approval = new M_Approval();
        $this->reaksi =  new M_EvaluasiReaksi();
        $this->unplanned = new M_TnaUnplanned();
        $this->user = new UserModel();
    }
    public function index()
    {
        $schedule  = $this->tna->getSchedule();
        $data = [
            'tittle' => 'Schedule Training',
            'schedule' => $schedule,
            'user' => $this->user
        ];
        return view('admin/schedule', $data);
    }

    public function otherSchedule()
    {
        $schedule  = $this->tna->getOtherSchedule();

        $data = [
            'tittle' => 'Other Schedule Training',
            'schedule' => $schedule,
            'user' => $this->user
        ];
        return view('admin/schedule', $data);
    }

    public function askForEvaluation($id)
    {
        $mulai_training = $this->request->getVar('mulai_training');
        $rencana_training = $this->request->getVar('rencana_training');
        $tittle = $this->request->getVar('tittle');

        $true  = $this->approval->getIdApproval($id);
        $Tna = $this->tna->getAllTna($id);
        $checked = $this->tna->checkedApproval($Tna[0]->mulai_training, $Tna[0]->id_training);
        
        if (!empty($checked)) {
            session()->setFlashdata('talk', 'There Are Still Trainings That Have Not Been Approved!');
            return redirect()->to('/schedule_training');
        }
        $data = [
            'id_approval' => $true['id_approval'],
            'status_training' => true
        ];

        $data2 = [
            'id_tna' => $id,
            'mulai_training' => $mulai_training,
            'rencana_training' => $rencana_training
        ];

        $this->approval->save($data);
        $this->tna->save($data2);

        if($tittle == 'Schedule Training'){            
            return redirect()->to('/schedule_training');
        } elseif($tittle == 'Other Schedule Training'){
            return redirect()->to('/schedule_other_training');
        }else{
            return redirect()->to('/schedule_unplanned');
        }

    }

    public function unplannedSchedule()
    {
        $schedule  = $this->unplanned->getSchedule();
        //dd($schedule);

        $data = [
            'tittle' => 'Schedule Unplanned Training',
            'schedule' => $schedule,
            'user' => $this->user
        ];
        return view('admin/schedule', $data);
    }

    public function askForEvaluationUnplanned($id)
    {
        $true  = $this->approval->getIdApproval($id);
        $Tna = $this->tna->getAllTna($id);
        $checked = $this->tna->checkedApproval($Tna[0]->mulai_training, $Tna[0]->id_training);
        //  dd($checked);
        if (!empty($checked)) {
            session()->setFlashdata('talk', 'There Are Still Trainings That Have Not Been Approved!');
            return redirect()->to('/schedule_unplanned');
        }
        $data = [
            'id_approval' => $true['id_approval'],
            'status_training' => true
        ];

        // dd($data);

        $this->approval->save($data);

        return redirect()->to('/schedule_unplanned');
    }

    public function NotImplemented()
    {
        $id = $this->request->getPost('id_tna');
        $title = $this->request->getPost('title');

        $reason = $this->request->getPost('alasan');
        $false  = $this->approval->getIdApproval($id);
        $status = $this->tna->getTrainingNotImplemented();
        $data = [
            'id_approval' => $false['id_approval'],
            'alasan' => $reason,
            'status_training' => false
        ];

        $this->approval->save($data);

        if($title == "Schedule Unplanned Training"){
            return redirect()->to('/schedule_unplanned');
        }else{
            return redirect()->to('/schedule_training');  
        }
    }
}