<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Akomodasi extends Model
{
    protected $table            = 'accommodation';
    protected $primaryKey       = 'id_akomodasi';
    protected $allowedFields    = ['id_tna','biaya_akomodasi','attachment'];

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];

    public function getAllAkomodasi(){
        $this->select();
        return $this->get()->getResultArray();
    }

    public function getAkomodasiByTna($id_tna){
        $this->select()->where('id_tna',$id_tna);
        $result = $this->get()->getRow();
        return $result ? (array) $result : []; 
    }

    public function getAttachmentByTna($id_tna){
        $this->select('attachment')->where('id_tna',$id_tna);
        return $this->get()->getResultArray();
    }
}
