<?= $this->extend('/template/template') ?>
<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title"><?= $tittle ?></h3>
    </div>
    <!-- /.card-header -->
    <?php $i = 0;
    foreach ($budgetData as $bd) : ?>
    <?php $sum = 0; ?>
    <div class="card-body table-responsive p-0">
        <table class="table table-hover" id="example">
            <thead class="bg-primary">
                <tr>
                    <th>Name</th>
                    <th>Training Group</th>
                    <th>Department</th>
                    <th>Training Category</th>
                    <th>Training</th>
                    <th>Vendor</th>
                    <th>Start Training</th>
                    <th>Finished Training</th>
                    <th>Planning Budget</th>
                    <th>Actual Budget</th>
                    <th>Acomodation Budget</th>
                    <th>Total Budget</th>
                    <th>Attachment</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody id="admin-verify">
                <?php
                    foreach ($status as $tnas) : ?>
                    <?php if($tnas['id_budget'] == $bd['id_budget']) :?>
                <tr>
                    <td><?= $tnas['nama'] ?></td>
                    <td>
                        <?php if($tnas['kelompok_training'] == 'cross_budget'){
                            echo "Cross Budget";
                        }else{
                            echo "AMDI AOP";
                        } ?>
                    </td>
                    <td><?= $tnas['departemen'] ?></td>
                    <td><?= $tnas['kategori_training'] ?></td>
                    <td><?= $tnas['training'] ?></td>
                    <?php if ($tnas['status_approval_3'] == null) : ?>
                    <?php $nextYear = date('Y')+1; ?>
                    <td><input style="width:130px;" class=" form-control" type="text" name="vendor<?= $i ?>"
                            id="vendor<?= $i ?>" value="<?= $tnas['vendor'] ?>" readonly></td>
                    <td><input class="form-control" type="date" value="<?= $tnas['mulai_training'] ?>"
                            name="mulai-training<?= $i ?>" id="mulai-training<?= $i ?>" min="<?= $nextYear ?>-01-01" max="<?= $nextYear ?>-12-31"></td>
                    <td><input class="form-control" type="date" value="<?= $tnas['rencana_training'] ?>"
                            name="rencana-training<?= $i ?>" id="rencana-training<?= $i ?>" min="<?= $nextYear ?>-01-01" max="<?= $nextYear ?>-12-31"></td>
                    <td>
                        <p style="width:100px;"><?= "Rp. " . number_format($tnas['biaya'], 0, ',', '.'); ?></p>
                    </td>
                    <td>
                        <div class="d-flex flex-row">
                            <input style="width:130px;" class="form-control" type="text" id="biaya<?= $i ?>"
                                name="biaya<?= $i ?>"
                                value="<?= "Rp. " . number_format($tnas['biaya_actual'], 0, ',', '.'); ?>"
                                onkeyup="rupiah('biaya<?= $i ?>')" oninput="handleInputFieldChange(this, <?= $i ?>)" data-biaya-actual="<?= $tnas['biaya_actual'] ?>">
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-row">
                            <input style="width:130px;" class="form-control" type="text" id="inputField<?= $i ?>"
                                name="inputField<?= $i ?>" placeholder="Rp. 0" value='<?= "Rp. " . number_format(0, 0, ',', '.'); ?>'
                                oninput="handleInputFieldChange(this, <?= $i ?>)">
                        </div>
                    </td>
                    <td>
                    <div class="d-flex flex-row">
                        <input style="width:130px;" class="form-control" type="text" id="total<?= $i ?>"
                            name="total<?= $i ?>" placeholder="Rp. 0" value='<?= "Rp. " . number_format($tnas['biaya_actual'], 0, ',', '.'); ?>' readonly>
                    </div>
                    </td>
                    <td>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="attachment<?= $i ?>" name="attachment<?= $i ?>" required>
                            <label class="custom-file-label" for="customFile">Upload</label>
                        </div>
                    </td>
                    <?php $budgets = $budget->getDataBudgetById($status[0]['id_budget']); ?>
                    <td>
                        <a id="acceptadmin<?= $i ?>" href="javascript:;" class="btn btn-success m-1"
                            style="color:white;width:100px;" data-acceptadmin="<?= $tnas['id_tna'] ?>"
                            data-biaya-actual="<?= $tnas['biaya_actual'] ?>" data-biaya-available="<?= $budgets['available_budget'] ?>"
                            onclick="AcceptAdmin(<?= $i ?>)"><i class=" fa fa-fw fa-check"></i>Accept</a>
                        <a id="reject-admin<?= $i ?>" href="javascript:;"
                            class="admin-verify btn btn-danger m-1" style="width:100px;"
                            data-reject-admin="<?= $tnas['id_tna']  ?>" onclick="verify_admin(<?= $i ?>)"><i
                                class="fa fa-fw fa-close"></i>Reject</a>
                        <input type="hidden" id="reject-admin-input<?= $i ?>" value="<?= $tnas['id_tna'] ?>">

                    </td>
                    <?php else : ?>
                    <?php $dataAkom = $akomodasi->getAkomodasiByTna($tnas['id_tna']); ?>
                    <td><?= $tnas['vendor'] ?></td>
                    <td><?= $tnas['mulai_training'] ?></td>
                    <td><?= $tnas['rencana_training'] ?></td>
                    <td>Rp<?= " " . number_format($tnas['biaya'], 0, ',', '.') ?></td>
                    <td>
                        <?php if(!empty($dataAkom)) : ?>
                            Rp. <?= " " . number_format(intval($tnas['biaya_actual'])-intval(preg_replace("/[^0-9]/", "", $dataAkom['biaya_akomodasi'])), 0, ',', '.') ?>
                        <?php else : ?>
                        Rp<?= " " . number_format($tnas['biaya_actual'], 0, ',', '.') ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(!empty($dataAkom)) : ?>
                            <?php echo $dataAkom['biaya_akomodasi']; ?>
                        <?php else : ?>
                            Rp. 0
                        <?php endif; ?>
                    </td>
                    <td>
                        Rp<?= " " . number_format($tnas['biaya_actual'], 0, ',', '.') ?>
                    </td>
                    <td>
                    <?php if(!empty($dataAkom)) : ?>
                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"></button> -->
                        <button type="button" id="akomodasiView" class="btn btn-primary" data-toggle="modal" data-target="#akomodasiView" onclick="showModal(<?= $dataAkom['id_tna'] ?>)" data-tna-attachment="<?= $dataAkom['attachment'] ?>">View</button>
                        <?php else : ?>
                            Tidak ada file
                        <?php endif; ?>
                    </td>
                    <td>
                    <h4 style="text-transform:uppercase;"><span class="badge badge-pill badge-success"><?= $tnas['status_approval_2'] ?></span></h4>
                    </td>
                    <?php endif; ?>
                    <?php $sum += $tnas['biaya_actual']; ?>
                    <?php endif; ?>
                </tr>

                <!-- MODAL VIEW PDF -->
                <div class="modal fade bd-example-modal-lg" id="akomodasiView" tabindex="-1" role="dialog" aria-labelledby="akomodasiViewTitle" aria-hidden="true">;
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="akomodasiViewLongTitle">Attachment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12" id="viewPdf">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END -->

                <div class=" modal fade" id="rejectAdmin<?= $i ?>" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Reject Reason</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <label for="alasan">Reason</label>
                                    <textarea id="alasan<?= $i ?>" class="mt-1" name="alasan<?= $i ?>"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a id="admin-reject<?= $i ?>" href="javascript:;" class="btn btn-danger"
                                    style="color:white;" onclick="Reject_Admin(<?= $i ?>) "><i
                                        class=" fa fa-fw fa-close"></i>Reject</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++;
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php if($tittle == 'Training Realization') : ?>
        <?php $year = date('Y')+1 ?>
    <?php else : ?>
        <?php $year = date('Y') ?>
    <?php endif; ?>
    <?php $budgets = $budget->getDataBudgetById($bd['id_budget']); ?>
    <div class="m-3 d-flex justify-content-around">
        <div><strong>Budget From : </strong>
            <?= $budgets['department']?> <?= $budgets['year'] ?>
        </div>
        <div><strong>Alocated Budget : </strong>
            <?= "Rp " . number_format($budgets['alocated_budget'], 0, ',', '.') ?>
        </div>
        <div><strong>Available
                Budget : </strong><?= "Rp " . number_format($budgets['available_budget'], 0, ',', '.') ?></div>
        <div><strong>Used Budget : </strong><?= "Rp " . number_format($budgets['used_budget'], 0, ',', '.') ?></div>
        <div><strong>Temporary Calculation:
            </strong><?= "Rp " . number_format($budgets['temporary_calculation'], 0, ',', '.') ?>
            <!-- PROBLEM -->
            <input type="text" style="display:none;" type="hidden" id='biaya_tersedia<?= $i ?>' data-biaya="<?= $budgets['available_budget'] ?>" value='<?php $budgets['available_budget'] ?>'>
        </div>

    </div>
    <?php endforeach; ?>
    <!-- /.card-body -->
</div>
<script>

function showModal(tnaId){
    var id_tna = tnaId
    console.log(id_tna)

    $.ajax({
        type: 'POST',
        url: '<?= base_url() ?>view_attachment',
        data: {
            id_tna: id_tna
        },
        success: function(response) {
            var data = response;
            console.log(data)
            var iframe = document.createElement('iframe');
            iframe.src = data.attachment;
            iframe.style.width = '100%';
            iframe.style.height = '700px';
            iframe.frameBorder = 0;
            iframe.allowfullscreen = true;

            $("#viewPdf").empty().append(iframe);
            }
    });
    
}

// function handleInputFieldChange(input, index) {
//     let value = input.value;
//     value = value.replace(/\D/g, '');
//     value = value.replace(/^0+/,'');

//     if (value === '') {
//         value = '0';
//     }

//     let biayaElement = document.getElementById('biaya' + index);
//     var biayaActual = biayaElement.getAttribute('data-biaya-actual');
//     let existingValue = biayaActual;
//     let biaya = existingValue.replace(/\D/g, '');

//     console.log(biayaElement)

    
//     let formattedValue = 'Rp. ' + formatRupiah(value);
    
//     input.value = formattedValue
//     biayaElement.value = 'Rp. ' + formatRupiah(parseInt(biaya)+parseInt(value));
//     console.log(value)
//     console.log(biaya)
// }
    
function handleInputFieldChange(input, index) {
    let value = input.value;
    value = value.replace(/\D/g, '');
    value = value.replace(/^0+/, '');

    if (value === '') {
        value = '0';
    }

    let biayaElement = document.getElementById('biaya' + index);
    let existingValue = biayaElement.value;
    let originalBiaya = biayaElement.getAttribute('data-biaya-actual');

    let newBiaya = parseInt(originalBiaya) + parseInt(value);

    let formattedValue = 'Rp. ' + formatRupiah(value);
    input.value = formattedValue;

    // biayaElement.value = 'Rp. ' + formatRupiah(newBiaya);

    updateTotal(index);
}

function updateTotal(index) {
    let biayaElement = document.getElementById('biaya' + index);
    let inputFieldElement = document.getElementById('inputField' + index);
    let totalElement = document.getElementById('total' + index);

    let biaya = biayaElement.value.replace(/\D/g, '');
    let inputField = inputFieldElement.value.replace(/\D/g, '');

    let total = parseInt(biaya) + parseInt(inputField);

    totalElement.value = 'Rp. ' + formatRupiah(total);
}

function formatRupiah(angka) {
    var reverse = angka.toString().split('').reverse().join('');
    var ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    return ribuan;
}


function rupiah(id) {

    var rupiah = document.getElementById(id);
    rupiah.addEventListener("keyup", function(e) {
    rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

}

function AcceptAdmin(i) {
    var id_tna = $('#acceptadmin' + i).attr('data-acceptadmin');
    var biaya2 = $('#acceptadmin' + i).attr('data-biaya-available');
    var biaya_actual = $('#biaya' + i).val()
    var biaya_total = $('#total' + i).val()
    var biaya_akomodasi = $('#inputField' +i).val()
    var rencana_training = $('#rencana-training' + i).val()
    var mulai_training = $('#mulai-training' + i).val()
    var vendor = $('#vendor' + i).val()
    var biaya1 = biaya_actual.replace(/\D/g, '');
    var total = biaya_total.replace(/\D/g, '');
    var akomodasi = biaya_akomodasi.replace(/\D/g, '');
    var fileInput = document.getElementById('attachment' + i);
    var attachment = fileInput.files[0];

    console.log(total)
    console.log(biaya_actual)
    console.log(akomodasi)
    console.log(biaya1)
    console.log(biaya2)

    if (biaya_actual.trim() === "") {
        Swal.fire({
            icon: 'error',
            title: 'Biaya Tidak Boleh Kosong',
            text: 'Silakan isi biaya aktual.'
        });
    } else if(rencana_training < mulai_training){
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: 'Rencana Training Finished berada pada tanggal sebelum Training Start',
        });
    }else {
        biaya1 = parseFloat(biaya1);
        biaya2 = parseFloat(biaya2);

        if (biaya1 > biaya2) {
            Swal.fire({
                icon: 'error',
                title: 'Budget Aktual Melebihi Budget yang Tersedia',
                text: 'Silakan ubah dan turunkan budget untuk melanjutkan.'
            });
        } 
        else if (biaya1 < biaya2) {
        if (!attachment) {
            Swal.fire({
                icon: 'error',
                title: 'File Attachment Kosong',
                text: 'Anda harus memilih file attachment.'
            });
            return;
        }

        if (attachment.size > 10000000) {
        Swal.fire({
            icon: 'error',
            title: 'File Terlalu Besar',
            text: 'Ukuran file attachment harus kurang dari 10 MB.'
        });
        return; 
        }

        if (!attachment.name.endsWith('.pdf')) {
            Swal.fire({
                icon: 'error',
                title: 'Format File Tidak Dukung',
                text: 'Hanya file PDF yang diperbolehkan.'
            });
            return;
        }

        var formData = new FormData();
            formData.append('id_tna', id_tna);
            formData.append('biaya_actual', total);
            formData.append('biaya', biaya1);
            formData.append('rencana_training', rencana_training);
            formData.append('mulai_training', mulai_training);
            formData.append('vendor', vendor);
            formData.append('akomodasi', biaya_akomodasi);
            formData.append('attachment', attachment);


        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>accept_adminfixed",
            async: true,
            processData: false,
            contentType: false,
            data: formData,  
            success: function(data) {
                Swal.fire({
                icon: 'success',
                title: 'Training Disetujui',
                text: 'Silahkan cek menu Schedule > Schedule Training.'
            }).then(function () {
                location.reload();
            });
            }

        })
        }
    }
}

// function AcceptAdmin(i) {
//     var id_tna = $('#acceptadmin' + i).attr('data-acceptadmin');
//     var biaya2 = $('#acceptadmin' + i).attr('data-biaya-available');
//     var biaya_actual = $('#biaya' + i).val()
//     var biaya_akomodasi = $('#inputField' +i).val()
//     var rencana_training = $('#rencana-training' + i).val()
//     var mulai_training = $('#mulai-training' + i).val()
//     var vendor = $('#vendor' + i).val()
//     var biaya1 = biaya_actual.replace(/\D/g, '');
//     var akomodasi = biaya_akomodasi.replace(/\D/g, '');
//     var fileInput = document.getElementById('attachment' + i);
//     var attachment = fileInput.files[0];

//     console.log(biaya_actual)
//     console.log(akomodasi)
//     console.log(biaya1)
//     console.log(biaya2)

//     if (biaya_actual.trim() === "") {
//         Swal.fire({
//             icon: 'error',
//             title: 'Biaya Tidak Boleh Kosong',
//             text: 'Silakan isi biaya aktual.'
//         });
//     } else if(rencana_training < mulai_training){
//         Swal.fire({
//             icon: 'error',
//             title: 'Gagal',
//             text: 'Rencana Training Finished berada pada tanggal sebelum Training Start',
//         });
//     }else {
//         biaya1 = parseFloat(biaya1);
//         biaya2 = parseFloat(biaya2);

//         if (biaya1 > biaya2) {
//             Swal.fire({
//                 icon: 'error',
//                 title: 'Budget Aktual Melebihi Budget yang Tersedia',
//                 text: 'Silakan ubah dan turunkan budget untuk melanjutkan.'
//             });
//         } else if (biaya1 < biaya2) {
//             Swal.fire({
//                 icon: 'success',
//                 title: 'Training Disetujui',
//                 text: 'Silahkan cek menu Schedule > Schedule Training.'
//         });


//         $.ajax({
//             type: 'post',
//             url: "<?= base_url() ?>accept_adminfixed",
//             async: true,
//             dataType: "json",
//             data: {
//                 id_tna: id_tna,
//                 biaya_actual: biaya_actual,
//                 rencana_training: rencana_training,
//                 mulai_training: mulai_training,
//                 vendor: vendor,
//                 akomodasi: akomodasi
                
//             },  
//             success: function(data) {
//                 window.location.reload()

//             }

//         })
//         }
//     }
// }


// $('#admin-verify').on('click', '.admin-verify', function() {
//     var id_tna = $(this).attr('data-reject-admin');
//     console.log(id_tna);
//     jQuery.noConflict()
//     $("#rejectAdmin").modal("show");
// })

function verify_admin(i) {
    var id_tna = $('#acceptadmin' + i).attr('data-acceptadmin');
    console.log(id_tna);
    jQuery.noConflict()
    $("#rejectAdmin" + i).modal("show");
}


function Reject_Admin(i) {
    var id_tna = $('#reject-admin-input' + i).val();
    let alasan = $('#alasan' + i).val()
    var biaya_actual = $('#biaya' + i).val()
    console.log(id_tna)
    if (alasan != '') {
        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>reject_adminfixed",
            async: true,
            dataType: "json",
            data: {
                id_tna: id_tna,
                alasan: alasan,
                biaya_actual: biaya_actual
            },
            success: function(data) {
                window.location.reload()

            }
        })
    }
}
</script>
<?= $this->endSection() ?>