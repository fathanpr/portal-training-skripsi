<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencySoft extends Model
{
    protected $table      = 'competency_profile_soft';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_soft';
    protected $allowedFields = ['id_user', 'npk', 'id_soft', 'score_soft', 'updated_at'];


    public function getIdSoft($id)
    {
        return $this->where(['id_soft' => $id])->first();
    }

    public function getProfileSoftCompetency($id_user, $npk)
    {

        $this->select('soft_competency.soft,soft_competency.proficiency,competency_profile_soft.id_competency_soft,competency_profile_soft.score_soft,competency_profile_soft.updated_at')->where('competency_profile_soft.id_user', $id_user)->orWhere('competency_profile_soft.npk', $npk);
        $this->join('soft_competency', 'soft_competency.id_soft = competency_profile_soft.id_soft');
        return $this->get()->getResultArray();
    }
    public function getSoftByIdCompetency($id)
    {

        $this->select('soft_competency.soft,soft_competency.proficiency,competency_profile_soft.id_competency_soft,competency_profile_soft.score_soft')->where('competency_profile_soft.id_competency_soft', $id);
        $this->join('soft_competency', 'soft_competency.id_soft = competency_profile_soft.id_soft');
        return $this->get()->getResultArray();
    }

    public function getSoftIdCompetency($id_user,$npk)
    {
        $this->select('competency_profile_soft.id_soft')->where('competency_profile_soft.id_user', $id_user)->orWhere('competency_profile_soft.npk', $npk);
        $this->join('soft_competency', 'soft_competency.id_soft = competency_profile_soft.id_soft');
        return $this->get()->getResultArray();
    }

    public function getSoftId($id_soft, $npk)
    {
        $this->select()->where('id_soft', $id_soft)->where('npk',$npk);
        return $this->get()->getResultArray();
    }

    public function getSoftCompetencySkillMap(){
        $this->select('soft,npk,score_soft,proficiency,updated_at')
            ->join('soft_competency', 'soft_competency.id_soft = competency_profile_soft.id_soft')
            ->where('npk IS NOT NULL')
            // ->where('competency_profile_soft.score_soft > 0')
            // ->where('competency_profile_soft.updated_at IS NOT NULL')
            ->orderBy('competency_profile_soft.npk', 'ASC');
        return $this->get()->getResultArray();
    }
}