<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Education extends Model
{
    protected $table      = 'education';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_education';
    protected $allowedFields = ['id_user', 'npk', 'grade', 'year', 'institution', 'major'];

    public function getIdEducation($npk)
    {
        return $this->where(['npk' => $npk])->first();
    }

    public function getDataEducation($npk)
    {
        $this->select()->where('npk', $npk);
        return $this->get()->getResultArray();
    }
}