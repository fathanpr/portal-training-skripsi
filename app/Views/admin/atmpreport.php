<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <div class="row d-flex justify-content-between">
            <div class="col">
                <h3 class="card-title">Annual Training Master Plan Report</h3>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="<?php base_url() ?>download_atmp_report" class="btn btn-success"><i class="fa-solid fa-file-arrow-down fa-xs"></i> Export</a>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <div class="row">
            <div class="col ml-3">
                <h2>ATMP Training</h2>
                <table class="table table-hover" id="tnaAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hour</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Title</th>
                            <th>Vendor</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th style="width:100px;">Actual Budget</th>
                            <th>Accomodation Budget</th>
                            <th style="width:100px;">Total Budget</th>
                            <!-- <th>Training Group</th> -->
                            <th>Status</th>
                        </tr>
                    </thead>
                    <?php foreach ($Atmp as $Atmps) : ?>
                    <?php if($Atmps['kelompok_training'] == 'training') :?>
                        <tr>
                        <td><?= $Atmps['nama'] ?></td>
                        <td><?= $Atmps['man_hour'] ?> Hours</td>
                        <td><?= $Atmps['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($Atmps['npk']) ?><?= $departemen->departemen ?></td>
                        <td><?= $Atmps['training'] ?></td>
                        <td><?= $Atmps['vendor'] ?></td>
                        <td><?= $Atmps['jenis_training'] ?></td>
                        <td><?= $Atmps['kategori_training'] ?></td>
                        <td><?= $Atmps['metode_training'] ?></td>
                        <td><?= $Atmps['tujuan_training'] ?></td>
                        <td><?= $Atmps['request_training'] ?></td>
                        <td><?= $Atmps['mulai_training'] ?></td>
                        <td><?= $Atmps['rencana_training'] ?></td>
                        <td>Rp. <?= number_format(intval($Atmps['biaya_actual'])-intval(preg_replace("/[^0-9]/", "", $Atmps['biaya_akomodasi'])), 0, ',', '.') ?></td>
                        <td><?= $Atmps['biaya_akomodasi'] ?></td>
                        <td>Rp. <?= number_format(intval($Atmps['biaya_actual']), 0, ',', '.') ?></td>
                        <!-- <td style="text-transform:uppercase;"><?= $Atmps['kelompok_training'] ?></td> -->
                        <td>
                            <?php if($Atmps['status_training'] == NULL) : ?>
                                <h4><span class="badge badge-secondary">Not Yet Implemented</span></h4>
                            <?php elseif($Atmps['status_training'] == '1') : ?>
                                <h4><span class="badge badge-success">Implemented</span></h4>
                            <?php elseif($Atmps['status_approval_0'] == 'reject' || $Atmps['status_approval_1'] == 'reject' || $Atmps['status_approval_2'] == 'reject' || $Atmps['status_approval_3'] == 'reject') : ?>
                                <h4><span class="badge badge-danger">Reject</span></h4>
                            <?php else : ?>
                                <h4><span class="badge badge-danger">Not Implemented</span></h4>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col ml-3">
                <h2>ATMP Unplanned</h2>
            <table class="table table-hover" id="unplannedAtmp">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Hour</th>
                    <th>NPK</th>
                    <th>Department</th>
                    <th>Training Title</th>
                    <th>Vendor</th>
                    <th>Training Type</th>
                    <th>Training Category</th>
                    <th>Training Method</th>
                    <th>Training Goals</th>
                    <th>Request Training</th>
                    <th>Training Start</th>
                    <th>Training Finished</th>
                    <th style="width:100px;">Actual Budget</th>
                    <th>Accomodation Budget</th>
                    <th style="width:100px;">Total Budget</th>
                    <!-- <th>Training Group</th> -->
                    <th>Status</th>
                </tr>
            </thead>
            <?php foreach ($Atmp as $Atmps) : ?>
            <?php if($Atmps['kelompok_training'] == 'unplanned') : ?>
                <tr>
                <td><?= $Atmps['nama'] ?></td>
                <td><?= $Atmps['man_hour'] ?> Hours</td>
                <td><?= $Atmps['npk'] ?></td>
                <td><?php $departemen = $user->getDepartemenByNpk($Atmps['npk']) ?><?= $departemen->departemen ?></td>
                <td><?= $Atmps['training'] ?></td>
                <td><?= $Atmps['vendor'] ?></td>
                <td><?= $Atmps['jenis_training'] ?></td>
                <td><?= $Atmps['kategori_training'] ?></td>
                <td><?= $Atmps['metode_training'] ?></td>
                <td><?= $Atmps['tujuan_training'] ?></td>
                <td><?= $Atmps['request_training'] ?></td>
                <td><?= $Atmps['mulai_training'] ?></td>
                <td><?= $Atmps['rencana_training'] ?></td>
                <td>Rp. <?= number_format(intval($Atmps['biaya_actual'])-intval(preg_replace("/[^0-9]/", "", $Atmps['biaya_akomodasi'])), 0, ',', '.') ?></td>
                <td><?= $Atmps['biaya_akomodasi'] ?></td>
                <td>Rp. <?= number_format(intval($Atmps['biaya_actual']), 0, ',', '.') ?></td>
                <!-- <td style="text-transform:uppercase;"><?= $Atmps['kelompok_training'] ?></td> -->
                <td>
                    <?php if($Atmps['status_training'] == NULL) : ?>
                        <h4><span class="badge badge-secondary">Not Yet Implemented</span></h4>
                    <?php elseif($Atmps['status_training'] == '1') : ?>
                        <h4><span class="badge badge-success">Implemented</span></h4>
                    <?php elseif($Atmps['status_approval_0'] == 'reject' || $Atmps['status_approval_1'] == 'reject' || $Atmps['status_approval_2'] == 'reject' || $Atmps['status_approval_3'] == 'reject') : ?>
                        <h4><span class="badge badge-danger">Reject</span></h4>
                    <?php else : ?>
                        <h4><span class="badge badge-danger">Not Implemented</span></h4>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
            <tbody>
            </tbody>
        </table>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col ml-3 table-responsive">
                <h2>ATMP Other Training</h2>
                <table class="table table-hover" id="otherAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hour</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Group</th>
                            <th>Training Title</th>
                            <th>Vendor</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th style="width:100px;">Actual Budget</th>
                            <th>Accomodation Budget</th>
                            <th style="width:100px;">Total Budget</th>
                            <!-- <th>Training Group</th> -->
                            <th>Status</th>
                        </tr>
                    </thead>
                    <?php foreach ($Atmp as $Atmps) : ?>
                    <?php if($Atmps['kelompok_training'] == 'amdi_aop' || $Atmps['kelompok_training'] == 'cross_budget') :?>
                        <tr>
                        <td><?= $Atmps['nama'] ?></td>
                        <td><?= $Atmps['man_hour'] ?> Hours</td>
                        <td><?= $Atmps['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($Atmps['npk']) ?><?= $departemen->departemen ?></td>
                        <td>
                            <?php if($Atmps['kelompok_training'] == 'amdi_aop') : ?>
                            <span class="badge badge-info">AMDI AOP</span>
                            <?php elseif($Atmps['kelompok_training'] == 'cross_budget') :  ?>
                            <span class="badge badge-success">CROSS BUDGET</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $Atmps['training'] ?></td>
                        <td><?= $Atmps['vendor'] ?></td>
                        <td><?= $Atmps['jenis_training'] ?></td>
                        <td><?= $Atmps['kategori_training'] ?></td>
                        <td><?= $Atmps['metode_training'] ?></td>
                        <td><?= $Atmps['tujuan_training'] ?></td>
                        <td><?= $Atmps['request_training'] ?></td>
                        <td><?= $Atmps['mulai_training'] ?></td>
                        <td><?= $Atmps['rencana_training'] ?></td>
                        <td>Rp. <?= number_format(intval($Atmps['biaya_actual'])-intval(preg_replace("/[^0-9]/", "", $Atmps['biaya_akomodasi'])), 0, ',', '.') ?></td>
                        <td><?= $Atmps['biaya_akomodasi'] ?></td>
                        <td>Rp. <?= number_format(intval($Atmps['biaya_actual']), 0, ',', '.') ?></td>
                        <!-- <td style="text-transform:uppercase;"><?= $Atmps['kelompok_training'] ?></td> -->
                        <td>
                            <?php if($Atmps['status_training'] == NULL) : ?>
                                <h4><span class="badge badge-secondary">Not Yet Implemented</span></h4>
                            <?php elseif($Atmps['status_training'] == '1') : ?>
                                <h4><span class="badge badge-success">Implemented</span></h4>
                            <?php elseif($Atmps['status_approval_0'] == 'reject' || $Atmps['status_approval_1'] == 'reject' || $Atmps['status_approval_2'] == 'reject' || $Atmps['status_approval_3'] == 'reject') : ?>
                                <h4><span class="badge badge-danger">Reject</span></h4>
                            <?php else : ?>
                                <h4><span class="badge badge-danger">Not Implemented</span></h4>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<?= $this->endSection() ?>