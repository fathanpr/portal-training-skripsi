<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>


<div id="accordion">
  <h4 class="p-3">Detail Request Sylabus</h4>
<?php $i = 1; ?>
<?php foreach($data as $item) :?>
  <div class="card ml-3 mr-3">
    <div class="card-header" id="heading<?= $i ?>">
      <h5 class="mb-0">
        <button class="btn btn-link" id="bacaButton<?= $i ?>" data-toggle="collapse" data-target="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
          REQUESTED BY : <?= ($item['departemen'] == 'AMDI AOP') ? $item['divisi'] : $item['departemen'] ?> / <?php $nama = $nama_user->getNamaById($item['id_user']); echo $nama['nama']  ?> / <?= $item['tanggal_request'] ?>
          <?php if(empty($item['status_baca'])) : ?>
            <span class="badge badge-warning badge-pill">!</span>
          <?php endif; ?>
        </button>
      </h5>
    </div>
    <div id="collapse<?= $i ?>" class="collapse" aria-labelledby="heading<?= $i ?>" data-parent="#accordion">
      <div class="card-body table-responsive">
        <table class="table table-hover">
        <thead>
            <tr class="bg-warning">
            <th scope="col">Aksi</th>
            <th scope="col">Judul</th>
            <th scope="col">Jenis</th>
            <th scope="col">Pelaksanaan</th>
            <th scope="col">Sasaran Peserta</th>
            <th scope="col">Sasaran Departemen</th>
            <th scope="col">Kompetensi</th>
            <th scope="col">Tujuan</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Metode</th>
            <th scope="col">Durasi</th>
            <th scope="col">Kelengkapan</th>
            <th scope="col">Evaluasi</th>
            <th scope="col">Notes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                <?php if(empty($item['status_baca'])) : ?>
                  <button type="button" id="baca<?= $i ?>" class="btn btn-success" onclick="bacaSilabus(<?= $i ?>,<?= $item['id_silabus'] ?>)">Baca</button>
                  <?php else: ?>
                  <h3><i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i></h3>
                  <?php endif; ?>
                </td>
                <td><?= $item['judul'] ?></td>
                <td><?= $item['jenis'] ?></td>
                <td><?= ucfirst($item['pelaksanaan']) ?></td>
                <td><?= $item['sasaran_peserta'] == 'a' ? 'Golongan 1-3' : 'Golongan 4up' ?></td>
                <td><?= $item['sasaran_departemen'] ?></td>
                <td>
                    <ul>
                    <?php 
                    $kompetensi = explode(',', $item['kompetensi']);
                    array_pop($kompetensi);
                    foreach($kompetensi as $k) {
                        echo "<li>$k</li>";
                    }
                    ?>
                    </ul>
                </td>
                <td><?= $item['tujuan'] ?></td>
                <td><?= $item['deskripsi'] ?></td>
                <td>
                    <ul>
                    <?php 
                    $metode = explode(',', $item['metode']);
                    foreach($metode as $m) {
                        $m = str_replace('_', ' ', $m);
                        echo "<li>" . ucfirst($m) . "</li>";
                    }
                    ?>
                    </ul>
                </td>
                <td><?= $item['durasi'] ?> Jam</td>
                <td>
                    <ul>
                    <?php 
                    $kelengkapan = explode(',', $item['kelengkapan']);
                    foreach($kelengkapan as $k) {
                        $k = str_replace('_', ' ', $k);
                        echo "<li>" . ucfirst($k) . "</li>";
                    }
                    ?>
                    </ul>
                </td>
                <td>
                    <ul>
                    <?php 
                    $metode_evaluasi = explode(',', $item['metode_evaluasi']);
                    foreach($metode_evaluasi as $m) {
                        $m = str_replace('_', ' ', $m);
                        echo "<li>" . ucfirst($m) . "</li>";
                    }
                    ?>
                    </ul>
                </td>
                <td><?= $item['notes'] ?></td>
            </tr>
            <tr>
        </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php $i++; ?>
  <?php endforeach; ?>
</div>

<script>
  function bacaSilabus(index, id_silabus) {
    console.log(id_silabus)
    $.ajax({
        url: '<?= base_url() ?>baca_silabus',
        method: 'POST',
        data: { id_silabus: id_silabus },
        success: function(response) {
          var badge_sidebar = $('#requestSylabusBadge').text();
            console.log(response);
            $('#bacaButton' + index).find('.badge').remove();
            var newBadgeValue = badge_sidebar - 1;
            if(newBadgeValue == 0) {
                $('#requestSylabusBadge').remove();
            } else {
                $('#requestSylabusBadge').text(newBadgeValue);
            }
            $('#baca' + index).parent().html('<h3><i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i></h3>');
        },
        error: function(jqXHR, textStatus, errorThrown) {
          Swal.fire({
              icon: 'error',
              title: 'Error!',
              text: 'Gagal mengirim request.',
          })
        }
    });
  }
</script>
<?= $this->endSection() ?>