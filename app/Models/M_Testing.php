<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Testing extends Model
{
    protected $table      = 'testing';
    protected $primaryKey = 'id_test';
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['testing'];
}