<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="m-1">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-tools d-flex">
                    <div>
                        <form action="<?= base_url() ?>import" method="post" enctype="multipart/form-data">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="hidden" value="<?= $category['id_categories'] ?>" name="id_category">
                                    <input type="file" class="custom-file-input" name="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>

                                </div>
                                <div class="input-group-append">
                                    <button type="submit" class="input-group-text" id="">Upload</button>
                                </div>
                                <a class="btn btn-primary ml-2" href="<?= base_url() ?>template/template_upload_training.xlsx" role="button">Template</a>
                                <div class="ml-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#exampleModalLong">
                                        <i class="fa fa-plus"></i> Add Training
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="ml-2">
                        <form action="<?= base_url() ?>delete_all" method="post">
                            <button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i>
                                Delete All</button>
                        </form>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 400px;">
            <table class="table table-head-fixed display" id="example2">
                <thead>
                    <tr>
                        <th>Training Title</th>
                        <th>Training Type</th>
                        <th>Description</th>
                        <th>Vendor</th>
                        <th>Budget</th>
                        <th>Module</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;

                    foreach ($jenis as $trainings) : ?>
                    <tr>
                        <td><?= $trainings->judul_training ?></td>
                        <td><?= $trainings->category ?></td>
                        <td><?= $trainings->deskripsi ?></td>
                        <td><?= $trainings->vendor ?></td>
                        <td><?= "Rp " . number_format($trainings->biaya, 0, ',', '.') ?></td>
                        <td>
                            <form id="Materialform<?= $i ?>" action="<?= base_url() ?>material_upload" method="post">
                                <div class="input-group">
                                    <?php if ($trainings->material_path == null) : ?>
                                    <div class="input-group mb-3">
                                        <input type="hidden" name="id_training" value="<?= $trainings->id_training ?>">
                                        <input type="hidden" class="form-control" name="page"
                                            value="<?= $category['id_categories'] ?>">
                                        <input type="text" class="form-control" name="link">
                                        <div class="input-group-append">
                                            <span class="btn btn-primary" onclick="MaterialForm(<?= $i ?>)"><i
                                                    class="fa-solid fa-upload"></i></span>
                                        </div>
                                        <?php else : ?>
                                        <div class="input-group mb-3">
                                            <input type="hidden" name="id_training"
                                                value="<?= $trainings->id_training ?>">
                                            <input type="hidden" class="form-control" name="page"
                                                value="<?= $category['id_categories'] ?>">
                                            <input type="text" class="form-control" name="link"
                                                value="<?= $trainings->material_path ?>">
                                            <div class="input-group-append">
                                                <span class="btn btn-primary" onclick="MaterialForm(<?= $i ?>)"><i
                                                        class="fas fa-check"></i></span>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                            </form>
                        </td>
                        <td>
                            <div class="d-flex">
                                <form action="<?= base_url() ?>delete_training" method="POST">
                                    <input type="hidden" value="<?= $category['id_categories'] ?>" name="category">
                                    <input type="hidden" value="<?= $trainings->id_training ?>" name="id">
                                    <button class="btn btn-danger btn-sm btn-delete"><i
                                            class="fa fa-trash"></i></button>
                                </form>
                                <div class="ml-2">
                                    <input type="hidden" value="<?= $trainings->id_training ?>" name="id">
                                    <button class="btn btn-warning btn-sm"
                                        onclick="edit('<?= $trainings->id_training ?>','<?= $trainings->judul_training ?>','<?= $trainings->jenis_training ?>','<?= $trainings->category ?>','<?= $trainings->deskripsi ?>','<?= $trainings->vendor ?>','<?= $trainings->biaya ?>')"><i
                                            class="fa-solid fa-pen-to-square"></i></button>
                                </div>
                            </div>

                        </td>

                    </tr>
                    <?php
                        $i++;;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Training</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-primary">
                    <!-- form start -->

                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Training Title</label>
                            <input type="text" class="form-control" name="add" placeholder="Masukan Judul">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Training Type</label>
                            <div class="form-group">
                                <select class="form-control" disabled name="add">
                                    <option value="<?= $category['id_categories'] ?>"><?= $category['category'] ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" placeholder="Enter ..." name="add"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Vendor</label>
                            <input type="text" class="form-control" name="add" placeholder="Masukan Vendor">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Budget</label>
                            <input type="text" class="form-control" id="biaya" name="add" placeholder="Biaya"
                                onkeyup="rupiah('biaya')">
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" onclick="sendTraining()">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editTraining" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Training Edited</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="edit[]" id="id">
                            <label for=" exampleInputEmail1">Training Title</label>
                            <input type="text" class="form-control" name="edit[]" id="judul"
                                placeholder="Masukan Judul">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Training Type</label>
                            <select class="form-control" name="edit[]" id="jenis" disabled>
                            </select>
                            <!--                             
                            <input type="text" class="form-control" placeholder="Jenis Training" name="edit[]"
                                id="jenis"> -->
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" placeholder="Enter ..." name="edit[]"
                                id="deskripsi"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Vendor</label>
                            <input type="text" class="form-control" name="edit[]" placeholder="Masukan Vendor"
                                id="vendor">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Budget</label>
                            <input type="text" class="form-control" id="biayaedit" name="edit[]" placeholder="Biaya"
                                onkeyup="rupiah('biayaedit')">
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" onclick="sendEditTraining()">Submit</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
function edit(id, judul, jenis, category, deskripsi, vendor, biaya) {
    jQuery.noConflict();
    var bilangan = biaya;

    var number_string = bilangan.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $('#editTraining #id').val(id);
    $('#editTraining #judul').val(judul);
    $('#editTraining #jenis').html(`<option value="${jenis}">${category}</option>`);
    $('#editTraining #deskripsi').val(deskripsi);
    $('#editTraining #vendor').val(vendor);
    $('#editTraining #biayaedit').val('Rp. ' + rupiah);
    $('#editTraining').modal('show');

}

function sendTraining() {
    let add = $("[name='add']").map(function() {
        return $(this).val();
    }).get();
    // console.log(add)
    if (add[0] == '' || add[0] == "" || add[1] == '' || add[1] == "" || add[2] == '' || add[2] == "" || add[3] ==
        '' ||
        add[3] == "") {
        add = []
    } else {
        $.ajax({
            type: 'POST',
            url: "<?= base_url() ?>save_training",
            async: true,
            dataType: "json",
            data: {
                add: add,

            },
            success: function(data) {
                window.location.reload()
                //console.log(data)
            }
        })

    }

}

function sendEditTraining() {
    let id = $("#editTraining #id").val();
    let judul = $("#editTraining #judul").val();
    let jenis = $("#editTraining #jenis").val();
    let deskripsi = $("#editTraining #deskripsi").val();
    let vendor = $("#editTraining #vendor").val();
    let biayaedit = $("#editTraining #biayaedit").val();
    console.log(deskripsi)
    if (id == '' || judul == '' || jenis == '' || deskripsi == '' || vendor == '' || biayaedit == '') {
        add = []
    } else {
        $.ajax({
            type: 'POST',
            url: "<?= base_url() ?>edit_training",
            async: true,
            dataType: "json",
            data: {
                id: id,
                judul: judul,
                jenis: jenis,
                deskripsi: deskripsi,
                vendor: vendor,
                biaya: biayaedit
            },
            success: function(data) {
                window.location.reload()
                //console.log(data)
            }
        })

    }
}

function MaterialForm(data) {
    $('#Materialform' + data).submit();
}


function rupiah(id) {

    var rupiah = document.getElementById(id);
    rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

}
</script>
<?= $this->endSection() ?>