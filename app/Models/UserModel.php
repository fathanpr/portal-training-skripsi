<?php

namespace App\Models;

use CodeIgniter\Model;
use PHPSQLParser\Test\Creator\functionTest;

class UserModel extends Model
{
    protected $table      = 'user';
    protected $primaryKey = 'id_user';
    // protected $useAutoIncrement = true;
    protected $allowedFields = [
        'npk', 'nama', 'status', 'dic',
        'divisi', 'departemen', 'seksi', 'bagian', 'nama_jabatan', 'type_golongan', 'username', 'password',
        'level', 'profile', 'email', 'tgl_masuk', 'tahun', 'bulan', 'golongan', 'promosi_terakhir', 'type_user'
    ];

    function get_data_login($username)
    {
        $this->select()->where('username', $username);
        $data = $this->get()->getRow();
        return $data;
    }

    function get_data_login_api($npk)
    {
        $this->select()->where('npk', $npk);
        $data = $this->get()->getResult();
        return $data;
    }

    public function getAllUser($id = false)
    {
        if ($id == false) {
            $this->select()->where('level', 'USER');
            return $this->get()->getResult();
        }

        return $this->where(['id_user' => $id])->first();
    }

    public function getAllAtasan()
    {
        $this->select()->where('level', 'USER')->whereIn('bagian',['KASIE','KADEPT','KADIV','BOD']);
        return $this->get()->getResult();
    }

    public function get_all_user_with_npk($id_user=false,$npk = false)
    {
        if ($npk == false && $id_user == false) {
            $this->select()->where('level', 'USER');
            return $this->get()->getResult();
        }

        return $this->where(['id_user' => $id_user])->where(['npk' => $npk])->first();
    }

    public function getDepartemenByNpk($npk){
        $this->select()->where('npk',$npk);
        return $this->get()->getRow();
    }

    public function getUserByNama($nama){
        return $this->select()->where('nama',$nama)->first();
    }

    public function getNamaById($id){
        return $this->select('nama')->where('id_user',$id)->first();
    }

    public function getNama($id_user){
        $this->select('nama');
        $this->where('id_user', $id_user);
        $query = $this->get();
        $result = $query->getRow();
        return $result->nama;
    }

    public function getNamaByNpk($npk){
        $this->select('nama');
        $this->where('npk', $npk);
        $query = $this->get();
        $result = $query->getRow();
        return $result->nama;
    }

    public function getAllNamaUser($id = false){
        if ($id == false) {
            $this->select('id_user,nama')->where('level', 'USER');
            return $this->get()->getResult();
        }

        return $this->where(['id_user' => $id])->first();
    }


    public function searchUsers($searchKeyword)
    {
        $this->select()->like('nama',$searchKeyword);
        return $this->get()->getResult();
    }


    public function filter($id)
    {
        $data = $this->getAllUser($id);

        if ($data['bagian'] == "BOD") {
            $this->select()->where('bagian', 'KADIV')->where('dic', $data['dic']);
            return $this->get()->getResult();
        } elseif ($data['bagian'] == "KADIV") {
            $this->select()->where('bagian', 'KADEPT')->where('divisi', $data['divisi'])->where('type_user','REGULAR');
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select()->whereIn('bagian', $bagian)->where('departemen', $data['departemen'])->where('level', 'USER')->whereIn('type_user',['EXPERT','REGULAR']);
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KASIE" || $data['bagian']  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE'];
            $this->select()->where('seksi', $data['seksi'])->where('level', 'USER')->whereNotIn('bagian', $bagian)->where('type_user','REGULAR');
            return $this->get()->getResult();
        } else {
            $this->select()->where('id_user', $id);
            return $this->get()->getResult();
        }
    }

    public function filterGolongan($id, $golongan)
    {
        $data = $this->getAllUser($id);

        if ($data['bagian'] == "BOD") {
            $this->select()->where('bagian', 'KADIV')->where('dic', $data['dic']);
            return $this->get()->getResult();
        } elseif ($data['bagian'] == "KADIV") {
            $this->select()->where('bagian', 'KADEPT')->where('divisi', $data['divisi'])->where('type_user','REGULAR');
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'STAFF'];
            $this->select()->whereIn('bagian', $bagian)->where('departemen', $data['departemen'])->where('level', 'USER')->whereIn('type_user',['EXPERT','REGULAR']);
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KASIE" || $data['bagian']  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE'];
            $this->select()->where('seksi', $data['seksi'])->where('level', 'USER')->whereNotIn('bagian', $bagian)->where('type_user','REGULAR')->where('type_golongan',$golongan);
            return $this->get()->getResult();
        } else {
            $this->select()->where('id_user', $id);
            return $this->get()->getResult();
        }
    }

    public function filterOperator($seksi){
        $this->select()->where('seksi',$seksi)->where('level','USER')->where('type_user','REGULAR')->where('type_golongan','B')->where('nama_jabatan','Operator')->ORDERBY('nama_jabatan','ASC');
        return $this->get()->getResult();
    }

    public function newFilter($bagian,$member)
    {

        if ($bagian == "BOD") {
            $this->select()->where('bagian', 'KADIV')->where('dic', $member);
            return $this->get()->getResultArray();
        } elseif ($bagian == "KADIV") {
            $this->select()->where('bagian', 'KADEPT')->where('divisi', $member);
            return $this->get()->getResultArray();
        } elseif ($bagian  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select()->whereIn('bagian', $bagian)->where('departemen', $member)->where('level', 'USER');
            return $this->get()->getResultArray();
        } elseif ($bagian  == "KASIE" || $bagian  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE','EXPERT'];
            $this->select()->where('seksi', $member)->where('level', 'USER')->whereNotIn('bagian', $bagian)->where('type_user','REGULAR');
            return $this->get()->getResultArray();
        } else {
            return  $member  =  array();
        }
    }

    public function filterIdUser($id)
    {
        $data = $this->getAllUser($id);

        if ($data['bagian'] == "BOD") {
            $this->select('id_user')->where('bagian', 'KADIV')->where('dic', $data['dic']);
            return $this->get()->getResult();
        } elseif ($data['bagian'] == "KADIV") {
            $this->select('id_user')->where('bagian', 'KADEPT')->where('divisi', $data['divisi']);
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'STAFF', 'KASUBSIE'];
            $this->select('id_user')->whereIn('bagian', $bagian)->where('departemen', $data['departemen'])->where('level', 'USER');
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KASIE" || $data['bagian']  == "STAFF 4UP" || $data['bagian']  == "KASUBSIE") {
            $bagian = ['STAFF 4UP', 'KASIE', 'KASUBSIE'];
            $this->select('id_user')->where('seksi', $data['seksi'])->where('level', 'USER')->whereNotIn('bagian', $bagian);
            return $this->get()->getResult();
        } else {
            $this->select('id_user')->where('id_user', $id);
            return $this->get()->getResult();
        }
    }

    public function filterKasubsie($id)
    {
        $data = $this->getAllUser($id);
        $this->select()->where('bagian', 'KASUBSIE')->where('departemen', $data['departemen'])->where('level', 'USER');
        return $this->get()->getResult();
    }


    public function filterMember($id)
    {
        $data = $this->getAllUser($id);

        if ($data['bagian'] == "BOD") {
            $this->select()->where('dic', $data['dic']);
            return $this->get()->getResult();
        } elseif ($data['bagian'] == "KADIV") {
            $this->select()->where('divisi', $data['divisi']);
            $this->where('nama_jabatan !=', 'Kepala Departemen');
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'STAFF','KASUBSIE'];
            $this->select()->where('departemen', $data['departemen'])->where('level', 'USER')->whereNotIn('bagian', $bagian)->where('type_user','REGULAR');
            $this->where('nama_jabatan !=', 'Kepala Departemen');
            return $this->get()->getResult();
        } elseif ($data['bagian']  == "KASIE" || $data['bagian']  == "STAFF 4UP" || $data['bagian']  == "KASUBSIE") {
            $bagian = ['STAFF 4UP', 'KASIE', 'KASUBSIE'];
            $this->select()->where('seksi', $data['seksi'])->where('level', 'USER')->whereNotIn('bagian', $bagian);
            return $this->get()->getResult();
        } else {
            $this->select()->where('id_user', $id);
            return $this->get()->getResult();
        }
    }


    public function getDataKadept($departemen)
    {
        $this->select()->where('bagian', 'KADEPT')->where('departemen', $departemen);
        return $this->get()->getResultArray();
    }

    public function getDataKadiv($divisi)
    {
        $this->select()->where('bagian', 'KADIV')->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }
    public function getDataBod($dic)
    {
        $this->select()->where('bagian', 'BOD')->where('dic', $dic);
        return $this->get()->getResultArray();
    }

    public function DistinctSeksi()
    {
        $this->select('seksi')->distinct()->where('seksi IS NOT NULL');
        return $this->get()->getResultArray();
    }
    public function DistinctDic()
    {
        $this->select('dic')->distinct()->where('dic IS NOT NULL');
        return $this->get()->getResultArray();
    }
    public function DistinctStatus()
    {
        $this->select('status')->distinct();
        return $this->get()->getResultArray();
    }
    public function DistinctDivisi()
    {
        $this->select('divisi')->distinct()->where('divisi IS NOT NULL');
        return $this->get()->getResultArray();
    }
    public function DistinctDepartemen()
    {
        $this->select('departemen')->distinct()->where('departemen IS NOT NULL');
        return $this->get()->getResultArray();
    }

    public function DistinctDepartemenForBOD()
    {
        $this->select('departemen')->distinct()->where('departemen IS NOT NULL')->where('departemen !=', 'AMDI AOP');
        return $this->get()->getResultArray();
    }

    public function DistinctJabatan()
    {
        $this->select('nama_jabatan')->distinct()->where('nama_jabatan IS NOT NULL');
        return $this->get()->getResultArray();
    }
    public function DistinctBagian()
    {
        $this->select('bagian')->distinct()->where('bagian IS NOT NULL');
        return $this->get()->getResultArray();
    }
    public function DistinctDivisiByDic($dic){
        $this->select('divisi')->where('dic',$dic)->distinct();
        return $this->get()->getResultArray();
    }
    
    public function DistinctDepartemenByDivisi($divisi){
        $this->select('departemen')->where('divisi',$divisi)->distinct();
        return $this->get()->getResultArray();
    }
    public function DistinctSeksiByDepartemen($departemen){
        $this->select('seksi')->where('departemen',$departemen)->whereNotIn('seksi',['X'])->distinct();
        return $this->get()->getResultArray();
    }
    public function DistinctBagianBySeksi($seksi){
        $this->select('bagian')->where('seksi',$seksi)->distinct();
        return $this->get()->getResultArray();
    }

    public function getDivisiByDepartment($department) {
        $result = $this->select('divisi')->where('departemen', $department)->get()->getRow();
        return $result ? $result->divisi : null;
    }
    


    public function getLastUser()
    {
        $this->select('id_user,npk');
        return $this->get()->getLastRow();
    }
    public function getIdUser()
    {
        $this->select('id_user');
        return $this->get()->getResultArray();
    }



    public function getUserAstra()
    {
        $bagian = ['BOD', 'KADIV', 'KADEPT', 'KASIE', 'STAFF 4UP'];
        $this->select('nama,id_user,npk')->whereIn('bagian', $bagian)->where('type_user', 'REGULAR')->where('type_golongan', 'A         ')->where('level', 'USER');
        $this->orderBy('nama', 'ASC');
        return $this->get()->getResultArray();
    }


    public function getUserExpert()
    {
        $this->select('nama,id_user')->where('type_user', 'EXPERT')->where('bagian', 'STAFF 4UP')->where('type_golongan', 'A         ')->where('level', 'USER');
        return $this->get()->getResultArray();
    }

    public function getUserTechnicalA($departemen)
    {
        $this->select('nama,id_user')->where('departemen', $departemen)->where('level', 'USER')->where('type_golongan', 'A         ');
        return $this->get()->getResultArray();
    }

    public function getIdUserTechnicalA($departemen,$nama_jabatan)
    {
        $this->select('id_user,npk')->where('departemen', $departemen)->where('nama_jabatan',$nama_jabatan)->where('level', 'USER')->where('type_golongan', 'A         ');
        return $this->get()->getResultArray();
    }

    public function getUserTechnicalB($jabatan, $departemen)
    {
        $this->select('nama,id_user')->where('nama_jabatan', $jabatan)->where('departemen', $departemen)->where('level', 'USER')->where('type_golongan', 'B');
        return $this->get()->getResultArray();
    }
    
    public function getIdUserTechnicalB($departemen, $nama_jabatan, $seksi){
        $this->select('id_user,npk')->where('departemen',$departemen)->where('nama_jabatan',$nama_jabatan)->where('seksi',$seksi)->where('type_user','REGULAR')->where('type_golongan', 'B');
        return $this->get()->getResultArray();
    }


    public function getGroupB()
    {
        $this->select()->where('type_golongan', 'B');
        return $this->get()->getResultArray();
    }

    public function getGroupA()
    {
        $this->select()->where('type_golongan', 'A');
        return $this->get()->getResultArray();
    }
    public function getUserDivisi($divisi)
    {
        $this->select('nama,id_user')->where('divisi', $divisi)->where('level', 'USER')->where('type_golongan', 'B        ');
        return $this->get()->getResultArray();
    }

    public function getIdUserDivisi($divisi)
    {
        $this->select('id_user,npk')->where('divisi', $divisi)->where('level', 'USER')->where('type_golongan', 'B        ');
        return $this->get()->getResultArray();
    }

    public function getUserBagian()
    {
        $this->select('bagian')->distinct();
        return $this->get()->getResultArray();
    }

    public function getUserJabatan()
    {
        $this->select('nama_jabatan')->distinct()->where('nama_jabatan IS NOT NULL')->where('nama_jabatan !=', 'Admin');
        return $this->get()->getResultArray();
    }


    public function getJabatanInUser($department)
    {
        $this->select('nama_jabatan')->where('departemen', $department)->where('type_golongan', 'B')->distinct();
        return $this->get()->getResultArray();
    }

    public function getUserByDepartment($department)
    {
        $this->select()->where('departemen', $department)->where('type_golongan', 'B');
        return $this->get()->getResultArray();
    }


    function M_test()
    {
        $data = $this->get();
        return $data;
    }
    public function getDataByDic($dic)
    {
        $this->select('id_user,npk,nama,dic')->where('dic', $dic);
        return $this->get()->getResultArray();
    }
    public function getDataByDivisi($divisi)
    {
        $this->select('id_user,npk,nama,divisi')->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }
    public function getDataByDepartment($department)
    {
        $this->select('id_user,npk,nama,departemen')->where('departemen', $department);
        return $this->get()->getResultArray();
    }
    public function getDataBySeksi($seksi)
    {
        $this->select('id_user,npk,nama,seksi')->where('seksi', $seksi);
        return $this->get()->getResultArray();
    }

    public function getTypeGolongan()
    {
        $this->select('type_golongan')->distinct();
        return $this->get()->getResultArray();
    }

    public function getTypeUser()
    {
        $this->select('type_user')->distinct();
        return $this->get()->getResultArray();
    }

    public function GetUserByNPK($NPK)
    {
        return $this->where(['npk' => $NPK])->first();
    }

    public function saveData($data)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('user')->insert($data);

        return $builder;
    }

    public function getDepartemenForKadivBod($bagian,$member){
        if($bagian == 'BOD'){
            $this->select('departemen')->where('dic',$member)->distinct();
        }elseif($bagian == 'KADIV'){
            $this->select('departemen')->where('divisi',$member)->distinct();
        }
        return $this->get()->getResultArray();
    }

    public function getDepartmentDivision($departemen){
        $this->select('divisi')->where('departemen',$departemen)->distinct();
        return $this->get()->getResultArray();
    }

    public function getCountPerDepartment(){
        $query = $this->db->query("SELECT departemen as nama_departemen,
        COUNT(*) AS jumlah
        FROM
            [user]
        WHERE
            departemen IS NOT NULL AND departemen != 'AMDI AOP'
        GROUP BY
            departemen
        ORDER BY
            departemen;");
        return $query->getResultArray();
    }

    public function getCountPerDivisi($divisi){
        $query = $this->db->query("SELECT departemen as nama_departemen,
        COUNT(*) AS jumlah
        FROM
            [user]
        WHERE
            departemen IS NOT NULL 
        AND 
        departemen != 'AMDI AOP'
        AND
            divisi = ?
        GROUP BY
            departemen
        ORDER BY
            departemen;",[$divisi]);
        return $query->getResultArray();
    }
    
    public function getCountPerSection($departemen){
        $query = $this->db->query("SELECT seksi as nama_seksi,
        COUNT(*) AS jumlah
        FROM
            [user]
        WHERE
            departemen = ?
        AND
            seksi IS NOT NULL AND seksi != 'X'
        GROUP BY
            seksi
        ORDER BY
            seksi;",[$departemen]);
        return $query->getResultArray();
    }

    
}