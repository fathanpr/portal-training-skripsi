<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="talk" data-talk="<?= session()->get('talk'); ?>"></div>
<?php 
$page = basename($_SERVER['PHP_SELF']); ?>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title">
            Detail Evaluasi Reaksi (Admin)
        </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Training Title</th>
                    <th>Training Type</th>
                    <th>Training Category</th>
                    <th>Training Method</th>
                    <th>Training Goals</th>
                    <th>Training Implementation</th>
                    <!-- <th>Module</th> -->
                    <th>Description</th>
                </tr>
            </thead>
            <?php foreach ($evaluasi as $Atmps) : ?>
            <tr>
                <td><?= $Atmps['nama'] ?></td>
                <td><?= $Atmps['departemen'] ?></td>
                <td><?= $Atmps['training'] ?></td>
                <td><?= $Atmps['jenis_training'] ?></td>
                <td><?= $Atmps['kategori_training'] ?></td>
                <td><?= $Atmps['metode_training'] ?></td>
                <td><?= $Atmps['tujuan_training'] ?></td>
                <td><?= date('d M', strtotime($Atmps['mulai_training'])) . ' - ' . date('d M Y', strtotime($Atmps['rencana_training'])) ?></td>
                <!-- <td>
                    <?= $Atmps['status_evaluasi'] == null ? '<p>Material</p>' : '<a href="'.$Atmps['material_path'].'">Material</a>' ?>
                </td> -->
                <td>
                    <div class="d-flex justify-content-start">
                        <?php 
                            if($Atmps['status_evaluasi'] == null) {
                                echo '<h5><span class="badge badge-danger">NOT YET EVALUATED</span></h5>';
                            } else {
                                $status = $Atmps['status_evaluasi'] == null ? 'NOT YET EVALUATED' : 'ALREADY EVALUATED';
                                $color = $Atmps['status_evaluasi'] == null ? 'danger' : 'success';
                                $url = $Atmps['status_evaluasi'] == null ? 'detail_evaluasi_reaksi_admin' : 'form_evaluasi_selesai';
                                if($page == 'detail_evaluasi_reaksi_admin') {
                                    $url = $Atmps['status_evaluasi'] == null ? 'detail_evaluasi_reaksi_admin' : 'form_evaluasi_selesai';
                                }
                                echo '<a href="'.base_url().$url.'/'.$Atmps['id_tna'].'" class="btn btn-'.$color.'">'.$status.'</a>';
                            }
                        ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
    <tbody>
    </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
<?= $this->endSection() ?>