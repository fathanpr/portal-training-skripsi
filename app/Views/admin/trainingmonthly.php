<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title">
            <?php if($tittle == 'Training Monthly') : ?>
                Training Monthly <?= date('Y')+1 ?>
            <?php elseif($tittle == 'Other Training Monthly') : ?>
                Other Training Monthly
            <?php else : ?>
                Unplanned Training Monthly
            <?php endif; ?>
        </h3>
        <div class="row">
            <div class="col d-flex justify-content-end">
                <div class="btn-group dropleft">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Export
                    </button>
                    <div class="dropdown-menu">
                        <?php foreach ($training as $dates) : ?>
                            <?php if($tittle == 'Training Monthly') : ?>
                                <a class="dropdown-item" href="<?= base_url() ?>export_training_monthly/<?= $dates['Month Training'] ?>"><?= $dates['Planning Training'] ?></a>
                            <?php elseif($tittle == 'Other Training Monthly') : ?>
                                <a class="dropdown-item" href="<?= base_url() ?>export_training_monthly_other/<?= $dates['Month Training'] ?>/<?= $dates['Year Training'] ?>"><?= $dates['Planning Training'].' '.$dates['Year Training'] ?></a>
                            <?php else:  ?>
                                <a class="dropdown-item" href="<?= base_url() ?>export_training_monthly_unplanned/<?= $dates['Month Training'] ?>/<?= $dates['Year Training'] ?>"><?= $dates['Planning Training'].' '.$dates['Year Training'] ?></a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Planning Training</th>
                    <th>Number of Trainings</th>
                    <th>Kadiv Approval</th>
                    <th>BOD Approval</th>
                    <th style="color:red;">Reject</th>
                </tr>
            </thead>
            <?php
            $i = 0;
            foreach ($training as $dates) : ?>
            <tr>
                <?php $page = basename($_SERVER['PHP_SELF']);
                    if ($page == 'training_monthly') : ?>
                <td>
                    <a href="<?= base_url() ?>kadiv_accept/<?= $dates['Planning Training'] ?>">
                    <?php
                        $planningTrainingDate = strtotime($dates['Planning Training']);
                        $newDate = date('F Y', strtotime('+1 year', $planningTrainingDate));
                        echo $dates['Planning Training']; 
                    ?>
                    </a>
                    <?php 
                    $lists = $tna->getDataTrainingMonthly($dates['Month Training'],$dates['Year Training']);
                    if(!empty($lists)){
                        echo "<span class='badge badge-pill badge-primary'>!</span>";
                    }
                    ?>
                    
                </td>
                <?php elseif($page == 'unplanned_monthly') : ?>
                <td>
                    <a href="<?= base_url() ?>kadiv_accept_unplanned/<?= $dates['Planning Training'] ?>/<?= $dates['Year Training'] ?>">
                    <?php
                        echo $dates['Planning Training'].' '.$dates['Year Training'];
                    ?>
                    </a>
                    <?php 
                    $unplannedMonthly = $unplanned->getDataTrainingMonthly($dates['Month Training'],$dates['Year Training']);
                    if(!empty($unplannedMonthly)){
                        echo "<span class='badge badge-pill badge-primary'>!</span>";
                    }
                    ?>
                </td>
                <?php else : ?>
                    <td>
                    <a href="<?= base_url() ?>kadiv_accept_other/<?= $dates['Planning Training'] ?>/<?= $dates['Year Training'] ?>">
                    <?php
                        echo $dates['Planning Training'].' '.$dates['Year Training'];
                    ?>
                    </a>
                    <?php 
                    $otherMonthly = $other->getDataOtherTrainingMonthly($dates['Month Training'],$dates['Year Training']);
                    
                    if(!empty($otherMonthly)){
                        echo "<span class='badge badge-pill badge-primary'>!</span>";
                    }
                    ?>
                </td>
                <?php endif; ?>

                <td>
                    <?= $dates['Jumlah Training'] ?>
                </td>
                <td><?= $dates['Admin Approval'] ?></td>
                <td><?= $dates['BOD Approval'] ?></td>
                <td><?= $dates['Reject'] ?>
                </td>
            </tr>
            <?php $i++;
            endforeach; ?>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<?= $this->endSection() ?>