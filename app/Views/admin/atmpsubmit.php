<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
    <div class="row d-flex justify-content-between">
            <div class="col">
                <h3 class="card-title"><?= $tittle ?></h3>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="<?php base_url() ?>download_atmp_submit" class="btn btn-success"><i class="fa-solid fa-file-arrow-down fa-xs"></i> Export</a>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <div class="row">
            <div class="col ml-3">
                <h3>TNA Submit</h3>
                <table class="table table-hover" id="tnaAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Title</th>
                            <th>Vendor Suggest</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th>Budget</th>
                            <!-- <th>Training Group</th> -->
                        </tr>
                    </thead>
                    <?php foreach ($atmpSubmit as $atmpSubmits) : ?>
                    <?php if($atmpSubmits['kelompok_training'] == 'training') : ?>
                        <tr>
                        <td><?= $atmpSubmits['nama'] ?></td>
                        <td><?= $atmpSubmits['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($atmpSubmits['npk']) ?><?= $departemen->departemen ?></td>
                        <td><?= $atmpSubmits['training'] ?></td>
                        <td><?= $atmpSubmits['vendor_suggest'] ?></td>
                        <td><?= $atmpSubmits['jenis_training'] ?></td>
                        <td><?= $atmpSubmits['kategori_training'] ?></td>
                        <td><?= $atmpSubmits['metode_training'] ?></td>
                        <td><?= $atmpSubmits['tujuan_training'] ?></td>
                        <td><?= $atmpSubmits['request_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['mulai_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['rencana_training'] ?? '-' ?></td>
                        <td>Rp<?= number_format($atmpSubmits['biaya'], 0, ',', '.') ?></td>
                        <!-- <td style="text-transform:uppercase;"><?= $atmpSubmits['kelompok_training'] ?></td> -->
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col ml-3">
                <h3>Unplanned Submit</h3>
                <table class="table table-hover" id="unplannedAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Title</th>
                            <th>Vendor Suggest</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th>Budget</th>
                            <!-- <th>Training Group</th> -->
                        </tr>
                    </thead>
                    <?php foreach ($atmpSubmit as $atmpSubmits) : ?>
                    <?php if($atmpSubmits['kelompok_training'] == 'unplanned') : ?>
                        <tr>
                        <td><?= $atmpSubmits['nama'] ?></td>
                        <td><?= $atmpSubmits['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($atmpSubmits['npk']) ?><?= $departemen->departemen ?></td>
                        <td><?= $atmpSubmits['training'] ?></td>
                        <td><?= $atmpSubmits['vendor_suggest'] ?></td>
                        <td><?= $atmpSubmits['jenis_training'] ?></td>
                        <td><?= $atmpSubmits['kategori_training'] ?></td>
                        <td><?= $atmpSubmits['metode_training'] ?></td>
                        <td><?= $atmpSubmits['tujuan_training'] ?></td>
                        <td><?= $atmpSubmits['request_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['mulai_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['rencana_training'] ?? '-' ?></td>
                        <td>Rp<?= number_format($atmpSubmits['biaya'], 0, ',', '.') ?></td>
                        <!-- <td style="text-transform:uppercase;"><?= $atmpSubmits['kelompok_training'] ?></td> -->
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col ml-3">
                <h3>Other Submit</h3>
                <table class="table table-hover" id="otherAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Group</th>
                            <th>Training Title</th>
                            <th>Vendor Suggest</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th>Budget</th>
                            <!-- <th>Training Group</th> -->
                        </tr>
                    </thead>
                    <?php foreach ($atmpSubmit as $atmpSubmits) : ?>
                    <?php if($atmpSubmits['kelompok_training'] == 'amdi_aop' || $atmpSubmits['kelompok_training'] == 'cross_budget') : ?>
                        <tr>
                        <td><?= $atmpSubmits['nama'] ?></td>
                        <td><?= $atmpSubmits['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($atmpSubmits['npk']) ?><?= $departemen->departemen ?></td>
                        <td>
                            <?php if($atmpSubmits['kelompok_training'] == 'amdi_aop') : ?>
                                <span class="badge badge-info">AMDI AOP</span>
                                <?php elseif($atmpSubmits['kelompok_training'] == 'cross_budget') :  ?>
                                <span class="badge badge-success">CROSS BUDGET</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $atmpSubmits['training'] ?></td>
                        <td><?= $atmpSubmits['vendor_suggest'] ?></td>
                        <td><?= $atmpSubmits['jenis_training'] ?></td>
                        <td><?= $atmpSubmits['kategori_training'] ?></td>
                        <td><?= $atmpSubmits['metode_training'] ?></td>
                        <td><?= $atmpSubmits['tujuan_training'] ?></td>
                        <td><?= $atmpSubmits['request_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['mulai_training'] ?? '-' ?></td>
                        <td><?= $atmpSubmits['rencana_training'] ?? '-' ?></td>
                        <td>Rp<?= number_format($atmpSubmits['biaya'], 0, ',', '.') ?></td>
                        <!-- <td style="text-transform:uppercase;"><?= $atmpSubmits['kelompok_training'] ?></td> -->
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<?= $this->endSection() ?>