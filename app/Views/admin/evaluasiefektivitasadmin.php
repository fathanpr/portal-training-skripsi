<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">

        <h3 class="card-title"> <?= $tittle ?></h3>

    </div>
    <div class="card-body table-responsive p-0">
        <table class="table table-head-fixed display" id="example2">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0;
                foreach ($user as $users) : ?>
                <tr>
                    <td>
                    <form id="myform<?= $i ?>" action="<?= base_url() ?>detail_evaluasi_efektivitas_admin" method="post">
                        <input type="hidden" name="evaluasi" value="<?= $users['id'] ?>" />
                        <input type="hidden" name="npk" value="<?= $users['npk'] ?>" />
                        <input type="hidden" name="bagian" value="<?= $users['bagian'] ?>" />
                    </form>
                    <a href="#" onclick="document.getElementById('myform<?= $i ?>').submit();"><?= $users['nama'] ?></a>
                    <?php if($tna->countAllEvaluasiReaksi($users['npk'],$users['bagian']) > 0) : ?>
                            <span class="badge badge-primary badge-pill">!</span>
                        <?php endif; ?> 
                    </td>
                </tr>
                <?php
                    $i++;
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<?= $this->endSection() ?>