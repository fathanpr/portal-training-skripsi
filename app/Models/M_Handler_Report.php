<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Handler_Report extends Model
{
    protected $table      = 'report_handler';
    protected $primaryKey = 'id_handler_report';
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['id_training', 'date'];

    public function getByTrainingDate($id_training, $date)
    {
        return $this->select()->where('id_training', $id_training)->where('date', $date)->first();
    }
}