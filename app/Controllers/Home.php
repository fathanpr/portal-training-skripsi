<?php

namespace App\Controllers;

use App\Models\M_Tna;
use App\Models\UserModel;

class Home extends BaseController
{
    private M_Tna $tna;
    private UserModel $user;
    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
    }
    public function index()
    {
        $id = session()->get('id');
        $user = $this->user->filter($id);
        $all_user = $this->user->getAllUser();
        $data = [
            'tittle' => 'Portal Training & Development',
            'user' => $user,
            'all_user' => $all_user
        ];
        // dd($all_user);
        return view('admin/home', $data);
    }

    public function search()
    {

        $searchKeyword = $this->request->getPost('searchKeyword');
        $users = $this->user->searchUsers($searchKeyword);
        
        echo json_encode($data); // Gantilah 'user_search_result' dengan nama View yang sesuai
    }
}