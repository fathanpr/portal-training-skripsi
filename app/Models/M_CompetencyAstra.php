<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencyAstra extends Model
{
    protected $table      = 'competency_profile_astra';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_astra';
    protected $allowedFields = ['id_user', 'npk', 'id_astra', 'score_astra', 'updated_at'];
    // protected $useTimestamps = true;
    // protected $dateFormat = 'datetime';
    // protected $updatedField  = 'updated_at';


    public function getIdAstra($id)
    {
        return $this->where(['id_tna' => $id])->first();
    }

    public function getProfileAstraCompetency($id_user,$npk)
    {
        $this->select('astra_competency.astra,astra_competency.proficiency,competency_profile_astra.id_competency_astra,competency_profile_astra.score_astra,competency_profile_astra.updated_at')->where('competency_profile_astra.npk', $npk)->orWhere('competency_profile_astra.id_user', $id_user);
        $this->join('astra_competency', 'astra_competency.id_astra = competency_profile_astra.id_astra');
        return $this->get()->getResultArray();
    }
    public function getAstraByIdCompetency($id)
    {
        $this->select('astra_competency.astra,astra_competency.proficiency,competency_profile_astra.id_competency_astra,competency_profile_astra.score_astra')->where('competency_profile_astra.id_competency_astra', $id);
        $this->join('astra_competency', 'astra_competency.id_astra = competency_profile_astra.id_astra');
        return $this->get()->getResultArray();
    }
    public function getAstraIdCompetency($id_user,$npk)
    {
        $this->select('competency_profile_astra.id_astra')->where('competency_profile_astra.id_user', $id_user)->orWhere('competency_profile_astra.npk', $npk);
        $this->join('astra_competency', 'astra_competency.id_astra = competency_profile_astra.id_astra');
        return $this->get()->getResultArray();
    }

    public function getAstraId($id_astra, $npk)
    {
        $this->select()->where('id_astra', $id_astra)->where('npk',$npk);
        return $this->get()->getResultArray();
    }

    public function getAstraCompetencySkillMap(){
        $this->select('astra,npk,score_astra,proficiency,updated_at')
            ->join('astra_competency', 'astra_competency.id_astra = competency_profile_astra.id_astra')
            // ->where('competency_profile_astra.score_astra > 0')
            // ->where('competency_profile_astra.updated_at IS NOT NULL')
            ->orderBy('competency_profile_astra.npk', 'ASC');
        return $this->get()->getResultArray();
    }
}