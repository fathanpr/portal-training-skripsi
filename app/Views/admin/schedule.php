<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="talk" data-talk="<?= session()->get('talk'); ?>"></div>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title"><?= $tittle ?></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover" id="personal-schedule">
            <thead></thead>
            <tr>
                <th>Name</th>
                <?php if($tittle == 'Other Schedule Training') : ?>
                    <th>Training Group</th>
                <?php endif; ?>
                <th>Department</th>
                <th>Training Title</th>
                <th>Training Type</th>
                <th>Training Category</th>
                <th>Training Method</th>
                <th>Training Goals</th>
                <th>Training Implementation</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php foreach ($schedule as $Atmps) : ?>
            <form action="<?= base_url() ?>schedule_action/<?= $Atmps['id_tna'] ?>" method="POST">
            <input type="hidden" value="<?= $tittle ?>" name="tittle">
            <tr>
                <td><?= $Atmps['nama'] ?></td>
                <?php if($tittle == 'Other Schedule Training') : ?>
                    <td>
                        <?php if($Atmps['kelompok_training'] == 'cross_budget') : ?>
                            Cross Budget
                        <?php else : ?>
                            AMDI AOP
                        <?php endif ?>
                    </td>
                <?php endif; ?>
                <td><?php $dpt = $user->getDepartemenByNpk($Atmps['npk']) ?><?= $dpt->departemen ?></td>
                <td><?= $Atmps['training'] ?></td>
                <td><?= $Atmps['jenis_training'] ?></td>
                <td><?= $Atmps['kategori_training'] ?></td>
                <td><?= $Atmps['metode_training'] ?></td>
                <td><?= $Atmps['tujuan_training'] ?></td>
                <td>
                <?php $reScheduleDay = date('Y', strtotime($Atmps['mulai_training'])); ?>
                <div class="row">
                    <div class="col">
                        <input type="date" class="form-control" required value="<?= $Atmps['mulai_training'] ?>" name="mulai_training" min="<?= $reScheduleDay ?>-01-01" max="<?= $reScheduleDay ?>-12-31">
                    </div>
                    <div class="col">
                        <input type="date" class="form-control" required value="<?= $Atmps['rencana_training'] ?>" name="rencana_training" min="<?= $reScheduleDay ?>-01-01" max="<?= $reScheduleDay ?>-12-31">
                        <!-- <?= date('d M', strtotime($Atmps['mulai_training'])) . ' - ' . date('d M Y', strtotime($Atmps['rencana_training'])) ?> -->
                    </div>
                </div>
                </td>
                <?php $page = basename($_SERVER['PHP_SELF']);
                    if ($page == 'schedule_training') : ?>
                <td>
                    <div class="row">
                        <!-- <a href="<?= base_url() ?>schedule_action/<?= $Atmps['id_tna'] ?>"
                            class="btn btn-success btn-sm" style="font-size:10px;">Done</a> -->
                        <div class="col">
                            <button type="submit" class="btn btn-success">Done</button>
                            <a href="#" onclick="ModalReasons(<?= $Atmps['id_tna'] ?>,'<?= $tittle ?>')" class="btn btn-danger">Not implemented</a>
                        </div>
                    </div>
                </td> 
                <?php else : ?>
                <td>
                    <div class="row">
                        <div class="col">
                            <!-- <a href="<?= base_url() ?>schedule_action_unplanned/<?= $Atmps['id_tna'] ?>"
                                class="btn btn-success btn-sm" style="font-size:10px;width:87px">Done</a> -->
                            <button type="submit" class="btn btn-success">Done</button>
                            <a href="#" onclick="ModalReasons(<?= $Atmps['id_tna'] ?>,'<?= $tittle ?>')" class="btn btn-danger">Not implemented</a>
                        </div>
                    </div>
                </td>
                <?php endif; ?>
            </tr>
            </form>
            <?php endforeach; ?>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- Modal -->
<div class="modal fade" id="Reason" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>schedule_not_implemented" method="post">
                    <input type="hidden" name="id_tna" id="id_tna">
                    <input type="hidden" name="title" id="title">
                    <div class="d-flex justify-content-center">
                        <textarea rows="4" cols="50" name="alasan" required></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="Submit" class="btn btn-primary"><i class=" fa-solid fa-paper-plane"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
function ModalReasons(id, title) {
    jQuery.noConflict();
    $('#id_tna').val(id)
    $('#title').val(title)
    $('#Reason').modal('show')
}
</script>
<?= $this->endSection() ?>