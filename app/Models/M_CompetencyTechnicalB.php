<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencyTechnicalB extends Model
{
    protected $table      = 'competency_profile_technicalB';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_technicalB';
    protected $allowedFields = ['id_user', 'npk', 'id_technicalB', 'score', 'updated_at'];

    public function getProfileTechnicalCompetencyB($id_user, $npk)
    {
        $this->select()->where('competency_profile_technicalB.id_user', $id_user)->orWhere('competency_profile_technicalB.npk', $npk);
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB =competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBCurrentPosition($id_user, $npk, $seksi)
    {
        $this->select()
            ->groupStart()
                ->where('competency_profile_technicalB.id_user', $id_user)
                ->where('seksi',$seksi)
            ->groupEnd()
            ->orGroupStart()
                ->where('competency_profile_technicalB.npk', $npk)
                ->where('seksi',$seksi)
            ->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBValue($id_user, $npk, $technical)
    {
        $this->select()
            ->groupStart()
                ->where('competency_profile_technicalB.id_user', $id_user)
                ->where('competency_technicalB.technicalB', $technical)
            ->groupEnd()
            ->orGroupStart()
                ->where('competency_profile_technicalB.npk', $npk)
                ->where('competency_technicalB.technicalB', $technical)
            ->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBWithDepartment($id_user, $npk, $department)
    {
        $this->select()
            ->groupStart()
                ->where('id_user', $id_user)
                ->where('department', $department)
            ->groupEnd()
            ->orGroupStart()
                ->where('npk', $npk)
                ->where('department', $department)
            ->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBUpdate($id_user, $npk, $department,$nama_jabatan,$seksi)
    {
        $this->select()
            ->groupStart()
                ->where('id_user', $id_user)
                ->where('department', $department)
                ->where('nama_jabatan', $nama_jabatan)
                ->where('seksi', $seksi)
            ->groupEnd()
            ->orGroupStart()
                ->where('npk', $npk)
                ->where('department', $department)
                ->where('nama_jabatan', $nama_jabatan)
                ->where('seksi', $seksi)
            ->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getTechnicalBIdCompetency($id_user, $npk)
    {
        $this->select('competency_profile_technicalB.id_technicalB')->where('competency_profile_technicalB.id_user', $id_user)->orWhere('competency_profile_technicalB.npk', $npk);
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getTechnicalByIdCompetencyB($id)
    {
        $this->select()->where('id_competency_technicalB', $id);
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB =competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getTechnicalByIdCompetencyBValue($id, $technical)
    {
        $this->select()->where('id_competency_technicalB', $id);
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB =competency_profile_technicalB.id_technicalB')->where('technical', $technical);
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBDistinct($id_user,$npk)
    {
        $this->select('competency_technicalB.department')->where('id_user', $id_user)->orWhere('npk', $npk)->distinct();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB =competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }
    public function getProfileTechnicalCompetencyBDepartment($id_user, $npk, $department)
    {
        $this->select()
        ->groupStart()->where('id_user', $id_user)->where('competency_technicalB.department', $department)->groupEnd()
        ->orGroupStart()->where('npk', $npk)->where('competency_technicalB.department', $department)->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB =competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyBSeksi($id_user, $npk, $seksi)
    {
        $this->select()
        ->groupStart()->where('id_user', $id_user)->where('competency_technicalB.seksi', $seksi)->groupEnd()
        ->orGroupStart()->where('npk', $npk)->where('competency_technicalB.seksi', $seksi)->groupEnd();
        $this->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB');
        return $this->get()->getResultArray();
    }

    public function getTechnicalBId($id_technicalB, $npk)
    {
        $this->select()->where('id_technicalB', $id_technicalB)->where('npk',$npk);
        return $this->get()->getResultArray();
    }

    public function getTechnicalBCompetencySkillMap($seksi){
        $this->select('technicalB,npk,score,proficiency,updated_at')
            ->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB')
            // ->where('competency_profile_technicalB.score > 0')
            // ->where('competency_profile_technicalB.updated_at IS NOT NULL')
            ->where('seksi',$seksi)
            ->orderBy('competency_profile_technicalB.npk', 'ASC');
        return $this->get()->getResultArray();
    }
    
    public function userHaveTechnicalB($technicalB,$npk){
        $this->select()
            ->join('competency_technicalB', 'competency_technicalB.id_technicalB = competency_profile_technicalB.id_technicalB')
            ->where('technicalB',$technicalB)
            ->where('npk',$npk);
        return $this->first();
    }
}