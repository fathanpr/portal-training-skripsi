<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Trainer extends Model
{
    protected $table      = 'trainer';
    protected $primaryKey = 'id_trainer';
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['id_user', 'npk', 'nama', 'trainer_since', 'vendor'];


    public function getAllTrainer(){
       return $this->select()->get()->getResultArray();
    }
    public function getTrainer($id)
    {
        $this->select('trainer.*,user.npk,user.divisi,user.departemen,user.seksi')->where('id_trainer', $id);
        $this->join('user', 'user.id_user=trainer.id_user');
        return $this->get()->getResultArray();
    }
    public function getLastColumn()
    {
        $this->select();
        return $this->get()->getLastRow();
    }

    public function getInternalTrainer()
    {
        $this->select('trainer.*,user.npk,user.divisi,user.departemen,user.seksi')->where('vendor', null);
        $this->join('user', 'user.id_user=trainer.id_user');
        return $this->get()->getResultArray();
    }
    public function getExternalTrainer()
    {
        $this->select()->whereNotIn('vendor', ['null']);
        return $this->get()->getResultArray();
    }
    public function getTrainerByIdUser($id)
    {
        return $this->select()->where('id_user', $id)->first();
    }
    public function getTrainerByNamaUser($nama)
    {
        return $this->select()->where('nama', $nama)->first();
    }

    public function getTrainerById($id)
    {
        return  $this->select()->where('id_trainer', $id)->first();
    }

    public function getTrainerName($id){
        return  $this->select()->where('id_trainer', $id)->get()->getResultArray();
    }

    public function getTrainerWithScore(){
        $this->select('trainer.*,report.*,training.judul_training');
        $this->join('report','report.id_trainer = trainer.id_trainer');
        $this->join('training','training.id_training = report.id_training');
        // $this->join('tna','tna.id_training = training.id_training');
        // $this->join('evaluasi_reaksi','evaluasi_reaksi.id_tna = tna.id_tna');
        $this->where('report.score IS NOT NULL');
        return $this->get()->getResultArray();
    }

    public function getTrainerWithDetailScore(){
        $this->select('trainer.*,report.*,training.judul_training,tna.id_tna,evaluasi_reaksi.*');
        $this->join('report','report.id_trainer = trainer.id_trainer');
        $this->join('training','training.id_training = report.id_training');
        $this->join('tna','tna.id_training = training.id_training');
        $this->join('evaluasi_reaksi','evaluasi_reaksi.id_tna = tna.id_tna');
        $this->where('pengetahuan1 IS NOT NULL');
        $this->where('report.score IS NOT NULL');
        return $this->get()->getResultArray();
    }
}