<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Budget extends Model
{
    protected $table      = 'budget';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_budget';
    protected $allowedFields = ['alocated_budget', 'available_budget', 'used_budget', 'year', 'department', 'current', 'temporary_calculation', 'divisi'];

    public function getAllBudget()
    {
        $this->select();
        return $this->get()->getResultArray();
    }

    public function getBudgetByDivisi($divisi)
    {
        $this->select()->where('divisi', $divisi);
        return $this->get()->getResultArray();
    }

    public function BudgetDashboard($date)
    {
        $this->select()->where('year', $date);
        return $this->get()->getResultArray();
    }

    public function BudgetDashboardDivisi($date,$divisi)
    {
        $this->select()->where('year', $date)->where('divisi',$divisi);
        return $this->get()->getResultArray();
    }

    public function BudgetDashboardDepartment($date,$department)
    {
        $this->select()->where('year', $date)->where('department',$department);
        return $this->get()->getResultArray();
    }

    public function getYearBudget(){
        $this->select('year')->orderBy('year', 'DESC')->distinct();
        return $this->get()->getResultArray();
    }

    public function getBudgetCurrent($department)
    {
        $year = date('Y');
        return $this->where('year', $year)->where('department', $department)->first();
    }

    public function getBudgetByYear($department,$year)
    {
        return $this->where('year', $year)->where('department', $department)->first();
    }

    //UNTUK BUDGET TAHUN DEPAN
    // UBAH date(Y) menjadi date(Y)+1
    public function getBudgetNextYear($department){
        $nextYear = date('Y');
        // $nextYear = date('Y')+1;
        return $this->where('year',$nextYear)->where('department',$department)->first();
    }

    public function getDataBudgetById($id)
    {
        return $this->where(['id_budget' => $id])->first();
    }

    public function getBudgetById($id){
        return $this->select()->where('id_budget',$id)->first();
    }

    public function getBudgetByDepartment($department){
        $this->select()->where('department', $department);
        return $this->get()->getResultArray();
    }

    public function getBudgetByDepartmentAndYear($department,$year){
        $this->select()->where('department', $department)->where('year',$year);
        return $this->get()->getResultArray();
    }

    public function getIdBudgetByDepartment($department){
        $this->select('id_budget')->where('department', $department);
        return $this->get()->getResultArray();
    }

    public function getIdBudgetByDepartmentAndYear($department,$year){
        return $this->select('id_budget')->where('department', $department)->where('year',$year)->first();;
    }
    
    public function getCrossBudget(){
        $this->select()->whereNotIn('department', ['AMDI AOP']);
        return $this->get()->getResultArray();
    }

    public function getAmdiAOPBudget(){
        $this->select()->whereIn('department', ['AMDI AOP']);
        return $this->get()->getResultArray();
    }
}