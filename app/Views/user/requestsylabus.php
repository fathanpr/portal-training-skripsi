<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<div class="success" data-success="<?= session()->get('success'); ?>"></div>

    <div class="row">
        <div class="col">
        <div class="card bg-light m-3">
            <div class="card-header">
                <div class="row">
                    <div class="col"><h3>Request Sylabus</h3></div>
                    <div class="col d-flex justify-content-end">
                        <button type="button" class="btn btn-transparent" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-table" aria-hidden="true"></i></button></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-2 text-bold">DIVISION / DEPARTMEN</div>
                    <div class="col-10">:
                    <?= (session()->get('departemen') == 'AMDI AOP') ? session()->get('divisi') : session()->get('departemen') ?></div>
                </div>
                <div class="row">
                    <div class="col-2 text-bold">DATE REQUEST</div>
                    <div class="col-10">: <?php echo date('Y-m-d') ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                    <form id="formSylabus" onsubmit="return validateForm(event);">
                        <!-- TAMBAH INPUT HIDDEN DISINI UNTUK DEPARTEMEN DAN DATE REQUEST -->
                        <!-- END -->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Judul Training<span style="color:red;"> *</span></label>
                            <input type="text" class="form-control" name="judul_training" placeholder="Contoh : Core Value" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jenis Training<span style="color:red;"> *</span></label>
                            <select class="form-control" name="jenis_training" id="jenis_training" required>
                                <option value="" disabled selected>Choose</option>
                                <?php foreach($jenis_training as $jt) : ?>
                                    <option value="<?= $jt['category']; ?>"><?= $jt['category']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pelaksanaan Training<span style="color:red;"> *</span></label>
                            <select class="form-control" name="pelaksanaan_training" id="pelaksanaan_training" required>
                                <option value="" disabled selected>Choose</option>
                                <option value="online">Online</option>
                                <option value="offline">Offline</option>
                                <option value="antara_keduanya">Antara Keduanya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Sasaran Peserta<span style="color:red;"> *</span></label>
                            <select class="form-control" name="sasaran_peserta" id="sasaran_peserta" required>
                                <option value="" disabled selected>Choose</option>
                                <option value="a">Golongan 1-3</option>
                                <option value="b">Golongan 4up</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Sasaran Departemen<span style="color:red;"> *</span></label>
                            <select class="form-control" name="sasaran_departemen" id="sasaran_departemen" required>
                                <option value="" disabled selected>Choose</option>
                                <?php foreach($departemen as $d) : ?>
                                    <option value="<?= $d; ?>"><?= $d; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kompetensi Berkaitan<span style="color:red;"> *</span></label>
                            <select class="form-control" name="kompetensi_berkaitan" id="kompetensi_berkaitan" required>
                            </select>
                            <textarea class="form-control" name="pilihan_kompetensi" id="pilihan_kompetensi" cols="30" rows="10" readonly required></textarea>
                        </div>
                        <div class="row pl-2 d-flex justify-content-end">
                            <div id="deleteButtonContainer"></div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tujuan Training<span style="color:red;"> *</span></label>
                            <textarea class="form-control" name="tujuan_training" id="tujuan_training" cols="30" rows="10" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Deskripsi Materi<span style="color:red;"> *</span></label>
                            <textarea class="form-control" name="deskripsi_materi" id="deskripsi_materi" cols="30" rows="10" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Metode Training<span style="color:red;"> *</span></label>
                            <div class="form-check">
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="lecturing">
                                <label class="form-check-label mr-4" for="exampleCheck1">Lecturing</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="case_study">
                                <label class="form-check-label mr-4" for="exampleCheck1">Case Study</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="discussion">
                                <label class="form-check-label mr-4" for="exampleCheck1">Discussion</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="sharing">
                                <label class="form-check-label mr-4" for="exampleCheck1">Sharing</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="role_play">
                                <label class="form-check-label mr-4" for="exampleCheck1">Role Play</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="computer_practice">
                                <label class="form-check-label mr-4" for="exampleCheck1">Computer Practice</label>
                                <input type="checkbox" name="metode_training[]" class="form-check-input" value="simulation">
                                <label class="form-check-label mr-4" for="exampleCheck1">Simulation</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label label for="exampleInputEmail1">Durasi Training<span style="color:red;"> *</span>(total jam)</label>
                            <input type="number" name="durasi_training" id="durasi_training" class="form-control" required min="1" oninput="validity.valid||(value='');">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Kelengkapan yang dibutuhkan<span style="color:red;"> *</span></label>
                            <div class="form-check">
                                <input type="checkbox" name="kelengkapan[]" class="form-check-input" value="laptop">
                                <label class="form-check-label mr-4" for="exampleCheck1">Laptop</label>
                                <input type="checkbox" name="kelengkapan[]" class="form-check-input" value="papan_tulis">
                                <label class="form-check-label mr-4" for="exampleCheck1">Papan Tulis</label>
                                <input type="checkbox" name="kelengkapan[]" class="form-check-input" value="proyektor">
                                <label class="form-check-label mr-4" for="exampleCheck1">Proyektor</label>
                                <input type="checkbox" name="kelengkapan[]" class="form-check-input" value="lainnya">
                                <label class="form-check-label mr-4" for="exampleCheck1">Lainnya</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Metode Evaluasi<span style="color:red;"> *</span></label>
                            <div class="form-check">
                                <input type="checkbox" name="metode_evaluasi[]" class="form-check-input" value="evaluasi_reaksi">
                                <label class="form-check-label mr-4" for="exampleCheck1">Evaluasi Reaksi</label>
                                <input type="checkbox" name="metode_evaluasi[]" class="form-check-input" value="evaluasi_pembelajaran">
                                <label class="form-check-label mr-4" for="exampleCheck1">Evaluasi Pembelajaran (Pre dan Post Test)</label>
                                <input type="checkbox" name="metode_evaluasi[]" class="form-check-input" value="evaluasi_efektivitas">
                                <label class="form-check-label mr-4" for="exampleCheck1">Evaluasi Efektivitas (3 Bulan) </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Notes<span style="color:red;"> *</span></label>
                            <textarea class="form-control" name="notes" id="notes" cols="30" rows="10" placeholder="Mohon di isi dengan rencana pelaksanaan training terdekat dan lokasi pelaksanaan training apabila selain di ruang meeting" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">History Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
                <table class="table table-hover dataTable dtr-inline" id="dataRS">
                    <thead>
                        <th>Judul Training</th>
                        <th>Tanggal Request Training</th>
                        <th>Pelaksanaan</th>
                        <th>Sasaran Peserta</th>
                        <th>Sasaran Departemen</th>
                        <th>Notes</th>
                    </thead>
                    <tbody>
                        <?php foreach($data_request as $item) : ?>
                            <tr>
                                <td><?= $item['judul'] ?></td>
                                <td><?= $item['tanggal_request'] ?></td>
                                <td><?= ucfirst($item['pelaksanaan']) ?></td>
                                <td><?= $item['sasaran_peserta'] == 'a' ? 'Golongan 1-3' : 'Golongan 4up' ?></td>
                                <td><?= $item['sasaran_departemen'] ?></td>
                                <td><?= $item['notes'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>

    <script>
        $(document).ready(function(){
            $('#sasaran_peserta').change(function(){
                $('#kompetensi_berkaitan').empty();
                $('#pilihan_kompetensi').val('');
                $('#sasaran_departemen').trigger('change');
            });

            $('#sasaran_departemen').change(function(){
                var golongan = $('#sasaran_peserta').val();
                var id = $(this).val();
                console.log(golongan, id);

                if (golongan == 'b') {
                    url = "<?= base_url() ?>get_competency_a/" + id;
                } else if (golongan == 'a') {
                    url = "<?= base_url() ?>get_competency_b/" + id;
                } else {
                    url = ''
                }

                $.ajax({
                    url: url,
                    method: "GET",
                    dataType: "JSON",
                    success: function(data){
                        var html = '';
                        for(var i=0; i<data.length; i++){
                            html += '<option value="'+data[i].category+'">'+data[i].category+'</option>';
                        }
                        $('#kompetensi_berkaitan').html(html);
                    }
                });
            });

            $('#kompetensi_berkaitan').change(function(){
                var selectedOption = $(this).val();
                var currentText = $('#pilihan_kompetensi').val();
                var deleteButton = '<button type="button" id="deleteCompetency" class="btn btn-danger mr-2" onclick="deleteLastCompetency()">Delete Last Row</button>';

                $('#pilihan_kompetensi').val(currentText + selectedOption + ',\n');
                $('#deleteButtonContainer').html(deleteButton);
            });
        });

        function deleteLastCompetency() {
            var currentText = $('#pilihan_kompetensi').val();
            var lines = currentText.split('\n');
            lines.splice(-2, 1);
            var newText = lines.join('\n');
            $('#pilihan_kompetensi').val(newText);

            if (newText.trim() === '') {
                $('#deleteButtonContainer').empty();
            }
        }

        function validateForm(event) {
            event.preventDefault();

            var checkedMetode = $('input[name="metode_training[]"]:checked').length;
            var checkedKelengkapan = $('input[name="kelengkapan[]"]:checked').length;
            var checkedEvaluasi = $('input[name="metode_evaluasi[]"]:checked').length;

            if (checkedMetode < 1 || checkedKelengkapan < 1 || checkedEvaluasi < 1) {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Pilihlah checkbox minimal 1!',
            });
            } else {
                $.ajax({
                type: 'POST',
                url: "<?= base_url() ?>submit_request_sylabus",
                data: $('#formSylabus').serialize(),
                success: function(response) {
                    console.log(response);
                    Swal.fire({
                        icon: 'success',
                        title: 'Request sylabus berhasil dikirim!',
                        text: 'Silahkan tunggu konfirmasi dari admin terkait sylabus baru yang diajukan.',
                    }).then(function (){
                        $('#formSylabus')[0].reset();
                        $(`#kompetensi_berkaitan`).empty();
                        $('#deleteButtonContainer').empty();
                    });
                    
                },
                error: function(error) {
                    console.log(error);

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan saat mengirim data!',
                    });
                }
                });
            }
        }
    </script>

<?= $this->endSection() ?>