<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\UserModel;

class History extends BaseController
{
    private M_Tna $tna;
    private UserModel $user;
    private M_TnaUnplanned $unplanned;


    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
        $this->unplanned = new M_TnaUnplanned();
    }

    public function index()
    {
        $id = session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');
        $history = $this->tna->getDetailHistory($npk,$bagian);
        $data = [
            'tittle' => 'Personal History Training',
            'history' => $history
        ];
        return view('user/historypersonal', $data);
    }

    public function memberHistory()
    {
        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');
        $seksi = session()->get('seksi');


        if ($bagian == 'BOD') {
            $status =  $this->tna->getMemberHistory($bagian, $dic);
            $member = $this->user->newFilter($bagian,$dic);
        } elseif ($bagian == 'KADIV') {
            $status =  $this->tna->getMemberHistory($bagian, $divisi);
            $member = $this->user->newFilter($bagian,$divisi);
        } elseif ($bagian == 'KADEPT') {
            $status = $this->tna->getMemberHistory($bagian, $departemen);
            $member = $this->user->newFilter($bagian,$departemen);
        } elseif ($bagian == 'KASIE'){
            $status = $this->tna->getMemberHistory($bagian, $seksi);
            $member = $this->user->newFilter($bagian,$seksi);
        }else {
            $status  =  array();
        }


        // dd($status[0]);
        // $user = $this->user->getAllUser();
        $DataHistory = [];

        if (empty($status[0]['id_user'])) {
            $history = [
                'id' => '',
                'nama' => '',
                'npk' => '',
                'bagian' => '',
                'jumlah_training' => ''
            ];
        } else {
            $training = $this->tna->getTnaUserHistory($status[0]['npk'],$status[0]['bagian']);
            $history = [
                'id' => $status[0]['id_user'],
                'nama' => $status[0]['nama'],
                'npk' => $status[0]['npk'],
                'bagian' => $status[0]['bagian'],
                'jumlah_training' => $training[0]['npk']
            ];
        }

        $history = [];

        foreach ($member as $item) {
            $id_user = $item['id_user'];
            $npk = $item['npk'];
            $bagian = $item['bagian'];
            $training = $this->tna->getTnaUserHistory($npk,$bagian);

            $history[] = [
                'id' => $id_user,
                'npk' => $npk,
                'bagian' => $bagian,
                'nama' => $item['nama'],
                'jumlah_training' => $training[0]['npk']
            ];
        }

        array_push($DataHistory, $history);


        $data = [
            'tittle' => 'Member History Training',
            'user' => $history,
        ];
        return view('user/memberhistory', $data);
    }

    public function download($id)
    {
        // dd($id);
        //$sertifikat = $this->tna->getDataHistory($id);
        // dd($sertifikat[0]['sertifikat']);
        header('Content-type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        // readfile(base_url() . "\public" . "$sertifikat[0]['sertifikat']");

        // var_dump($sertifikat[0]['sertifikat']);
        // $data = [
        //     'tittle' => 'Sertifikat View',
        //     'sertifikat' => $sertifikat[0]['sertifikat']
        // ];
        // return view('user/viewsertifikat', $data);
    }


    public function detailHistoryMember()
    {
        $id = $_POST['history'];
        $npk = $_POST['npk'];
        $bagian = $_POST['bagian'];
        $history = $this->tna->getDetailHistory($npk,$bagian);
        $data = [
            'tittle' => 'Member History Training',
            'history' => $history
        ];
        return view('user/detailhistorymember', $data);
    }
}