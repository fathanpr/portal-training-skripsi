<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Technical extends Model
{
    protected $table      = 'technical_competency';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_technical';
    protected $allowedFields = ['proficiency', 'nama_jabatan', 'technical', 'departemen', 'golongan'];

    public function getDataTechnical()
    {
        $this->select();
        return $this->get()->getResultArray();
    }

    public function getDataTechnicalDepartemen($departemen)
    {
        $this->select()->where('departemen', $departemen);
        return $this->get()->getResultArray();
    }

    public function getDataTechnicalUpdate($departemen,$nama_jabatan,$type_user)
    {
        if(trim($type_user) == 'REGULAR'){
            $type_user = NULL;
        }else{
            $type_user = 'EXPERT';
        };

        $this->select()->where('departemen', $departemen)->where('nama_jabatan', trim($nama_jabatan))->where('type_user', $type_user);
        return $this->get()->getResultArray();
    }

    public function getDataTechnicalDepartemenJabatan($departemen,$jabatan)
    {
        $this->select()->where('departemen', $departemen)->where('nama_jabatan',$jabatan)->where('type_user',NULL);
        return $this->get()->getResultArray();
    }
    public function getDataTechnicalExpert($departemen,$jabatan)
    {
        $this->select()->where('departemen', $departemen)->where('nama_jabatan',$jabatan)->where('type_user','EXPERT');
        return $this->get()->getResultArray();
    }

    public function getTechnicalLastRow()
    {
        $this->select('id_technical');
        return $this->get()->getLastRow();
    }

    public function getCompetencyTechnicalDepartment()
    {
        $this->select('departemen')->distinct();
        return $this->get()->getResultArray();
    }
}