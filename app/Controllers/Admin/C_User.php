<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Astra;
use App\Models\M_Career;
use App\Models\M_Company;
use App\Models\M_CompetencyAstra;
use App\Models\M_CompetencyCompany;
use App\Models\M_CompetencyExpert;
use App\Models\M_CompetencySoft;
use App\Models\M_CompetencyTechnical;
use App\Models\M_CompetencyTechnicalB;
use App\Models\M_Education;
use App\Models\M_Expert;
use App\Models\M_Soft;
use App\Models\M_Technical;
use App\Models\M_TechnicalB;
use App\Models\UserModel;
use function PHPUnit\Framework\isEmpty;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\FilterType;

class C_User extends BaseController
{
    private UserModel $user;
    private M_Education $education;
    private M_Career $career;

    private M_Astra $astra;

    private M_Technical $technical;

    private M_Expert $expert;
    private M_Company $company;

    private M_TechnicalB $technicalB;

    private M_Soft $soft;

    private M_CompetencyAstra $competencyAstra;
    private M_CompetencyTechnical $competencyTechnical;
    private M_CompetencyExpert $competencyExpert;
    private M_CompetencyCompany $competencyCompany;
    private M_CompetencyTechnicalB $competencyTechnicalB;
    private M_CompetencySoft $competencySoft;

    private $db;
    public function __construct()
    {
        $this->user = new UserModel();
        $this->career = new M_Career();
        $this->education = new M_Education();
        $this->astra = new M_Astra();
        $this->technical = new M_Technical();
        $this->expert = new M_Expert();
        $this->company = new M_Company();
        $this->technicalB = new M_TechnicalB();
        $this->soft = new M_Soft();
        $this->competencyAstra = new M_CompetencyAstra();
        $this->competencyExpert = new M_CompetencyExpert();
        $this->competencyTechnical = new M_CompetencyTechnical();
        $this->competencyCompany = new M_CompetencyCompany();
        $this->competencyTechnicalB = new M_CompetencyTechnicalB();
        $this->competencySoft = new M_CompetencySoft();
        $this->db = db_connect();
    }
    public function index()
    {
        $user = $this->user->getAllUser();
        $seksi = $this->user->DistinctSeksi();
        $dic = $this->user->DistinctDic();
        $departemen = $this->user->DistinctDepartemen();
        $divisi = $this->user->DistinctDivisi();
        $bagian = $this->user->getUserBagian();
        $jabatan = $this->user->getUserJabatan();
        // dd($dic,$divisi,$departemen,$seksi);
        $data = [
            'tittle' => 'User',
            'user' => $user,
            'SEKSI' => $seksi,
            'DIC' => $dic,
            'DIVISI' => $divisi,
            'DEPARTEMEN' => $departemen,
            'BAGIAN' => $bagian,
            'JABATAN' => $jabatan,
        ];
        return view('admin/user', $data);
    }

    public function getDivisiByDic(){
        $selectedDIC = $this->request->getPost("dic");

        $division = $this->user->DistinctDivisiByDic($selectedDIC);
        return $this->response->setJSON($division);
    }

    public function getDepartemenByDivisi(){
        $selectedDivision = $this->request->getPost("divisi");
        $department = $this->user->DistinctDepartemenByDivisi($selectedDivision);
        return $this->response->setJSON($department);
    }

    public function getSeksiByDepartemen(){
        $selectedDepartment= $this->request->getPost("departemen");
        $section = $this->user->DistinctSeksiByDepartemen($selectedDepartment);
        return $this->response->setJSON($section);
    }

    public function getBagianBySeksi(){
        $selectedSection = $this->request->getPost("bagian");
        $bagian = $this->user->DistinctBagianBySeksi($selectedSection);
        return $this->response->setJSON($bagian);
    }

    public function singleUser()
    {
        $level = $this->request->getVar('level');
        $group = $this->request->getVar('group');
        $department = $this->request->getVar('departemen');
        $type_user = $this->request->getvar('type_user');
        // dd($type_user);
        if ($level == "ADMIN") {
            $data = [
                'nama' => $this->request->getVar('nama'),
                'npk' => $this->request->getVar('npk'),
                'seksi' => $this->request->getVar('seksi'),
                'status' => $this->request->getVar('status'),
                'dic' => $this->request->getVar('dic'),
                'divisi' => $this->request->getVar('divisi'),
                'departemen' => $department,
                'bagian' => $this->request->getVar('bagian'),
                'nama_jabatan' => $this->request->getVar('jabatan'),
                'level' => $level,
                'username' => $this->request->getVar('username'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                'email' => $this->request->getVar('email'),
            ];
            $this->user->save($data);
        } else {
            $image = $this->request->getFile('image');
            if ($image->isValid()) {
                $image->getName();
                $image->getClientExtension();
                $newName = $image->getRandomName();
                $image->move("../public/profile", $newName);
                $filepath = "/profile/" . $newName;
                $data = [
                    'nama' => $this->request->getVar('nama'),
                    'npk' => $this->request->getVar('npk'),
                    'status' => $this->request->getVar('status'),
                    'seksi' => $this->request->getVar('seksi'),
                    'dic' => $this->request->getVar('dic'),
                    'divisi' => $this->request->getVar('divisi'),
                    'departemen' => $this->request->getVar('departemen'),
                    'bagian' => $this->request->getVar('bagian'),
                    'nama_jabatan' => $this->request->getVar('jabatan'),
                    'level' => $this->request->getVar('level'),
                    'username' => $this->request->getVar('username'),
                    'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                    'email' => $this->request->getVar('email'),
                    'profile' => $filepath,
                    'type_golongan' => $group,
                    'type_user' => $type_user,
                ];
            } else {
                $data = [
                    'nama' => $this->request->getVar('nama'),
                    'npk' => $this->request->getVar('npk'),
                    'status' => $this->request->getVar('status'),
                    'seksi' => $this->request->getVar('seksi'),
                    'dic' => $this->request->getVar('dic'),
                    'divisi' => $this->request->getVar('divisi'),
                    'departemen' => $this->request->getVar('departemen'),
                    'bagian' => $this->request->getVar('bagian'),
                    'nama_jabatan' => $this->request->getVar('jabatan'),
                    'level' => $this->request->getVar('level'),
                    'username' => $this->request->getVar('username'),
                    'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                    'email' => $this->request->getVar('email'),
                    'type_golongan' => $group,
                    'type_user' => $type_user,
                ];
            }

            $this->user->save($data);
            $last = $this->user->getLastUser();
            // dd($last);
            $education = [
                'id_user' => $last->id_user,
                'npk' => $last->npk,
                'grade' => $this->request->getVar('grade'),
                'year' => $this->request->getVar('year'),
                'institution' => $this->request->getVar('institution'),
                'major' => $this->request->getVar('major'),

            ];
            $career = [
                'id_user' => $last->id_user,
                'npk' => $last->npk,
                'year_start' => $this->request->getVar('year_start'),
                'year_end' => $this->request->getVar('year_end'),
                'position' => $this->request->getVar('position'),
                'department' => $this->request->getVar('department'),
                'division' => $this->request->getVar('division'),
                'company' => $this->request->getVar('company'),
            ];
            $this->education->save($education);
            $this->career->save($career);

            $user = $this->user->get_all_user_with_npk($last->id_user, $last->npk);
            //d($user);
            if ($group == 'A') {
                if ($type_user == 'REGULAR') {
                    $AstraList = $this->astra->getDataAstra();
                    foreach ($AstraList as $list) {
                        $Astra = [
                            'id_user' => $last->id_user,
                            'id_astra' => $list['id_astra'],
                            'score_astra' => 0,
                        ];
                        // save d($Astra);
                        $this->competencyAstra->save($Astra);
                    }
                    $departmentUser = $this->technical->getDataTechnicalDepartemenJabatan($user['departemen'],$user['nama_jabatan']);
                    if (!empty($departmentUser)) {
                        foreach ($departmentUser as $departmentUsers) {
                            $technical = [
                                'id_user' => $last->id_user,
                                'npk' => $last->npk,
                                'id_technical' => $departmentUsers['id_technical'],
                                'score_technical' => 0,
                            ];
                            // save technical A
                            $this->competencyTechnical->save($technical);
                        }
                    }
                } else {
                    $ExpertList = $this->expert->getDataExpert();
                    foreach ($ExpertList as $list) {
                        $Expert = [
                            'id_user' => $last->id_user,
                            'npk' => $last->npk,
                            'id_expert' => $list['id_expert'],
                            'score_expert' => 0,
                        ];
                        //save
                        $this->competencyExpert->save($Expert);
                    }
                    $departmentUser = $this->technical->getDataTechnicalExpert($user['departemen'],$user['nama_jabatan']);
                    if (!empty($departmentUser)) {
                        foreach ($departmentUser as $departmentUsers) {
                            $technical = [
                                'id_user' => $last->id_user,
                                'npk' => $last->npk,
                                'id_technical' => $departmentUsers['id_technical'],
                                'type_technical' => 'EXPERT',
                                'score_technical' => 0,
                            ];
                            // save
                            $this->competencyTechnical->save($technical);
                        }
                    }
                }
            } else {
                $CompanyList = $this->company->getDataCompanyDivisi($user['divisi']);
                foreach ($CompanyList as $Clist) {
                    $company = [
                        'id_user' => $last->id_user,
                        'npk' => $last->npk,
                        'id_company' => $Clist['id_company'],
                        'score_company' => 0,
                    ];
                    //save
                    $this->competencyCompany->save($company);
                }
                // Update
                $TechnicalBList = $this->technicalB->getDataTechnicalBDepartemenSeksi($user['departemen'], $user['nama_jabatan'],$user['seksi']);
                foreach ($TechnicalBList as $TBList) {
                    $TechnicalB = [
                        'id_user' => $last->id_user,
                        'npk' => $last->npk,
                        'id_technicalB' => $TBList['id_technicalB'],
                        'score' => 0,
                    ];
                    // save
                    $this->competencyTechnicalB->save($TechnicalB);
                }
                $SoftList = $this->soft->getDataSoft();
                foreach ($SoftList as $SList) {
                    $Soft = [
                        'id_user' => $last->id_user,
                        'npk' => $last->npk,
                        'id_soft' => $SList['id_soft'],
                        'score_soft' => 0,
                    ];
                    // save
                    $this->competencySoft->save($Soft);
                }
            }
        }
        session()->setFlashdata('success', 'Data Saved Successfully');
        return redirect()->to('/user');
    }

    public function addUser()
    {

        ini_set('max_execution_time', 900);
        $file = $this->request->getFile('file');
        //dd($file);
        if ($file == "") {
            return redirect()->to('/user');
        }
        $ext = $file->getClientExtension();
        if ($ext == 'xls') {
            $render = new
                \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new
                \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $render->load($file);
        $sheet = $spreadsheet->getActiveSheet()->toArray();

        $user = [];
        $total = count($sheet) - 1;
        for ($i = 1; $i <= $total; $i++) {
            // URUTAN $data diubah 
            $data = [
                'npk' => $sheet[$i][0],
                'nama' => $sheet[$i][1],
                'status' => $sheet[$i][2],
                'dic' => $sheet[$i][3],
                'divisi' => $sheet[$i][4],
                'departemen' => $sheet[$i][5],
                'seksi' => $sheet[$i][6],
                'bagian' => $sheet[$i][7],
                'nama_jabatan' => $sheet[$i][8],
                'username' => $sheet[$i][9],
                'password' => password_hash($sheet[$i][10], PASSWORD_DEFAULT),
                'level' => $sheet[$i][11],
                'type_golongan' => $sheet[$i][12],
                'type_user' => $sheet[$i][13],
            ];
            $this->user->save($data);

            $last = $this->user->getLastUser();
            $user = $this->user->get_all_user_with_npk($last->id_user,$last->npk);
            if (trim($user['type_golongan']) == 'A') {
                if(trim($user['type_user']) == 'EXPERT'){
                    $ExpertList = $this->expert->getDataExpert();
                    foreach ($ExpertList as $list) {
                        $existingExpert =  $this->competencyExpert->getExpertById($list['id_expert'],$last->npk);
                        if(empty($existingExpert)){
                            $Expert = [
                                'id_user' => $last->id_user,
                                'npk' => $last->npk,
                                'id_expert' => $list['id_expert'],
                                'score_expert' => 0,
                            ];
                            $this->competencyExpert->save($Expert);
                        }
                    }
                    // dd($Expert);
                    $departmentUser = $this->technical->getDataTechnicalExpert($user['departemen'],$user['nama_jabatan']);
                    if (!empty($departmentUser)) {
                        foreach ($departmentUser as $departmentUsers) {
                            $existingTechnicalExpert = $this->competencyTechnical->getTechnicalExpertId($departmentUsers['id_technical'],$last->npk);
                            if(empty($existingTechnicalExpert)){
                                $technical = [
                                    'id_user' => $last->id_user,
                                    'npk' => $last->npk,
                                    'id_technical' => $departmentUsers['id_technical'],
                                    'type_technical' => 'EXPERT',
                                    'score_technical' => 0,
                                ];
                                $this->competencyTechnical->save($technical);
                            }
                        }
                    }
                }
                else {
                    $AstraList = $this->astra->getDataAstra();
                    foreach ($AstraList as $list) {
                        $existingAstra = $this->competencyAstra->getAstraId($list['id_astra'],$last->npk);
                        // dd($existingAstra);
                        if(empty($existingAstra)){
                            $Astra = [
                                'id_user' => $last->id_user,
                                'npk' => $last->npk,
                                'id_astra' => $list['id_astra'],
                                'score_astra' => 0,
                            ];
                            $this->competencyAstra->save($Astra);
                        }
                    }
                    $departmentUser = $this->technical->getDataTechnicalDepartemenJabatan($user['departemen'],$user['nama_jabatan']);
                    if (!empty($departmentUser)) {
                        foreach ($departmentUser as $departmentUsers) {
                            $existingTechnical = $this->competencyTechnical->getTechnicalId($departmentUsers['id_technical'],$last->npk);
                            if(empty($existingTechnical)){
                                $technical = [
                                    'id_user' => $last->id_user,
                                    'npk' => $last->npk,
                                    'id_technical' => $departmentUsers['id_technical'],
                                    'score_technical' => 0,
                                ];
                                $this->competencyTechnical->save($technical);
                            }
                        }
                    }
                } 

            } 
            else {
                $CompanyList = $this->company->getDataCompanyDivisi($user['divisi']);
                foreach ($CompanyList as $Clist) {
                    $existingCompany = $this->competencyCompany->getCompanyById($Clist['id_company'],$last->npk);
                    if(empty($existingCompany)){
                        $company = [
                            'id_user' => $last->id_user,
                            'npk' => $last->npk,
                            'id_company' => $Clist['id_company'],
                            'score_company' => 0,
                        ];
                        $this->competencyCompany->save($company);
                    }
                }
                $TechnicalBList = $this->technicalB->getDataTechnicalBDepartemenSeksi($user['departemen'], $user['nama_jabatan'],$user['seksi']);
                foreach ($TechnicalBList as $TBList) {
                    $existingTechnicalB = $this->competencyTechnicalB->getTechnicalBId($TBList['id_technicalB'],$last->npk);
                    if(empty($existingTechnicalB)){
                        $TechnicalB = [
                            'id_user' => $last->id_user,
                            'npk' => $last->npk,
                            'id_technicalB' => $TBList['id_technicalB'],
                            'score' => 0,
                        ];
                        $this->competencyTechnicalB->save($TechnicalB);
                    }
                }
                $SoftList = $this->soft->getDataSoft();
                foreach ($SoftList as $SList) {
                    $existingSoft = $this->competencySoft->getSoftId($SList['id_soft'],$last->npk);
                    if(empty($existingSoft)){
                        $Soft = [
                            'id_user' => $last->id_user,
                            'npk' => $last->npk,
                            'id_soft' => $SList['id_soft'],
                            'score_soft' => 0,
                        ];
                        $this->competencySoft->save($Soft);
                    }
                }
            }
        }
        // $this->db->transCommit();

        session()->setFlashdata('success', 'Data Berhasil Di Import');
        return redirect()->to('/user');
    }

    public function update()
    {
        $id = $this->request->getVar('id_user');
        $npk = $this->request->getVar('npk');
        $user = $this->user->getAllUser($id);
        $dic = $this->user->DistinctDic();
        $status = $this->user->DistinctStatus();
        $divisi = $this->user->DistinctDivisi();
        $departemen = $this->user->DistinctDepartemen();
        $seksi = $this->user->DistinctSeksi();
        $jabatan = $this->user->DistinctJabatan();
        $bagian = $this->user->DistinctBagian();
        $type_golongan = $this->user->getTypeGolongan();
        $type_user = $this->user->getTypeUser();

        // $this->EquateArray($id,$profile[1]);
        // dd($this->EquateArray($id,$npk));

        $data = [
            'tittle' => 'Edit User',
            'user' => $this->user->getAllUser($id),
            'education' => $this->education->getDataEducation($user['npk']),
            'career' => $this->career->getDataCareer($user['npk']),
            'dic' => $dic,
            'status'=>$status,
            'divisi' => $divisi,
            'departemen' => $departemen,
            'seksi' => $seksi,
            'bagian' => $bagian,
            'jabatan' => $jabatan,
            'type_golongan' => $type_golongan,
            'type_user' => $type_user
        ];

        return view('admin/edituser', $data);
    }


    //stuck 

    public function EditUser()
    {
        $dataFixes = [];
        $profile = $this->request->getPost('individual');
        $Equate = $this->EquateArray($profile[0],$profile[1]);
        $input = [
            'divisi' => $profile[5],
            'department' => $profile[6],
            'seksi' => $profile[7],
            'nama_jabatan' => $profile[17],
            'type_golongan' => trim($profile[9]),
            'type_user' => trim($profile[10])
        ];

        //Add New Competency On New User Structure
        // $compare = array_diff($input, $Equate);
        if(trim($input['type_golongan']) == 'A' && trim($input['type_user']) == 'REGULAR'){
            //Astra Update
            $check_astra = $this->astra->getDataAstra();
            $check_competency_profile_astra = $this->competencyAstra->getAstraIdCompetency($profile[0],$profile[1]);
            $add_new_astra = [];

            if (!empty($check_competency_profile_astra)) {
                foreach ($check_astra as $astra) {
                    $found = false;
                    foreach ($check_competency_profile_astra as $competency) {
                        if ($astra['id_astra'] == $competency['id_astra']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $add_new_astra[] = $astra['id_astra'];
                    }
                }
                $data_astra = [];
                foreach ($add_new_astra as $astra) {
                    $data_astra = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_astra' => $astra,
                        'score_astra' => 0,
                    ];
                    $this->competencyAstra->save($data_astra);
                }
            } else {
                foreach ($check_astra as $astra) {
                    $add_new_astra[] = $astra['id_astra'];
                }
                $data_astra = [];
                foreach ($add_new_astra as $astra) {
                    $data_astra = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_astra' => $astra,
                        'score_astra' => 0,
                    ];
                    $this->competencyAstra->save($data_astra);
                }
            }

            //Technical Update
            $check_technical = $this->technical->getDataTechnicalUpdate($input['department'],$input['nama_jabatan'],$input['type_user']);
            $check_competency_profile_technical = $this->competencyTechnical->getTechnicalIdCompetency($profile[0],$profile[1]);
            $add_new_technical = [];

            if (!empty($check_competency_profile_technical)) {
                foreach ($check_technical as $technical) {
                    $found = false;
                    foreach ($check_competency_profile_technical as $competency) {
                        if ($technical['id_technical'] == $competency['id_technical']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $add_new_technical[] = $technical['id_technical'];
                    }
                }
                $data_technical = [];
                foreach ($add_new_technical as $technical) {
                    $data_technical = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_technical' => $technical,
                        'score_technical' => 0,
                    ];
                    $this->competencyTechnical->save($data_technical);
                }
            } else {
                foreach ($check_technical as $technical) {
                    $add_new_technical[] = $technical['id_technical'];
                }
                $data_technical = [];
                foreach ($add_new_technical as $technical) {
                    $data_technical = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_technical' => $technical,
                        'score_technical' => 0,
                    ];
                    $this->competencyTechnical->save($data_technical);
                }
            }
            $status_update = 'Astra dan Technical Berhasil Di Update (REGULAR)';

        } elseif (trim($input['type_golongan']) == 'A' && trim($input['type_user']) == 'EXPERT'){
            //Expert Update
            $check_expert = $this->expert->getDataExpert();
            $check_competency_profile_expert = $this->competencyExpert->getExpertIdCompetency($profile[0],$profile[1]);
            $add_new_expert = [];

            if (!empty($check_competency_profile_expert)) {
                foreach ($check_expert as $expert) {
                    $found = false;
                    foreach ($check_competency_profile_expert as $competency) {
                        if ($expert['id_expert'] == $competency['id_expert']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $add_new_expert[] = $expert['id_expert'];
                    }
                }
                $data_expert = [];
                foreach ($add_new_expert as $expert) {
                    $data_expert = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_expert' => $expert,
                        'score_expert' => 0,
                    ];
                    $this->competencyExpert->save($data_expert);
                }
            } else {
                foreach ($check_expert as $expert) {
                    $add_new_expert[] = $expert['id_expert'];
                }
                $data_expert = [];
                foreach ($add_new_expert as $expert) {
                    $data_expert = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_expert' => $expert,
                        'score_expert' => 0,
                    ];
                    $this->competencyExpert->save($data_expert);
                }
            }

             //Technical Update
             $check_technical = $this->technical->getDataTechnicalUpdate($input['department'],$input['nama_jabatan'],$input['type_user']);
             $check_competency_profile_technical = $this->competencyTechnical->getTechnicalIdCompetency($profile[0],$profile[1]);
             $add_new_technical = [];
 
             if (!empty($check_competency_profile_technical)) {
                 foreach ($check_technical as $technical) {
                     $found = false;
                     foreach ($check_competency_profile_technical as $competency) {
                         if ($technical['id_technical'] == $competency['id_technical']) {
                             $found = true;
                             break;
                         }
                     }
                     if (!$found) {
                         $add_new_technical[] = $technical['id_technical'];
                     }
                 }
                 $data_technical = [];
                 foreach ($add_new_technical as $technical) {
                     $data_technical = [
                         'id_user' => $profile[0],
                         'npk' => $profile[1],
                         'id_technical' => $technical,
                         'score_technical' => 0,
                     ];
                     $this->competencyTechnical->save($data_technical);
                 }
             } else {
                 foreach ($check_technical as $technical) {
                     $add_new_technical[] = $technical['id_technical'];
                 }
                 $data_technical = [];
                 foreach ($add_new_technical as $technical) {
                     $data_technical = [
                         'id_user' => $profile[0],
                         'npk' => $profile[1],
                         'id_technical' => $technical,
                         'score_technical' => 0,
                     ];
                     $this->competencyTechnical->save($data_technical);
                 }
             }
            $status_update = 'Expert dan Technical Expert Berhasil Di Update';

        } else{
            // Soft Update
            $check_soft = $this->soft->getDataSoft();
            $check_competency_profile_soft = $this->competencySoft->getSoftIdCompetency($profile[0],$profile[1]);
            $add_new_soft = [];

            if (!empty($check_competency_profile_soft)) {
                foreach ($check_soft as $soft) {
                    $found = false;
                    foreach ($check_competency_profile_soft as $competency) {
                        if ($soft['id_soft'] == $competency['id_soft']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $add_new_soft[] = $soft['id_soft'];
                    }
                }
                $data_soft = [];
                foreach ($add_new_soft as $soft) {
                    $data_soft = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_soft' => $soft,
                        'score_soft' => 0,
                    ];
                    $this->competencySoft->save($data_soft);
                }
            } else {
                foreach ($check_soft as $soft) {
                    $add_new_soft = $soft['id_soft'];
                }
                $data_soft = [];
                foreach ($add_new_soft as $soft) {
                    $data_soft = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_soft' => $soft,
                        'score_soft' => 0,
                    ];
                    $this->competencySoft->save($data_soft);
                }
            }

            //Company Update
            $check_company = $this->company->getDataCompanyDivisi($input['divisi']);
            $check_competency_profile_company = $this->competencyCompany->getCompanyIdCompetency($profile[0],$profile[1]);
            $add_new_company = [];

            if (!empty($check_competency_profile_company)) {
                foreach ($check_company as $company) {
                    $found = false;
                    foreach ($check_competency_profile_company as $competency) {
                        if ($company['id_company'] == $competency['id_company']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $add_new_company[] = $company['id_company'];
                    }
                }
                $data_company = [];
                foreach ($add_new_company as $company) {
                    $data_company = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_company' => $company,
                        'score_company' => 0,
                    ];
                    $this->competencyCompany->save($data_company);
                }
            } else {
                foreach ($check_company as $company) {
                    $add_new_company[] = $company['id_company'];
                }
                $data_company = [];
                foreach ($add_new_company as $company) {
                    $data_company = [
                        'id_user' => $profile[0],
                        'npk' => $profile[1],
                        'id_company' => $company,
                        'score_company' => 0,
                    ];
                    $this->competencyCompany->save($data_company);
                }
            }

            $check_technical = $this->technicalB->getDataByDepartmentSeksi($input['nama_jabatan'],$input['department'],$input['seksi']);
            $check_competency_profile_technical = $this->competencyTechnicalB->getTechnicalBIdCompetency($profile[0],$profile[1]);
            $add_new_technical = [];
 
             if (!empty($check_competency_profile_technical)) {
                 foreach ($check_technical as $technical) {
                     $found = false;
                     foreach ($check_competency_profile_technical as $competency) {
                         if ($technical['id_technicalB'] == $competency['id_technicalB']) {
                             $found = true;
                             break;
                         }
                     }
                     if (!$found) {
                         $add_new_technical[] = $technical['id_technicalB'];
                     }
                 }
                 $data_technical = [];
                 foreach ($add_new_technical as $technical) {
                     $data_technical = [
                         'id_user' => $profile[0],
                         'npk' => $profile[1],
                         'id_technicalB' => $technical,
                         'score' => 0,
                     ];
                     $this->competencyTechnicalB->save($data_technical);
                 }
             } else {
                 foreach ($check_technical as $technical) {
                     $add_new_technical[] = $technical['id_technicalB'];
                 }
                 $data_technical = [];
                 foreach ($add_new_technical as $technical) {
                     $data_technical = [
                         'id_user' => $profile[0],
                         'npk' => $profile[1],
                         'id_technicalB' => $technical,
                         'score' => 0,
                     ];
                     $this->competencyTechnicalB->save($data_technical);
                 }
             }
            $status_update = 'Soft, Company dan Technical B Berhasil Di Update';
        };

        // if (trim($input['type_golongan']) == 'A') {
        //     if (trim($input['type_user']) == 'REGULAR') {
        //         //$Astra = "Check";
        //         $old_competency = $this->competencyAstra->getAstraIdCompetency($profile[0],$profile[1]);
        //         //check data sudah ada atau belum
        //         if (!empty($old_competency)) {
        //             $Astra = $this->astra->getAllIdAstra();
        //             for ($i = 0; $i < count($old_competency); $i++) {
        //                 foreach ($Astra as $key => $values) {
        //                     if (in_array($values['id_astra'], $old_competency[$i])) {
        //                         unset($Astra[$key]);
        //                     }
        //                 }
        //             }
        //             foreach ($Astra as $astra) {
        //                 $DataAstra = [
        //                     'id_user' => $profile[0],
        //                     'npk' => $profile[1],
        //                     'id_astra' => $astra['id_astra'],
        //                     'score_astra' => 0
        //                 ];
        //                 // $this->competencyAstra->save($DataAstra);
        //                 //dd($DataAstra);
        //             }
        //         } else {
        //             $Astra = $this->astra->getAllIdAstra();
        //             foreach ($Astra as $AstraCompetency) {
        //                 $DataAstraEmpty = [
        //                     'id_astra' => $AstraCompetency['id_astra'],
        //                     'npk' => $AstraCompetency['npk'],
        //                     'id_user' => $profile[0],
        //                     'score_astra' => 0
        //                 ];

        //                 //save
        //                 // $this->competencyAstra->save($DataAstraEmpty);
        //             }
        //         }
        //     } else {
        //         $old_competency = $this->competencyExpert->getProfileExpertCompetency($profile[0],$profile[1]);
        //         // check data sudah ada atau belum
        //         if (!empty($old_competency)) {
        //             $Expert = $this->expert->getAllIdExpert();
        //             for ($i = 0; $i < count($old_competency); $i++) {
        //                 foreach ($Expert as $key => $values) {
        //                     if (in_array($values['id_expert'], $old_competency[$i])) {
        //                         unset($Expert[$key]);
        //                     }
        //                 }
        //             }
        //             foreach ($Expert as $expert) {
        //                 $DataExpert = [
        //                     'id_user' => $profile[0],
        //                     'npk' => $profile[1],
        //                     'id_astra' => $expert['id_expert'],
        //                     'score_expert' => 0
        //                 ];
        //                 // save
        //                 // $this->competencyExpert->save($DataExpert);
        //             }
        //         } else {
        //             $Expert = $this->expert->getAllIdExpert();
        //             //$test = 'Masuk';
        //             foreach ($Expert as $expert) {
        //                 $DataExpert = [
        //                     'id_user' => $profile[0],
        //                     'npk' => $profile[1],
        //                     'id_expert' => $expert['id_expert'],
        //                     'score_expert' => 0
        //                 ];
        //                 //save
        //                 // $this->competencyExpert->save($DataExpert);
        //             }
        //         }
        //     }
        // } else {
        //     $old_competency = $this->competencySoft->getProfileSoftCompetency($profile[0],$profile[1]);
        //     if (!empty($old_competency)) {
        //         $Soft = $this->soft->getAllIdSoft();
        //         for ($i = 0; $i < count($old_competency); $i++) {
        //             foreach ($Soft as $key => $values) {
        //                 if (in_array($values['id_soft'], $old_competency[$i])) {
        //                     unset($Soft[$key]);
        //                 }
        //             }
        //         }
        //         foreach ($Soft as $soft) {
        //             $DataSoft = [
        //                 'id_user' => $profile[0],
        //                 'npk' => $profile[1],
        //                 'id_soft' => $soft['id_soft'],
        //                 'score_soft' => 0
        //             ];
        //             //save 
        //             //$this->competencySoft->save($DataSoft);
        //         }
        //     } else {
        //         $Soft = $this->soft->getAllIdSoft();
        //         foreach ($Soft as $soft) {
        //             $DataSoft = [
        //                 'id_user' => $profile[0],
        //                 'npk' => $profile[1],
        //                 'id_soft' => $soft['id_soft'],
        //                 'score_soft' => 0
        //             ];
        //             //save 
        //             // $this->competencySoft->save($DataSoft);
        //         }
        //     }
        // }
        // //mengecek Apakah Divisi Diubah
        // if (array_key_exists("divisi", $compare)) {
        //     $old_divisi_competency = $this->competencyCompany->getProfileCompanyCompetencyDivision($profile[0],$profile[1], $compare['divisi']);
        //     //mengecek apakah pernah Berada Dalam Divisi Sebelumnya
        //     if (!empty($old_divisi_competency)) {
        //         $all_division_competency = $this->competencyCompany->getProfileCompanyCompetency($profile[0],$profile[1]);
        //         $SameDivision = [];

        //         //menghilangkan data yang sama 
        //         $Company = $this->company->getDataCompanyDivisi($compare['divisi']);
        //         for ($i = 0; $i < count($old_divisi_competency); $i++) {
        //             foreach ($Company as $key => $values) {
        //                 if (in_array($values['company'], $old_divisi_competency[$i])) {
        //                     unset($Company[$key]);
        //                 }
        //             }
        //         }

        //         //mengecek apakah variable $Company Masih Ada Data
        //         if (!empty($Company)) {

        //             //memisahkan mengirim data yang sama dan menghapus data $Company
        //             for ($i = 0; $i < count($all_division_competency); $i++) {

        //                 foreach ($Company as $key => $values) {

        //                     if (in_array($values['company'], $all_division_competency[$i])) {
        //                         array_push($SameDivision, $Company[$key]);
        //                         unset($Company[$key]);
        //                     }
        //                 }
        //             }

        //             //mengecek apakah variable $Company Masih Ada Data
        //             //Jika ada maka disimpan sebagai data baru
        //             if (!empty($Company)) {
        //                 foreach ($Company as $CompanyUser) {
        //                     $DataCompany = [
        //                         'id_company' => $CompanyUser['id_company'],
        //                         'id_user' => $profile[0],
        //                         'npk' => $profile[1],
        //                         'score_company' => 0
        //                     ];
        //                     //save
        //                     // $this->competencyCompany->save($DataCompany);
        //                 }
        //             }
        //         }

        //         //jika ada disimpan sebagai competency yang sudah ada
        //         if (!empty($SameDivision)) {
        //             $SerializeCompetency = array_map("unserialize", array_unique(array_map("serialize", $SameDivision)));

        //             foreach ($SerializeCompetency as $DivisionSame) {
        //                 $CompanyScore = $this->competencyCompany->getProfileCompanyCompetencyValue($profile[0],$profile[1], $DivisionSame['company']);
        //                 $CompetencyDivisionSame = [
        //                     'id_user' => $profile[0],
        //                     'npk' => $profile[1],
        //                     'id_company' => $DivisionSame['id_company'],
        //                     'score_company' => $CompanyScore[0]['score_company']
        //                 ];
        //             }
        //         }
        //     } else {
        //         $all_division_competency = $this->competencyCompany->getProfileCompanyCompetency($profile[0],$profile[1]);
        //         $SameDivision = [];

        //         $Company = $this->company->getDataCompanyDivisi($compare['divisi']);
        //         for ($i = 0; $i < count($all_division_competency); $i++) {
        //             foreach ($Company as $key => $values) {
        //                 if (in_array($values['company'], $all_division_competency[$i])) {
        //                     array_push($SameDivision, $Company[$key]);
        //                     unset($Company[$key]);
        //                 }
        //             }
        //         }

        //         if (!empty($Company)) {
        //             foreach ($Company as $CompanyUser) {
        //                 $DataCompany = [
        //                     'id_company' => $CompanyUser['id_company'],
        //                     'id_user' => $profile[0],
        //                     'npk' => $profile[1],
        //                     'score_company' => 0
        //                 ];
        //                 //save
        //                 // $this->competencyCompany->save($DataCompany);
        //             }
        //         }

        //         if (!empty($SameDivision)) {
        //             $SerializeCompetency = array_map("unserialize", array_unique(array_map("serialize", $SameDivision)));
        //             foreach ($SerializeCompetency as $DivisionSame) {
        //                 $CompanyScore = $this->competencyCompany->getProfileCompanyCompetencyValue($profile[0],$profile[1], $DivisionSame['company']);
        //                 $CompetencyDivisionSame = [
        //                     'id_user' => $profile[0],
        //                     'npk'=> $profile[1],
        //                     'id_company' => $DivisionSame['id_company'],
        //                     'score_company' => $CompanyScore[0]['score_company']
        //                 ];
        //             }
        //         }
        //     }
        // }
        // // copetency technical save
        // //mengecek apakah departemen diganti
        // if (array_key_exists("department", $compare)) {
        //     //clear

        //     $DataUser = $this->user->get_all_user_with_npk($profile[0],$profile[1]);

        //     $check = 0;

        //     //Mengecek Apakah data type golongan Sebelumnya
        //     //Jika Type Golongan A
        //     if (trim($DataUser['type_golongan']) == 'A') {
        //         $check = 'Competency A Susah Ada';

        //         $old_department_competency = $this->competencyTechnical->getProfileTechnicalCompetencyDept($profile[0],$profile[1],$compare['department'],$compare['nama_jabatan'],$compare['type_user']);

        //         //mengecek Apakah Competency Sebelumnya Sudah Ada
        //         //Jika Competency Sebelumnya Ada
        //         if (!empty($old_department_competency)) {


        //             //department sudah ada isinya
        //             $department =  $this->technical->getDataTechnicalUpdate($compare['department'],$compare['nama_jabatan'],$compare['type_user']);

        //             $all_competency = $this->competencyTechnical->getProfileTechnicalCompetency($profile[0],$profile[1]);

        //             for ($i = 0; $i < count($old_department_competency); $i++) {

        //                 foreach ($department as $key => $values) {

        //                     if (in_array($values['technical'], $old_department_competency[$i])) {
        //                         unset($department[$key]);
        //                     }
        //                 }
        //             }

        //             //mengecek Apakah Competency Departemen Tidak Kosong
        //             if (!empty($department)) {
        //                 $DuplicateCompetency = [];
        //                 //searcing data same value
        //                 for ($i = 0; $i < count($all_competency); $i++) {

        //                     foreach ($department as $key => $values) {

        //                         if (in_array($values['technical'], $all_competency[$i])) {
        //                             array_push($DuplicateCompetency, $department[$key]);
        //                             unset($department[$key]);
        //                         }
        //                     }
        //                 }
        //                 // Mengecek Kembali Apakah Competenncy Department Masih ada
        //                 if (!empty($department)) {
        //                     foreach ($department as $Dept) {
        //                         $TechnicalNew = [
        //                             'id_user' => $profile[0],
        //                             'npk' => $profile[1],
        //                             'id_technical' => $Dept['id_technical'],
        //                             'score_technical' => 0
        //                         ];
        //                         $this->competencyTechnical->save($TechnicalNew);
        //                     }
        //                 }

        //                 if (!empty($DuplicateCompetency)) {
        //                     //Menyimpan Data Competency Yang Sama
        //                     $SameCompetency = array_map("unserialize", array_unique(array_map("serialize", $DuplicateCompetency)));
        //                     foreach ($SameCompetency as $competency) {
        //                         $technical_score = $this->competencyTechnical->getProfileTechnicalCompetencyValue($profile[0],$profil[1], $competency['technical']);
        //                         $TechnicalSame = [
        //                             'id_user' => $profile[0],
        //                             'npk' => $profile[1],
        //                             'id_technical' => $competency['id_technical'],
        //                             'score_techncial' => $technical_score[0]['score_technical']

        //                         ];
        //                         $this->competencyTechnical->save($TechnicalSame);
        //                     }
        //                 }
        //             }

        //             //Jika Competency Sebelumnya Kosong
        //         } else {
        //             //clear
        //             $check = 'Competency A Kosong';
        //             //This Condition for New Department User

        //             $all_competency = $this->competencyTechnical->getProfileTechnicalCompetency($profile[0],$profile[1]);

        //             $department =  $this->technical->getDataTechnicalUpdate($compare['department'],$compare['nama_jabatan'],$compare['type_user']);
        //             $DuplicateCompetency = [];

        //             //searcing data same value
        //             for ($i = 0; $i < count($all_competency); $i++) {

        //                 foreach ($department as $key => $values) {

        //                     if (in_array($values['technical'], $all_competency[$i])) {
        //                         array_push($DuplicateCompetency, $department[$key]);
        //                         unset($department[$key]);
        //                     }
        //                 }
        //             }

        //             if (!empty($department)) {

        //                 foreach ($department as $Department) {
        //                     $TechnicalNew = [
        //                         'id_user' => $profile[0],
        //                         'npk' => $profile[1],
        //                         'id_technical' => $Department['id_technical'],
        //                         'score_technical' => 0
        //                     ];
        //                     $this->competencyTechnicalB->save($TechnicalNew);
        //                 }
        //             }

        //             if (!empty($DuplicateCompetency)) {
        //                 $SameCompetency = array_map("unserialize", array_unique(array_map("serialize", $DuplicateCompetency)));
        //                 foreach ($SameCompetency as $competency) {
        //                     $technical_score = $this->competencyTechnical->getProfileTechnicalCompetencyValue($profile[0],$profile[1], $competency['technical']);
        //                     $TechnicalSame = [
        //                         'id_user' => $profile[0],
        //                         'npk' => $profile[1],
        //                         'id_technical' => $competency['id_technical'],
        //                         'score_techncial' => $technical_score[0]['score_technical']

        //                     ];
        //                     $this->competencyTechnicalB->save($TechnicalSame);
        //                 }
        //             }
        //         }

        //         //jika Golongan B
        //     } else {
        //         $old_department_competency = $this->competencyTechnicalB->getProfileTechnicalCompetencyBUpdate($profile[0],$profile[1], $compare['department'],$compare['nama_jabatan'],$compare['seksi']);
        //         $all_competency = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($profile[0],$profile[1]);
        //         //Mengecek Apakah Competency Golongan B sebelumnya ada 
        //         //Jika Kompetency Golongan B sebelumya adA
        //         if (!empty($old_department_competency)) {
        //             $check = 'Competency B Ada';
        //             $department = $this->technicalB->getDataByDepartment($compare['department']);

        //             for ($i = 0; $i < count($old_department_competency); $i++) {
        //                 foreach ($department as $key => $values) {
        //                     if (in_array($values['technicalB'], $old_department_competency[$i])) {
        //                         unset($department[$key]);
        //                     }
        //                 }
        //             }

        //             //mengecek Apakah Competency Departemen Tidak Kosong
        //             if (!empty($department)) {
        //                 $DuplicateCompetency = [];
        //                 //searcing data same value
        //                 for ($i = 0; $i < count($all_competency); $i++) {

        //                     foreach ($department as $key => $values) {

        //                         if (in_array($values['technicalB'], $all_competency[$i])) {
        //                             array_push($DuplicateCompetency, $department[$key]);
        //                             unset($department[$key]);
        //                         }
        //                     }
        //                 }
        //                 // Mengecek Kembali Apakah Competenncy Department Maisih ada
        //                 if (!empty($department)) {
        //                     foreach ($department as $Dept) {
        //                         $TechnicalNew = [
        //                             'id_user' => $profile[0],
        //                             'npk' => $profile[1],
        //                             'id_technicalB' => $Dept['id_technicalB'],
        //                             'score' => 0
        //                         ];
        //                         $this->competencyTechnicalB->save($TechnicalNew);
        //                     }
        //                 }

        //                 if (!empty($DuplicateCompetency)) {
        //                     //Menyimpan Data Competency Yang Sama
        //                     $SameCompetency = array_map("unserialize", array_unique(array_map("serialize", $DuplicateCompetency)));
        //                     foreach ($SameCompetency as $competency) {
        //                         $technical_score = $this->competencyTechnicalB->getProfileTechnicalCompetencyBValue($profile[0], $profile[1], $competency['technical']);
        //                         $TechnicalSame = [
        //                             'id_user' => $profile[0],
        //                             'npk' => $profile[1],
        //                             'id_technicalB' => $competency['id_technicalB'],
        //                             'score' => $technical_score[0]['score']

        //                         ];
        //                         $this->competencyTechnicalB->save($TechnicalSame);
        //                     }
        //                 }
        //             }
        //         } else {

        //             $check = 'Competency B Kosong';
        //             $department = $this->technicalB->getDataByDepartmentSeksi($DataUser['nama_jabatan'], $compare['department'],$DataUser['seksi']);

        //             $all_competency = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($profile[0],$profile[1]);

        //             $DuplicateCompetency = [];

        //             for ($i = 0; $i < count($all_competency); $i++) {
        //                 foreach ($department as $key => $values) {
        //                     if (in_array($values['technicalB'], $all_competency[$i])) {
        //                         array_push($DuplicateCompetency, $department[$key]);
        //                         unset($department[$key]);
        //                     }
        //                 }
        //             }

        //             if (!empty($department)) {
        //                 foreach ($department as $Department) {
        //                     $TechnicalNew = [
        //                         'id_user' => $profile[0],
        //                         'npk' => $profile[1],
        //                         'id_technicalB' => $Department['id_technicalB'],
        //                         'score' => 0
        //                     ];
        //                     $this->competencyTechnicalB->save($TechnicalNew);
        //                 }
        //             }

        //             if (!empty($DuplicateCompetency)) {
        //                 //delete duplicate data
        //                 $SameCompetency = array_map("unserialize", array_unique(array_map("serialize", $DuplicateCompetency)));
        //                 foreach ($SameCompetency as $competency) {
        //                     $technical_score = $this->competencyTechnicalB->getTechnicalByIdCompetencyBValue($profile[0], $profile[1], $competency['technicalB']);
        //                     $TechnicalSame = [
        //                         'id_user' => $profile[0],
        //                         'npk' => $profile[1],
        //                         'id_technicalB' => $competency['id_technicalB'],
        //                         'score' => $technical_score[0]['score']

        //                     ];
        //                     //save
        //                     $this->competencyTechnicalB->save($TechnicalSame);
        //                 }
        //             }
        //         }
        //     }
        // }

        // save data user
        $tgl_masuk = date_create($profile[11]);
        $today = date('d-m-Y');
        $interval = date_diff($tgl_masuk, date_create($today));
        $interval->format('%R%y years %m months');
        $data = [
            'id_user' => $profile[0],
            'npk' => $profile[1],
            'nama' => $profile[2],
            'status' => $profile[3],
            'dic' => $profile[4],
            'divisi' => $profile[5],
            'departemen' => $profile[6],
            'seksi' => $profile[7],
            'bagian' => $profile[8],
            'type_golongan' => $profile[9],
            'type_user' => $profile[10],
            'promosi_terakhir' => $profile[11],
            'golongan' => $profile[12],
            'tgl_masuk' => $profile[13],
            'tahun' => $interval->format('%y'),
            'bulan' => $interval->format('%m'),
            'email' => $profile[16],
        ];
        $this->user->save($data);

        //user education save 
        $old_education = $this->request->getPost('education_old');
        if (!empty($old_education)) {

            $oldfix = [];
            foreach ($old_education as $old) {

                $education_old = [
                    'id_education' => $old[1],
                    'grade' => $old[0],
                    'year' => $old[2],
                    'institution' => $old[3],
                    'major' => $old[4],
                ];
                array_push($oldfix, $education_old);
                $this->education->save($education_old);
            }
            array_push($dataFixes, $oldfix);
        }

        $new_education = $this->request->getPost('education_new');
        if (!empty($new_education)) {
            $newfix = [];
            foreach ($new_education as $new) {
                $education_new = [
                    'npk' => $profile[1],
                    'grade' => $new[0],
                    'year' => $new[1],
                    'institution' => $new[2],
                    'major' => $new[3],
                ];
                array_push($newfix, $education_new);
                $this->education->save($education_new);
            }
            array_push($dataFixes, $newfix);
        }

        //career user save
        $old_career = $this->request->getPost('career_old');
        if (!empty($old_career)) {
            $oldfixCareer = [];
            foreach ($old_career as $old_career) {
                $career_old = [
                    'id_career' => $old_career[0],
                    'year_start' => $old_career[1],
                    'year_end' => $old_career[2],
                    'position' => $old_career[3],
                    'departement' => $old_career[4],
                    'division' => $old_career[5],
                    'company' => $old_career[6],
                ];
                array_push($oldfixCareer, $career_old);
                 $this->career->save($career_old);
            }
            array_push($dataFixes, $oldfixCareer);
        }

        $new_career = $this->request->getPost('career_new');
        if (!empty($new_career)) {
            $newfixCareer = [];
            foreach ($new_career as $new_careers) {
                $career_new = [
                    'npk' => $profile[1],
                    'year_start' => $new_careers[0],
                    'year_end' => $new_careers[1],
                    'position' => $new_careers[2],
                    'departement' => $new_careers[3],
                    'division' => $new_careers[4],
                    'company' => $new_careers[5],
                ];
                array_push($newfixCareer, $career_new);
                $this->career->save($career_new);
            }
            array_push($dataFixes, $newfixCareer);
        }
        echo json_encode($status_update);
    }



    public function DuplicateCompetncy($department, $old_competency)
    {

        $DuplicateCompetency = [];

        for ($i = 0; $i < count($old_competency); $i++) {

            foreach ($department as $key => $values) {

                if (in_array($values['technical'], $old_competency[$i])) {

                    array_push($DuplicateCompetency, $department[$key]);
                }
            }
        }

        ///delete duplicate data
        $try = array_map("unserialize", array_unique(array_map("serialize", $DuplicateCompetency)));
    }


    public function UnsetCompetency()
    {
    }


    //function for catch same value data  array
    public function SameCompetency($department, $old_competency)
    {
        for ($i = 0; $i < count($old_competency); $i++) {
            foreach ($department as $key => $values) {
                if (in_array($values['technical'], $old_competency[$i])) {
                    return $department[$key];
                }
            }
        }
    }

    //Equate array structure for Edit User
    public function EquateArray($id,$npk)
    {
        $user  = $this->user->get_all_user_with_npk($id,$npk);
        $data = [
            'divisi' => $user['divisi'],
            'department' => $user['departemen'],
            'seksi' => $user['seksi'],
            'nama_jabatan' => $user['nama_jabatan'],
            'type_golongan' => trim($user['type_golongan']),
            'type_user' => trim($user['type_user'])
        ];
        return $data;
    }



    public function delete($id)
    {
        $this->user->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Di Hapus');
        return redirect()->to('/user');
    }

    public function deleteEducation()
    {
        $id = $this->request->getPost('id');
        $data = $this->education->delete($id);
        echo json_encode($data);
    }
    public function deleteCareer()
    {
        $id = $this->request->getPost('id');
        $data = $this->career->delete($id);
        echo json_encode($data);
    }

    public function AddEducation()
    {
        $data = [
            'id_user' => $this->request->getVar('id'),
            'grade' => $this->request->getVar('grade'),
            'year' => $this->request->getVar('year'),
            'institution' => $this->request->getVar('institution'),
            'major' => $this->request->getVar('major'),

        ];
        $this->education->save($data);
        session()->setFlashdata('success', 'Data Education Berhasil Di Simpan');
        return redirect()->to('/user');
    }

    public function AddCareer()
    {
        $data = [
            'id_user' => $this->request->getVar('id'),
            'year_start' => $this->request->getVar('year_start'),
            'year_end' => $this->request->getVar('year_end'),
            'position' => $this->request->getVar('position'),
            'departement' => $this->request->getVar('department'),
            'division' => $this->request->getVar('division'),
            'company' => $this->request->getVar('company'),

        ];
        $this->career->save($data);
        session()->setFlashdata('success', 'Data Career Berhasil Di Simpan');
        return redirect()->to('/user');
    }

    public function test()
    {
        $technical = $this->competencyTechnical->getProfileTechnicalCompetency(7017);
        $astra = $this->competencyAstra->getProfileAstraCompetency(7017);
        // dd($technical);
    }
    public function EditCompetencyUser()
    {
        $id = $this->request->getPost('id');
        $npk = $this->request->getPost('npk');
        $user = $this->user->get_all_user_with_npk($id,$npk);
        $seksi = $user['seksi'];
        session()->set('edit_competency_id', $id);
        session()->set('edit_competency_npk', $npk);
        if (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'REGULAR') {
            $astra = $this->competencyAstra->getProfileAstraCompetency($id,$npk);
            $technicalA = $this->competencyTechnical->getProfileTechnicalCompetency($id,$npk);
            echo '
                        <div class="d-flex">
                        <div class="card w-100 m-1">
                        <input type="hidden" value="Astra" name="competency">
                            <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Astra Leadership Competency	</th>
                                                <th>Proficiency</th>
                                                <th>Score</th>
                                                <th>Last Update</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
            foreach ($astra as $Astra) {
                echo '<tr>
                        <td>' . $Astra['astra'] . '</td>
                        <td>' . $Astra['proficiency'] . '</td>
                        <td>
                            <input type="hidden" value="' . $Astra['id_competency_astra'] . '" name="astra[]">
                            <input style="width:30px;" onkeyup="limitValue(this)" name="astra[]" value="' . $Astra['score_astra'] . '">
                        </td>
                        <td>';
                        
                if (empty($Astra['updated_at'])) {
                    echo 'NULL';
                } else {
                    $updatedDate = date("Y-m-d", strtotime($Astra['updated_at']));
                    echo $updatedDate;
                }
                
                echo '</td>
                      </tr>';
            }

            echo '                           </tbody>
                                    </table>
                                    </div>
                           ';
            echo '
             <div class="card w-100 m-1">
                <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Technical Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Last Update</th>
                                </tr>
                            </thead>
                            <tbody>';
            foreach ($technicalA as $technicals) {
                echo '<tr>
            <td>' . $technicals['technical'] . '</td>
            <td>' . $technicals['proficiency'] . '</td>
            <td>
            <input type="hidden" value="' . $technicals['id_competency_technical'] . '" name="technical[]">
            <input style="width:30px;" onkeyup="limitValue(this)" name="technical[]" value="' . $technicals['score_technical'] . '">
            </td>
            <td>';
            if (empty($technicals['updated_at'])) {
                    echo 'NULL';
                } else {
                    $updatedDate = date("Y-m-d", strtotime($technicals['updated_at']));
                    echo $updatedDate;
                }
                
                echo '</td>
                      </tr>';
            }

            echo '                           </tbody>
                        </table>
                        </div>
                         </div>
               ';
        } elseif (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'EXPERT') {
            $expert = $this->competencyExpert->getProfileExpertCompetency($id,$npk);
            $technical = $this->competencyTechnical->getProfileTechnicalExpertCompetency($id,$npk);
            echo '
            <div class="d-flex">
            <div class="card w-100 m-1">
            <input type="hidden" value="Expert" name="competency">
                <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Expert Behavior Competencies</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Last Update</th>
                                </tr>
                            </thead>
                            <tbody>';
            foreach ($expert as $experts) {
            echo '<tr>
            <td>' . $experts['expert'] . '</td>
            <td>' . $experts['proficiency'] . '</td>
            <td>
            <input type="hidden"  value="' . $experts['id_competency_expert'] . '" name="expert[]">
            <input style="width:30px;" onkeyup="limitValue(this)" value="' . $experts['score_expert'] . '" name="expert[]">
            </td>
            <td>';
            if (empty($experts['updated_at'])) {
                echo 'NULL';
            } else {
                $updatedDate = date("Y-m-d", strtotime($experts['updated_at']));
                echo $updatedDate;
            }
            
            echo '</td>
                  </tr>';
            }

            echo '                           </tbody>
                        </table>
                        </div>
               ';
            echo '
             <div class="card w-100 m-1">
                <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Technical Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Last Update</th>
                                </tr>
                            </thead>
                            <tbody>';
            foreach ($technical as $technicals) {
            echo '<tr>
            <td>' . $technicals['technical'] . '</td>
            <td>' . $technicals['proficiency'] . '</td>
            <td>
            <input type="hidden" value="' . $technicals['id_competency_technical'] . '" name="technical[]">
            <input style="width:30px;" name="technical[]" onkeyup="limitValue(this)" value="' . $technicals['score_technical'] . '"></td>
            </td><td>';
            if (empty($technicals['updated_at'])) {
                echo 'NULL';
            } else {
                $updatedDate = date("Y-m-d", strtotime($technicals['updated_at']));
                echo $updatedDate;
            }
            echo '</td>
                  </tr>';
            }

            echo '</tbody>
                    </table>
                    </div>
                    </div>';
        } else {
            $company = $this->competencyCompany->getProfileCompanyCompetency($id,$npk);
            $soft = $this->competencySoft->getProfileSoftCompetency($id,$npk);
            $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyBCurrentPosition($id,$npk,$seksi);
            echo '
            <div class="d-flex">
            <div class="card w-100 m-1">
             <input type="hidden" value="B" name="competency">
                <table class="table table-striped">
                    <thead>
                            <tr>
                                <th>Company General Competency</th>
                                <th>Proficiency</th>
                                <th>Score</th>
                                <th>Last Update</th>
                            </tr>
                    </thead>
            <tbody>';
            foreach ($company as $companies) {
            echo '<tr>
            <td>' . $companies['company'] . '</td>
            <td>' . $companies['proficiency'] . '</td>
            <td>
            <input type="hidden" name="company[]" value="' . $companies['id_competency_company'] . '">
            <input style="width:30px;" name="company[]" id="companyInput" onkeyup="limitValue(this)" value="' . $companies['score_company'] . '">
            </td><td>';
            if (empty($companies['updated_at'])) {
                echo 'NULL';
            } else {
                $updatedDate = date("Y-m-d", strtotime($companies['updated_at']));
                echo $updatedDate;
            }
            echo '</td>
                  </tr>';
            }

            echo '                           </tbody>
                        </table>
                        </div>
               ';

            echo '
            <div class="card w-100 m-1">
            <table class="table table-striped">
                <thead>
                        <tr>
                            <th>Technical Competency</th>
                            <th>Proficiency</th>
                            <th>Score</th>
                            <th>Last Update</th>
                        </tr>
                </thead>
            <tbody>';
            foreach ($technicalB as $technical) {
                echo '<tr>
            <td>' . $technical['technicalB'] . '</td>
            <td>' . $technical['proficiency'] . '</td>
            <td>
            <input type="hidden" name="technicalB[]" value="' . $technical['id_competency_technicalB'] . '">
            <input style="width:30px;" name="technicalB[]"  onkeyup="limitValue(this)" value="' . $technical['score'] . '">
            </td><td>';
            if (empty($technical['updated_at'])) {
                echo 'NULL';
            } else {
                $updatedDate = date("Y-m-d", strtotime($technical['updated_at']));
                echo $updatedDate;
            }
            echo '</td>
                  </tr>';
            }

            echo '                           </tbody>
                        </table>
                        </div>
                       
               ';

            echo
            ' <div class="card w-100 m-1">
                <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Soft Competency</th>
                        <th>Proficiency</th>
                        <th>Score</th>
                        <th>Last Update</th>
                    </tr>
                </thead>
                <tbody>';
            foreach ($soft as $softy) {
                echo '<tr>
            <td>' . $softy['soft'] . '</td>
            <td>' . $softy['proficiency'] . '</td>
            <td>
            <input type="hidden" name="soft[]" value="' . $softy['id_competency_soft'] . '">
            <input style="width:30px;" name="soft[]" onkeyup="limitValue(this)" value="' . $softy['score_soft'] . '">
            </td><td>';
            if (empty($softy['updated_at'])) {
                echo 'NULL';
            } else {
                $updatedDate = date("Y-m-d", strtotime($softy['updated_at']));
                echo $updatedDate;
            }
            echo '</td>
                  </tr>';
            }

            echo '</tbody>
                </table>
                </div>
                </div>
               ';
        }
    }

    public function ChangeCompetency()
    {
        $id = session()->get('edit_competency_id');
        $npk = session()->get('edit_competency_npk');
        $competency  = $this->request->getVar('competency');
        $user = $this->user->get_all_user_with_npk($id,$npk);
        // dd($competency);

        if ($competency == 'Astra') {
            //Astra Competency
            $astra = $this->request->getVar('astra');
            if(!empty($astra)){
                $AstraCompetency = array_chunk($astra, 2);
                foreach ($AstraCompetency as $Astra) {
                    // $AstraData = [
                    //     'id_competency_astra' => $Astra[0],
                    //     'score_astra' => $Astra[1],
                    // ];
                    // $this->competencyAstra->save($AstraData);
                    $existingData = $this->competencyAstra->find($Astra[0]);
                    $AstraData = [
                        'id_competency_astra' => $Astra[0],
                        'score_astra' => $Astra[1],
                    ];

                    if ($existingData['score_astra'] != $Astra[1]) {
                        $AstraData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencyAstra->save($AstraData);
                }
            } else {
                $dataAstra= $this->astra->getDataAstra();
                $id_user = $id;
                $astraDataArray = [];

                foreach($dataAstra as $da){
                    $astraData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_astra' => $da['id_astra'],
                        'score_astra' => 0
                    ];
                    $astraDataArray[] = $astraData;
                }

                foreach ($astraDataArray as $astraData) {
                    $this->competencyAstra->save($astraData);
                }
            }

            $technical = $this->request->getVar('technical');
            if(!empty($technical)){
                $TechnicalCompetency = array_chunk($technical, 2);
                foreach ($TechnicalCompetency as $Technical) {
                    // $TechnicalData = [
                    //     'id_competency_technical' => $Technical[0],
                    //     'score_technical' => $Technical[1]
                    // ];
                    // $this->competencyTechnical->save($TechnicalData);
                    $existingData = $this->competencyTechnical->find($Technical[0]);
                    $TechnicalData = [
                        'id_competency_technical' => $Technical[0],
                        'score_technical' => $Technical[1],
                    ];

                    if ($existingData['score_technical'] != $Technical[1]) {
                        $TechnicalData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencyTechnical->save($TechnicalData);
                }
            } else {
                $dataTechnical = $this->technical->getDataTechnicalDepartemen($user['departemen']);
                $id_user = $id;
                $technicalDataArray = [];

                foreach($dataTechnical as $dt){
                    $technicalData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_technical' => $ds['id_technical'],
                        'score_technical' => 0
                    ];
                    $technicalDataArray[] = $technicalData;
                }

                foreach ($technicalDataArray as $technicalData) {
                    $this->competencyTechnical->save($technicalData);
                }
            }
            return redirect()->to('/user');
        } elseif ($competency == 'Expert') {
            //Expert Competency
            $expert = $this->request->getVar('expert');
            if(!empty($expert)){
                $ExpertCompetency = array_chunk($expert, 2);
                foreach ($ExpertCompetency as $Expert) {
                    // $ExpertData = [
                    //     'id_competency_expert' => $Expert[0],
                    //     'score_expert' => $Expert[1]
                    // ];
                    // $this->competencyExpert->save($ExpertData);
                    $existingData = $this->competencyExpert->find($Expert[0]);
                    $ExpertData = [
                        'id_competency_expert' => $Expert[0],
                        'score_expert' => $Expert[1],
                    ];

                    if ($existingData['score_expert'] != $Expert[1]) {
                        $ExpertData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencyExpert->save($ExpertData);
                }
            } else {
                $dataExpert= $this->expert->getDataExpert();
                $id_user = $id;
                $expertDataArray = [];

                foreach($dataExpert as $de){
                    $expertData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_expert' => $de['id_expert'],
                        'score_expert' => 0
                    ];
                    $expertDataArray[] = $expertData;
                }

                foreach ($expertDataArray as $expertData) {
                    $this->competencyExpert->save($expertData);
                }
            }
            // $ExpertCompetency = array_chunk($expert, 2);
            // foreach ($ExpertCompetency as $Expert) {
            //     $ExpertData = [
            //         'id_competency_expert' => $Expert[0],
            //         'score_expert' => $Expert[1]
            //     ];
            //     $this->competencyExpert->save($ExpertData);
            // }
            $technical = $this->request->getVar('technical');
            // dd($technical);
            if(!empty($technical)){
                $TechnicalCompetency = array_chunk($technical, 2);
                foreach ($TechnicalCompetency as $Technical) {
                    // $TechnicalData = [
                    //     'id_competency_technical' => $Technical[0],
                    //     'score_technical' => $Technical[1]
                    // ];
                    // $this->competencyTechnical->save($TechnicalData);
                    $existingData = $this->competencyTechnical->find($Technical[0]);
                    $TechnicalData = [
                        'id_competency_technical' => $Technical[0],
                        'score_technical' => $Technical[1],
                    ];

                    if ($existingData['score_technical'] != $Technical[1]) {
                        $TechnicalData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencyTechnical->save($TechnicalData);
                }
            } else {
                $dataTechnical = $this->technical->getDataTechnicalExpert($user['departemen'],$user['nama_jabatan']);   
                $id_user = $id;
                $technicalDataArray = [];

                foreach($dataTechnical as $dt){
                    // dd($dt);
                    $technicalData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_technical' => $dt['id_technical'],
                        'score_technical' => 0
                    ];
                    $technicalDataArray[] = $technicalData;
                }

                foreach ($technicalDataArray as $technicalData) {
                    $this->competencyTechnical->save($technicalData);
                }
            }
            return redirect()->to('/user');
        } else {
            $company = $this->request->getVar('company');
            if(!empty($company)){
                $CompanyCompetency = array_chunk($company, 2);
                foreach ($CompanyCompetency as $Company) {
                    $existingData = $this->competencyCompany->find($Company[0]);
                    $CompanyData = [
                        'id_competency_company' => $Company[0],
                        'score_company' => $Company[1],
                    ];

                    if ($existingData['score_company'] != $Company[1]) {
                        $CompanyData['updated_at'] = date('Y-m-d H:i:s');
                    }
                    
                    $this->competencyCompany->save($CompanyData);
                    // $CompanyData = [
                    //     'id_competency_company' => $Company[0],
                    //     'score_company' => $Company[1]
                    // ];
                    // $this->competencyCompany->save($CompanyData);
                }
            } else {
                $dataCompany = $this->company->getDataCompanyDivisi($user['divisi']);
                $id_user = $id;
                $companyDataArray = [];

                foreach($dataCompany as $dc){
                    $companyData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_company' => $dc['id_company'],
                        'score_company' => 0
                    ];
                    $companyDataArray[] = $companyData;
                }

                foreach ($companyDataArray as $companyData) {
                    $this->competencyCompany->save($companyData);
                }
            }
            //Group B Competency
            // $company = $this->request->getVar('company');
            // $CompanyCompetency = array_chunk($company, 2);
            // foreach ($CompanyCompetency as $Company) {
            //     $CompanyData = [
            //         'id_competency_company' => $Company[0],
            //         'score_company' => $Company[1]
            //     ];
            //     $this->competencyCompany->save($CompanyData);
            // }
            $soft = $this->request->getVar('soft');
            if(!empty($soft)){
                $SoftCompetency = array_chunk($soft, 2);
                foreach ($SoftCompetency as $Soft) {
                    // $SoftData = [
                    //     'id_competency_soft' => $Soft[0],
                    //     'score_soft' => $Soft[1]
                    // ];
                    // $this->competencySoft->save($SoftData);
                    $existingData = $this->competencySoft->find($Soft[0]);
                    $SoftData = [
                        'id_competency_soft' => $Soft[0],
                        'score_soft' => $Soft[1],
                    ];

                    if ($existingData['score_soft'] != $Soft[1]) {
                        $SoftData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencySoft->save($SoftData);
                }
            } else {
                $dataSoft = $this->soft->getDataSoft();
                $id_user = $id;
                $softDataArray = [];

                foreach($dataSoft as $ds){
                    $softData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_soft' => $ds['id_soft'],
                        'score_soft' => 0
                    ];
                    $softDataArray[] = $softData;
                }

                foreach ($softDataArray as $softData) {
                    $this->competencySoft->save($softData);
                }
            }
            
            $technicalb = $this->request->getVar('technicalB');
            if(!empty($technicalb)){
                $TechnicalbCompetency = array_chunk($technicalb, 2);
                foreach ($TechnicalbCompetency as $Technicalb) {
                    // $TechnicalbData = [
                    //     'id_competency_technicalB' => $Technicalb[0],
                    //     'score' => $Technicalb[1]
                    // ];
                    // $this->competencyTechnicalB->save($TechnicalbData);
                    $existingData = $this->competencyTechnicalB->find($Technicalb[0]);
                    $TechnicalbData = [
                        'id_competency_technicalB' => $Technicalb[0],
                        'score' => $Technicalb[1],
                    ];

                    if ($existingData['score'] != $Technicalb[1]) {
                        $TechnicalbData['updated_at'] = date('Y-m-d H:i:s');
                    }

                    $this->competencyTechnicalB->save($TechnicalbData);
                }
            } else {
                $dataTechnicalb = $this->technicalB->getDataByDepartmentSeksi($user['nama_jabatan'],$user['departemen'],$user['seksi']);
                $id_user = $id;
                $technicalbDataArray = [];

                foreach($dataTechnicalb as $dt){
                    $technicalbData = [
                        'id_user' => $id_user,
                        'npk' => $npk,
                        'id_technicalB' => $dt['id_technicalB'],
                        'score' => 0
                    ];
                    $technicalbDataArray[] = $technicalbData;
                }

                foreach ($technicalbDataArray as $technicalbData) {
                    $this->competencyTechnicalB->save($technicalbData);
                }
            }
            // $TechnicalBCompetency = array_chunk($technicalB, 2);
            // foreach ($TechnicalBCompetency as $TechnicalB) {
            //     $TechnicalBData = [
            //         'id_competency_technicalB' => $TechnicalB[0],
            //         'score' => $TechnicalB[1]
            //     ];
            //     $this->competencyTechnicalB->save($TechnicalBData);
            // }
            return redirect()->to('/user');
        }
    }

    public function AstraSkillMap()
    {
        $astra = $this->competencyAstra->getAstraCompetencySkillMap();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $astra_array = [];
        foreach ($astra as $item) {
            $key = $item['astra'];
            $value = [$item['npk'], $item['proficiency'], $item['score_astra'], $item['updated_at']];

            if (!isset($astra_array[$key])) {
                $astra_array[$key] = [];
            } 

            $astra_array[$key][] = $value;
        }
        // dd($astra_array);
        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;

        foreach ($astra_array as $key => $item) {
            $sheet->setCellValue($columnIndex . $rowIndex, $key);
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFA500');
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;

            foreach ($item as $data) {
                $sheet->setCellValue('A' . ($rowIndex+1), $this->user->getNamaByNpk($data[0]));
                if (date('d-m-Y', strtotime($data[3])) == '01-01-1970') {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), 'NULL');
                } else {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), $data[1].' / '.$data[2].' / '.date('d-m-Y', strtotime($data[3])));
                }
                $rowIndex += 1;
            }

            $columnIndex++;
            $rowIndex = 1;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-SM-AstraComp.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function ExpertSkillMap()
    {
        $expert = $this->competencyExpert->getExpertCompetencySkillMap();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $expert_array = [];
        foreach ($expert as $item) {
            $key = $item['expert'];
            $value = [$item['npk'], $item['proficiency'], $item['score_expert'], $item['updated_at']];

            if (!isset($expert_array[$key])) {
                $expert_array[$key] = [];
            } 

            $expert_array[$key][] = $value;
        }
        // dd($astra_array);
        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;

        foreach ($expert_array as $key => $item) {
            $sheet->setCellValue($columnIndex . $rowIndex, $key);
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFA500');
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;

            foreach ($item as $data) {
                $sheet->setCellValue('A' . ($rowIndex+1), $this->user->getNamaByNpk($data[0]));
                if (date('d-m-Y', strtotime($data[3])) == '01-01-1970') {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), 'NULL');
                } else {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), $data[1].' / '.$data[2].' / '.date('d-m-Y', strtotime($data[3])));
                }
                $rowIndex += 1;
            }

            $columnIndex++;
            $rowIndex = 1;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-SM-ExpertComp.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function SoftSkillMap()
    {
        $soft = $this->competencySoft->getSoftCompetencySkillMap();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $soft_array = [];
        foreach ($soft as $item) {
            $key = $item['soft'];
            $value = [$item['npk'], $item['proficiency'], $item['score_soft'], $item['updated_at']];

            if (!isset($soft_array[$key])) {
                $soft_array[$key] = [];
            } 

            $soft_array[$key][] = $value;
        }
        // dd($astra_array);
        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;


        foreach ($soft_array as $key => $item) {
            $sheet->setCellValue($columnIndex . $rowIndex, $key);
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFA500');
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;

            foreach ($item as $data) {
                $sheet->setCellValue('A' . ($rowIndex+1), $this->user->getNamaByNpk($data[0]));
                if (date('d-m-Y', strtotime($data[3])) == '01-01-1970') {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), 'NULL');
                } else {
                    $sheet->setCellValue($columnIndex . ($rowIndex+1), $data[1].' / '.$data[2].' / '.date('d-m-Y', strtotime($data[3])));
                }
                $rowIndex += 1;
            }

            $columnIndex++;
            $rowIndex = 1;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-SM-SoftComp.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function TechnicalSkillMap($departemen)
    {
        $technical = $this->competencyTechnical->getTechnicalCompetencySkillMap($departemen);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $technical_array = [];
        $npk_array = [];
        foreach ($technical as $item) {
            $key = $item['technical'];
            $value = [$item['npk'], $item['proficiency'], $item['score_technical'], $item['updated_at']];

            $technical_array[$key][] = $value;
            $npk_array[$item['npk']][] = $value;
        }

        $unique_technical = array_keys($technical_array);
        $unique_npk = array_keys($npk_array);

        $merge_array = [];
        foreach ($unique_technical as $tech) {
            foreach ($unique_npk as $npk) {
                $merge_array[] = [$tech, $npk];
            }
        }

        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;

        $columnIndex = 'B';
        foreach ($unique_technical as $techA) {
            $sheet->setCellValue($columnIndex . $rowIndex, $techA);
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFA500');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FF00FF00');
            $columnIndex++;
        }
        $rowIndex = 3;

        foreach ($unique_npk as $npk) {
            $sheet->setCellValue('A' . $rowIndex, $this->user->getNamaByNpk($npk));
            $sheet->getStyle('A' . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;
        }

        $columnKompetensi = 'B';
        $rowKompetensi = 3; 
        $array_check = [];

        foreach($unique_technical as $item){
            foreach($unique_npk as $items){
                if (isset($technical_array[$item]) && isset($npk_array[$items])) {
                    $values = $this->competencyTechnical->userHaveTechnical($item,$items);

                    if(!empty($values)){
                        $values;
                        if ($values['updated_at'] == NULL) {
                            $updated_at = 'NULL';
                        } else {
                            $updated_at = date('d-m-Y', strtotime($values['updated_at']));
                        }
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, $values['proficiency'] . ' / ' . $values['score_technical'] . ' / ' . $updated_at);
                        $rowKompetensi++;
                    } else{
                        $values = [
                            'proficiency' => 'NULL',
                            'score_technical' => 'NULL',
                            'updated_at' => 'NULL'
                        ];
                        $updated_at = 'NULL';
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, 'NULL');
                        $rowKompetensi++;
                    }
                } 
            }
            $rowKompetensi = 3;
            $columnKompetensi++;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-SM-TechnicalComp.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function TechnicalBSkillMap($seksi)
    {
        $technicalB = $this->competencyTechnicalB->getTechnicalBCompetencySkillMap($seksi);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $technicalB_array = [];
        $npk_array = [];
        foreach ($technicalB as $item) {
            $key = $item['technicalB'];
            $value = [$item['npk'], $item['proficiency'], $item['score'], $item['updated_at']];

            $technicalB_array[$key][] = $value;
            $npk_array[$item['npk']][] = $value;
        }

        $unique_technicalB = array_keys($technicalB_array);
        $unique_npk = array_keys($npk_array);

        $merge_array = [];
        foreach ($unique_technicalB as $techB) {
            foreach ($unique_npk as $npk) {
                $merge_array[] = [$techB, $npk];
            }
        }

        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;

        $columnIndex = 'B';
        foreach ($unique_technicalB as $techB) {
            $sheet->setCellValue($columnIndex . $rowIndex, $techB);
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFA500');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FF00FF00');
            $columnIndex++;
        }
        $rowIndex = 3;

        foreach ($unique_npk as $npk) {
            $sheet->setCellValue('A' . $rowIndex, $this->user->getNamaByNpk($npk));
            $sheet->getStyle('A' . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;
        }

        $columnKompetensi = 'B';
        $rowKompetensi = 3; 
        $array_check = [];
        foreach($unique_technicalB as $item){
            foreach($unique_npk as $items){
                if (isset($technicalB_array[$item]) && isset($npk_array[$items])) {
                    $values = $this->competencyTechnicalB->userHaveTechnicalB($item,$items);

                    if(!empty($values)){
                        $values;
                        if ($values['updated_at'] == NULL) {
                            $updated_at = 'NULL';
                        } else {
                            $updated_at = date('d-m-Y', strtotime($values['updated_at']));
                        }
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, $values['proficiency'] . ' / ' . $values['score'] . ' / ' . $updated_at);
                        $rowKompetensi++;
                    } else{
                        $values = [
                            'proficiency' => 'NULL',
                            'score' => 'NULL',
                            'updated_at' => 'NULL'
                        ];
                        $updated_at = 'NULL';
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, 'NULL');
                        $rowKompetensi++;
                    }
                } 
            }
            $rowKompetensi = 3;
            $columnKompetensi++;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-SM-TechB-'."$seksi".'.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

    public function CompanySkillMap($divisi)
    {
        $company = $this->competencyCompany->getCompanyCompetencySkillMap($divisi);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $company_array = [];
        $npk_array = [];
        foreach ($company as $item) {
            $key = $item['company'];
            $value = [$item['npk'], $item['proficiency'], $item['score_company'], $item['updated_at']];

            $company_array[$key][] = $value;
            $npk_array[$item['npk']][] = $value;
        }

        $unique_company = array_keys($company_array);
        $unique_npk = array_keys($npk_array);

        $merge_array = [];
        foreach ($unique_company as $comp) {
            foreach ($unique_npk as $npk) {
                $merge_array[] = [$comp, $npk];
            }
        }

        $sheet->getDefaultColumnDimension()->setWidth(30);
        $columnIndex = 'B';
        $rowIndex = 1;

        $columnIndex = 'B';
        foreach ($unique_company as $comp) {
            $sheet->setCellValue($columnIndex . $rowIndex, $comp);
            $sheet->setCellValue($columnIndex . $rowIndex+1, 'P / S / D');
            $sheet->getStyle($columnIndex . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFA500');
            $sheet->getStyle($columnIndex . $rowIndex+1)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FF00FF00');
            $columnIndex++;
        }
        $rowIndex = 3;

        foreach ($unique_npk as $npk) {
            $sheet->setCellValue('A' . $rowIndex, $this->user->getNamaByNpk($npk));
            $sheet->getStyle('A' . $rowIndex)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFFFF00');
            $rowIndex++;
        }

        $columnKompetensi = 'B';
        $rowKompetensi = 3; 
        $array_check = [];
        foreach($unique_company as $item){
            foreach($unique_npk as $items){
                if (isset($company_array[$item]) && isset($npk_array[$items])) {
                    $values = $this->competencyCompany->userHaveCompany($item,$items);

                    if(!empty($values)){
                        $values;
                        if ($values['updated_at'] == NULL) {
                            $updated_at = 'NULL';
                        } else {
                            $updated_at = date('d-m-Y', strtotime($values['updated_at']));
                        }
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, $values['proficiency'] . ' / ' . $values['score_company'] . ' / ' . $updated_at);
                        $rowKompetensi++;
                    } else{
                        $values = [
                            'proficiency' => 'NULL',
                            'score' => 'NULL',
                            'updated_at' => 'NULL'
                        ];
                        $updated_at = 'NULL';
                        $sheet->setCellValue($columnKompetensi.$rowKompetensi, 'NULL');
                        $rowKompetensi++;
                    }
                } 
            }
            $rowKompetensi = 3;
            $columnKompetensi++;
        }

        $writer = new Xlsx($spreadsheet);
        $tempDir = WRITEPATH . 'spreadsheet/';
        $filename = date('Y-m-d-His') . '-Company General-'."$divisi".'.xlsx';
        $writer->save($tempDir . $filename);

        return $this->response->download($tempDir . $filename, null);
    }

}