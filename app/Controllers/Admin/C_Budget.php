<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Budget;
use App\Models\UserModel;

class C_Budget extends BaseController
{
    private UserModel $user;
    private M_Budget $budget;

    public function __construct()
    {
        $this->user = new UserModel();
        $this->budget = new M_Budget();
    }
    public function index()
    {
        $department = $this->user->DistinctDepartemen();
        $budget = $this->budget->getAllBudget();
        $years = $this->budget->getYearBudget();

        $plainYears = [];
        foreach ($years as $yearData) {
            $plainYears[] = $yearData['year'];
        }
        $year = array_merge($plainYears);


        $data = [
            'tittle' => 'Training Budget',
            'department' => $department,
            'year' => $year,
            'budget' => $budget
        ];
        // dd($department);
        return view('admin/budget', $data);
    }

    public function strFilter($string)
    {
        $number =  str_replace("Rp.", "", $string);
        $angka = str_replace(".", "", $number);
        $fixes = str_replace(" ", "", $angka);
        return $fixes;
    }

    public function SaveBudget()
    {
        $id_budget = $this->request->getVar('id_budget');
        $department = $this->request->getVar('department');
        $alocated = $this->strFilter($this->request->getVar('alocated'));
        $used = $this->strFilter($this->request->getVar('used'));
        $temporary = $this->strFilter($this->request->getVar('temporary'));
        $available = $this->strFilter($this->request->getVar('available'));
        $year = $this->request->getVar('year');

        if($used == "" && $temporary == ""){
            $used = 0;
            $temporary = 0;
        };

        if($department == "AMDI AOP"){
            $divisi = NULL;
        } else{
            $divisi = $this->user->getDivisiByDepartment($department);
        }

        $existingBudget = $this->budget->getBudgetByDepartmentAndYear($department,$year);

        $year = $this->request->getVar('year');
        if($used>$alocated || $available >$alocated){
            session()->setFlashdata('error', 'Available dan Used tidak boleh melebihi Alocated Budget');
            return redirect()->to('/budget');
        }else{
            if ($id_budget != "") {
                
                $budgetData = $this->budget->getDataBudgetById($id_budget);
                $budgetAvailable = $alocated - $budgetData['used_budget'];
                // dd($budgetAvailable);
                if ($year < date('Y')) {
                    $condition = 'past';
                } elseif ($year == date('Y')) {
                    $condition = 'current';
                } else {
                    $condition = 'future';
                }
                $data1 = [
                    'id_budget' => $id_budget,
                    'department' => $department,
                    'alocated_budget' => $alocated,
                    'available_budget' => $budgetAvailable,
                    'used_budget' => $used,
                    'temporary_calculation' => $temporary,
                    'year' => $year,
                    'current' => $condition,
                    'divisi' => $divisi
                ];
                $this->budget->update($id_budget,$data1);
                return redirect()->to('/budget');
            } else {
                if ($existingBudget) {
                    // A budget with the same department already exists, show a SweetAlert notification
                    session()->setFlashdata('error', 'Data budget dengan departemen tersebut sudah tersedia.');
                        return redirect()->to('/budget');
                }else{
                if ($year < date('Y')) {
                    $condition = 'past';
                } elseif ($year == date('Y')) {
                    $condition = 'current';
                } else {
                    $condition = 'future';
                }
                // dd($data2);
                $data2 = [
                    'department' => $department,
                    'alocated_budget' => $alocated,
                    'available_budget' => $available,
                    'used_budget' => $used,
                    'temporary_calculation' => $temporary,
                    'year' => $year,
                    'current' => $condition,
                    'divisi' => $divisi
                ];
                $this->budget->save($data2);
                session()->setFlashdata('success', 'Data Berhasil Ditambah');
                return redirect()->to('/budget');
        }
    }
        }  
        //dd($id_budget);
    }

    //function run from cron cob

}