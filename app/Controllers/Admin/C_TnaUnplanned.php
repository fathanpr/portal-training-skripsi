<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Approval;
use App\Models\M_Budget;
use App\Models\M_Tna;
use App\Models\UserModel;
use App\Models\M_TnaUnplanned;
use App\Models\M_Akomodasi;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\FilterType;

class C_TnaUnplanned extends BaseController
{
    private M_Tna $tna;
    private M_Akomodasi $akomodasi;
    private UserModel $user;

    private M_Approval $approval;

    private M_TnaUnplanned $unplanned;

    private M_Budget $budget;

    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
        $this->approval = new M_Approval();
        $this->unplanned = new M_TnaUnplanned();
        $this->budget = new M_Budget();
        $this->akomodasi = new M_Akomodasi();
    }

    public function index()
    {
        $departemen = $this->unplanned->getStatusWaitAdminUnplannedDepartemen();

        $data = [
            'tittle' => 'Form TNA',
            'dept' => $departemen,
            'tna' => $this->unplanned,
            'budget' => $this->budget,

        ];
        // dd($data);

        return view('admin/tnaunplanned', $data);
    }

    public function kadivStatusUnplanned()
    {
        $tna = $this->unplanned->getKadivStatusUnplanned();
        $tahun = [];
        foreach ($tna as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadiv Status Approval Unplanned Training',
            'tna' => $tna,
            'year' => $years,
            'user' => $this->user
        ];
        // dd($data);
        return view('admin/tnakadiv', $data);
    }

    public function kadivStatusUnplannedFilter($year)
    {
        $tna = $this->unplanned->getKadivStatusUnplannedFilter($year);
        $tna_year = $this->unplanned->getKadivStatusUnplanned();
        $tahun = [];


        foreach ($tna_year as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }

        if(empty($tahun)){
            $years = [date('Y'),date('Y')+1];
        }else{
            $years = array_unique($tahun);
        };

        $data = [
            'tittle' => 'Kadiv Status Approval Unplanned Training',
            'tna' => $tna,
            'year' => $years
        ];
        // dd($data);
        return view('admin/tnakadiv', $data);
    }

    public function bodStatusUnplanned()
    {
        $tna = $this->unplanned->getBodStatusUnplanned();
        $tahun = [];

        $filteredUnplanned = [];
    
        foreach ($tna as $item) {
            if ($item['biaya_actual'] > 2500000 || $tna['bagian'] == 'KADIV') {
                $filteredUnplanned[] = $item;
            }
        }

        
        if (!empty($filteredUnplanned)) {
            foreach ($filteredUnplanned as $item){
                $tahun[]  = substr($item['mulai_training'], 0, 4);
            }
            $years = array_unique($tahun);
        } else {
            $years = [date('Y')-1, date('Y')];
        }
        

        $data = [
            'tittle' => 'BOD Status Approval Unplanned Training',
            'tna' => $filteredUnplanned,
            'year' => $years
        ];
        // dd($data);
        return view('admin/tnabod', $data);
    }

    public function bodStatusUnplannedFilter($year)
    {
        $tna = $this->unplanned->getBodStatusUnplannedFilter($year);
        $tna_year = $this->unplanned->getBodStatusUnplanned();

        $filteredUnplanned = [];
        $filteredYear = [];
    
        foreach ($tna as $item) {
            if ($item['biaya_actual'] > 2600000) {
                $filteredUnplanned[] = $item;
            }
        }

        foreach ($tna_year as $item) {
            if ($item['biaya_actual'] > 2000000) {
                $filteredYear[] = $item;
            }
        }

        foreach ($filteredYear as $item){
            $tahun[]  = substr($item['mulai_training'], 0, 4);
        }
        $years = array_unique($tahun);


        $data = [
            'tittle' => 'BOD Status Approval Unplanned Training',
            'tna' => $filteredUnplanned,
            'year' => $years
        ];
        // dd($data);
        return view('admin/tnabod', $data);
    }

    // changed
    public function unplannedMonthly()
    {
        $TrainingMonthly = $this->unplanned->getUnplannedMonthly();
        $TrainingMothlyName = [];

        foreach ($TrainingMonthly as $Month) {
            $MONTH = [
                'Year Training' => $Month['Year Training'],
                'Month Training' => $Month['Planing Training'],
                'Planning Training' => date("F", mktime(0, 0, 0, $Month['Planing Training'], 10)),
                'Jumlah Training' => $Month['Jumlah Training'],
                'Admin Approval' => $Month['Admin Approval'],
                'BOD Approval' => $Month['BOD Approval'],
                'Reject' => $Month['Reject']
            ];
            array_push(
                $TrainingMothlyName,
                $MONTH
            );
        }

        $data = [
            'tittle' => 'Training Monthly Unplanned',
            'training' => $TrainingMothlyName,
            'unplanned' => $this->unplanned
        ];

        // dd($TrainingMothlyName);

        return view('admin/trainingmonthly', $data);
    }

    public function kadivAccept($Month,$Year)
    {
        $date = date_parse($Month);

        $departemen = $this->unplanned->getKadivAcceptDistinct($date['month'], $Year);

        // dd($departemen);
        $data = [
            'tittle' => 'KADIV Unplanned Accepted',
            'departemen' => $departemen,
            'stat' => $this->unplanned,
            'date' => $date['month'],
            'year' => $Year,
            'budget' => $this->budget,
            'user' => $this->user,
            'akomodasi' => $this->akomodasi
        ];
        
        return view('admin/kadivaccept', $data);
    }

    public function exportTrainingMonthly($month,$year){
        $data = $this->unplanned->getKadivAcceptForExport($month,$year);
 
         $spreadsheet = new Spreadsheet();
         $sheet = $spreadsheet->getActiveSheet();
 
         $spreadsheet->getActiveSheet()->getStyle('A1:N1')->getFill()
         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
         ->getStartColor()->setARGB('FFFFFF00');
 
         $sheet->getStyle('B')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
         $sheet->getStyle('J')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
         $sheet->getColumnDimension('A')->setWidth(30);
         $sheet->getColumnDimension('B')->setWidth(8);
         $sheet->getColumnDimension('C')->setWidth(40);
         $sheet->getColumnDimension('D')->setWidth(45);
         $sheet->getColumnDimension('E')->setWidth(30);
         $sheet->getColumnDimension('F')->setWidth(20);
         $sheet->getColumnDimension('G')->setWidth(40);
         $sheet->getColumnDimension('H')->setWidth(18);
         $sheet->getColumnDimension('I')->setWidth(18);
         $sheet->getColumnDimension('J')->setWidth(17);
         $sheet->getColumnDimension('K')->setWidth(25);
         $sheet->getColumnDimension('L')->setWidth(25);
         $sheet->getColumnDimension('M')->setWidth(25);
         $sheet->getColumnDimension('N')->setWidth(20);
 
         $sheet->setCellValue('A1', 'Name');
         $sheet->setCellValue('B1', 'NPK');
         $sheet->setCellValue('C1', 'Department');
         $sheet->setCellValue('D1', 'Training Title');
         $sheet->setCellValue('E1', 'Training Type');
         $sheet->setCellValue('F1', 'Training Category');
         $sheet->setCellValue('G1', 'Training Goals');
         $sheet->setCellValue('H1', 'Training Start');
         $sheet->setCellValue('I1', 'Training Finished');
         $sheet->setCellValue('J1', 'Actual Budget');
         $sheet->setCellValue('K1', 'Accomodation Budget');
         $sheet->setCellValue('L1', 'Total Budget');
         $sheet->setCellValue('M1', 'Training Group');
         $sheet->setCellValue('N1', 'Status');
 
         $row = 2;
         foreach ($data as $item) {
 
             if($item['status_training'] == NULL){
                 $statusTraining = 'Not Yet';
             }elseif($item['status_training'] == '1'){
                 $statusTraining = 'Implemented';
             }elseif($item['status_training'] == NULL && ($item['status_approval_0'] == 'reject' || $item['status_approval_1'] == 'reject' || $item['status_approval_2'] == 'reject' || $item['status_approval_3'] == 'reject')){
                 $statusTraining = 'Reject';
             }
 
             if(empty($item['biaya_akomodasi'])){
                 $biaya_akomodasi = 0;
             }else{
                 $biaya_akomodasi = intval(preg_replace("/[^0-9]/", "", $item['biaya_akomodasi']));
             }
 
             $new_biaya_actual = intval($item['biaya_actual'])-$biaya_akomodasi;
             $biayaRupiah = 'Rp. ' . number_format($new_biaya_actual, 0, ',', '.');
             $biaya_akomodasi = $item['biaya_akomodasi'] === NULL ? 'Biaya Akomodasi Belum Diinput' : $item['biaya_akomodasi'];
             
             $sheet->setCellValue('A' . $row, $item['nama']);
             $sheet->setCellValue('B' . $row, $item['npk']);
             $sheet->setCellValue('C' . $row, $item['departemen']);
             $sheet->setCellValue('D' . $row, $item['training']);
             $sheet->setCellValue('E' . $row, $item['jenis_training']);
             $sheet->setCellValue('F' . $row, $item['kategori_training']);
             $sheet->setCellValue('G' . $row, $item['tujuan_training']);
             $sheet->setCellValue('H' . $row, $item['mulai_training']);
             $sheet->setCellValue('I' . $row, $item['rencana_training']);
             $sheet->setCellValue('J' . $row, $biayaRupiah);
             $sheet->setCellValue('K' . $row, $biaya_akomodasi);
             $sheet->setCellValue('L' . $row, 'Rp. ' . number_format($item['biaya_actual'], 0, ',', '.'));
             $sheet->setCellValue('M' . $row, $item['kelompok_training']);
             $sheet->setCellValue('N' . $row, $statusTraining);
             $row++;
         }
 
         $spreadsheet->getActiveSheet()->setAutoFilter("A1:N1");
 
         $writer = new Xlsx($spreadsheet);
         $tempDir = WRITEPATH . 'spreadsheet/';
         $filename = date('Y-m-d-His'). '-Training-Monthly-Unplanned.xlsx';
         $writer->save($tempDir . $filename);
 
         return $this->response->download($tempDir . $filename, null);
     }
}