<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/png" href="<?= base_url('favicon3.ico') ?>">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="<?= base_url() ?>login/fonts/icomoon/style.css">

        <link rel="stylesheet" href="<?= base_url() ?>login/css/owl.carousel.min.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>login/css/bootstrap.min.css">

        <!-- Style -->
        <link rel="stylesheet" href="<?= base_url() ?>login/css/style.css">
    
        <title>Pilih Akun</title>
    </head>

    <body>



        <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
            <div class="card shadow p-3 mb-5 bg-white rounded" style="width: 400px;">
                <img src="<?= base_url() ?>logo.png" alt="Logo" style="margin: 0 auto;height:5rem;" class="my-3">
                <!-- <h5 class="text-center">Pilih Akun</h5> -->
                <div class="card-body">
                <div class="list-group">
                    <?php foreach($data as $item) :?>
                        <a href="<?= base_url() ?>pilih_akun/<?= $item->id_user ?>" class="list-group-item list-group-item-action"><span style="font-weight:bold;color:blue;"><?= $item->bagian ?></span> - <?= $item->seksi ?> - <?= $item->departemen ?></a>
                    <?php endforeach; ?>
                </div>
                <p style="font-size:12px;text-align:center;" class="my-2">Note :  Setiap Akun Memiliki Kompetensi yang berbeda.</p>
                </div>
                <a href="<?= base_url('/') ?>" class="text-center mb-3" style="color:blue;">Back to Login Menu</a>
            </div>
        </div>



        <script src="<?= base_url() ?>login/js/jquery-3.3.1.min.js"></script>
        <script src="<?= base_url() ?>login/js/popper.min.js"></script>
        <script src="<?= base_url() ?>login/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>login/js/main.js"></script>
    </body>

</html>