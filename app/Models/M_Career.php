<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Career extends Model
{
    protected $table      = 'career';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_career';
    protected $allowedFields = ['id_user', 'npk', 'year_start', 'year_end', 'position', 'departement', 'division', 'company'];
    public function getIdCareer($npk)
    {
        return $this->where(['npk' => $npk])->first();
    }
    public function getDataCareer($npk)
    {
        $this->select()->where('npk', $npk);
        return $this->get()->getResultArray();
    }
}