<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencyTechnical extends Model
{
    protected $table      = 'competency_profile_technical';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_technical';
    protected $allowedFields = ['id_user', 'npk', 'id_technical','type_technical', 'score_technical', 'updated_at'];


    public function getIdAstra($id)
    {
        return $this->where(['id_tna' => $id])->first();
    }

    public function getProfileTechnicalCompetency($id_user, $npk)
    {
        $this->select('technical_competency.id_technical,technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical,competency_profile_technical.updated_at');
        $this->groupStart()->where('competency_profile_technical.id_user',$id_user)->where('type_technical IS NULL')->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk',$npk)->where('type_technical IS NULL')->groupEnd();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }

    public function getTechnicalIdCompetency($id_user, $npk)
    {
        $this->select('technical_competency.id_technical');
        $this->groupStart()->where('competency_profile_technical.id_user',$id_user)->where('type_technical IS NULL')->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk',$npk)->where('type_technical IS NULL')->groupEnd();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalExpertCompetency($id_user,$npk)
    {
        $this->select('technical_competency.id_technical,technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical,competency_profile_technical.updated_at');
        $this->groupStart()->where('competency_profile_technical.id_user',$id_user)->where('type_technical','EXPERT')->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk',$npk)->where('type_technical','EXPERT')->groupEnd();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }


    public function getProfileTechnicalExpertCompetencyValue($id_user, $npk, $technical)
    {
        $this->select('technical_competency.id_technical,technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical');
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        $this->groupStart()->where('competency_profile_technical.id_user', $id_user)->where('technical_competency.technical', $technical)->where('competency_profile_technical.type_technical', 'EXPERT')->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk', $npk)->where('technical_competency.technical', $technical)->where('competency_profile_technical.type_technical', 'EXPERT')->groupEnd();
        return $this->get()->getResultArray();
    }


    public function getProfileTechnicalCompetencyValue($id_user, $npk, $technical)
    {
        $this->select('technical_competency.id_technical,technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical');
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        $this->groupStart()->where('competency_profile_technical.id_user', $id_user)->where('technical_competency.technical', $technical)->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk', $npk)->where('technical_competency.technical', $technical)->groupEnd();
        return $this->get()->getResultArray();
    }

    // public function getProfileTechnicalCompetencyWithDepartment($id, $department)
    // {
    //     $this->select('technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical')->where('competency_profile_technical.id_user', $id)->where('technical_competency.departemen');
    //     $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
    //     return $this->get()->getResultArray();
    // }

    public function getProfileTechnicalCompetencyDept($id_user, $npk, $department, $nama_jabatan, $type_user)
    {

        if(trim($type_user) == 'REGULAR'){
            $type_user = NULL;
        } else{
            $type_user = 'EXPERT';
        }
        
        $this->select('technical_competency.id_technical,technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical');
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        $this->groupStart()->where('competency_profile_technical.id_user', $id_user)->where('technical_competency.departemen', $department)->where('technical_competency.type_user', $type_user)->where('technical_competency.nama_jabatan', $nama_jabatan)->groupEnd();
        $this->orGroupStart()->where('competency_profile_technical.npk', $npk)->where('technical_competency.departemen', $department)->where('technical_competency.type_user', $type_user)->where('technical_competency.nama_jabatan', $nama_jabatan)->groupEnd();
        return $this->get()->getResultArray();
    }

    // public function getDataTechnical()
    // {
    //     $this->select();
    //     return $this->get()->getResultArray();
    // }


    public function getTechnicalByCompetency($id)
    {
        $this->select('technical_competency.technical,technical_competency.proficiency,competency_profile_technical.id_competency_technical,competency_profile_technical.score_technical')->where('competency_profile_technical.id_competency_technical', $id);
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }



    public function getDataDepertemenA()
    {
        $this->select('technical_competency.departemen,technical_competency.golongan')->distinct();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }
    public function getDataDepertemenB()
    {
        $this->select('technical_competency.departemen,technical_competency.golongan')->distinct();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical')->where('technical_competency.golongan', 'B');
        return $this->get()->getResultArray();
    }

    public function getProfileTechnicalCompetencyDepartment($id_user, $npk)
    {
        $this->select('technical_competency . departemen')->where('competency_profile_technical.id_user', $id_user)->orWhere('competency_profile_technical.npk', $npk)->distinct();
        $this->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical');
        return $this->get()->getResultArray();
    }

    public function getTechnicalExpertId($id_technical, $npk)
    {
        $this->select()->where('id_technical', $id_technical)->where('npk',$npk)->where('type_technical','EXPERT');
        return $this->get()->getResultArray();
    }

    public function getTechnicalId($id_technical, $npk)
    {
        $this->select()->where('id_technical', $id_technical)->where('npk',$npk)->where('type_technical IS NULL');
        return $this->get()->getResultArray();
    }

    public function getTechnicalCompetencySkillMap($department){
        $this->select('technical,npk,score_technical,proficiency,updated_at')
            ->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical')
            // ->where('competency_profile_technical.score_technical > 0')
            // ->where('competency_profile_technical.updated_at IS NOT NULL')
            ->where('departemen',$department)
            ->orderBy('competency_profile_technical.npk', 'ASC');
        return $this->get()->getResultArray();
    }
    
    public function userHaveTechnical($technical,$npk){
        $this->select()
            ->join('technical_competency', 'technical_competency.id_technical = competency_profile_technical.id_technical')
            ->where('competency_profile_technical.score_technical > 0')
            ->where('technical',$technical)
            ->where('npk',$npk);
        return $this->first();
    }
    
}