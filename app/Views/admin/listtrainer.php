<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="import" data-import="<?= session()->get('import'); ?>"></div>
<div class="card m-1">
    <div class="d-flex m-1">
        <div style="width:15%;">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Input
            </button>
        </div>

        <div class="col d-flex justify-content-end">
            <div style="width:30%;" class="ml-2">
                <form action="<?= base_url() ?>multiple_trainer" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" name="trainer">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="input-group-text">Upload</span>
                            </div>
                            <a class="btn btn-primary ml-2" href="<?= base_url() ?>template/template_upload_trainer.xlsx" role="button">Template</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    <div class="card-header">
        <h3 class="card-title">Trainer Internal List</h3>
    </div>

    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered" id="trainer-internal">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Trainer Name</th>
                    <th>Title Training</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1;
                foreach ($Internal as $internal) : ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $internal['nama'] ?></td>
                        <td>
                            <?php $materi = $training->getDataTrainingTrainer($internal['id_trainer']);
                            foreach ($materi as $material) {
                                echo $material['judul_training'] . '<br>';
                            }
                            ?>
                        </td>
                        <td>
                            <div class="d-flex">
                                <button type="button" class="btn btn-warning" onclick="TrainerProfile(<?= $internal['id_trainer'] ?>);">
                                    <i class="fa-solid fa-user"></i>
                                </button>
                                <form action="<?= base_url() ?>delete_trainer" method="post">
                                    <input type="hidden" value="<?= $internal['id_trainer'] ?>" name="id">
                                    <button type="submit" class="btn btn-danger btn-sm ml-2 btn-delete"><i class="fa-solid fa-trash"></i></button>
                                </form>

                            </div>
                        </td>

                    </tr>
                    <!-- Modal -->
                <?php $i++;
                endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="card-header">
        <h3 class="card-title">Trainer External List</h3>
    </div>

    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered" id="trainer-external">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Trainer Name</th>
                    <th>Title Training</th>
                    <th>Vendor</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1;
                foreach ($External as $external) : ?>
                    <tr>

                        <td><?= $i ?></td>
                        <td><?= $external['nama'] ?></td>
                        <td>
                            <?php $materi = $training->getDataTrainingTrainer($external['id_trainer']);
                            foreach ($materi as $material) {
                                echo $material['judul_training'] . '<br>';
                            }
                            ?>
                        </td>
                        <td><?= $external['vendor'] ?></td>

                    </tr>
                <?php $i++;
                endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url() ?>single_save_trainer" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input Trainer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Trainer Type</label>
                        <select class="form-control input-trainer-internal" id="TrainerType" name="TrainerType" required>
                            <option value="">Choose</option>
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                        </select>
                    </div>
                    <div class="form-group" id="SelectInternal">
                        <label>Select Trainer</label>
                        <select class="form-control js-example-basic-single" name="Trainer" style="width:100%;">
                                <option value="">Choose</option>
                                <?php foreach($user as $item) : ?>
                                    <option value="<?= $item->id_user ?>,<?= $item->nama  ?>"><?= $item->nama ?></option>
                                <?php endforeach; ?>
                        </select>
                    </div>
                    <div id="single_trainer">
                    </div>
                                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="TrainerProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Trainer Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- head -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">

                            <!-- Profile Image -->
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                                    </div>

                                    <h3 id="name" class="profile-username text-center">
                                    </h3>

                                    <p id="npk" class="text-muted text-center"></p>

                                    <ul class="list-group list-group-unbordered mb-3">
                                        <li class="list-group-item">
                                            <b>Divisi</b> <a id="division" class="float-right"></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Department</b> <a id="department" class="float-right"></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Section</b> <a id="section" class="float-right"></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Trainer Since</b> <input type="date" id="trainer_since" class="float-right">
                                            <input type="hidden" id="id_trainer">
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>

                        <!-- /.col -->
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header p-2">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">List Training</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Report
                                                Evaluasi Trainer</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Add
                                                List Training</a></li>
                                    </ul>
                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="activity">
                                            <div id="list_training"></div>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="timeline">
                                            <div id="report"></div>
                                        </div>
                                        <!-- /.tab-pane -->

                                        <div class="tab-pane" id="settings">
                                            <div id="edit"></div>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div><!-- /.card-body -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- foot -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="ChangeSinceTrainer()">Save changes</button>
            </div>
        </div>
    </div>
</div>


<script>    
    $('.js-example-basic-single').select2()
    $(document).ready(function() {
        $('#trainer-internal').DataTable();
        $('#trainer-external').DataTable();
        $('#SelectInternal').hide();
    });

    $('#TrainerType').change(function() {
        let type = $('#TrainerType').val()

        if (type === 'Internal') {
        $('#SelectInternal').show();
        } else{
        $('#SelectInternal').hide();
        }

        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>trainer_type",
            // async: true,
            dataType: "text",
            data: {
                type: type
            },
            success: function(data) {
                jQuery.noConflict();
                $('#exampleModal #single_trainer').html(data);
            }
        });

    });

    function Material(i) {
        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>trainer_material_adding",
            // async: true,
            dataType: "text",
            data: {
                i: i
            },
            success: function(data) {
                jQuery.noConflict();
                $('#exampleModal #single_trainer').append(data);
            }
        });

    }

    function RemoveMaterial(i) {
        $('#materi' + i).remove()
        $('#Remove' + i).remove()
        $('#add' + i).remove()
        $('#label' + i).remove()

    }

    //
    function TrainerProfile(id) {
        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>data_trainer_profile",
            async: true,
            dataType: "json",
            data: {
                id: id
            },
            success: function(data) {
                jQuery.noConflict();
                $('#list_training').html(data[3])
                $('#id_trainer').val(data[0][0].id_trainer)
                $('#name').text(data[0][0].nama)
                $('#npk').text(data[0][0].npk)
                $('#division').text(data[0][0].divisi)
                $('#department').text(data[0][0].departemen)
                $('#section').text(data[0][0].seksi)
                $('#trainer_since').val(data[0][0].trainer_since)
                $('#edit').html(data[1])
                $('#report').html(data[2])
                $('#TrainerProfile').modal('show')
            }
        });

    }

    function AddList(id) {
        let list = $('#list').val()
        let row = $("#material-table tr").length;
        let count = row;
        if (list != "") {
            $.ajax({
                type: 'post',
                url: "<?= base_url() ?>adding_training",
                async: true,
                dataType: "json",
                data: {
                    id: id,
                    list: list,
                    row: count
                },
                success: function(data) {
                    jQuery.noConflict();
                    $('#body-material').append(data);
                }
            });
        }
    }

    function DeleteMaterial(id_row, id_material) {
        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>delete_material",
            async: true,
            dataType: "json",
            data: {
                id: id_material
            },
            success: function(data) {
                jQuery.noConflict();
                document.getElementById("material-table").deleteRow(id_row);

            }
        });
    }

    function ChangeSinceTrainer() {

        let id_trainer = $('#id_trainer').val();
        let trainer_since = $('#trainer_since').val()

        $.ajax({
            type: 'post',
            url: "<?= base_url() ?>change_since",
            async: true,
            dataType: "json",
            data: {
                id_trainer: id_trainer,
                trainer_since: trainer_since
            },
            success: function(data) {
                console.log(data)
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        });
    }
</script>
<?= $this->endSection() ?>