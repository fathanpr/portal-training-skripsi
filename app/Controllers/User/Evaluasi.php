<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Report;
use App\Models\M_Tna;
use App\Models\M_Trainer;
use App\Models\UserModel;

class Evaluasi extends BaseController
{

    private M_Tna $tna;
    private M_EvaluasiReaksi $evaluasiReaksi;
    private UserModel $user;

    private M_Trainer $trainer;

    private M_Report $report;
    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->evaluasiReaksi = new M_EvaluasiReaksi();
        $this->user = new UserModel();
        $this->trainer = new M_Trainer();
        $this->report = new M_Report();
    }
    public function index()
    {
        $id = session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');

        $evaluasi =  $this->tna->getEvaluasiReaksi($npk,$bagian);

        $data = [
            'tittle' => 'Evaluasi Pelatihan',
            'evaluasi' => $evaluasi
        ];
        return view('user/daftarreaksi', $data);
    }

    public function other()
    {
        $id = session()->get('id');
        $npk = session()->get('npk');
        $bagian = session()->get('bagian');

        $evaluasi =  $this->tna->getEvaluasiReaksiOther($npk,$bagian);

        $data = [
            'tittle' => 'Evaluasi Pelatihan (AMDI AOP & CROSS BUDGET)',
            'evaluasi' => $evaluasi
        ];
        return view('user/daftarreaksi', $data);
    }

    public function EvaluasiForm($id)
    {
        $tna = $this->tna->getTnaById($id);
        $id = $this->tna->getDataForEvaluation($id,$tna[0]['npk'],$tna[0]['bagian']);
        // dd($id);

        $trainer = [];

        foreach ($id as $instruktur) {
            ($instruktur['instruktur_1'] != null) ? array_push($trainer, $this->trainer->getTrainerById($instruktur['instruktur_1'])) : "";
            ($instruktur['instruktur_2'] != null) ? array_push($trainer, $this->trainer->getTrainerById($instruktur['instruktur_2'])) : "";
            ($instruktur['instruktur_3'] != null) ? array_push($trainer, $this->trainer->getTrainerById($instruktur['instruktur_3'])) : "";
            ($instruktur['instruktur_4'] != null) ? array_push($trainer, $this->trainer->getTrainerById($instruktur['instruktur_4'])) : "";
            ($instruktur['instruktur_5'] != null) ? array_push($trainer, $this->trainer->getTrainerById($instruktur['instruktur_5'])) : "";
        }
        //dd($trainer);

        if (empty($trainer)) {
            session()->setFlashdata('talk', "Admin hasn't Input Trainer Yet");
            return redirect()->to('/evaluasi_reaksi');
        }

        // if($id[0]['id_user'] != session()->get('id')){
        //     session()->setFlashdata('talk', "Hanya boleh diisi oleh member yang bersangkutan!");
        //     return redirect()->redirect('/evaluasi_reaksi_member#');
        // }

        $data = [
            'tittle' => 'Evaluasi Reaksi',
            'data' => $id,
            'trainer' => $trainer
        ];
        return view('user/evaluasireaksi', $data);
    }

    //sampe sini
    public function SendEvaluasiReaksi()
    {
        $id =  $this->request->getPost('id_tna');
        $training = $this->evaluasiReaksi->getIdReaksi($id);
        $data = [
            'id_reaksi' =>  $training['id_reaksi'],
            'id_tna' =>  $training['id_tna'],
            'program' => $this->request->getPost('program[0]'),
            'tampilan' => $this->request->getPost('tampilan[0]'),
            'program_training' => $this->request->getPost('program_training[0]'),
            'metode' => $this->request->getPost('metode[0]'),
            'penambahan_keterampilan' => $this->request->getPost('penambahan[0]'),
            'kelayakan' => $this->request->getPost('kelayakan[0]'),
            'kelayakan_akomodasi' => $this->request->getPost('kelayakan_akomodasi[0]'),
            'harapan' => $this->request->getPost('harapan'),
            'perbaikan_program' => $this->request->getPost('perbaikan_program'),
            'instruktur_1' => $this->request->getPost('instruktur1'),
            'instruktur_2' => $this->request->getPost('instruktur2'),
            'instruktur_3' => $this->request->getPost('instruktur3'),
            'instruktur_4' => $this->request->getPost('instruktur4'),
            'instruktur_5' => $this->request->getPost('instruktur5'),
            'pengetahuan1' => $this->request->getPost('pengetahuan1'),
            'pengetahuan2' => $this->request->getPost('pengetahuan2'),
            'pengetahuan3' => $this->request->getPost('pengetahuan3'),
            'pengetahuan4' => $this->request->getPost('pengetahuan4'),
            'pengetahuan5' => $this->request->getPost('pengetahuan5'),
            'kemampuan1' => $this->request->getPost('kemampuan1'),
            'kemampuan2' => $this->request->getPost('kemampuan2'),
            'kemampuan3' => $this->request->getPost('kemampuan3'),
            'kemampuan4' => $this->request->getPost('kemampuan4'),
            'kemampuan5' => $this->request->getPost('kemampuan5'),
            'kemampuan_melibatkan1' => $this->request->getPost('kemampuan_melibatkan1'),
            'kemampuan_melibatkan2' => $this->request->getPost('kemampuan_melibatkan2'),
            'kemampuan_melibatkan3' => $this->request->getPost('kemampuan_melibatkan3'),
            'kemampuan_melibatkan4' => $this->request->getPost('kemampuan_melibatkan4'),
            'kemampuan_melibatkan5' => $this->request->getPost('kemampuan_melibatkan5'),
            'kemampuan_menanggapi1' => $this->request->getPost('kemampuan_menanggapi1'),
            'kemampuan_menanggapi2' => $this->request->getPost('kemampuan_menanggapi2'),
            'kemampuan_menanggapi3' => $this->request->getPost('kemampuan_menanggapi3'),
            'kemampuan_menanggapi4' => $this->request->getPost('kemampuan_menanggapi4'),
            'kemampuan_menanggapi5' => $this->request->getPost('kemampuan_menanggapi5'),
            'kemampuan_mengendalikan1' => $this->request->getPost('kemampuan_mengendalikan1'),
            'kemampuan_mengendalikan2' => $this->request->getPost('kemampuan_mengendalikan2'),
            'kemampuan_mengendalikan3' => $this->request->getPost('kemampuan_mengendalikan3'),
            'kemampuan_mengendalikan4' => $this->request->getPost('kemampuan_mengendalikan4'),
            'kemampuan_mengendalikan5' => $this->request->getPost('kemampuan_mengendalikan5'),
            'harapan_instruktur' => $this->request->getPost('harapan_instruktur'),
            'peningkatan_instruktur' => $this->request->getPost('peningkatan_instruktur'),
            'wawasan' => $this->request->getPost('wawasan'),
            'skill' => (int) $this->request->getPost('skill[0]'),
            'rekomendasi' => $this->request->getPost('rekomendasi[0]'),
            'kebutuhan' => $this->request->getPost('kebutuhan'),
            'status_evaluasi' => true,
        ];
        //dd($data);

        $this->evaluasiReaksi->save($data);

        $evaluation_tna = $this->tna->getAllTna($id);

        //$evaluation_Done = $this->tna->getDataScoreForTrainerDone($evaluation_tna[0]->id_training, $evaluation_tna[0]->mulai_training);
        $evaluation_Count = $this->tna->CountDataScoreForTrainer($evaluation_tna[0]->id_training, $evaluation_tna[0]->mulai_training);
        $report_trainer = $this->report->getReportByHandler($evaluation_tna[0]->id_handler_report);
        //dd($);
        // dd(count($report_trainer));
        for ($loop = 1; $loop < count($report_trainer) + 1; $loop++) {
            if ($data['instruktur_' . $loop] != null) {
                $sum = $data['pengetahuan' . $loop] + $data['kemampuan' . $loop] + $data['kemampuan_melibatkan' . $loop] + $data['kemampuan_menanggapi' . $loop] + $data['kemampuan_mengendalikan' . $loop];
                $nilai = $sum / 5;
                $score = $nilai / $evaluation_Count[0]['id_training'];
                $report = $this->report->getTrainerForPushScore($evaluation_tna[0]->id_handler_report, $data['instruktur_' . $loop]);
                $update_score = [
                    'id_report' => $report['id_report'],
                    'score' => $report['score'] + $score
                ];
                //d($update_score);
                $this->report->save($update_score);
            }
        }


        //session()->setFlashdata('success', 'Data Berhasil Di Import');
        return redirect()->to('/evaluasi_reaksi');
    }

    // public function SendEvaluasiReaksiUnplanned()
    // {
    //     $id =  $this->request->getPost('id_tna');


    //     if (!$this->validate([
    //         'instruktur1' => 'required',
    //         'pengetahuan1' => 'required',
    //         'kemampuan1' => 'required',
    //         'wawasan' => 'required',
    //     ])) {
    //         $validation = \Config\Services::validation();
    //         return redirect()->to('/form_evaluasi/' . $id)->withInput()->with('validation', $validation);
    //     }
    //     $training = $this->evaluasiReaksi->getIdReaksi($id);

    //     $data = [
    //         'id_reaksi' =>  $training['id_reaksi'],
    //         'id_tna' =>  $training['id_tna'],
    //         'program' => $this->request->getPost('program[0]'),
    //         'tampilan' => $this->request->getPost('tampilan[0]'),
    //         'program_training' => $this->request->getPost('program_training[0]'),
    //         'metode' => $this->request->getPost('metode[0]'),
    //         'penambahan_keterampilan' => $this->request->getPost('penambahan[0]'),
    //         'kelayakan' => $this->request->getPost('kelayakan[0]'),
    //         'kelayakan_akomodasi' => $this->request->getPost('kelayakan_akomodasi[0]'),
    //         'harapan' => $this->request->getPost('harapan'),
    //         'perbaikan_program' => $this->request->getPost('perbaikan_program'),
    //         'instruktur_1' => $this->request->getPost('instruktur1'),
    //         'instruktur_2' => $this->request->getPost('instruktur2'),
    //         'instruktur_3' => $this->request->getPost('instruktur3'),
    //         'instruktur_4' => $this->request->getPost('instruktur4'),
    //         'instruktur_5' => $this->request->getPost('instruktur5'),
    //         'pengetahuan1' => $this->request->getPost('pengetahuan1'),
    //         'pengetahuan2' => $this->request->getPost('pengetahuan2'),
    //         'pengetahuan3' => $this->request->getPost('pengetahuan3'),
    //         'pengetahuan4' => $this->request->getPost('pengetahuan4'),
    //         'pengetahuan5' => $this->request->getPost('pengetahuan5'),
    //         'kemampuan1' => $this->request->getPost('kemampuan1'),
    //         'kemampuan2' => $this->request->getPost('kemampuan2'),
    //         'kemampuan3' => $this->request->getPost('kemampuan3'),
    //         'kemampuan4' => $this->request->getPost('kemampuan4'),
    //         'kemampuan5' => $this->request->getPost('kemampuan5'),
    //         'kemampuan_melibatkan1' => $this->request->getPost('kemampuan_melibatkan1'),
    //         'kemampuan_melibatkan2' => $this->request->getPost('kemampuan_melibatkan2'),
    //         'kemampuan_melibatkan3' => $this->request->getPost('kemampuan_melibatkan3'),
    //         'kemampuan_melibatkan4' => $this->request->getPost('kemampuan_melibatkan4'),
    //         'kemampuan_melibatkan5' => $this->request->getPost('kemampuan_melibatkan5'),
    //         'kemampuan_menanggapi1' => $this->request->getPost('kemampuan_menanggapi1'),
    //         'kemampuan_menanggapi2' => $this->request->getPost('kemampuan_menanggapi2'),
    //         'kemampuan_menanggapi3' => $this->request->getPost('kemampuan_menanggapi3'),
    //         'kemampuan_menanggapi4' => $this->request->getPost('kemampuan_menanggapi4'),
    //         'kemampuan_menanggapi5' => $this->request->getPost('kemampuan_menanggapi5'),
    //         'kemampuan_mengendalikan1' => $this->request->getPost('kemampuan_mengendalikan1'),
    //         'kemampuan_mengendalikan2' => $this->request->getPost('kemampuan_mengendalikan2'),
    //         'kemampuan_mengendalikan3' => $this->request->getPost('kemampuan_mengendalikan3'),
    //         'kemampuan_mengendalikan4' => $this->request->getPost('kemampuan_mengendalikan4'),
    //         'kemampuan_mengendalikan5' => $this->request->getPost('kemampuan_mengendalikan5'),
    //         'harapan_instruktur' => $this->request->getPost('harapan_instruktur'),
    //         'peningkatan_instruktur' => $this->request->getPost('peningkatan_instruktur'),
    //         'wawasan' => $this->request->getPost('wawasan'),
    //         'skill' => (int) $this->request->getPost('skill[0]'),
    //         'rekomendasi' => $this->request->getPost('rekomendasi[0]'),
    //         'kebutuhan' => $this->request->getPost('kebutuhan'),
    //         'status_evaluasi' => true,
    //     ];
    //     // dd($data);
    //     $this->evaluasiReaksi->save($data);
    //     session()->setFlashdata('success', 'Data Berhasil Di Import');
    //     return redirect()->to('/evaluasi_reaksi_unplanned');
    // }

    public function DetailEvaluasiReaksi($id)
    {

        // getDetailEvaluasiReaksi
        $tna = $this->tna->getTnaById($id);
        $evaluasi  = $this->tna->getDetailEvaluasiReaksi($id, $tna[0]['npk'], $tna[0]['bagian']);
        $id_trainer = $evaluasi[0]['instruktur_1'];
        $instrukturArray = [];

        for ($i = 1; $i <= 5; $i++) {
            $instrukturKey = "instruktur_$i";

            if (isset($evaluasi[0][$instrukturKey])) {
                $instrukturArray[] = $evaluasi[0][$instrukturKey];
            }
        }
        $instrukturArray = array_values($instrukturArray);
        $dataTrainer = $this->trainer->getTrainerById($id_trainer);
        
        // $evaluasi = $this->tna->getDataForEvaluation($id);

        $data = [
            'tittle' => 'Evaluasi Pelatihan',
            'data' => $evaluasi,
            'dataTrainer' => $dataTrainer,
            'instrukturArray' => $instrukturArray,
            'trainer' => $this->trainer
        ];


        return view('user/detailevaluasireaksi', $data);
    }

    public function EvaluasiReaksiAdmin()
    {
        $user =  $this->user->getAllUser();
        $person = [];
        for ($i = 0; $i < count($user); $i++) {
            $users = [
                'id' => $user[$i]->id_user,
                'nama' => $user[$i]->nama,
                'npk' => $user[$i]->npk,
                'bagian' => $user[$i]->bagian
            ];

            array_push($person, $users);
        }
        // dd($user);

        $data = [
            'tittle' => 'Evaluasi Pelatihan (Admin)',
            'user' => $person,
            'tna' => $this->tna
        ];
        return view('admin/evaluasireaksiadmin', $data);
    }

    public function EvaluasiEfektivitasAdmin()
    {
        $user =  $this->user->getAllUser();
        $person = [];
        for ($i = 0; $i < count($user); $i++) {
            $users = [
                'id' => $user[$i]->id_user,
                'nama' => $user[$i]->nama,
                'npk' => $user[$i]->npk,
                'bagian' => $user[$i]->bagian
            ];

            array_push($person, $users);
        }
        // dd($user);

        $data = [
            'tittle' => 'Evaluasi Kompetensi (Admin)',
            'user' => $person,
            'tna' => $this->tna
        ];
        return view('admin/evaluasiefektivitasadmin', $data);
    }

    public function DetailEvaluasiReaksiAdmin()
    {
        $id =  $this->request->getPost('evaluasi');
        $npk =  $this->request->getPost('npk');
        $bagian =  $this->request->getPost('bagian');
        $evaluasi =  $this->tna->getAllEvaluasiReaksi($npk,$bagian);

        $data = [
            'tittle' => 'Detail Evaluasi Pelatihan (Admin)',
            'evaluasi' => $evaluasi,
        ];
        return view('admin/daftarreaksiadmin', $data);
    }

    public function DetailEvaluasiEfektivitasAdmin()
    {
        $id =  $this->request->getPost('evaluasi');
        $npk =  $this->request->getPost('npk');
        $bagian =  $this->request->getPost('bagian');
        $efektifitas = $this->tna->getDataForEvaluationTraining($id,$npk,$bagian);
        $dataEvaluasifixed = [];

        // dd($efektifitas);
        foreach ($efektifitas as $efektif) {

            $date_training = date_create($efektif['rencana_training']);
            $date_now = date_create(date('Y-m-d'));
            $compare = date_diff($date_training, $date_now);
            $due_date = (int)$compare->format('%a');
            $year_training = date('Y', strtotime($efektif['rencana_training']));
            $current_year = date('Y');
            $kelompok_training = $efektif['kelompok_training'];

            if ($due_date >= 90 && $year_training == $current_year) {
                $dataEvaluasiProcess = [
                    'id_tna' => $efektif['id_tna'],
                    'nama' => $efektif['nama'],
                    'judul' => $efektif['training'],
                    'jenis' => $efektif['jenis_training'],
                    'tanggal' => $efektif['rencana_training'],
                    'status' =>  $efektif['status_efektivitas'],
                    'kelompok_training' => $efektif['kelompok_training']
                ];

                array_push($dataEvaluasifixed, $dataEvaluasiProcess);
            }
        }
        // dd($dataEvaluasifixed);
        $data = [
            'tittle' => 'Detail Evaluasi Kompetensi (Admin)',
            'evaluasi' => $dataEvaluasifixed
        ];
        return view('admin/daftarefektivitasadmin', $data);
    }

    public function EvaluasiMember()
    {
        $id = session()->get('id');

        $user =  $this->user->filter($id);
        $person = [];
        for ($i = 0; $i < count($user); $i++) {
            $users = [
                'id' => $user[$i]->id_user,
                'nama' => $user[$i]->nama,
                'npk' => $user[$i]->npk,
                'bagian' => $user[$i]->bagian
            ];

            array_push($person, $users);
        }
        // dd($user);

        $data = [
            'tittle' => 'Evaluasi Pelatihan',
            'user' => $person,
            'tna' => $this->tna
        ];
        return view('user/evaluasireaksimember', $data);
    }
    

    public function EvaluasiMemberOther()
    {
        $id = session()->get('id');

        $user =  $this->user->filter($id);
        $person = [];
        for ($i = 0; $i < count($user); $i++) {
            $users = [
                'id' => $user[$i]->id_user,
                'nama' => $user[$i]->nama,
                'npk' => $user[$i]->npk,
                'bagian' => $user[$i]->bagian
            ];

            array_push($person, $users);
        }
        // dd($user);

        $data = [
            'tittle' => 'Evaluasi Pelatihan Lainnya',
            'user' => $person,
            'tna' => $this->tna
        ];
        return view('user/evaluasireaksimember', $data);
    }

    public function detailEvaluasimember()
    {
        $id =  $this->request->getPost('evaluasi');
        $npk =  $this->request->getPost('npk');
        $bagian =  $this->request->getPost('bagian');
        $evaluasi =  $this->tna->getEvaluasiReaksi($npk,$bagian);

        $data = [
            'tittle' => 'Evaluasi Pelatihan',
            'evaluasi' => $evaluasi,
        ];
        return view('user/daftarreaksi', $data);
    }

    public function detailEvaluasiMemberOther()
    {
        $id =  $this->request->getPost('evaluasi');
        $npk =  $this->request->getPost('npk'); 
        $bagian =  $this->request->getPost('bagian');
        $evaluasi =  $this->tna->getEvaluasiReaksiOther($npk,$bagian);

        $data = [
            'tittle' => 'Evaluasi Pelatihan Lainnya',
            'evaluasi' => $evaluasi
        ];
        return view('user/daftarreaksi', $data);
    }


    public function DataEvaluasi()
    {

        $id_training = $this->request->getPost('id_training');
        $evaluasi  = $this->tna->getDetailEvaluasiReaksi($id_training);
        for ($instruktur = 1; $instruktur <= 5; $instruktur++) {
            if ($evaluasi[0]['instruktur_' . $instruktur] != null) {
                $trainer = $this->trainer->getTrainer($evaluasi[0]['instruktur_' . $instruktur]);
                $evaluasi[0]['instruktur_' . $instruktur] = $trainer[0]['nama'];
            }
        }
        echo json_encode($evaluasi);
    }
}