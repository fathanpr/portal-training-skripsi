<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_Career;
use App\Models\M_CompetencyAstra;
use App\Models\M_CompetencyTechnicalB;
use App\Models\M_CompetencyTechnical;
use App\Models\M_CompetencyCompany;
use App\Models\M_CompetencyExpert;
use App\Models\M_CompetencySoft;
use App\Models\M_Contact;
use App\Models\M_Education;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Tna;
use App\Models\UserModel;


class Competency extends BaseController
{

    private M_CompetencyAstra $competencyAstra;
    private M_CompetencyTechnical $competencyTechnical;
    private M_CompetencyTechnicalB $competencyTechnicalB;
    private M_CompetencyCompany $competencyCompany;
    private M_CompetencyExpert $competencyExpert;
    private M_CompetencySoft $competencySoft;
    private UserModel $user;
    private M_Education $education;
    private M_Career $career;

    public function __construct()
    {
        $this->competencyAstra = new M_CompetencyAstra();
        $this->competencyTechnical = new M_CompetencyTechnical();
        $this->competencyTechnicalB = new M_CompetencyTechnicalB();
        $this->competencyExpert = new M_CompetencyExpert();
        $this->competencySoft = new M_CompetencySoft();
        $this->competencyCompany = new M_CompetencyCompany();
        $this->user = new UserModel();
        $this->education = new M_Education();
        $this->career = new M_Career();
    }

    public function index()
    {

        $id = session()->get('id');
        $bagian = $this->user->getAllUser($id);
        $user = $this->user->filter($id);

        if($bagian['bagian'] == 'KASIE' || $bagian['bagian'] == 'STAFF 4UP' || $bagian['bagian'] == 'KASUBSIE'){
            $memberUser = [];
            $kasubsie = [];
        }else{
            $memberUser = $this->user->filterMember($id);
            $kasubsie = $this->user->filterKasubsie($id);
        }

        // dd($kasubsie);

        $data = [
            'tittle' => 'Data Member',
            'user' => $user,
            'memberUser' => $memberUser,
            'kasubsie' => $kasubsie
        ];
        return view('user/membercompetency', $data);
    }


    public function MemberProfile()
    {
        $id = $this->request->getVar('member');
        $user = $this->user->getAllUser($id);
        $education = $this->education->getDataEducation($user['npk']);
        $career = $this->career->getDataCareer($user['npk']);
        $astraCompetency = $this->competencyAstra->getProfileAstraCompetency($id,$user['npk']);
        $technicalCompetency = $this->competencyTechnical->getProfileTechnicalCompetency($id,$user['npk']);
        $technicalbCompetency = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id,$user['npk']);
        $companyCompetency = $this->competencyCompany->getProfileCompanyCompetency($id,$user['npk']);
        $softCompetency = $this->competencySoft->getProfileSoftCompetency($id,$user['npk']);
        $expertCompetency = $this->competencyExpert->getProfileExpertCompetency($id,$user['npk']);

        // dd($technicalCompetency);

        //dd($astraCompetency);
        // dd($id);

        $data = [
            'tittle' => 'Profile',
            'person' => $user,
            'education' => $education,
            'career' => $career,
            'astra' => $astraCompetency,
            'technical' => $technicalCompetency,
            'technicalB' => $technicalbCompetency,
            'company' => $companyCompetency,
            'expert' => $expertCompetency,
            'soft' => $softCompetency,
            'id' => $id
        ];

        // dd($data);

        return view('user/profile', $data);
    }
}