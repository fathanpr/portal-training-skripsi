<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Approval;
use App\Models\M_Contact;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Handler_Report;
use App\Models\M_ListTraining;
use App\Models\M_Materi;
use App\Models\M_Report;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\M_Trainer;
use App\Models\UserModel;
use PhpParser\Builder\Function_;

class C_Trainer extends BaseController
{
    private UserModel $user;

    private M_Trainer $trainer;

    private M_Materi $materi;

    private M_ListTraining $training;

    private M_Tna $tna;

    private M_EvaluasiReaksi $reaksi;

    private M_Report $report;

    private M_Handler_Report $hanlder_report;
    public function __construct()
    {
        $this->user = new UserModel();
        $this->trainer = new M_Trainer();
        $this->materi = new M_Materi();
        $this->training = new M_ListTraining();
        $this->tna = new M_Tna();
        $this->reaksi = new M_EvaluasiReaksi();
        $this->report = new M_Report();
        $this->hanlder_report = new M_Handler_Report();
    }

    // function untuk menampilkan view list trainer 
    public function index()
    {
        $Internal = $this->trainer->getInternalTrainer();
        $External = $this->trainer->getExternalTrainer();
        $users = $this->user->getAllUser();

        // dd($Internal);
        $data = [
            'tittle' => 'Trainer',
            'Internal' => $Internal,
            'External' => $External,
            'user' => $users,
            'training' => $this->materi
        ];
        return view('admin/listtrainer', $data);
    }

    public function Type()
    {
        $type =  $this->request->getPost('type');
        $judul_training = $this->tna->getTna();

        
        $trainingData = [];
        $vendorData = [];

        foreach ($judul_training as $item) {
            $vendor = $item['vendor'];
            $vendorData[] = $vendor;
        }

        foreach ($judul_training as $item) {
            $training = $item['training'];
            $trainingData[] = $training;
        }

        $vendorData = array_filter($vendorData, function($value) {
            return $value !== null;
        });

        $vendorDistinct = array_unique($vendorData);

        $distinctTraining = array_unique($trainingData);

        

        echo $this->SingleHtmlInput($type,$distinctTraining,$vendorDistinct);
    }

    public function SingleHtmlInput($type, $distinctTraining,$vendorDistinct)
    {
        $users = $this->user->getAllUser();
        $trainings = $this->training->getAll();

        foreach ($distinctTraining as $item) {
            $trainingByTna = $this->training->getTrainingByTna($item);
            $judulTraining[] = $trainingByTna;
        }

        $mergedTrainings = [];
        foreach ($judulTraining as $trainingArray) {
            $mergedTrainings = array_merge($mergedTrainings, $trainingArray);
        }

        $i = 0;
        if ($type == 'Internal') {
            // echo '<div class="form-group">
            //             <label>Select Trainer</label>
            //             <select class="form-control" id="SelectTrainer" name="Trainer" required data-select2="true">
            //                 <option value="">Choose</option>';
            // foreach ($users as $trainer) {
            //     echo  '<option value="' . $trainer->id_user . "," . $trainer->nama . '">' . $trainer->nama . '</option>';
            // }
            // echo '</select> </div>';
            echo '<div class="form-group">
                    <label for="exampleInputEmail1">Trainer Since</label>
                    <input type="date" class="form-control" id="exampleInputEmail1" placeholder="Enter Trainer Since" name="since">
                  </div>';
            echo '<div id="materi">
            <div class="form-group">
                        <label>Select Training</label>
                        <select class="form-control" id="materi' . $i . '" name="material[]">
                          <option value="">Choose</option>';

            foreach ($mergedTrainings as $training) {
                echo '<option value="' . $training['id_training'] . '"">' . $training['judul_training'] . '</option>';
            }
            echo '</select>
                      </div>';
            echo '<button class="btn btn-primary btn-sm" type="button" onclick="Material(' . $i . ')"><i class="fa-solid fa-plus"></i></button></div>';
        } else {
            echo '
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" name="nama">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Vendor</label>
                    <select class="form-control" name="vendor">
                        <option value="">Choose</option>';
                        foreach($vendorDistinct as $vendor){
                            echo '<option value="'.$vendor.'">'.$vendor.'</option>';
                        }
                        echo '</select> </div>';
            echo '<div id="materi">
            <div class="form-group">
                        <label>Select Training</label>
                        <select class="form-control" id="materi' . $i . '" name="material[]">
                          <option value="">Choose</option>';

            foreach ($mergedTrainings as $training) {
                echo '<option value="' . $training['id_training'] . '"">' . $training['judul_training'] . '</option>';
            }
            echo '</select>
                      </div>';
            echo '<button class="btn btn-primary btn-sm" type="button" onclick="Material(' . $i . ')"><i class="fa-solid fa-plus"></i></button></div>';
        }
    }


    //function for dynamical add training for trainer
    public function Material()
    {
        $trainings = $this->training->getAll();
        $i = $this->request->getPost('i');
        $i++;
        echo '<div id="material">
            <div class="form-group">
                        <label id="label' . $i . '">Select Training</label>
                        <select class="form-control" id="materi' . $i . '" name="material[]">
                          <option value="">Choose</option>';

        foreach ($trainings as $training) {
            echo '<option value="' . $training['id_training'] . '"">' . $training['judul_training'] . '</option>';
        }
        echo '</select>
                      </div>';
        echo '<button  class="btn btn-primary btn-sm" type="button" onclick="Material(' . $i . ')" id="add' . $i . '"><i class="fa-solid fa-plus"></i></button>
        <button class="btn btn-warning btn-sm mr-2" type="button" onclick="RemoveMaterial(' . $i . ')" id="Remove' . $i . '"><i class="fa-solid fa-trash"></i></button></div>';
    }


    //function untuk menambahkan materi training ke trainer
    public function SingleSave()
    {
        $type =  $this->request->getVar('TrainerType');
        $material = $this->request->getPost('material');
        if ($type == 'Internal') {
            $trainerData = explode(",", $this->request->getVar('Trainer'));
            $trainer = [
                'id_user' => $trainerData[0],
                'nama' => $trainerData[1],
                'trainer_since' => $this->request->getVar('since')
            ];
            $this->trainer->save($trainer);

            $DataTrainer =  $this->trainer->getLastColumn();

            for ($i = 0; $i < count($material); $i++) {
                $MaterialsData = [
                    'id_training' => $material[$i],
                    'id_trainer' => $DataTrainer->id_trainer
                ];
                $this->materi->save($MaterialsData);
                //save
            }
        } else {
            $trainer = [
                'nama' => $this->request->getVar('nama'),
                'vendor' => $this->request->getVar('vendor')
            ];
            $this->trainer->save($trainer);

            $DataTrainer =  $this->trainer->getLastColumn();

            for ($i = 0; $i < count($material); $i++) {
                $MaterialsData = [
                    'id_training' => $material[$i],
                    'id_trainer' => $DataTrainer->id_trainer
                ];
                $this->materi->save($MaterialsData);
                //save
            }
        }
        return redirect()->to('/trainer');
    }


    //function untuk menampilkan profile trainer
    public function getDataTrainerProfile()
    {
        $id = $this->request->getPost('id');
        $data = [];
        $profile = $this->TrainerProfiles($id);
        $ListTraining = $this->TrainingListHtml($id);
        $report = $this->ReportTrainer($id);
        $list = $this->ReportList($id);
        array_push($data, $profile);
        array_push($data, $ListTraining);
        array_push($data, $report);
        array_push($data, $list);

        echo json_encode($data);
    }

    public function TrainerProfiles($id)
    {
        return  $this->trainer->getTrainer($id);
    }

    public  function TrainingListHtml($id = false)
    {

        $materi = $this->materi->getDataTrainingTrainer($id);
        $getTraining = $this->training->getAll();
        $body = "";
        $footer = "";
        $header = '<div class="card">
              <div class="card-header">
                <h3 class="card-title">Add Training</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered" id="material-table">
                  <thead>                  
                    <tr>
                      <th>Training</th>
                       <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="body-material">';
        $i = 1;
        foreach ($materi as $material) {
            $body .= '<tr>
                      <td>' . $material['judul_training'] . '</td>
                      <td><button class="btn btn-warning btn-sm" onclick="DeleteMaterial(' . $i . ',' . $material['id_materi'] . ')"><i class="fa fa-trash"></i></button></td>
                    </tr>';
            $i++;
        }
        $body .= '</tbody>
                </table>
              </div>
              </div>
              <div class="d-flex">
           <div class="form-group">
                        <select class="form-control" id="list">
                        <option value="">Choose</option>';
        foreach ($getTraining as $gets) {
            $footer .= '<option value="' . $gets['id_training'] . '">' . $gets['judul_training'] . '</option>';
        }
        $footer .= '</select>
                      </div>
                      <button class="btn btn-success btn-sm ml-2" style="height:38px;" onclick="AddList(' . $id . ')"><i class="fa-solid fa-plus"></i></button>
              </div>
            </div>';
        $html = $header . $body . $footer;

        return $html;
    }


    public function ReportTrainer($id)
    {
        $trainer = $this->report->getReportTrainer($id);

        $body = "";
        $head = '<div class="card">
              <div class="card-header">
                <h3 class="card-title">Report Trainer Evaluation</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Training Title</th>
                      <th>Date</th>
                      <th>Number of Hours</th>
                      <th>Score</th>
                    </tr>
                  </thead>
                  <tbody>';

        foreach ($trainer as $Trainer) {
            $body .= '<tr>
                      <td>' . $Trainer['judul_training'] . '</td>
                      <td>' . $Trainer['date'] . '</td>
                      <td>' . $Trainer['clock'] . '</td>
                      <td>' . $Trainer['score'] . '</td>
                    </tr>';
        }
        $footer =  '</tbody>
                </table>
              </div>
            </div>';


        $html = $head . $body . $footer;
        return $html;
    }


    public function ReportList($id)
    {
        $ReportList = $this->report->getReportList($id);
        $body = "";
        $head = '<div class="card">
              <div class="card-header">
                <h3 class="card-title">List Training</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Training Title</th>
                      <th>Number of Hours</th>
                    </tr>
                  </thead>
                  <tbody>';

        foreach ($ReportList as $Trainer) {
            $body .= '<tr>
                      <td>' . $Trainer['judul_training'] . '</td>
                      <td>' . $Trainer['clock'] . '</td>
                    </tr>';
        }
        $footer =  '</tbody>
                </table>
              </div>
            </div>';


        $html = $head . $body . $footer;
        return $html;
    }



    //4B 68 75 73 6E 75 6C
    public function AddingTraining()
    {
        $id_trainer =  $this->request->getPost('id');
        $id_training = $this->request->getPost('list');
        $row = $this->request->getPost('row');

        $data = [
            'id_trainer' => $id_trainer,
            'id_training' => $id_training
        ];

        $this->materi->save($data);
        $training = $this->materi->getMaterialLastRow($id_trainer);

        $html = '<tr>
        <td>' . $training->judul_training . '</td>
          <td><button class="btn btn-warning btn-sm" onclick="DeleteMaterial(' . $row . ',' . $training->id_materi . ')"><i class="fa fa-trash"></i></button></td>
        </tr>';

        echo json_encode($html);
    }



    public function DeleteMaterial()
    {
        $id = $this->request->getPost('id');
        $this->materi->delete($id);

        echo json_encode('success');
    }

    public function SaveMultipleTrainer()
    {
        $file = $this->request->getFile('trainer');
        if ($file == "") {
            return redirect()->to('/trainer');
        }
        $ext = $file->getClientExtension();
        if ($ext == 'xls') {
            $render = new
                \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new
                \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $render->load($file);
        $sheet = $spreadsheet->getActiveSheet()->toArray();
        // dd($sheet[1][1]);

        //PERUBAHAN
        for ($i = 1; $i < count($sheet); $i++) {
            if ($sheet[$i][1] != null) {
                $person = $this->user->getUserByNPK($sheet[$i][1]);
                if (!empty($person)) {
                    $trainer = [
                        'id_user' => $person['id_user'],
                        'npk' => $sheet[$i][1],
                        'nama' => $person['nama'],
                        'trainer_since' => $sheet[$i][3],
                        'vendor' => 'PT Century Battery Indonesia'
                    ];
                    $this->trainer->save($trainer);
                    $GetTrainer  = $this->trainer->getTrainerByIdUser($person['id_user']);
                    //dd($GetTrainer);
                    $append = explode(',', $sheet[$i][4]);
                    for ($m = 0; $m < count($append); $m++) {
                        $value = $this->training->getTna(trim($append[$m]));
                        if (!empty($value)) {
                            $material = [
                                'id_trainer' => $GetTrainer['id_trainer'],
                                'id_training' => $value[0]->id_training
                            ];
                            $this->materi->save($material);
                        }
                    }
                } else{
                    $trainer = [
                        'id_user' => NULL,
                        'npk' => NULL,
                        'nama' => $sheet[$i][2],
                        'trainer_since' => $sheet[$i][3],
                        'vendor' => $sheet[$i][5]
                    ];
                    // dd($trainer);
                    $this->trainer->save($trainer);
                    $GetTrainer  = $this->trainer->getTrainerByNamaUser($sheet[$i][2]);
                    //dd($GetTrainer);
                    $append = explode(',', $sheet[$i][4]);
                    for ($m = 0; $m < count($append); $m++) {
                        $value = $this->training->getTna(trim($append[$m]));
                        if (!empty($value)) {
                            $material = [
                                'id_trainer' => $GetTrainer['id_trainer'],
                                'id_training' => $value[0]->id_training
                            ];
                            $this->materi->save($material);
                        }
                    }
                }
            } else{
                $person = $this->user->getUserByNPK($sheet[$i][1]);
                if (!empty($person)) {
                    $trainer = [
                        'id_user' => $person['id_user'],
                        'npk' => $person['npk'],
                        'nama' => $person['nama'],
                        'trainer_since' => $sheet[$i][3],
                        'vendor' => 'PT Century Battery Indonesia'
                    ];
                    $this->trainer->save($trainer);
                    $GetTrainer  = $this->trainer->getTrainerByIdUser($person['id_user']);
                    //dd($GetTrainer);
                    $append = explode(',', $sheet[$i][4]);
                    for ($m = 0; $m < count($append); $m++) {
                        $value = $this->training->getTna(trim($append[$m]));
                        if (!empty($value)) {
                            $material = [
                                'id_trainer' => $GetTrainer['id_trainer'],
                                'id_training' => $value[0]->id_training
                            ];
                            $this->materi->save($material);
                        }
                    }
                } else{
                    $trainer = [
                        'id_user' => NULL,
                        'npk' => NULL,
                        'nama' => $sheet[$i][2],
                        'trainer_since' => $sheet[$i][3],
                        'vendor' => $sheet[$i][5]
                    ];
                    $this->trainer->save($trainer);
                    $GetTrainer  = $this->trainer->getTrainerByNamaUser($sheet[$i][2]);
                    //dd($GetTrainer);
                    $append = explode(',', $sheet[$i][4]);
                    for ($m = 0; $m < count($append); $m++) {
                        $value = $this->training->getTna(trim($append[$m]));
                        if (!empty($value)) {
                            $material = [
                                'id_trainer' => $GetTrainer['id_trainer'],
                                'id_training' => $value[0]->id_training
                            ];
                            $this->materi->save($material);
                        }
                    }
                }
            }
        }

        return redirect()->to('/trainer');
    }

    public function TrainerSelect()
    {
        $year = $this->tna->getYear();

        $mergedYear = [];
        foreach ($year as $yr) {
            $mergedYear[] = $yr['Years'];
        }
        $Year = array_merge($mergedYear);


        $trainings = [];
        foreach($Year as $years){
            $training =  $this->tna->TnaWillBeEvaluated($years);
            $trainings[] = $training;
        }

        $mergedData = [];
        foreach ($trainings as $training) {
            foreach ($training as $data) {
                $mergedData[] = $data;
            }
        }

        $countData = [];
        foreach($mergedData as $data){
            $lists = $this->tna->getTnaForTrainer($data['bulan'], $data['tahun']);
            $filteredLists = array_filter($lists, function($item) {
                return $item['instruktur_1'] === null;
            }); 
            $countData = array_merge($countData, $filteredLists);
        }
        

        // $training =  $this->tna->TnaWillBeEvaluated(date('Y')+1);

        // $Month = [];
        // foreach ($mergedData as $month) {
        //     $Month[] = date("F", mktime(0, 0, 0, $month['bulan'], 10));
        // }

        $data = [
            'tittle' => 'Trainer',
            'month' => $mergedData,
            'tna' => $this->tna,
            'unselected_trainer' => $countData
        ];


        return view('admin/selecttrainer', $data);
    }

    public function DetailTrainerForTna($month,$year)
    {
        $list = $this->tna->getTnaForTrainer($month, $year);

        // $date_start =  '2024-01-01';
        // $date_finish = '2024-01-06';
        // $id_training = 2;
        // $tna_data = $this->tna->getIntegrateTrainer($id_training,$date_start);
        // // $tna_data = $this->tna->getTnaByDateIdTraining($date_start,$date_finish,$id_training);
        // dd($tna_data);

        $data = [
            'tittle' => 'Add Trainer',
            'training' => $list,
            'trainer' =>  $this->materi
        ];

        // dd($list);
        

        return view('admin/traineraddingtna', $data);
    }

    public function IntegrateTrainer()
    {
        $handler = $this->request->getPost('handler');
        $duration = $this->request->getPost('duration');
        $id_training = $this->request->getPost('id_training');
        $date_start = $this->request->getPost('date_start');
        $date_finish = $this->request->getPost('date_finish');
        $date_start_old = $this->request->getPost('date_start_old');
        $date_finish_old = $this->request->getPost('date_finish_old');
        $instruktur1 = ($this->request->getPost('instruktur1') == 'NULL') ? NULL : $this->request->getPost('instruktur1');
        $instruktur2 = ($this->request->getPost('instruktur2') == 'NULL') ? NULL : $this->request->getPost('instruktur2');
        $instruktur3 = ($this->request->getPost('instruktur3') == 'NULL') ? NULL : $this->request->getPost('instruktur3');
        $instruktur4 = ($this->request->getPost('instruktur4') == 'NULL') ? NULL : $this->request->getPost('instruktur4');
        $instruktur5 = ($this->request->getPost('instruktur5') == 'NULL') ? NULL : $this->request->getPost('instruktur5');

        $combine = [$instruktur1, $instruktur2, $instruktur3, $instruktur4, $instruktur5];

        // $isValidInstruktur = true;
        // for ($i = 0; $i < count($combine); $i++) {
        //     if (!is_null($combine[$i]) && $combine[$i] != ($i + 1)) {
        //         $isValidInstruktur = false;
        //         break;
        //     }
        // }

        $input = array_filter($combine);

        $duplicates = $this->CheckDuplicate($input);

        if ($duplicates == false) {
            if ($handler != null) {
                $handler_edit = [
                    'id_handler_report' => $handler,
                    'date' => $date_start
                ];
                $this->hanlder_report->save($handler_edit);


                $new_tna = $this->tna->getIntegrateHasTrainer($handler);

                foreach ($new_tna as $New) {
                    $training = [
                        'id_tna' => $New['id_tna'],
                        'mulai_training' => $date_start,
                        'rencana_training' => $date_finish,
                        'man_hour' => $duration
                    ];
                    $this->tna->save($training);
                    $trainer = [
                        'id_reaksi' => $New['id_reaksi'],
                        'instruktur_1' => $instruktur1,
                        'instruktur_2' => $instruktur2,
                        'instruktur_3' => $instruktur3,
                        'instruktur_4' => $instruktur4,
                        'instruktur_5' => $instruktur5,
                        'duration' => $duration

                    ];
                    $this->reaksi->save($trainer);
                }

                $trainers_update = $this->report->getTrainerByHandler($handler);

                if (count($trainers_update) == count($input)) {
                    $loop = 0;
                    foreach ($trainers_update as $key) {
                        $updated = [
                            'id_report' => $key['id_report'],
                            'id_trainer' => $input[$loop],
                            'clock' => $duration
                        ];
                        $this->report->save($updated);
                        $loop++;
                    }
                } else {
                    if (count($trainers_update) < count($input)) {
                        for ($d = 0; $d < count($trainers_update); $d++) {
                            unset($input[$d]);
                        }
                        foreach ($input as $inputs) {

                            $new_input = [
                                'id_handler_report' => $handler,
                                'id_training' => $id_training,
                                'id_trainer' => $inputs,
                                'clock' => $duration
                            ];
                            $this->report->save($new_input);
                        }
                    } else {
                        for ($d = 0; $d < count($trainers_update); $d++) {
                            unset($trainers_update[$d]);
                        }
                        foreach ($trainers_update as $input_old) {
                            $check =  $this->report->getDataByTrainerAndDate($input_old['id_trainer'], $handler);
                            $this->report->delete($check[0]['id_report']);
                        }
                    }
                }
            } else {
                $training =  $this->tna->getIntegrateTrainer($id_training, $date_start_old);

                $handler_save = [
                    'id_training' => $id_training,
                    'date' => $date_start
                ];
                $this->hanlder_report->save($handler_save);
                $has_handler = $this->hanlder_report->getByTrainingDate($id_training, $date_start);


                for ($c = 0; $c < count($combine); $c++) {
                    if ($combine[$c] != null) {
                        $reports = [
                            'id_training' => $id_training,
                            'id_handler_report' => $has_handler['id_handler_report'],
                            'id_trainer' => $combine[$c],
                            'clock' => $duration
                        ];
                        $this->report->save($reports);
                    }
                }

                foreach ($training as $integrate) {
                    $tna = [
                        'id_tna' => $integrate['id_tna'],
                        'id_handler_report' => $has_handler['id_handler_report'],
                        'mulai_training' => $date_start,
                        'rencana_training' => $date_finish,
                        'man_hour' => $duration
                    ];
                    $this->tna->save($tna);
                    $trainer = [
                        'id_reaksi' => $integrate['id_reaksi'],
                        'instruktur_1' => $instruktur1,
                        'instruktur_2' => $instruktur2,
                        'instruktur_3' => $instruktur3,
                        'instruktur_4' => $instruktur4,
                        'instruktur_5' => $instruktur5,
                        'duration' => $duration

                    ];
                    $this->reaksi->save($trainer);
                }
            }
        }

        echo json_encode($duplicates);
    }


    public function CheckDuplicate($array)
    {
        if (count($array) != count(array_unique($array))) {
            return true;
        } else {
            return false;
        }
    }


    //main function For Report Training
    public function ReportTraining()
    {
        $id_training = $this->request->getPost('id_training');
        $date = $this->request->getPost('date_start');
        $reports = $this->tna->GetTrainingScoreReport($id_training, $date);
        $count = count($reports);
        $data = [];
        $participant = $this->Participant($id_training, $date);
        $ReportScore = $this->TrainingScoreReport($id_training, $date);
        $kesesuaian = $this->getTrainingKesesuaianProgramScore($reports, $count);
        $tampilan = $this->TampilanMateriTrainingScore($reports, $count);
        $program_training = $this->TampilanProgramTrainingScore($reports, $count);
        $kesesuaian_metode = $this->KesesuaianMetodeScore($reports, $count);
        $penambahan_keterampilan = $this->PenambahanKeterampilanScore($reports, $count);
        $kelayakanScore = $this->KelayakanScore($reports, $count);
        $kelayakanAkomodasi = $this->KelayakanAkomodasiScore($reports, $count);
        $harapan = $this->HarapanInstrukturHTML($reports);
        $peningkatan = $this->PeningkatanInstrukturHTML($reports);
        $wawasan = $this->WawasanHTML($reports);
        $skill = $this->ScoreSkill($reports, $count);
        $rekomendasi = $this->Rekomendasi($reports, $count);
        $kebutuhan = $this->KebutuhanHTML($reports);



        //$uisi / count($reports) * 100;

        array_push($data, $participant);
        array_push($data, $ReportScore);
        array_push($data, $kesesuaian);
        array_push($data, $tampilan);
        array_push($data, $program_training);
        array_push($data, $kesesuaian_metode);
        array_push($data, $penambahan_keterampilan);
        array_push($data, $kelayakanScore);
        array_push($data, $kelayakanAkomodasi);
        array_push($data, $harapan);
        array_push($data, $peningkatan);
        array_push($data, $wawasan);
        array_push($data, $skill);
        array_push($data, $rekomendasi);
        array_push($data, $kebutuhan);

        for ($loop = 1; $loop <= 5; $loop++) {
            $pengetahuanIstruktur = $this->Pengetahuaninstructur($reports, $count, $loop);
            $kemampuanInstruktur = $this->kemampuaninstructur($reports, $count, $loop);
            $kemampuanMelibatkan = $this->KemampuanMelibatkanInstructur($reports, $count, $loop);
            $kemampuanMenanggapi = $this->KemampuanMenanggapiInstructur($reports, $count, $loop);
            $kemampuanMengendalikan = $this->KemampuanMengendalikanInstructur($reports, $count, $loop);
            array_push($data, $pengetahuanIstruktur);
            array_push($data, $kemampuanInstruktur);
            array_push($data, $kemampuanMelibatkan);
            array_push($data, $kemampuanMenanggapi);
            array_push($data, $kemampuanMengendalikan);
        }

        echo json_encode($data);
    }


    public function Participant($id_training, $date)
    {

        $participants =  $this->tna->GetParticipants($id_training, $date);
        $body = '';
        $header = '<div class="form-group">
                        <h4>Participants</h4>
                        <select multiple="" class="form-control" disabled="">';
        foreach ($participants as $participant) {
            $body .= '<option>' . $participant['nama'] . '</option>';
        }
        $footer =       '</select>
                      </div>';

        $html = $header . $body . $footer;

        return $html;
    }


    //Function untuk menghitung Score Training User

    public function TrainingScoreReport($id_training, $date)
    {
        $reports = $this->tna->GetTrainingScoreReport($id_training, $date);

        $AvgProgram = [];

        // $count = Count($reports);
        //  $AvgInstruktur = [];
        foreach ($reports as $report) {

            // $names .= $report['nama'];
            if ($report['program'] != null) {

                $Avg = $this->AvgIsiProgram($report['program'], $report['tampilan'], $report['program_training'], $report['metode'], $report['penambahan_keterampilan'], $report['kelayakan'], $report['kelayakan_akomodasi']);

                $AvgInstruktur = array($report['instruktur_1'], $report['instruktur_2'], $report['instruktur_3'], $report['instruktur_4'], $report['instruktur_5']);

                $IntrsukturAvg = $this->AvgInstruktur($AvgInstruktur, $report['id_tna']);

                $Sum = $Avg + $IntrsukturAvg;
                $AvgValue = $Sum / 2;

                array_push($AvgProgram, $AvgValue);
            }
        }
        if ($AvgProgram != null) {
            $score = $this->ScoreLable($AvgProgram, count($reports));
        } else {
            $score = [0, 0, 0, 0];
        }

        return $score;
    }


    //Fungsi Menghitung Rata - Rata Nilai Isi Program Per User
    public function AvgIsiProgram($program, $tampilan, $program_training, $metode, $penambahan_keterampilan, $kelayakan, $kelayakan_akomodasi)
    {
        $sum = $program + $tampilan + $program_training + $metode + $penambahan_keterampilan + $kelayakan + $kelayakan_akomodasi;
        $avg = $sum / 7;
        return $avg;
    }

    //Fungsi Untuk Menghitung Nilai Rata - Rata Instruktur Per Evaluasi User 
    public function AvgInstruktur($insturkturs, $id_tna)
    {
        $data = $this->reaksi->getIdReaksi($id_tna);
        $loop = 1;
        $sum = 0;
        foreach ($insturkturs as $instruktur) {
            if ($instruktur != null) {
                $sum += $data['pengetahuan' . $loop] + $data['kemampuan' . $loop] + $data['kemampuan_melibatkan' . $loop] + $data['kemampuan_menanggapi' . $loop] + $data['kemampuan_mengendalikan' . $loop];

                $count = $loop;
            }
            $loop++;
        }
        $avg  = $sum / 5;
        $Avg = $avg / $count;
        return $Avg;
    }


    //funcion untuk Melable data Hasil Evaluasi User
    public function ScoreLable($score, $count)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        foreach ($score as $Score) {
            if ($Score <= 1) {
                $SangatTidakPuas++;
            } elseif ($Score <= 2) {
                $TidakPuas++;
            } elseif ($Score <= 3) {
                $Puas++;
            } else {
                $SangatPuas++;
            }
        }

        $SangatPuas = $SangatPuas / $count * 100;
        $Puas = $Puas / $count * 100;
        $TidakPuas = $TidakPuas / $count * 100;
        $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    //function For Program Score
    public function getTrainingKesesuaianProgramScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($IsiProgram)) {
            foreach ($IsiProgram as $Score) {

                if ($Score['program'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['program'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['program'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }


            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }

        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function TampilanMateriTrainingScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($IsiProgram)) {
            foreach ($IsiProgram as $Score) {

                if ($Score['tampilan'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['tampilan'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['tampilan'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }

        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function TampilanProgramTrainingScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($IsiProgram)) {
            foreach ($IsiProgram as $Score) {

                if ($Score['program_training'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['program_training'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['program_training'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }

        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }
    public function KesesuaianMetodeScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($IsiProgram)) {


            foreach ($IsiProgram as $Score) {

                if ($Score['metode'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['metode'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['metode'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }


    public function PenambahanKeterampilanScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($IsiProgram)) {
            foreach ($IsiProgram as $Score) {

                if ($Score['penambahan_keterampilan'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['penambahan_keterampilan'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['penambahan_keterampilan'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }

        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function KelayakanScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($IsiProgram)) {


            foreach ($IsiProgram as $Score) {

                if ($Score['kelayakan'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['kelayakan'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['kelayakan'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function KelayakanAkomodasiScore($IsiProgram, $count)
    {
        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($IsiProgram)) {
            foreach ($IsiProgram as $Score) {
                if ($Score['kelayakan_akomodasi'] <= 1) {
                    $SangatTidakPuas++;
                } elseif ($Score['kelayakan_akomodasi'] <= 2) {
                    $TidakPuas++;
                } elseif ($Score['kelayakan_akomodasi'] <= 3) {
                    $Puas++;
                } else {
                    $SangatPuas++;
                }
            }
            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }

        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function Pengetahuaninstructur($results, $count, $loop)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;

        if (!empty($results)) {


            foreach ($results as $Score) {
                if ($Score['pengetahuan' . $loop] != null) {
                    if ($Score['pengetahuan' . $loop] <= 1) {
                        $SangatTidakPuas++;
                    } elseif ($Score['pengetahuan' . $loop] <= 2) {
                        $TidakPuas++;
                    } elseif ($Score['pengetahuan' . $loop] <= 3) {
                        $Puas++;
                    } elseif ($Score['pengetahuan' . $loop] <= 4) {
                        $SangatPuas++;
                    }
                }
            }


            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }


    public function kemampuaninstructur($results, $count, $loop)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($results)) {
            foreach ($results as $Score) {
                if ($Score['kemampuan' . $loop] != null) {
                    if ($Score['kemampuan' . $loop] <= 1) {
                        $SangatTidakPuas++;
                    } elseif ($Score['kemampuan' . $loop] <= 2) {
                        $TidakPuas++;
                    } elseif ($Score['kemampuan' . $loop] <= 3) {
                        $Puas++;
                    } elseif ($Score['kemampuan' . $loop] <= 4) {
                        $SangatPuas++;
                    }
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function KemampuanMelibatkanInstructur($results, $count, $loop)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($results)) {
            foreach ($results as $Score) {
                if ($Score['kemampuan_melibatkan' . $loop] != null) {
                    if ($Score['kemampuan_melibatkan' . $loop] <= 1) {
                        $SangatTidakPuas++;
                    } elseif ($Score['kemampuan_melibatkan' . $loop] <= 2) {
                        $TidakPuas++;
                    } elseif ($Score['kemampuan_melibatkan' . $loop] <= 3) {
                        $Puas++;
                    } elseif ($Score['kemampuan_melibatkan' . $loop] <= 4) {
                        $SangatPuas++;
                    }
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }
    public function KemampuanMenanggapiInstructur($results, $count, $loop)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($results)) {
            foreach ($results as $Score) {
                if ($Score['kemampuan_menanggapi' . $loop] != null) {
                    if ($Score['kemampuan_menanggapi' . $loop] <= 1) {
                        $SangatTidakPuas++;
                    } elseif ($Score['kemampuan_menanggapi' . $loop] <= 2) {
                        $TidakPuas++;
                    } elseif ($Score['kemampuan_menanggapi' . $loop] <= 3) {
                        $Puas++;
                    } elseif ($Score['kemampuan_menanggapi' . $loop] <= 4) {
                        $SangatPuas++;
                    }
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function KemampuanMengendalikanInstructur($results, $count, $loop)
    {

        $SangatPuas = 0;
        $Puas = 0;
        $TidakPuas = 0;
        $SangatTidakPuas = 0;
        if (!empty($results)) {
            foreach ($results as $Score) {
                if ($Score['kemampuan_mengendalikan' . $loop] != null) {

                    if ($Score['kemampuan_mengendalikan' . $loop] <= 1) {
                        $SangatTidakPuas++;
                    } elseif ($Score['kemampuan_mengendalikan' . $loop] <= 2) {
                        $TidakPuas++;
                    } elseif ($Score['kemampuan_mengendalikan' . $loop] <= 3) {
                        $Puas++;
                    } elseif ($Score['kemampuan_mengendalikan' . $loop] <= 4) {
                        $SangatPuas++;
                    }
                }
            }

            $SangatPuas = $SangatPuas / $count * 100;
            $Puas = $Puas / $count * 100;
            $TidakPuas = $TidakPuas / $count * 100;
            $SangatTidakPuas = $SangatTidakPuas / $count * 100;
        }
        $chart = [$SangatPuas, $Puas, $TidakPuas, $SangatTidakPuas];

        return $chart;
    }

    public function HarapanInstrukturHTML($results)
    {

        $body = '';
        $head = '<div class="form-group">
                        <label>Hal-hal apa saja dari Instruktur yang telah memenuhi harapan anda?</label>
                        <select multiple="" class="form-control" disabled="">';
        foreach ($results as $result) {
            $body .= '<option>' . $result['harapan_instruktur'] . '</option>';
        }

        $footer =   '</select>
                      </div>';

        $Html = $head . $body . $footer;
        return $Html;
    }

    public function PeningkatanInstrukturHTML($results)
    {

        $body = '';
        $head = '<div class="form-group">
                        <label>Hal-hal yang perlu diperbaiki/ ditingkatkan dari Instruktur</label>
                        <select multiple="" class="form-control" disabled="">';
        foreach ($results as $result) {
            $body .= '<option>' . $result['peningkatan_instruktur'] . '</option>';
        }

        $footer =   '</select>
                      </div>';

        $Html = $head . $body . $footer;
        return $Html;
    }

    public function WawasanHTML($results)
    {

        $body = '';
        $head = '<div class="form-group">
                        <label>Selama mengikuti training ini, insight (wawasan) apa yang anda dapatkan ?</label>
                        <select multiple="" class="form-control" disabled="">';
        foreach ($results as $result) {
            $body .= '<option>' . $result['wawasan'] . '</option>';
        }

        $footer =   '</select>
                      </div>';

        $Html = $head . $body . $footer;
        return $Html;
    }


    public function ScoreSkill($results, $count)
    {

        $seratus = 0;
        $tujuhlima = 0;
        $limapuluh = 0;
        $dualima = 0;
        if (!empty($results)) {
            foreach ($results as $Score) {

                if ($Score['skill'] == 100) {
                    $seratus++;
                } elseif ($Score['skill'] == 75) {
                    $tujuhlima++;
                } elseif ($Score['skill'] == 50) {
                    $limapuluh++;
                } else {
                    $dualima++;
                }
            }

            $seratus = $seratus / $count * 100;
            $tujuhlima = $tujuhlima / $count * 100;
            $limapuluh = $limapuluh / $count * 100;
            $dualima = $dualima / $count * 100;
        }
        $chart = [$seratus, $tujuhlima, $limapuluh, $dualima];

        return $chart;
    }

    public function Rekomendasi($results, $count)
    {

        $Ya = 0;
        $Tidak = 0;
        if (!empty($results)) {


            foreach ($results as $Score) {

                if ($Score['rekomendasi'] == 1) {
                    $Ya++;
                } else {
                    $Tidak++;
                }
            }

            $Ya = $Ya / $count * 100;
            $Tidak = $Tidak / $count * 100;
        }
        $chart = [$Ya, $Tidak];

        return $chart;
    }

    public function KebutuhanHTML($results)
    {

        $body = '';
        $head = '<div class="form-group">
                        <label>Training apa yang Anda butuhkan di masa yang akan datang, dan alasannya?</label>
                        <select multiple="" class="form-control" disabled="">';
        foreach ($results as $result) {
            $body .= '<option>' . $result['kebutuhan'] . '</option>';
        }

        $footer =   '</select>
                      </div>';

        $Html = $head . $body . $footer;
        return $Html;
    }




    public function ChangeSince()
    {
        $id_trainer = $this->request->getPost('id_trainer');
        $since = $this->request->getPost('trainer_since');


        $data = [
            'id_trainer' => $id_trainer,
            'trainer_since' => $since
        ];

        $this->trainer->save($data);

        echo json_encode('success');
    }

    public function DeleteTrainer()
    {
        $id_trainer = $this->request->getPost('id');
        //dd($id_trainer);

        $this->trainer->delete($id_trainer);

        return redirect()->to('/trainer');
    }
}
