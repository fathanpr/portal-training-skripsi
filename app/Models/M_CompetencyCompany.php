<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencyCompany extends Model
{
    protected $table      = 'competency_profile_company';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_company';
    protected $allowedFields = ['id_user', 'npk', 'id_company', 'score_company','updated_at'];


    public function getIdCompany($id)
    {
        return $this->where(['id_company' => $id])->first();
    }

    public function getProfileCompanyCompetencyDivisionById($id)
    {
        $this->select('company_competency . divisi')->where('competency_profile_company.id_user', $id)->distinct();
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        return $this->get()->getResultArray();
    }

    public function getProfileCompanyCompetency($id_user,$npk)
    {

        $this->select('company_competency.company,company_competency.proficiency,competency_profile_company.id_competency_company,competency_profile_company.score_company,competency_profile_company.updated_at')->where('competency_profile_company.id_user', $id_user)->orWhere('competency_profile_company.npk', $npk);
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        return $this->get()->getResultArray();
    }

    public function getProfileCompanyCompetencyValue($id_user,$npk, $company)
    {

        $this->select('company_competency.company,company_competency.proficiency,competency_profile_company.id_competency_company,competency_profile_company.score_company');
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        $this->groupStart()->where('competency_profile_company.id_user', $id_user)->where('company_competency.company', $company)->groupEnd();
        $this->orGroupStart()->where('competency_profile_company.npk', $npk)->where('company_competency.company', $company)->groupEnd();
        return $this->get()->getResultArray();
    }

    public function getProfileCompanyCompetencyDivision($id_user, $npk, $divisi)
    {

        $this->select('company_competency.company,company_competency.proficiency,competency_profile_company.id_competency_company,competency_profile_company.score_company');
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        $this->groupStart()->where('competency_profile_company.id_user', $id_user)->where('company_competency.divisi', $divisi)->groupEnd();
        $this->orGroupStart()->where('competency_profile_company.npk', $npk)->where('company_competency.divisi', $divisi)->groupEnd();
        return $this->get()->getResultArray();
    }
    
    public function getCompanyByIdCompetency($id)
    {

        $this->select('company_competency.company,company_competency.proficiency,competency_profile_company.id_competency_company,competency_profile_company.score_company')->where('competency_profile_company.id_competency_company', $id);
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        return $this->get()->getResultArray();
    }

    public function getCompanyIdCompetency($id_user, $npk)
    {
        $this->select('competency_profile_company.id_company')->where('competency_profile_company.id_user', $id_user)->orWhere('competency_profile_company.npk', $npk);
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        return $this->get()->getResultArray();
    }
    

    public function getDataDivisi()
    {
        $this->select('company_competency.divisi')->distinct();
        $this->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company');
        return $this->get()->getResultArray();
    }

    public function getCompanyById($id_company, $npk)
    {
        $this->select()->where('id_company', $id_company)->where('npk',$npk);
        return $this->get()->getResultArray();
    }

    public function getCompanyCompetencySkillMap($divisi){
        $this->select('company,npk,score_company,proficiency,updated_at')
            ->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company')
            // ->where('competency_profile_company.score > 0')
            // ->where('competency_profile_company.updated_at IS NOT NULL')
            ->where('divisi',$divisi)
            ->orderBy('competency_profile_company.npk', 'ASC');
        return $this->get()->getResultArray();
    }
    
    public function userHaveCompany($company,$npk){
        $this->select()
            ->join('company_competency', 'company_competency.id_company = competency_profile_company.id_company')
            ->where('company', $company)
            ->where('npk',$npk);
        return $this->first();
    }
}