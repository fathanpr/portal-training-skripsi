<?php

namespace App\Controllers;

use App\Models\UserModel;


class Login extends BaseController
{
    private UserModel $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function index()
    {

        $data = [
            'tittle' => 'Portal Training | Log In'
        ];
        return view('login', $data);
    }

    public function validation()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        //dd($username);
        $row = $this->userModel->get_data_login($username);
        //dd($row);

        if ($row == null) {
            // return redirect()->to(base_url('/'));
            // session()->setFlashdata('message', 'username salah');
            $url = "https://portal3.incoe.astra.co.id/production_control_v2/public/api/login";

            // Set the data
            $data = array(
                'username' => $username,
                'password' => $password
            );

            // Set the options
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );

            // Create the context
            $context  = stream_context_create($options);

            // Send the request
            $response = file_get_contents($url, false, $context);
            $responseData = json_decode($response);

            if ($responseData == null || $responseData == ' ') {
                return redirect()->to(base_url('/'));
            }

            $npk = $responseData->npk;

            $row2 = $this->userModel->get_data_login_api($npk);

            if (empty($row2)) {
                return redirect()->to(base_url('/'));
            } elseif (count($row2) > 1) {
                session()->set('log',true);
                return view('user/pilihakun', ['data' => $row2]);
            } else {
                $data = array(
                    'log' => true,
                    'id' => $row2[0]->id_user,
                    'nama' => $row2[0]->nama,
                    'npk' => $row2[0]->npk,
                    'username' => $row2[0]->username,
                    'dic' => $row2[0]->dic,
                    'divisi' => $row2[0]->divisi,
                    'departemen' => $row2[0]->departemen,
                    'seksi' => $row2[0]->seksi,
                    'bagian' => $row2[0]->bagian,
                    'level' => $row2[0]->level,
                    'image' => $row2[0]->profile,
                    'email' => $row2[0]->email
                );

                if ($data['level'] == 'USER') {
                    session()->set($data);
                    session()->setFlashdata('message', 'Login Berhasil');
                    return redirect()->to('/home_user');
                } else {
                    session()->set($data);
                    session()->setFlashdata('message', 'Login Berhasil');
                    return redirect()->to('/home');
                }
            }
        } elseif (password_verify($password, $row->password)) {
            $data = array(
                'log' => true,
                'id' => $row->id_user,
                'nama' => $row->nama,
                'npk' => $row->npk,
                'username' => $row->username,
                'dic' => $row->dic,
                'divisi' => $row->divisi,
                'departemen' => $row->departemen,
                'seksi' => $row->seksi,
                'bagian' => $row->bagian,
                'level' => $row->level,
                'image' => $row->profile,
                'email' => $row->email
            );

            if ($data['level'] == 'USER') {
                session()->set($data);
                session()->setFlashdata('message', 'Login Berhasil');
                return redirect()->to('/home_user');
            } else {
                session()->set($data);
                session()->setFlashdata('message', 'Login Berhasil');
                return redirect()->to('/home');
            }
        }
        // session()->setFlashdata('message', 'Password Salah');
        return redirect()->to(base_url('/'));
    }

    public function pilih_akun($id_user){
        $user = $this->userModel->getAllUser($id_user);
        $data = array(
            'log' => true,
            'id' => $user['id_user'],
            'nama' => $user['nama'],
            'npk' => $user['npk'],
            'username' => $user['username'],
            'dic' => $user['dic'],
            'divisi' => $user['divisi'],
            'departemen' => $user['departemen'],
            'seksi' => $user['seksi'],
            'bagian' => $user['bagian'],
            'level' => $user['level'],
            'image' => $user['profile'],
            'email' => $user['email']
        );

        if ($data['level'] == 'USER') {
            session()->set($data);
            session()->setFlashdata('message', 'Login Berhasil');
            return redirect()->to('/home_user');
        } else {
            session()->set($data);
            session()->setFlashdata('message', 'Login Berhasil');
            return redirect()->to('/home');
        }
    }

    public function ganti_password(){
        return view('ganti_password');
    }

    public function submit_change_password(){
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $new_password = $this->request->getPost('new_password');
    
        $row = $this->userModel->get_data_login($username);
        $response = [];
        if ($row == null) {
            $response['status'] = 'error';
            $response['message'] = 'User not found';
            echo json_encode($response);
            return;
        }
        if (password_verify($password, $row->password)) {
            $data = [
                'id_user' => $row->id_user,
                'password' => password_hash($new_password , PASSWORD_DEFAULT)
            ];
            $this->userModel->save($data);
            $response['status'] = 'success';
            $response['message'] = 'Password changed successfully';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Password incorrect';
        }
        echo json_encode($response);
    }


    public function logout()
    {
        $session = session();
        $session->destroy();
        session()->setFlashdata('message', 'berhasil Logout');
        return redirect()->to(base_url('/'));
    }
}