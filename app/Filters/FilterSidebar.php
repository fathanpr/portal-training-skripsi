<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class FilterSidebar implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        $tna = new \App\Models\M_Tna;
        $unplanned = new \App\Models\M_TnaUnplanned;
        $budget = new \App\Models\M_Budget;
        $user = new \App\Models\UserModel;
        $silabus = new \App\Models\M_Silabus;

        $bagian = session()->get('bagian');
        $dic = session()->get('dic');
        $divisi = session()->get('divisi');
        $departemen = session()->get('departemen');
        $id_user = session()->get('id');
        $npk = session()->get('npk');   
        $currentYear = date('Y');
        $nextYear = $currentYear + 1;


        $listTna = $tna->countStatusWaitAdmin();
        $listUnplanned = $unplanned->countStatusWaitAdminUnplanned();
        $kadeptStatusWait = $tna->countKadeptStatusWait();
        $kadeptStatusUnplannedWait = $unplanned->countKadeptStatusUnplannedWait();
        $kadivStatusWait = $tna->countKadivStatusWait();
        $kadivStatusUnplannedWait = $unplanned->countKadivStatusUnplannedWait();
        $bodStatusWait = $tna->countBodStatusWait();
        $bodStatusUnplannedWait = $unplanned->countBodStatusUnplannedWait();
        $trainingMonthly = $tna->countTrainingMonthly();
        $otherTrainingMonthly = $tna->countOtherTrainingMonthly();
        $unplannedMonthly = $unplanned->countUnplannedMonthly();
        $schedule =$tna->countSchedule();
        $scheduleOther =$tna->countScheduleOther();
        $scheduleUnplanned =$unplanned->countSchedule();
        $evaluasiEfektivitasAdmin = $tna->countTotalEvaluasiEfektivitas();
        $evaluasiReaksi = $tna->countEvaluasiReaksi($npk,$bagian);
        $evaluasiReaksiAdmin = $tna->countTotalEvaluasiReaksi();
        $evaluasiReaksiUnplanned = $unplanned->countEvaluasiReaksi($npk,$bagian);
        $evaluasiReaksiOther = $tna->countEvaluasiReaksiOther($npk,$bagian);
        $requestSylabusUnread = $silabus->countSylabusUnread();

        //EFEKTIVITAS UNPLANNED NOTIF
        $efektifitas = $tna->countDataForEvaluationUnplanned($npk,$bagian);
        $dataEvaluasifixed = [];
        foreach ($efektifitas as $efektif) {

            $date_training = date_create($efektif['rencana_training']);
            $date_now = date_create(date('Y-m-d'));
            $compare = date_diff($date_training, $date_now);
            $due_date = (int)$compare->format('%a');
            $year_training = date('Y', strtotime($efektif['rencana_training']));
            $current_year = date('Y');
            
            if ($due_date >= 90 && $year_training == $current_year) {
                $dataEvaluasiProcess = [
                    'id_tna' => $efektif['id_tna'],
                    'nama' => $efektif['nama'],
                    'judul' => $efektif['training'],
                    'jenis' => $efektif['jenis_training'],
                    'tanggal' => $efektif['rencana_training'],
                    'status' =>  $efektif['status_efektivitas']
                ];

                array_push($dataEvaluasifixed, $dataEvaluasiProcess);
            }
        }
        
        $evaluasiEfektivitasPersonalUnplanned = count($dataEvaluasifixed);
        // END

        // EFEKTIVITAS TRAINING NOTIF
        $efektifitas1 = $tna->countDataForEvaluationTraining($npk,$bagian);
        $dataEvaluasifixed1 = [];

        foreach ($efektifitas1 as $efektif) {

            $date_training = date_create($efektif['rencana_training']);
            $date_now = date_create(date('Y-m-d'));
            $compare = date_diff($date_training, $date_now);
            $due_date = (int)$compare->format('%a');
            $year_training = date('Y', strtotime($efektif['rencana_training']));
            $current_year = date('Y');
            
            if ($due_date >= 90 && $year_training == $current_year ) {
                $dataEvaluasiProcess = [
                    'id_tna' => $efektif['id_tna'],
                    'nama' => $efektif['nama'],
                    'judul' => $efektif['training'],
                    'jenis' => $efektif['jenis_training'],
                    'tanggal' => $efektif['rencana_training'],
                    'status' =>  $efektif['status_efektivitas']
                ];

                array_push($dataEvaluasifixed1, $dataEvaluasiProcess);
            }
        }

        $evaluasiEfektivitasPersonal = count($dataEvaluasifixed1);
        // END

        // EFEKTIVITAS OTHER NOTIF
        $efektifitas2 = $tna->countDataForEvaluationOther($npk,$bagian);
        $dataEvaluasifixed2 = [];

        foreach ($efektifitas2 as $efektif) {

            $date_training = date_create($efektif['rencana_training']);
            $date_now = date_create(date('Y-m-d'));
            $compare = date_diff($date_training, $date_now);
            $due_date = (int)$compare->format('%a');
            $year_training = date('Y', strtotime($efektif['rencana_training']));
            $current_year = date('Y');
            
            if ($due_date >= 90 && $year_training == $current_year ) {
                $dataEvaluasiProcess = [
                    'id_tna' => $efektif['id_tna'],
                    'nama' => $efektif['nama'],
                    'judul' => $efektif['training'],
                    'jenis' => $efektif['jenis_training'],
                    'tanggal' => $efektif['rencana_training'],
                    'status' =>  $efektif['status_efektivitas']
                ];

                array_push($dataEvaluasifixed2, $dataEvaluasiProcess);
            }
        }

        $evaluasiEfektivitasPersonalOther = count($dataEvaluasifixed2);
        // END
        
        //REQUEST TNA
        if ($bagian == 'BOD') {
            $distinct = $tna->countRequestTna($bagian, $dic);
            session()->set('distinct', $distinct);
        } elseif ($bagian == 'KADIV') {
            $distinct = $tna->countRequestTna($bagian, $divisi);
            session()->set('distinct', $distinct);
        } elseif ($bagian == 'KADEPT') {
            $distinct = $tna->countRequestTna($bagian, $departemen);
            session()->set('distinct', $distinct);
        }else{
            $distinct = 0;
            session()->set('distinct', $distinct);
        }
        // END

        //REQUEST OTHER
        if ($bagian == 'BOD') {
            $new_dept = [
                "departemen" => $user->getDepartemenForKadivBod($bagian,$dic)
            ];

            $new_depts = [];

            foreach($new_dept as $item){
                $total_index_departemen = count($item);
                for ($i = 0; $i < $total_index_departemen; $i++){
                    $departemens[] = $item[$i]['departemen'];
                }
            }

            $array1 = [];
            foreach($departemens as $item){
                $id_budget =  $budget->getIdBudgetByDepartment($item);
                $array1 = array_merge($array1,$id_budget);
            }

            $array2 = [];
            foreach($array1 as $item){
                $cross_budget = $tna->getRequestCrossBudgetBod($bagian,$item['id_budget']);
                if(!empty($cross_budget)){
                   $array2 = array_merge($array2,$cross_budget);
                } 
            }

            $total_index = count($array2);
            $distictOther = $total_index;
            session()->set('distictOther', $distictOther);
        } elseif ($bagian == 'KADIV') {
            $new_dept = [
                "departemen" => $user->getDepartemenForKadivBod($bagian,$divisi)
            ];
            $new_depts = [];

            foreach($new_dept as $item){
                $total_index_departemen = count($item);
                for ($i = 0; $i < $total_index_departemen; $i++){
                    $departemens[] = $item[$i]['departemen'];
                }
            }

            $array1 = [];
            foreach($departemens as $item){
                $id_budget =  $budget->getIdBudgetByDepartment($item);
                $array1 = array_merge($array1,$id_budget);
            }

            $array2 = [];
            foreach($array1 as $item){
                $cross_budget = $tna->getRequestCrossBudgetKadiv($bagian,$item['id_budget']);
                if(!empty($cross_budget)){
                   $array2 = array_merge($array2,$cross_budget);
                } 
            }

            $total_index = count($array2);
            $distictOther = $total_index;
            session()->set('distictOther', $distictOther);
        } elseif ($bagian == 'KADEPT') {
            $id_budget =  $budget->getIdBudgetByDepartment($departemen);
            $array_id_budget = [];
    
            
            foreach($id_budget as $item){
                $array_id_budget[] = $item['id_budget'];
            }
    
            $list_cross_budget = [];
            foreach ($array_id_budget as $item){
                $cross_budget = $tna->getRequestCrossBudget($bagian,$departemen,$item);
                if(!empty($cross_budget)){
                   $list_cross_budget = array_merge($list_cross_budget,$cross_budget);
                } 
            }
    
            $total_index = count($list_cross_budget);
            $distictOther = $total_index;
            session()->set('distictOther', $distictOther);
        }else{
            $distictOther = 0;
            session()->set('distictOther', $distictOther);
        }
        // END

        //REQUEST UNPLANNED
        if ($bagian == 'BOD') {
            $distinctUnplanned = $unplanned->countRequestUnplanned($bagian, $dic);
            session()->set('distinctUnplanned', $distinctUnplanned);
        } elseif ($bagian == 'KADIV') {
            $distinctUnplanned =  $unplanned->countRequestUnplanned($bagian, $divisi);
            session()->set('distinctUnplanned', $distinctUnplanned);
        } elseif ($bagian == 'KADEPT') {
            $distinctUnplanned = $unplanned->countRequestUnplanned($bagian, $departemen);
            session()->set('distinctUnplanned', $distinctUnplanned);
        } else {
            $distinctUnplanned  =  0;
            session()->set('distinctUnplanned', $distinctUnplanned);
        }
        // END

        //TRAINER
        $resultArray = []; 
        for ($year = $currentYear; $year <= $nextYear; $year++) {
            for ($month = 1; $month <= 12; $month++) {
                $lists = $tna->getTnaForTrainer($month, $year); 
                $resultArray = array_merge($resultArray, $lists);
            }
        }

        $trainerForTraining = [];

        foreach ($resultArray as $item) {
            if ($item['instruktur_1'] === NULL) {
                $count = count($item);
                $trainerForTraining[] = $count;
            }
        }
        session()->set('trainerForTraining',count($trainerForTraining));
        // END

        if ($bagian == 'BOD' || $bagian == 'KADIV' || $bagian == 'KADEPT' || $bagian == 'KASIE') {
            $memberUser = $user->filter($id_user);
            $array_member = [];
            foreach($memberUser as $item){
                $id_member = $item->id_user;
                $npk = $item->npk;
                $bagian = $item->bagian; 
                $array_member[] = [
                    'id_member' => $id_member,
                    'npk' => $npk,
                    'bagian' => $bagian
                ];
            }
            $member_reaksi = 0;
            $member_unplanned_reaksi = 0;
            $member_other_reaksi = 0;
            $member_efektivitas = 0;
            $member_unplanned_efektivitas = 0;
            $member_other_efektivitas = 0;
            $i = null;
            foreach($array_member as $index => $item){
                $count_member_reaksi = $tna->countEvaluasiReaksi($item['npk'],$item['bagian']);
                $count_member_other_reaksi = $tna->countEvaluasiReaksiOther($item['npk'],$item['bagian']);
                $count_member_unplanned_reaksi = $unplanned->countEvaluasiReaksi($item['npk'],$item['bagian']);
                $member_reaksi += $count_member_reaksi;
                $member_unplanned_reaksi += $count_member_unplanned_reaksi;
                $member_other_reaksi += $count_member_other_reaksi;

                $efektifitas1 = $tna->countDataForEvaluationTraining($item['npk'],$item['bagian']);
                $dataEvaluasifixed1 = [];
                foreach ($efektifitas1 as $efektif) {
                    $date_training = date_create($efektif['rencana_training']);
                    $date_now = date_create(date('Y-m-d'));
                    $compare = date_diff($date_training, $date_now);
                    $due_date = (int)$compare->format('%a');
                    $year_training = date('Y', strtotime($efektif['rencana_training']));
                    $current_year = date('Y');
                    
                    if ($due_date >= 90 && $year_training == $current_year) {
                        $dataEvaluasiProcess = [
                            'id_tna' => $efektif['id_tna'],
                            'nama' => $efektif['nama'],
                            'judul' => $efektif['training'],
                            'jenis' => $efektif['jenis_training'],
                            'tanggal' => $efektif['rencana_training'],
                            'status' =>  $efektif['status_efektivitas']
                        ];

                        array_push($dataEvaluasifixed1, $dataEvaluasiProcess);
                    }
                }

                $efektifitas = $tna->countDataForEvaluationUnplanned($item['npk'],$item['bagian']);
                $dataEvaluasifixed = [];
                foreach ($efektifitas as $efektif) {

                    $date_training = date_create($efektif['rencana_training']);
                    $date_now = date_create(date('Y-m-d'));
                    $compare = date_diff($date_training, $date_now);
                    $due_date = (int)$compare->format('%a');
                    $year_training = date('Y', strtotime($efektif['rencana_training']));
                    $current_year = date('Y');
                    
                    if ($due_date >= 90 && $year_training == $current_year) {
                        $dataEvaluasiProcess = [
                            'id_tna' => $efektif['id_tna'],
                            'nama' => $efektif['nama'],
                            'judul' => $efektif['training'],
                            'jenis' => $efektif['jenis_training'],
                            'tanggal' => $efektif['rencana_training'],
                            'status' =>  $efektif['status_efektivitas']
                        ];

                        array_push($dataEvaluasifixed, $dataEvaluasiProcess);
                    }
                }

                $efektifitas2 = $tna->countDataForEvaluationOther($item['npk'],$item['bagian']);
                $dataEvaluasifixed2 = [];

                foreach ($efektifitas2 as $efektif) {

                    $date_training = date_create($efektif['rencana_training']);
                    $date_now = date_create(date('Y-m-d'));
                    $compare = date_diff($date_training, $date_now);
                    $due_date = (int)$compare->format('%a');
                    $year_training = date('Y', strtotime($efektif['rencana_training']));
                    $current_year = date('Y');
                    
                    if ($due_date >= 90 && $year_training == $current_year) {
                        $dataEvaluasiProcess = [
                            'id_tna' => $efektif['id_tna'],
                            'nama' => $efektif['nama'],
                            'judul' => $efektif['training'],
                            'jenis' => $efektif['jenis_training'],
                            'tanggal' => $efektif['rencana_training'],
                            'status' =>  $efektif['status_efektivitas']
                        ];

                        array_push($dataEvaluasifixed2, $dataEvaluasiProcess);
                    }
                }

                $evaluasiEfektivitasMemberUnplanned = count($dataEvaluasifixed);
                $evaluasiEfektivitasMember = count($dataEvaluasifixed1);
                $evaluasiEfektivitasMemberOther = count($dataEvaluasifixed2);

                $member_efektivitas += $evaluasiEfektivitasMember;
                $member_unplanned_efektivitas += $evaluasiEfektivitasMemberUnplanned;
                $member_other_efektivitas += $evaluasiEfektivitasMemberOther;

                session()->set('ermTna', $member_reaksi);
                session()->set('ermUnplanned', $member_unplanned_reaksi);
                session()->set('ermOther', $member_other_reaksi);
                session()->set('eemTna', $member_efektivitas);
                session()->set('eemUnplanned', $member_unplanned_efektivitas);
                session()->set('eemOther', $member_other_efektivitas);
            }
        }

        session()->set('tna', $tna);
        session()->set('listTna', $listTna);
        session()->set('listUnplanned',$listUnplanned);
        session()->set('kadivStatusWait',$kadivStatusWait);
        session()->set('kadeptStatusWait',$kadeptStatusWait);
        session()->set('kadivStatusUnplannedWait',$kadivStatusUnplannedWait);
        session()->set('bodStatusWait',$bodStatusWait);
        session()->set('bodStatusUnplannedWait',$bodStatusUnplannedWait);
        session()->set('trainingMonthly',$trainingMonthly);
        session()->set('otherTrainingMonthly',$otherTrainingMonthly);
        session()->set('unplannedMonthly',$unplannedMonthly);
        session()->set('schedule',$schedule);
        session()->set('scheduleOther',$scheduleOther);
        session()->set('scheduleUnplanned',$scheduleUnplanned);
        session()->set('evaluasiEfektivitasAdmin',$evaluasiEfektivitasAdmin);
        session()->set('evaluasiReaksi',$evaluasiReaksi);
        session()->set('evaluasiReaksiAdmin',$evaluasiReaksiAdmin);
        session()->set('evaluasiReaksiUnplanned',$evaluasiReaksiUnplanned);
        session()->set('evaluasiReaksiOther',$evaluasiReaksiOther);
        session()->set('evaluasiEfektivitasPersonal',$evaluasiEfektivitasPersonal);
        session()->set('evaluasiEfektivitasPersonalUnplanned',$evaluasiEfektivitasPersonalUnplanned);
        session()->set('evaluasiEfektivitasPersonalOther',$evaluasiEfektivitasPersonalOther);
        session()->set('requestSylabusUnread',$requestSylabusUnread);

    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
