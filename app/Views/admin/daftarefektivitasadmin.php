<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="col-12">

        <div class="card-header">
            <h3 class="card-title"><?= $tittle ?></h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 400px;">
            <table class="table table-head-fixed display" id="example2">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Training Category</th>
                        <th>Training Title</th>
                        <th>Training Type</th>
                        <th>Implementation Date</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($evaluasi as $evaluation) : ?>
                    <tr>
                        <td><?= $evaluation['nama'] ?></td>
                        <td>
                            <?php if($evaluation['kelompok_training'] == 'amdi_aop') : ?>
                                <span class="badge badge-info">AMDI AOP</span>
                                <?php elseif($evaluation['kelompok_training'] == 'cross_budget') :  ?>
                                <span class="badge badge-success">CROSS BUDGET</span>
                                <?php elseif($evaluation['kelompok_training'] == 'training') : ?>
                                <span class="badge badge-primary">TRAINING</span>
                                <?php else : ?>
                                <span class="badge badge-warning">UNPLANNED</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $evaluation['judul'] ?></td>
                        <td><?= $evaluation['jenis'] ?></td>
                        <td><?= $evaluation['tanggal'] ?></td>

                        <?php if ($evaluation['status'] == null) : ?>
                        <td>
                            <a class="btn btn-danger btn-sm" style="color:white">Not Implemented</a>
                        </td>
                        <?php else : ?>
                        <td>
                            <a href="<?= base_url() ?>detail_efektivitas/<?= $evaluation['id_tna'] ?>"
                                class="btn btn-success btn-sm" style="color:white">Implemented</a>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>

<?= $this->endSection() ?>