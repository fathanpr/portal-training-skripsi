<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<style>
.my-custom-scrollbar {
    position: relative;
    height: 350px;
    overflow: auto;
}

.table-wrapper-scroll-y {
    display: block;
}
</style>

<!-- UBAH date(Y) menjadi date(Y)+1 -->
<div class="warning" data-warning="<?= session()->get('warning'); ?>"></div>
<div class="card m-1">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <?php 
                $currentDate = date('Y-m-d');
                $currentYear = date('Y');

                if($currentDate < "$currentYear-11-01" || $currentDate > "$currentYear-12-31"){
                    echo "<h3 class='card-title'>Training Need Analysis (Pendaftaran TNA dibuka bulan November-Desember $currentYear)</h3>";
                } else{
                    echo "<h3 class'card-title'>Training Need Analysis</h3>";
                }
            ?>
            <?php if (session()->get('bagian') == 'KADEPT' ||  session()->get('bagian') == 'BOD') : ?>
                <div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalOther">
                        <i class="fa-solid fa-share-from-square"></i>
                        Submit TNA
                    </button>
                </div>
            <?php elseif(session()->get('bagian') == 'KASIE') : ?>
                <div>
                    <?php if(!empty($user_member)): ?>
                        <button id="addMultiTnaButton" class="btn btn-success" data-toggle="modal" data-target="#multiTNA">Add Multi TNA</button>
                    <?php endif; ?>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalOther">
                        <i class="fa-solid fa-share-from-square"></i>
                        Submit TNA
                    </button>
                </div>
            <?php else : ?> 
                <div>   
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalKadiv">
                        <i class="fa-solid fa-share-from-square"></i>
                        Submit TNA
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="d-flex justify-content-end mt-2">
        
        </div>
    </div>

    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover" id="member">
            <thead>
                <tr>
                    <th>NPK</th>
                    <th>Name</th>
                    <th>Division</th>
                    <th>Department</th>
                    <th>Section</th>
                    <th>Status</th>
                    <th>Training Request</th>
                    <th>Training Submitted</th>
                    <th>Man Hour Training</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0;
                foreach ($user as $users) : ?>
                <tr>
                    <td><?= $users->npk ?></td>
                    <td><?php 
                        if (session()->get('bagian') == 'BOD' || session()->get('bagian') == 'KADIV' || session()->get('bagian') == 'KADEPT' ||  session()->get('bagian') == 'KASIE' ||  session()->get('bagian') == 'STAFF 4UP') : ?>
                        <form action="<?= base_url() ?>form_tna" id="dataform<?= $i ?>" method="post">
                            <input type="hidden" name="member" id="member<?= $i ?>" value="<?= $users->id_user ?>">
                        </form>
                        <?php
                            $currentDate = date('Y-m-d');
                            $currentYear = date('Y');

                            if($currentDate < "$currentYear-01-01" || $currentDate > "$currentYear-01-31"){
                                echo "<p class='fw-5'>$users->nama</p>";
                            } else{
                                echo "<a href='#' onclick=\"document.getElementById('dataform{$i}').submit();\">$users->nama</a>";
                                // if(empty($tnaKadept->getTrainingCurrentYearById($users->id_user))){
                                //     echo "<a href='#' onclick=\"document.getElementById('dataform{$i}').submit();\">$users->nama</a>";
                                // }else{
                                //     echo "<p class='fw-5'>$users->nama (Already Registered)</p>";
                                // }
                            }
                        ?>
                            <!-- <a href="#" onclick="document.getElementById('dataform<?= $i ?>').submit();"><?= $users->nama ?></a>
                            <p class="fw-5"> <?= $users->nama ?> (Already Registered)</p> -->
                        <?php else : ?>
                        <?= $users->nama ?>
                        <?php endif; ?>
                    </td>
                    <td><?= $users->divisi ?></td>
                    <td><?= $users->departemen ?></td>
                    <td><?= $users->bagian ?></td>
                    <td><?= $users->status ?></td>
                    <?php $training_request = $tnaKadept->getTrainingRequest($users->id_user);?>
                    <?php $training_submitted = $tnaKadept->getTrainingSubmitted($users->id_user,$users->npk,$users->bagian); ?>
                    <td><?= $training_request ?></td>
                    <td><?= $training_submitted ?></td>
                    <td><?= $tnaKadept->getManHour($users->npk,$users->bagian) ?> Hours</td>
                    <td>
                        <button type="button" id="trainingSubmittedButton" class="btn btn-primary mb-2" data-toggle="modal" data-target="#terdaftarModal" onclick="showModal(<?= $users->id_user ?>,<?= $users->npk ?>,'<?= $users->bagian ?>')" data-user-id="<?= $users->id_user ?>">Training Registered</button>
                    </td>
                </tr>
                <?php
                    $i++;
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <button id="createMultiTnaButton" class="btn btn-primary p-3 mt-5" style="display: none;">Create Multi TNA</button>
    <!-- /.card-body -->
    <!-- modal detail registered -->
    <!-- Modal -->
    <div class="modal fade" id="terdaftarModal" tabindex="-1" role="dialog" aria-labelledby="terdaftarModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="terdaftarModalLabel">Training Registered</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <table class="table table-striped" id="mytable">
                    <tbody></tbody>
                </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- end -->
    <!-- modal kadiv -->
    <div class="modal fade" id="exampleModalKadiv" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <form method="post" action="<?= base_url() ?>tna\send">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">TNA Save</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body p-0 table-wrapper-scroll-y my-custom-scrollbar">
                            <?php foreach ($user as $dept) : ?>
                            <table class="table table-bordered mb-0 overflow-auto">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Training</th>
                                        <th>Training Type</th>
                                        <th>Training Category</th>
                                        <th>Training Method</th>
                                        <th>Training Request</th>
                                        <th>Training Goals</th>
                                        <th>Notes</th>
                                        <th>Budget Estimation</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $sum = 0;
                                        $tnafixes = $tnaKadept->getTnaFilterKadept(session()->get('id'), $dept->departemen);
                                        foreach ($tnafixes as $Forms) : ?>
                                        <tr>
                                            <td><?= $Forms->nama ?></td>
                                            <td><?= $Forms->training ?></td>
                                            <td><?= $Forms->jenis_training ?></td>
                                            <td><?= $Forms->kategori_training ?></td>
                                            <td><?= $Forms->metode_training ?></td>
                                            <td><?= $Forms->request_training ?></td>
                                            <td><?= $Forms->tujuan_training ?></td>
                                            <td><?= $Forms->notes ?></td>
                                            <td>
                                                <div><?= "Rp " . number_format($Forms->biaya, 0, ',', '.') ?></div>
                                            </td>
                                            <?php $sum += $Forms->biaya ?>
                                            <td>
                                                <button type="button" class="btn btn-danger"
                                                    onclick="DeleteTna(<?= $Forms->id_tna ?>)">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" value="<?= $Forms->id_tna ?>" name="training[]">
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <table class="table mb-3" style="font-size: 12px;">
                                <?php 
                                $budgets = $budgetKadept->getBudgetNextYear($dept->departemen); 
                                if ($budgets == null) {
                                    $budgets = $budgetKadept->getBudgetCurrent($dept->departemen);
                                }
                                ?>
                                <?php if (isset($budgets)) : ?>
                                <tr style="background-color:aliceblue;">
                                    <td><strong><?= $dept->departemen ?></strong>&ensp;&ensp;</td>
                                    <td><strong>Alocated Budget</strong>&ensp;&ensp;</td>
                                    <td><?= "Rp " . number_format($budgets['alocated_budget'], 0, ',', '.') ?>
                                    &ensp;&ensp;</td>
                                    <td><strong>Available Budget</strong></td>
                                    <td><?= "Rp " . number_format($budgets['available_budget'], 0, ',', '.') ?>
                                    &ensp;&ensp;</td>
                                    <td><strong>Used Budget</strong></td>
                                    <td><?= "Rp " . number_format($budgets['used_budget'], 0, ',', '.') ?>&ensp;&ensp;</td>
                                    <td><strong>Total Budget Estimation</strong>&ensp;&ensp;</td>
                                    <td><?= "Rp " . number_format($sum, 0, ',', '.') ?></td>
                                </tr>
                                <?php else : ?>
                                <tr style="background-color:aliceblue;">
                                    <td><strong><?= $dept->departemen ?></strong></td>
                                    <td><strong>Alocated Budget</strong></td>
                                    <td><?= "Rp " . number_format(0, 0, ',', '.') ?>&ensp;&ensp;</td>
                                    <td><strong>Available Budget</strong></td>
                                    <td><?= "Rp " . number_format(0, 0, ',', '.') ?>&ensp;&ensp;</td>
                                    <td><strong>Used Budget</strong></td>
                                    <td><?= "Rp " . number_format(0, 0, ',', '.') ?>&ensp;&ensp;</td>
                                    <td><strong>Total Budget Estimation</strong></td>
                                    <td><?= "Rp " . number_format(0, 0, ',', '.') ?>&ensp;&ensp;</td>
                                </tr>
                                <?php endif; ?>
                            </table>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <?php 
                        if($sum > $budget['available_budget']){
                            echo "<button type='submit' class='btn btn-primary' disabled><i class='fa fa-fw fa-send' style='color:white;'></i>Submit TNA</button>";
                        }elseif(count($user) <= 1){
                            echo "<button type='submit' class='btn btn-primary'><i class='fa fa-fw fa-send' style='color:white;'></i>Submit TNA</button>";
                        }
                        else{
                            echo "<a class='btn btn-primary' data-toggle='modal' data-target='#modalConfirmKadiv' style='color:white;'><i class='fa fa-fw fa-send'></i>Submit TNA</a>";
                        }
                        ?>
                        <!-- MODAL TOMBOL VALIDASI -->
                        <div class="modal" tabindex="-1" role="dialog" id="modalConfirmKadiv">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Validation Alert</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Member dibawah ini masih belum mengikuti training.</p>
                                        <ul>
                                            <?php foreach($user as $users) : ?>
                                                <?php $getTna = $tnaKadept->getUserTna($users->bagian,$users->npk) ?>
                                                <?php if (empty($getTna)) : ?>
                                                    <li><?php echo $users->nama; ?></li>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' class='btn btn-primary'><i class='fa fa-fw fa-send'></i>Tetap Lanjutkan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>
                <input type="hidden" name="temporary_budget" value="<?= $sum ?>">
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalOther" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <form method="post" action="<?= base_url() ?>tna\send">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">TNA Save</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body p-0 table-wrapper-scroll-y my-custom-scrollbar">
                            <table class=" table table-striped table-bordered mb-0 overflow-auto">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Training</th>
                                        <th>Training Type</th>
                                        <th>Training Category</th>
                                        <th>Training Method</th>
                                        <th>Training Request</th>
                                        <th>Training Goals</th>
                                        <th>Notes</th>
                                        <th>Budget Estimation</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sum = 0;
                                    foreach ($tna as $Forms) : ?>
                                    <tr>
                                        <td><?= $Forms->nama ?></td>
                                        <td><?= $Forms->training ?></td>
                                        <td><?= $Forms->jenis_training ?></td>
                                        <td><?= $Forms->kategori_training ?></td>
                                        <td><?= $Forms->metode_training ?></td>
                                        <td><?= $Forms->request_training ?></td>
                                        <td><?= $Forms->tujuan_training ?></td>
                                        <td><?= $Forms->notes ?></td>
                                        <td>
                                            <div><?= "Rp " . number_format($Forms->biaya, 0, ',', '.') ?></div>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger"
                                                onclick="DeleteTnaOther(<?= $Forms->id_tna ?>)">
                                                <i class="fa fa-fw fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <input type="hidden" value="<?= $Forms->id_tna ?>" name="training[]">
                                    <?php
                                        $sum += $Forms->biaya;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-around">
                            <div class="ml-4">
                                <div><strong>Alocated
                                        Budget :</strong>
                                    <?php if ($budget != null) {
                                        echo "Rp " . number_format($budget['alocated_budget'], 0, ',', '.');
                                    } else {
                                        echo 0;
                                    } ?>
                                </div>
                            </div>
                            <div>
                                <strong>Available
                                    Budget :</strong>
                                <?php if ($budget != null) {
                                    echo "Rp " . number_format($budget['available_budget'], 0, ',', '.');
                                } else {
                                    echo 0;
                                } ?>
                            </div>
                            <div class="mr-4">
                                <strong>Used
                                    Budget :</strong>
                                <?php if ($budget != null) {
                                    echo "Rp " . number_format($budget['used_budget'], 0, ',', '.');
                                } else {
                                    echo 0;
                                } ?>
                            </div>
                            <div class="ml-4">
                            <?php 
                                if($sum > $budget['available_budget']){
                            
                                    echo " <strong style='color:red;'>(!) Total Estimasi Budget : ".'Rp' . number_format($sum, 0, ',', '.')."</strong>";
                                    echo "<p style='color:red;'>Note : Total estimasi melebilih budget yang tersedia</p>"; 
                                }else{
                                    echo '<strong>Total Estimasi Budget: '.'Rp '.number_format($sum, 0, ',', '.').'</strong>';
                                } 
                                ?>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <?php 
                        if($sum > $budget['available_budget']){
                            echo "<button type='submit' class='btn btn-primary' disabled><i class='fa fa-fw fa-send' style='color:white;'></i>Submit TNA</button>";
                        }elseif(count($user) <= 1){
                            echo "<button type='submit' class='btn btn-primary'><i class='fa fa-fw fa-send' style='color:white;'></i>Submit TNA</button>";
                        }else{
                            echo "<a class='btn btn-primary' data-toggle='modal' data-target='#modalConfirm' style='color:white;'><i class='fa fa-fw fa-send'></i>Submit TNA</a>";
                        }
                    ?>
                    <!-- MODAL TOMBOL VALIDASI -->
                    <div class="modal" tabindex="-1" role="dialog" id="modalConfirm">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Validation Alert</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Member dibawah ini masih belum mendaftarkan TNA tahun berikutnya.</p>
                            <ul>
                                <?php foreach($user as $users) : ?>
                                    <?php $getTna = $tnaKadept->getUserTna($users->bagian,$users->npk) ?>
                                    <?php if (empty($getTna)) : ?>
                                        <li><?php echo $users->nama; ?></li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="modal-footer">
                            <p class="mt-3 mr-2 font-weight-bold">Apakah anda tetap ingin melanjutkan? </p>
                            <button type='submit' class='btn btn-primary'><i class='fa fa-fw fa-send'></i> Tetap Lanjutkan</button>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- END -->
                    </div>
                </div>
                <input type="hidden" name="temporary_budget" value="<?= $sum ?>">
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="DeleteModalOther" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="<?= base_url() ?>delete_training_user" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="url" id="url" value="0">
                        <h6><strong>Are You Sure !</strong></h6>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <!-- Modal -->
    <div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="<?= base_url() ?>delete_training_user" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="idkadiv">
                        <input type="hidden" name="url" id="url" value="0">
                        <h6><strong>Are You Sure!</strong></h6>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Multi User -->
    <div class="modal fade" id="multiTNA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Multi TNA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="multiTNAForm" class="mt-3" action="<?= base_url() ?>multi_tna" method="POST">
        <div class="modal-body">
            <p>Note : Menu ini hanya dikhususkan untuk pendaftaran training yang sama dan hanya untuk Operator</p>
                <?php foreach ($user_member as $currentUser) : ?>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input userCheckbox" name="user[]" id="userCheckbox<?= $currentUser->npk ?>" value="<?= $currentUser->npk ?>,<?= $currentUser->id_user ?>,<?= $currentUser->bagian ?>,<?= $currentUser->nama ?>" data-username="<?= $currentUser->nama ?>">
                        <label class="form-check-label" for="userCheckbox<?= $currentUser->npk ?>">
                            <?= $currentUser->npk ?> / <?= $currentUser->nama ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create Multi TNA</button>
            </div>
        </div>
    </form>
    </div>
    </div>

<script>

    function showModal(userId,Npk,Bagian) {
    var id_user = userId;
    var npk = Npk;
    var bagian = Bagian;

    $.ajax({
        type: 'POST',
        url: '<?= base_url() ?>training_submitted',
        data: {
            id_user: id_user,
            npk: npk,
            bagian: bagian
        },
        success: function(response) {
            var data = response;
            console.log(data)
            $("#mytable tbody").empty();
            $("#mytable thead").remove();

        var tableHead = $("<thead>").append(
            $("<tr>").append(
                $("<th>").text("Training"),
                $("<th>").text("Training Type"),
                $("<th>").text("Training Category"),
                $("<th>").text("Training Method"),
                $("<th>").text("Training Start"),
                $("<th>").text("Training Finished"),
                $("<th>").text("Training Goals"),
                $("<th>").text("Notes"),
                $("<th>").text("Budget Estimation")
            )
        );

        $("#mytable").append(tableHead);
            data.forEach(function(item) {
            var newRow = $("<tr>");
            newRow.append("<td>" + item.training + "</td>");
            newRow.append("<td>" + item.jenis_training + "</td>");
            newRow.append("<td>" + item.kategori_training + "</td>");
            newRow.append("<td>" + item.metode_training + "</td>");
            newRow.append("<td>" + item.mulai_training + "</td>");
            newRow.append("<td>" + item.rencana_training + "</td>");
            newRow.append("<td>" + item.tujuan_training + "</td>");
            newRow.append("<td>" + item.notes + "</td>");
            newRow.append("<td>Rp " + item.biaya + "</td>");
            $("#mytable tbody").append(newRow);
        });
        }
    });
    }


    function DeleteTna(id) {
        jQuery.noConflict();
        $('#DeleteModal').modal('show')
        $('#idkadiv').val(id);

    }

    function DeleteTnaOther(id) {
        jQuery.noConflict();
        $('#DeleteModalOther').modal('show')
        $('#id').val(id);

    }

    document.getElementById('submitTNA').addEventListener('click', function() {
        Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
            )
        }
        })
    })
    
    // $(document).ready(function() {
    //     $('#multiTNAForm').on('submit', function(e) {
    //         e.preventDefault();

    //         $.ajax({
    //             type: 'POST',
    //             url: '<?= base_url(); ?>form_multi_tna',
    //             data: $(this).serialize(),
    //             success: function(response) {
    //                 console.log(response);
    //             },
    //             error: function(error) {
    //                 console.log(error);
    //             }
    //         });
    //     });
    // });
    
    </script>


    <?= $this->endSection() ?>