<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card card-primary m-1 overflow-auto">
    <div class="card-header">
        <h3 class="card-title"><?= $tittle ?></h3>
    </div>
    <!-- form start -->
    <div class="card-body overflow-auto">
        <form role="form" action="" method="POST">
            <div class="form-group">
                <label>NPK</label>
                <input type="hidden" class="form-control" placeholder="Input NPK" value="<?= $user['id_user'] ?>"
                    name="profile[]">
                <input type="text" class="form-control" placeholder="Input NPK" value="<?= $user['npk'] ?>"
                    name="profile[]">
            </div>
            <div class="form-group">
                <label>NAME</label>
                <input type="text" class="form-control" placeholder="Input Nama" value="<?= $user['nama'] ?>"
                    name="profile[]">
            </div>
            <div class="form-group">
                <label>STATUS</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['status'] ?>" selected><?= $user['status'] ?></option>
                    <?php foreach ($status as $stat) : ?>
                    <option><?= $stat['status'] ?></option>
                    <?php endforeach; ?>
                </select>
                <!-- <input type="text" class="form-control" placeholder="Input Status" value="<?= $user['status'] ?>"
                    name="profile[]"> -->
            </div>
            <div class="form-group">
                <label>DIC</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['dic'] ?>" selected><?= $user['dic'] ?></option>
                    <?php foreach ($dic as $Dic) : ?>
                    <option><?= $Dic['dic'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>DIVISI</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['divisi'] ?>" selected><?= $user['divisi'] ?></option>
                    <?php foreach ($divisi as $Divisi) : ?>
                    <option><?= $Divisi['divisi'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>DEPARTMENT</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['departemen'] ?>" selected><?= $user['departemen'] ?></option>
                    <?php foreach ($departemen as $Departemen) : ?>
                    <option><?= $Departemen['departemen'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>SEKSI</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['seksi'] ?>" selected><?= $user['seksi'] ?></option>
                    <?php foreach ($seksi as $Seksi) : ?>
                    <option><?= $Seksi['seksi'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>BAGIAN</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['bagian'] ?>" selected><?= $user['bagian'] ?></option>
                    <?php foreach ($bagian as $Bagian) : ?>
                    <option><?= $Bagian['bagian'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>TYPE GROUP</label>
                <select class="form-control" name="profile[]">
                    <?php foreach ($type_golongan as $golongan) : ?>
                        <?php if($golongan['type_golongan'] != null) : ?>
                    <option value="<?= $golongan['type_golongan'] ?>" <?= $golongan['type_golongan'] == $user['type_golongan'] ? 'selected' : '' ?>><?= $golongan['type_golongan'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>TYPE USER</label>
                <select class="form-control" name="profile[]">
                    <option value="<?= $user['type_user'] ?>" selected><?= $user['type_user'] ?></option>
                    <?php foreach ($type_user as $TypeUser) : ?>
                    <option value="<?= $TypeUser['type_user'] ?>"><?= $TypeUser['type_user'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>DATE OF ENTRY </label>
                <input type="date" class="form-control" value="<?= $user['promosi_terakhir'] ?>" name="profile[]">
            </div>
            <div class="form-group">
                <!-- <label>GROUP</label> -->
                <input type="hidden" class="form-control" value="<?= $user['golongan'] ?>" name="profile[]">
            </div>
            <div class="form-group">
                <label>LAST PROMOTION</label>
                <input type="date" class="form-control" value="<?= $user['tgl_masuk'] ?>" name="profile[]">
            </div>
            <div class="form-group">
                <label>YEARS OF SERVICE</label>
                <div class="d-flex">
                    <input type="text" class="form-control" value="<?= $user['tahun'] ?>" name="profile[]"
                        style="width:100px;" readonly>
                    <label class="m-2">YEAR </label>

                    <input type="text" class="form-control" value="<?= $user['bulan'] ?>" name="profile[]"
                        style="width:100px;" readonly>
                    <label class="m-2">MONTH </label>
                </div>
            </div>
            <div class="form-group">
                <label>EMAIL</label>
                <input type="text" class="form-control" placeholder="Input Email" value="<?= $user['email'] ?>"
                    name="profile[]">
                    <input type="hidden" class="form-control" placeholder="Input Email" value="<?= $user['nama_jabatan'] ?>"
                    name="profile[]">
            </div>
            <h6>EDUCATION</h6>
            <table class="table table-bordered overflow-auto">
                <thead>
                    <tr>
                        <th>Grade</th>
                        <th>Year</th>
                        <th>Institution</th>
                        <th>Major</th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody id="education-table">
                    <?php foreach ($education as $edu) : ?>
                    <tr>
                        <td>
                        <select name="old[]" class="form-control">
                            <option value="SMA" <?php if (trim($edu['grade']) === 'D3') echo 'selected'; ?>>SMA</option>
                            <option value="D3" <?php if (trim($edu['grade']) === 'D3') echo 'selected'; ?>>D3</option>
                            <option value="D4" <?php if (trim($edu['grade']) === 'D4') echo 'selected'; ?>>D4</option>
                            <option value="S1" <?php if (trim($edu['grade']) === 'S1') echo 'selected'; ?>>S1</option>
                            <option value="S2" <?php if (trim($edu['grade']) === 'S2') echo 'selected'; ?>>S2</option>
                            <option value="S3" <?php if (trim($edu['grade']) === 'S3') echo 'selected'; ?>>S3</option>
                            <option value="S4" <?php if (trim($edu['grade']) === 'S4') echo 'selected'; ?>>S4</option>
                        </select>
                            <input type="hidden" value="<?= $edu['id_education'] ?>" name="old[]">
                        </td>
                        <td>
                        <select name="old[]" class="form-control">
                            <?php
                            $currentYear = date("Y");
                            for ($year = 1980; $year <= $currentYear; $year++) {
                                $selected = ($year == trim($edu['year'])) ? 'selected' : '';
                                echo "<option value='$year' $selected>$year</option>";
                            }
                            ?>
                        </select>
                        </td>
                        <td><input type="text" class="form-control" value="<?= $edu['institution'] ?>" name="old[]"></td>
                        <td><input type="text" class="form-control" value="<?= $edu['major'] ?>" name="old[]"></td>
                        <td><button type="button" class="btn btn-danger btn-sm"
                                onclick="deleteEducationData(<?= $edu['id_education'] ?>)"><i
                                    class="fa fa-trash"></i></button></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="m-2"> <button type="button" class="btn btn-success btn-sm" onclick="addEducation(0)"><i
                        class="fa fa-plus"></i></button></div>
            <h6>CAREER</h6>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Year Start</th>
                        <th>Year End</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Division</th>
                        <th>Company</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="career-table">
                    <?php foreach ($career as $careers) : ?>
                    <tr>
                        <td>
                            <input type="hidden" value="<?= $careers['id_career'] ?>" name="old_career[]">
                            <select name="old_career[]" class="form-control">
                                <?php
                                $currentYear = date("Y");
                                for ($year = 1980; $year <= $currentYear; $year++) {
                                    $selected = ($year == trim($careers['year_start'])) ? 'selected' : '';
                                    echo "<option value='$year' $selected>$year</option>";
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <select name="old_career[]" class="form-control">
                                <?php
                                $currentYear = date("Y");
                                for ($year = 1980; $year <= $currentYear; $year++) {
                                    $selected = ($year == trim($careers['year_end'])) ? 'selected' : '';
                                    echo "<option value='$year' $selected>$year</option>";
                                }
                                ?>
                            <option value="unknown" <?php echo ($careers['year_end'] == 'unknown') ? 'selected' : ''; ?>>Unknown</option>
                            </select>
                        </td>
                        <td>
                        <select name="old_career[]" class="form-control">
                            <option value="Staff" <?php if (trim($careers['position']) === 'Staff') echo 'selected'; ?>>Staff</option>
                            <option value="Kasubsie" <?php if (trim($careers['position']) === 'Kasubsie') echo 'selected'; ?>>Kasubsie</option>
                            <option value="Kasie" <?php if (trim($careers['position']) === 'Kasie') echo 'selected'; ?>>Kasie</option>
                            <option value="Kadept" <?php if (trim($careers['position']) === 'Kadept') echo 'selected'; ?>>Kadept</option>
                            <option value="Kadiv" <?php if (trim($careers['position']) === 'Kadiv') echo 'selected'; ?>>Kadiv</option>
                        </select>
                        </td>
                        <td><input type="text" class="form-control" value="<?= $careers['departement'] ?>" name="old_career[]"></td>
                        <td><input type="text" class="form-control" value="<?= $careers['division'] ?>" name="old_career[]"></td>
                        <td><input type="text" class="form-control" value="<?= $careers['company'] ?>" name="old_career[]"></td>
                        <td><button type="button" class="btn btn-danger btn-sm"
                                onclick="deleteCareerData(<?= $careers['id_career'] ?>)"><i
                                    class="fa fa-trash"></i></button></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="m-2"> <button type="button" class="btn btn-success btn-sm" onclick="addCareers(0)"><i
                        class="fa fa-plus"></i></button></div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="button" onclick="saveAll()" class="btn btn-primary">Submit</button>
    </div>
    </form>
</div>
<script>


function addEducation(i) {
    i++
    $('#education-table').append(`
        <tr id="column${i}">
        <td>
            <select name="edu[]" class="form-control" required>
                <option value="SMA">SMA</option>
                <option value="D3">D3</option>
                <option value="D4">D4</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select>
        </td>
        <td>
            <select name="edu[]" class="form-control" required>
                <?php
                $currentYear = date("Y");
                for ($year = 1980; $year <= $currentYear; $year++) {
                    $selected = ($year == $currentYear) ? 'selected' : '';
                    echo "<option value='$year' $selected>$year</option>";
                    // echo "<option value='Unknown'>Unknown</option>";
                }
                ?>
            </select>
        </td>
        <td>
            <input class="educationadds form-control" name="edu[]" type="text" required>
        </td>
        <td>   
            <input class="educationadds form-control" name="edu[]" type="text" required>
        </td>
         <td>
            <button type="button" class="btn btn-danger btn-sm" id="delete(${i})" onclick="removeEducation(${i})"><i class="fa fa-close"></i></button>
        </td>
        </tr>
        `)
}



function addCareers(i) {
    i++
    $('#career-table').append(`
        <tr id="column_career${i}">
        <td>
            <select name="career[]" class="form-control" required>
                <?php
                $currentYear = date("Y");
                for ($year = 1980; $year <= $currentYear; $year++) {
                    $selected = ($year == $currentYear) ? 'selected' : '';
                    echo "<option value='$year' $selected>$year</option>";
                }
                ?>
            </select>
        </td>
        <td>
            <select name="career[]" class="form-control" required>
                <?php
                $currentYear = date("Y");
                for ($year = 1980; $year <= $currentYear; $year++) {
                    $selected = ($year == $currentYear) ? 'selected' : '';
                    echo "<option value='$year' $selected>$year</option>";
                }
                ?>
                <option value="unknown" selected>Unknown</option>
            </select>
        </td>
        <td>
        <select name="career[]" class="form-control">
            <option value="Staff">Staff</option>
            <option value="Kasubsie">Kasubsie</option>
            <option value="Kasie">Kasie</option>
            <option value="Kadept">Kadept</option>
            <option value="Kadiv">Kadiv</option>
        </select>
        </td>
        <td><input class="career form-control" name="career[]" type="text" required></td>
        <td><input class="career form-control" name="career[]" type="text" required></td>
        <td><input class="career form-control" name="career[]" type="text" required></td>
         <td>
                            <button type="button" class="btn btn-danger btn-sm" id="deleteCareer(${i})" onclick="removeCareer(${i})"><i
                                    class="fa fa-close"></i></button>
                        </td>
        </tr>
        `)
}

//function for save all
function saveAll() {

    let data = $("[name='profile[]']").map(function() {
        return $(this).val();
    }).get();
    console.log(data)

    let edu_old = $("[name='old[]']").map(function() {
        return $(this).val();
    }).get();
    console.log(edu_old)

    let edu_new = $("[name='edu[]']").map(function() {
        return $(this).val();
    }).get();
    console.log(edu_new.length)
    let career_old = $("[name='old_career[]']").map(function() {
        return $(this).val();
    }).get();
    console.log(career_old)
    let career_new = $("[name='career[]']").map(function() {
        return $(this).val();
    }).get();
    console.log(career_new)

    if (edu_old.length == 0 || edu_old == undefined) {
        old_education = []
    } else {
        old_education = []
        while (edu_old.length > 0) {
            old_education.push(edu_old.splice(0, 5))
        }
    }

    if (edu_new.length == 0 || edu_new == undefined) {
        new_education = []
    } else {
        new_education = []
        while (edu_new.length > 0) {
            new_education.push(edu_new.splice(0, 4))
        }
    }


    if (career_old.length == 0 || career_old == undefined) {
        old_career = []
    } else {
        old_career = []
        while (career_old.length > 0) {
            old_career.push(career_old.splice(0, 7))
        }
        console.log(old_career)
    }

    if (career_new.length == 0 || career_new == undefined) {
        new_career = []
    } else {
        new_career = []
        while (career_new.length > 0) {
            new_career.push(career_new.splice(0, 6))
        }
        console.log(new_career)
    }
    
    $.ajax({
        type: 'POST',
        url: "<?= base_url(); ?>edit_user",
        async: true,
        dataType: "json",
        data: {
            individual: data,
            education_old: old_education,
            education_new: new_education,
            career_old: old_career,
            career_new: new_career
        },
        success: function(data) {
            console.log(data)
        }
    })
    Swal.fire({
            icon: 'success',
            title: 'Sukses',
            text: 'Data telah berhasil disimpan!',
        })
    
}

function changeEducation(id) {
    $.ajax({
        type: 'POST',
        url: "<?= base_url() ?>get_education",
        async: true,
        dataType: "json",
        data: {
            id_education: id
        },
        success: function(data) {
            console.log(data)
            jQuery.noConflict()
            $('#educationEdit #grade').val(data.grade)
            $('#educationEdit #year').val(data.year)
            $('#educationEdit #institution').val(data.institution)
            $('#educationEdit #major').val(data.major)
            $('#educationEdit').modal('show')

        }
    })
}


function removeEducation(i) {
    $('#column' + i).remove();
}

function removeCareer(i) {
    $('#column_career' + i).remove();
}


function deleteEducationData(id) {
    console.log(id)
    $.ajax({
        type: 'POST',
        url: "<?= base_url() ?>delete_education",
        async: true,
        dataType: "json",
        data: {
            id: id
        },
        success: function(data) {
            window.location.reload()
        }
    })
}


function deleteCareerData(id) {
    console.log(id)
    $.ajax({
        type: 'POST',
        url: "<?= base_url() ?>delete_career",
        async: true,
        dataType: "json",
        data: {
            id: id
        },
        success: function(data) {
            window.location.reload()
        }
    })
}
</script>
<?= $this->endSection() ?>