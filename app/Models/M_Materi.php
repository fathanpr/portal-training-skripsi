<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Materi extends Model
{
    protected $table      = 'materi';
    protected $primaryKey = 'id_materi';    
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['id_trainer', 'id_training'];

    public function getDataTrainingTrainer($id)
    {
        $this->select('materi.*,training.judul_training')->where('id_trainer', $id);
        $this->join('training', 'training.id_training=materi.id_training');
        return $this->get()->getResultArray();
    }

    public function getMaterialLastRow($id)
    {
        $this->select('materi.*,training.judul_training')->where('id_trainer', $id);
        $this->join('training', 'training.id_training=materi.id_training');
        return $this->get()->getLastRow();
    }

    public function getTrainerByIdTraining($id_training)
    {
        $this->select('materi.*,trainer.*')->where('id_training', $id_training);
        $this->join('trainer', 'trainer.id_trainer=materi.id_trainer');
        return $this->get()->getResultArray();
    }
}