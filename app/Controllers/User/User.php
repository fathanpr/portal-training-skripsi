<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_Astra;
use App\Models\M_Career;
use App\Models\M_Company;
use App\Models\M_CompetencyAstra;
use App\Models\M_CompetencyCompany;
use App\Models\M_CompetencyExpert;
use App\Models\M_CompetencySoft;
use App\Models\M_CompetencyTechnical;
use App\Models\M_CompetencyTechnicalB;
use App\Models\M_Education;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Expert;
use App\Models\M_Technical;
use App\Models\M_TechnicalB;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\UserModel;
use App\Models\M_Soft;

class User extends BaseController
{
    private M_Tna $tna;
    private UserModel $user;
    private M_TnaUnplanned $unplanned;
    private M_Education $education;
    private M_Career $career;

    private M_Astra $astra;
    private M_Company $companys;
    private M_Soft $softs;
    private M_Expert $experts;
    private M_CompetencyAstra $competencyAstra;
    private M_Technical $technical;
    private M_TechnicalB $technicalb;
    private M_CompetencyTechnical $competencyTechnical;

    private M_CompetencyExpert $competencyExpert;
    private M_CompetencySoft $competencySoft;
    private M_CompetencyCompany $competencyCompany;

    private M_CompetencyTechnicalB $competencyTechnicalB;


    public function __construct()
    {
        $this->tna = new M_Tna();
        $this->user = new UserModel();
        $this->unplanned = new M_TnaUnplanned();
        $this->education = new M_Education();
        $this->career = new M_Career();
        $this->astra = new M_Astra();
        $this->competencyAstra = new M_CompetencyAstra();
        $this->technical = new M_Technical();
        $this->technicalb = new M_TechnicalB();
        $this->competencyTechnical = new M_CompetencyTechnical();
        $this->competencyExpert = new M_CompetencyExpert();
        $this->competencySoft = new M_CompetencySoft();
        $this->competencyCompany = new M_CompetencyCompany();
        $this->competencyTechnicalB = new M_CompetencyTechnicalB();
        $this->softs = new M_Soft;
        $this->experts = new M_Expert;
        $this->companys = new M_Company;
    }

    public function index()
    {
        $id = session()->get('id');
        $npk = session()->get('npk');
        $departemen = session()->get('departemen');
        $user = $this->user->get_all_user_with_npk($id,$npk);
        $education = $this->education->getDataEducation($npk);
        $career = $this->career->getDataCareer($npk);
        $astraCompetency = $this->competencyAstra->getProfileAstraCompetency($id,$npk);
        $technicalCompetency = $this->competencyTechnical->getProfileTechnicalCompetency($id,$npk);
        $companyCompetency = $this->competencyCompany->getProfileCompanyCompetency($id,$npk);
        $technicalbCompetency = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id,$npk);
        $softCompetency = $this->competencySoft->getProfileSoftCompetency($id,$npk);
        $expertCompetency = $this->competencyExpert->getProfileExpertCompetency($id,$npk);
        $softdata = $this->softs->getDataSoft();
        $expertdata = $this->experts->getDataExpert();
        $companydata = $this->companys->getDataCompany();
        // dd($education);

        // dd($softCompetency);

        $data = [
            'tittle' => 'Profile',
            'person' => $user,
            'education' => $education,
            'career' => $career,
            'astra' => $astraCompetency,
            'technical' => $technicalCompetency,
            'technicalB' => $technicalbCompetency,
            'company' => $companyCompetency,
            'soft' => $softCompetency,
            'expert' => $expertCompetency,
            'soft_data' => $softdata,
            'expert_data' => $expertdata,
            'company_data' => $companydata,
            'id' => $id
        ];
        return view('user/profile', $data);
    }

    public function UpdateProfile()
    {
        $image =  $this->request->getFile('foto');
        // dd($image);
        $image->getName();
        $image->getClientExtension();
        $newName = $image->getRandomName();
        $image->move("../public/profile", $newName);
        $filepath = "/profile/" . $newName;
        $data = [
            'id_user' => session()->get('id'),
            'npk' => session()->get('npk'),
            'profile' => $filepath,
        ];

        $this->user->save($data);
        return redirect()->to('/user_profile');
    }

    public function CompetencyProfile()
    {
        // dd($this->AstraCompetency($id));
        $id = $this->request->getPost('id');
        $user = $this->user->getAllUser($id);
        $npk = $user['npk'];
        $seksi = session()->get('seksi');
        // dd($user);
        $string = '<div class="card-header">
                        <h3 class="card-title">Competency Sebelumnya</h3>
                      </div>';
        // dd($user['type_golongan']);
        if (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'REGULAR') {
            echo '<div class="d-flex">';
            $this->AstraCompetency($id, $string=false, $npk);
            $this->TechnicalCompetencyA($id, $department=false, $npk);
            echo '</div>';
            
            echo '<div class="d-flex">';
            $technicalA = $this->competencyTechnical->getProfileTechnicalCompetencyDepartment($id, $npk);
            // dd($techincalA);
            foreach ($technicalA as $technical) {
                if ($technical['departemen'] != $user['departemen']) {
                    $this->TechnicalCompetencyA($id, $technical['departemen'], $npk);
                }
            }
            echo '</div>';
            
            $expert = $this->competencyExpert->getProfileExpertCompetency($id, $npk);
            if (!empty($expert)) {
                $this->ExpertCompetency($id, $string, $npk);
            }
            echo '<div class="d-flex">';
            $company = $this->competencyCompany->getProfileCompanyCompetency($id, $npk);
            if (!empty($company)) {
                $this->CompanyCompetency($id, $string, $npk);
            }
            // $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id);

            $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id, $npk);
            if (!empty($technicalB)) {
                $dept = $this->competencyTechnicalB->getProfileTechnicalCompetencyBDistinct($id, $npk);
                foreach ($dept as $department) {
                    $this->TechnicalCompetencyB($id, $department['department'], $npk);
                }
            }
            $soft = $this->competencySoft->getProfileSoftCompetency($id, $npk);
            if (!empty($soft)) {
                $this->SoftCompetency($id, $string, $npk);
            }
            echo '</div>';
        } elseif (trim($user['type_golongan']) == 'A' && trim($user['type_user']) == 'EXPERT') {
            echo '<div class="d-flex">';
            $this->ExpertCompetency($id,$string=false,$npk);
            $this->TechnicalCompetencyExpert($id, $department=false, $npk);
            echo '</div>';
            echo '<div class="d-flex">';
            $technicalA = $this->competencyTechnical->getProfileTechnicalCompetencyDepartment($id,$npk);
            foreach ($technicalA as $technical) {
                if ($technical['departemen'] != $user['departemen']) {
                    $this->TechnicalCompetencyExpert($id, $technical['departemen'], $npk);
                }
            }
            echo '</div>';
            $Astra = $this->competencyAstra->getProfileAstraCompetency($id, $npk);
            if (!empty($Astra)) {
                $this->AstraCompetency($id, $string, $npk);
            }
            echo '<div class="d-flex">';
            $company = $this->competencyCompany->getProfileCompanyCompetency($id, $npk);
            if (!empty($company)) {
                $this->CompanyCompetency($id, $string, $npk);
            }
            $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id, $npk);
            if (!empty($technicalB)) {
                $dept = $this->competencyTechnicalB->getProfileTechnicalCompetencyBDistinct($id, $npk);
                foreach ($dept as $department) {
                    $this->TechnicalCompetencyB($id, $department['department'], $npk);
                }
            }
            $soft = $this->competencySoft->getProfileSoftCompetency($id, $npk);
            if (!empty($soft)) {
                $this->SoftCompetency($id, $string, $npk);
            }
            echo '</div>';
        } else {
            // dd($user);
            echo '<div class="row d-flex">';
            $this->CompanyCompetency($id, $divisi=false,$npk);
            $this->TechnicalCompetencyB($id, $department=false,$seksi, $npk);
            $this->SoftCompetency($id,$string=false,$npk);
            echo '</div>';
            echo '</div>';
        }
    }


    public function AstraCompetency($id, $string = false, $npk)
    {
        $astra = $this->competencyAstra->getProfileAstraCompetency($id,$npk);
        $ac = $this->astra->getDataAstra();
        
        if(!empty($astra)){
            echo '<div class="card w-100 m-1">';
            echo $string;
            echo ' <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Astra Leadership Competency	</th>
                                                    <th>Proficiency</th>
                                                    <th>Score</th>
                                                    <th>Updated_at</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
                                            foreach ($astra as $Astra) {
                                                echo '<tr>
                                                <td>' . $Astra['astra'] . '</td>
                                                <td>' . $Astra['proficiency'] . '</td>
                                                <td>' . $Astra['score_astra'] . '</td>
                                                <td>';
                                                
                                                if (empty($Astra['updated_at'])) {
                                                    echo '';
                                                } else {
                                                    $updatedDate = date("Y-m-d", strtotime($Astra['updated_at']));
                                                    echo $updatedDate;
                                                }
                                                
                                                echo '</td>
                                                </tr>';
                                            }
    
            echo '                           </tbody>
                                        </table>
                                        </div>
                               ';
        }else{
            echo '<div class="card w-100 m-1">';
            echo $string;
            echo ' <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Astra Leadership Competency	</th>
                                                    <th>Proficiency</th>
                                                    <th>Score</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
            foreach ($ac as $item) {
                echo '<tr>
                <td>' . $item['astra'] . '</td>
                <td>' . $item['proficiency'] . '</td>
                <td>0</td>
                </tr>';
            }
    
            echo '                           </tbody>
                                        </table>
                                        </div>
                               ';
        }
    }


    public function TechnicalCompetencyA($id, $department = false, $npk)
    {
        $user = $this->user->get_all_user_with_npk($id,$npk);
        $technicalA = $this->competencyTechnical->getProfileTechnicalCompetency($id,$npk);
        $tc = $this->technical->getDataTechnicalExpert($user['departemen'],$user['nama_jabatan']);

               if (!empty($technicalA)){
                echo   '<table class="table table-striped">
                  
                                    <thead>
                                        <tr>
                                            <th>Technical Competency</th>
                                            <th>Proficiency</th>
                                            <th>Score</th>
                                            <th>Updated_at</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        
                                    foreach ($technicalA as $technicals) {
                                        echo '<tr>
                                        <td>' . $technicals['technical'] . '</td>
                                        <td>' . $technicals['proficiency'] . '</td>
                                        <td>' . $technicals['score_technical'] . '</td>
                                        <td>';
                                        
                                        if (empty($technicals['updated_at'])) {
                                            echo '';
                                        } else {
                                            $updatedDate = date("Y-m-d", strtotime($technicals['updated_at']));
                                            echo $updatedDate;
                                        }
                                        
                                        echo '</td>
                                        </tr>';
                                    }
                // foreach ($technicalA as $technicals) {
                //     echo '<tr>
                //             <td>' . $technicals['technical'] . '</td>
                //             <td>' . $technicals['proficiency'] . '</td>
                //             <td>
                //             ' . $technicals['score_technical'] . '
                //             </tr>';
                // }
                    echo '</tbody>
                                </table>
                                </div>
                                
                       ';
            } else{
                echo   '<table class="table table-striped">
                  
                                    <thead>
                                        <tr>
                                            <th>Technical Competency</th>
                                            <th>Proficiency</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        
                foreach ($tc as $item) {
                    echo '<tr>
                            <td>' . $item['technical'] . '</td>
                            <td>' . $item['proficiency'] . '</td>
                            <td>0</td>
                            </tr>';
                }
                    echo '</tbody>
                                </table>
                                </div>
                                
                       ';
            }
    }

    public function TechnicalCompetencyExpert($id, $department = false, $npk)
    {
        $user = $this->user->get_all_user_with_npk($id,$npk);
        $technicalA = $this->competencyTechnical->getProfileTechnicalExpertCompetency($id,$npk);
        // dd($technicalA);
        $tc = $this->technical->getDataTechnicalExpert($user['departemen'],$user['nama_jabatan']);

               if (!empty($technicalA)){
                echo   '<table class="table table-striped">
                  
                                    <thead>
                                        <tr>
                                            <th>Technical Competency</th>
                                            <th>Proficiency</th>
                                            <th>Score</th>
                                            <th>Updated_at</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        
                                    foreach ($technicalA as $technicals) {
                                        echo '<tr>
                                        <td>' . $technicals['technical'] . '</td>
                                        <td>' . $technicals['proficiency'] . '</td>
                                        <td>' . $technicals['score_technical'] . '</td>
                                        <td>';
                                        
                                        if (empty($technicals['updated_at'])) {
                                            echo '';
                                        } else {
                                            $updatedDate = date("Y-m-d", strtotime($technicals['updated_at']));
                                            echo $updatedDate;
                                        }
                                        
                                        echo '</td>
                                        </tr>';
                                    }
                // foreach ($technicalA as $technicals) {
                //     echo '<tr>
                //             <td>' . $technicals['technical'] . '</td>
                //             <td>' . $technicals['proficiency'] . '</td>
                //             <td>
                //             ' . $technicals['score_technical'] . '
                //             </tr>';
                // }
                    echo '</tbody>
                                </table>
                                </div>
                                
                       ';
            } else{
                echo   '<table class="table table-striped">
                  
                                    <thead>
                                        <tr>
                                            <th>Technical Competency</th>
                                            <th>Proficiency</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        
                foreach ($tc as $item) {
                    echo '<tr>
                            <td>' . $item['technical'] . '</td>
                            <td>' . $item['proficiency'] . '</td>
                            <td>0</td>
                            </tr>';
                }
                    echo '</tbody>
                                </table>
                                </div>
                                
                       ';
            }
    }

    public function ExpertCompetency($id, $string = false, $npk)
    {
        // dd($id);
        $user = $this->user->get_all_user_with_npk($id,$npk);
        $expert = $this->competencyExpert->getProfileExpertCompetency($id, $npk);
        $ex = $this->experts->getDataExpert();

        if(!empty($expert)){
            echo '
             <div class="card w-100 m-1">';
        echo $string;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Expert Behavior Competencies</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Updated_at</th>
                                </tr>
                            </thead>
                            <tbody>';
                            foreach ($expert as $experts) {
                                echo '<tr>
                                <td>' . $experts['expert'] . '</td>
                                <td>' . $experts['proficiency'] . '</td>
                                <td>' . $experts['score_expert'] . '</td>
                                <td>';
                                
                                if (empty($experts['updated_at'])) {
                                    echo '';
                                } else {
                                    $updatedDate = date("Y-m-d", strtotime($experts['updated_at']));
                                    echo $updatedDate;
                                }
                                
                                echo '</td>
                                </tr>';
                            }
        // foreach ($expert as $experts) {
        //     echo '<tr>
        // <td>' . $experts['expert'] . '</td>
        // <td>' . $experts['proficiency'] . '</td>
        // <td>
        // ' . $experts['score_expert'] . '
        // </td>
        // </tr>';
        //         }
                echo '                           </tbody>
                                </table>
                                </div>
                    ';
        }else {
            echo '
             <div class="card w-100 m-1">';
        echo $string;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Expert Behavior Competencies</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($ex as $item) {
            echo '<tr>
        <td>' . $item['expert'] . '</td>
        <td>' . $item['proficiency'] . '</td>
        <td>0</td>
        </tr>';
                }
                echo '                           </tbody>
                                </table>
                                </div>
                    ';
        }
    }

    public function CompanyCompetency($id, $divisi = false, $npk)
    {
        $user = $this->user->get_all_user_with_npk($id, $npk);
        $company = $this->competencyCompany->getProfileCompanyCompetency($id, $npk);
        $cm = $this->companys->getDataCompanyDivisi($user['divisi']);
        // dd($divisi);
        // $divisi = $user['divisi'];


        if(!empty($company)){
            echo "<div class='col'>";
            echo '
             <div class="card w-100 m-1">';
             if ($divisi != false) {
                echo'<div class="card-header">
                                    <h3 class="card-title">Competency Sebelumnya Divisi ' . $divisi . '</h3>
                                  </div>';
                $headerCompany = $this->competencyCompany->getProfileCompanyCompetencyDivision($id, $npk, $divisi);
            } else {
                echo '<div class="card-header">
                <h3 class="card-title">Competency Division ' . $user['divisi'] . '</h3>
                </div>';
                $headerCompany = $this->competencyCompany->getProfileCompanyCompetencyDivision($id, $npk, $user['divisi']);
            }
        echo $divisi;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Company General Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Updated_at</th>
                                </tr>
                            </thead>
                            <tbody>';
//         foreach ($company as $companies) {
//             echo '<tr>
// <td>' . $companies['company'] . '</td>
// <td>' . $companies['proficiency'] . '</td>
// <td>
// ' . $companies['score_company'] . '
// </td>
// </tr>';
//         }
        foreach ($company as $companies) {
            echo '<tr>
            <td>' . $companies['company'] . '</td>
            <td>' . $companies['proficiency'] . '</td>
            <td>' . $companies['score_company'] . '</td>
            <td>';
            
            if (empty($companies['updated_at'])) {
                echo '';
            } else {
                $updatedDate = date("Y-m-d", strtotime($companies['updated_at']));
                echo $updatedDate;
            }
            
            echo '</td>
            </tr>';
        }

        echo '                           </tbody>
                        </table>
                        </div></div>
               ';
        } else{
            echo '
             <div class="card w-100 m-1">';
        echo $divisi;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Company General Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($cm as $item) {
            echo '<tr>
<td>' . $item['company'] . '</td>
<td>' . $item['proficiency'] . '</td>
<td>0</td>
</tr>';
        }

        echo '                           </tbody>
                        </table>
                        </div></div>
               ';
        }
    }


    public function SoftCompetency($id, $string = false, $npk)
    {
        $profile_sc = $this->competencySoft->getProfileSoftCompetency($id,$npk);
        // dd($profile_sc);
        // $user = $this->user->getAllUser($id);
        $sf = $this->softs->getDataSoft();
        // dd($profile_sc);

        if(!empty($profile_sc)){
        echo "<div class='col'>";
        echo '<div class="card w-100 m-1">';
        echo $string;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Soft Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                    <th>Updated_at</th>
                                </tr>
                            </thead>
                            <tbody>';
        // foreach ($profile_sc as $softy) {
        //     echo '<tr>
        //     <td>' . $softy['soft'] . '</td>
        //     <td>' . $softy['proficiency'] . '</td>
        //     <td>
        //     ' . $softy['score_soft'] . '
        //     </td>
        //     </tr>';
        // }
        foreach ($profile_sc as $softy) {
            echo '<tr>
            <td>' . $softy['soft'] . '</td>
            <td>' . $softy['proficiency'] . '</td>
            <td>' . $softy['score_soft'] . '</td>
            <td>';
            
            if (empty($softy['updated_at'])) {
                echo '';
            } else {
                $updatedDate = date("Y-m-d", strtotime($softy['updated_at']));
                echo $updatedDate;
            }
            
            echo '</td>
            </tr>';
        }

        echo '                           </tbody>
                        </table>
                        </div></div>
               ';
        } else{
            echo "<div class='col'>";
            echo '
             <div class="card w-100 m-1">';
        echo $string;
        echo   '<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Soft Competency</th>
                                    <th>Proficiency</th>
                                    <th>Score</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($sf as $item) {
            echo '<tr>
        <td>' . $item['soft'] . '</td>
        <td>' . $item['proficiency'] . '</td>
        <td>0</td>
        </tr>';
        }

        echo '                           </tbody>
                        </table>
                        </div></div>
               ';
        }
    }

    public function TechnicalCompetencyB($id, $department = false, $seksi = false, $npk)
    {
        // dd($departemen);
        $tb = $this->competencyTechnicalB->getProfileTechnicalCompetencyBCurrentPosition($id, $npk, $seksi);
        $tb_history = $this->competencyTechnicalB->getProfileTechnicalCompetencyB($id, $npk);

        $tb_array = [];

        foreach ($tb_history as $item) {
            $found = false;
            foreach ($tb as $tb_item) {
                if ($item['id_technicalB'] == $tb_item['id_technicalB']) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $tb_array[] = $item;
            }
        }

        $user = $this->user->get_all_user_with_npk($id, $npk);
        $techb = $this->technicalb->getDataTechnicalBDepartemenSeksi($user['departemen'], $user['nama_jabatan'], $user['seksi']);
        // dd($techb);

        echo "<div class='col'>";
        echo '<div class="card w-100 m-1">';
        if ($department != false) {
            echo '<div class="card-header">
                    <h3 class="card-title">Competency Sebelumnya Department ' . $department . '</h3>
                  </div>';
            $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyBDepartment($id, $npk, $department);
        } else {
            echo '<div class="card-header">
                    <h3 class="card-title">Competency Department ' . $user['departemen'] . '</h3>
                </div>';
            $technicalB = $this->competencyTechnicalB->getProfileTechnicalCompetencyBDepartment($id, $npk, $user['departemen']);
        }

        if (!empty($tb)) {
            echo '<div style="overflow-x: auto;height:23rem;"><table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Technical Competency</th>
                            <th>Proficiency</th>
                            <th>Score</th>
                            <th>Updated_at</th>
                        </tr>
                    </thead>
                    <tbody>';

            foreach ($tb as $item) {
                echo '<tr>
                        <td>' . $item['technicalB'] . '</td>
                        <td>' . $item['proficiency'] . '</td>
                        <td>' . $item['score'] . '</td>
                        <td>';

                if (empty($item['updated_at'])) {
                    echo '';
                } else {
                    $updatedDate = date("Y-m-d", strtotime($item['updated_at']));
                    echo $updatedDate;
                }

                echo '</td>
                    </tr>';
            }
            echo '</tbody>
                </table></div>';
        } else {
            echo '<table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Technical Competency</th>
                            <th>Proficiency</th>
                            <th>Score</th>
                            <th>Updated_at</th>
                        </tr>
                    </thead>
                    <tbody>';

            foreach ($techb as $data) {
                echo '<tr>
                        <td>' . $data['technicalB'] . '</td>
                        <td>' . $data['proficiency'] . '</td>
                        <td>0</td>
                        <td></td>
                    </tr>';
            }
            echo '</tbody>
                </table>';
        }

        if (!empty($tb_array)) {
            echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    Other Section Competency
                </button>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Other Section Competency</h4>
                            </div>
                            <div class="modal-body">
                                <div style="height: 40rem; overflow: auto;">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Technical Competency</th>
                                                <th>Proficiency</th>
                                                <th>Score</th>
                                                <th>Updated_at</th>
                                            </tr>
                                        </thead>
                                        <tbody>';

            foreach ($tb_array as $item) {
                echo '<tr>
                        <td>' . $item['technicalB'] .' ('.($item['seksi']).')'.'</td>
                        <td>' . $item['proficiency'] . '</td>
                        <td>' . $item['score'] . '</td>'.
                        '<td>';
                
                if (empty($item['updated_at'])) {
                    echo '';
                } else {
                    $updatedDate = date("Y-m-d", strtotime($item['updated_at']));
                    echo $updatedDate;
                }
            
                echo '</td>
                    </tr>';
            }

            echo '</tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>';
        }

        echo '</div></div>';
    }
}