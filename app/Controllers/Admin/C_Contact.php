<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\M_Approval;
use App\Models\M_Contact;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Tna;
use App\Models\M_TnaUnplanned;
use App\Models\M_Silabus;
use App\Models\M_Categories;
use App\Models\UserModel;

class C_Contact extends BaseController
{
    public M_Contact $contact;
    public function __construct()
    {
        $this->contact = new M_Contact();
        $this->silabus = new M_Silabus();
        $this->category = new M_Categories();
        $this->user = new UserModel();
    }

    public function index()
    {
        $contact =  $this->contact->getDataContact();
        // dd($contact);

        $data = [
            'tittle' => 'Message From User',
            'contact' => $contact
        ];
        return view('admin/contac', $data);
    }
    public function delete()
    {
        $id = $this->request->getVar('id');
        //dd($id);
        $this->contact->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Dihapus');
        return redirect()->to('/massage_user');
    }

    public function getSilabus(){
        $data['data'] = $this->silabus->getAllSilabusRequest();
        $data['nama_user'] = $this->user;
        $data['tittle'] = 'Request Sylabus';
        $data['category'] = $this->category;

        return view('admin/silabus', $data);    
    }

    public function bacaSilabus(){
        $id_silabus = $this->request->getVar('id_silabus');

        $data = [
            'id_silabus' => $id_silabus,
            'status_baca' => 1
        ];
        $this->silabus->save($data);
        echo json_encode($data);
    }
}