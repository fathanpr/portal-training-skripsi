<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title">Evaluasi Reaksi Member Unplanned</h3>
    </div>
    <div class="card-body table-responsive p-0" style="height: 400px;">
        <table class="table table-head-fixed display" id="example2">
            <thead>
                <tr>
                    <th>Nama</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0;
                foreach ($user as $users) : ?>
                <tr>
                    <td>
                        <form id="myform<?= $i ?>" action="<?= base_url() ?>detail_evaluasi_member_unplanned"
                            method="post">
                            <input type="hidden" name="evaluasi" value="<?= $users['id'] ?>" />
                            <input type="hidden" name="npk" value="<?= $users['npk'] ?>" />
                            <input type="hidden" name="bagian" value="<?= $users['bagian'] ?>" />
                        </form>
                        <a href="#"
                            onclick="document.getElementById('myform<?= $i ?>').submit();"><?= $users['nama'] ?></a>
                        <?php if($unplanned->countEvaluasiReaksi($users['npk'],$users['bagian'])) : ?>
                            <span class="badge badge-primary badge-pill">!</span>
                        <?php endif; ?> 
                    </td>
                </tr>
                <?php
                    $i++;
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<?= $this->endSection() ?>