<?php

namespace App\Models;

use CodeIgniter\Model;

class M_TnaUnplanned extends Model
{
    protected $table      = 'tna';
    protected $primaryKey = 'id_tna';
    // protected $useAutoIncrement = true;
    protected $allowedFields = [
        'id_user','npk','bagian', 'id_training', 'dic', 'divisi',
        'departemen', 'nama', 'jabatan', 'golongan', 'seksi', 'jenis_training',
        'kategori_training', 'training', 'vendor', 'tempat', 'metode_training', 'mulai_training', 'rencana_training',
        'tujuan_training', 'notes', 'biaya', 'biaya_actual', 'status', 'kelompok_training'
    ];


    private UserModel $user;

    public function __construct()
    {
        $this->user = new UserModel();
    }


    public function getTnaUser($bagian, $npk)
    {
        $this->selectCount('npk')->where('bagian', $bagian)->where('npk',$npk)->where('kelompok_training', 'unplanned');
        return $this->get()->getResultArray();
    }

    public function getDetailHistory($id)
    {
        $this->select()->where('id_user', $id)->where('kelompok_training', 'unplanned');
        $this->join('history', 'history.id_tna=tna.id_tna');
        return $this->get()->getResultArray();
    }

    public function getAllTna($id = false)
    {
        if ($id == false) {
            $this->select();
            return $this->get()->getResult();
        }
        return $this->where('id_tna', $id)->get()->getResult();
    }
    public function getAllSave($id = false)
    {
        if ($id == false) {
            $this->select()->where('status', 'save');
            return $this->get()->getResult();
        }
        return $this->where(['id_tna' => $id])->get()->getResult();
    }

    public function getUserTna($bagian,$npk)
    {
        $this->select()->where(['bagian' => $bagian])->where(['npk' => $npk])->where('kelompok_training', 'unplanned');
        return $this->get()->getResultArray();
    }

    public function getUserTnaUnplanned($id)
    {
        $this->select()->where('id_user', $id)->where('kelompok_training', 'unplanned');
        return $this->get()->getResult();
    }


    // public function getTnaFilterUnplanned($id)
    // {
    //     $user = $this->user->getAllUser($id);

    //     if ($user['bagian'] == 'BOD') {
    //         $this->select()->where('dic', $user['dic'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } elseif ($user['bagian'] == 'KADIV') {
    //         $this->select()->where('divisi', $user['divisi'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } elseif ($user['bagian'] == 'KADEPT') {
    //         $this->select()->where('departemen', $user['departemen'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
    //         $this->select()->where('seksi', $user['seksi'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     } else {
    //         $this->select()->where('id_user', $user['id_user'])->where('status', 'save')->where('kelompok_training', 'unplanned');
    //         return $this->get()->getResult();
    //     }
    // }




    public function getTnaFilterDistinct($id)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select('user.departemen')->where('user.dic', $user['dic'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->distinct()->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select('user.departemen')->where('user.divisi', $user['divisi'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->distinct()->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select('user.departemen')->where('user.departemen', $user['departemen'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->distinct()->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select('user.departemen')->where('user.seksi', $user['seksi'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->distinct()->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select('user.departemen')->where('tna.npk', $user['npk'])->where('tna.bagian', $user['bagian'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->distinct();
            return $this->get()->getResult();
        }
    }

    public function getTnaFilterKadept($id, $departemen = false)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select()->where('user.dic', $user['dic'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select()->where('user.divisi', $user['divisi'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select()->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select()->where('user.seksi', $user['seksi'])->where('user.departemen', $departemen)->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select()->where('tna.npk', $user['npk'])->where('tna.bagian', $user['bagian'])->where('departemen', $departemen)->where('status', 'save')->where('kelompok_training', 'unplanned');
            return $this->get()->getResult();
        }
    }

    public function getTnaFilterUnplanned($id)
    {
        $user = $this->user->getAllUser($id);

        if ($user['bagian'] == 'BOD') {
            $this->select('tna.*,user.*')->where('user.dic', $user['dic'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->where('user.bagian', 'KADIV');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADIV') {
            $this->select('tna.*,user.*')->where('user.divisi', $user['divisi'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->where('user.bagian', 'KADEPT');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KADEPT') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select('tna.*,user.*')->where('user.departemen', $user['departemen'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->whereIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } elseif ($user['bagian'] == 'KASIE' || $user['bagian'] == 'STAFF 4UP') {
            $bagian = ['KASIE', 'STAFF 4UP','EXPERT'];
            $this->select('tna.*,user.*')->where('user.seksi', $user['seksi'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned')->WhereNotIn('user.bagian', $bagian);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResult();
        } else {
            $this->select()->where('tna.npk', $user['npk'])->where('tna.bagian', $user['bagian'])->where('tna.status', 'save')->where('kelompok_training', 'unplanned');
            return $this->get()->getResult();
        }
    }
    public function getStatusWaitAdminUnplannedDistinct()
    {
        $this->select('user.departemen')->where('tna.status', 'wait')->where('kelompok_training', 'unplanned')->distinct();
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResult();
    }
    public function getStatusWaitAdminUnplanned($departemen)
    {
        $this->select('tna.*,user.*')->where('tna.status', 'wait')->where('kelompok_training', 'unplanned')->where('user.departemen', $departemen);;
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        return $this->get()->getResult();
    }

    public function getStatusWaitAdminUnplannedDepartemen()
    {
        $this->select('user.departemen')->where('tna.status', 'wait')->where('kelompok_training', 'unplanned');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->groupBy('user.departemen');
        return $this->get()->getResult();
    }

    public function countStatusWaitAdminUnplanned()
    {
        $this->select('COUNT(*) as count')->where('tna.status', 'wait')->where('kelompok_training', 'unplanned');
        
        return $this->countAllResults();
    }

    // public function getStatusWaitAdminUnplanned()
    // {
    //     $this->select()->where('status', 'wait')->where('kelompok_training', 'unplanned');
    //     $this->join('approval', 'approval.id_tna = tna.id_tna');
    //     return $this->get()->getResult();
    // }

    public function getStatusWaitUser($bagian, $member, $id = null)
    {
        $status = ['wait', 'accept'];
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.bagian')->where('user.dic', $member)->whereIn('tna.status', $status)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $jabatan = ['KADIV'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.divisi', $member)->whereIn('tna.status', $status)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['STAFF 4UP','KASIE','EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.departemen', $member)->whereIn('tna.status', $status)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KASIE') {
            $jabatan = ['STAFF 4UP', 'KASIE', 'EXPERT'];
            $this->select('tna.*,approval.*,user.bagian')->where('user.seksi', $member)->whereIn('tna.status', $status)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereNotIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } else {
            $this->select('tna.*,approval.*,user.bagian')->where('tna.id_user', $id)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }
    }

    //function untuk menampilkan data tna yang sudah di accept
    public function getKadivStatusUnplanned()
    {
        $this->select()->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getKadivStatusUnplannedFilter($year)
    {
        $this->select()->where('tna.status', 'accept')->where('kelompok_training', 'unplanned')->like('mulai_training', $year, 'after');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'tna.bagian');
        return $this->get()->getResultArray();
    }

    public function getBodStatusUnplanned()
    {
        $this->select()->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function getBodStatusUnplannedFilter($year)
    {
        $this->select()->where('tna.status', 'accept')->where('kelompok_training', 'unplanned')->like('mulai_training',$year,'after');
        $this->join('approval', 'approval.id_tna = tna.id_tna'); //->where('status_approval_1', null)->orWhere('status_approval_1', 'reject')
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function countKadivStatusUnplannedWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->where('kelompok_training', 'unplanned');
        $this->where('approval.status_approval_1', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function countKadeptStatusUnplannedWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->where('kelompok_training', 'unplanned');
        $this->where('approval.status_approval_0', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function countBodStatusUnplannedWait()
    {
        $this->select('COUNT(*) as total');
        $this->where('tna.status', 'accept');
        $this->where('kelompok_training', 'unplanned');
        $this->where('approval.status_approval_1', 'accept');
        $this->where('approval.status_approval_2', null);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
    
        return $this->countAllResults();
    }

    public function countUnplannedMonthly()
    {
        $this->select('COUNT(*) as total')->where('kelompok_training', 'unplanned')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->countAllResults();
    }


    //function get Request Tna
    public function getRequestTnaUnplannedDistinct($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('user.departemen')->where('user.dic', $member)->where('tna.status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null)->where('kelompok_training', 'unplanned')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('user.departemen')->where('user.divisi', $member)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $this->select('user.departemen')->where('user.departemen', $member)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned')->distinct();
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        } else {
            return  $status  =  array();
        }
    }

    public function countRequestUnplanned($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('COUNT(*) as total')->where('user.dic', $member)->where('tna.status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->countAllResults();
        } elseif ($bagian == 'KADIV') {
            $this->select('COUNT(*) as total')->where('user.divisi', $member)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->countAllResults();
        } elseif ($bagian == 'KADEPT') {
            $this->select('COUNT(*) as total')->where('user.departemen', $member)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->countAllResults();
        } else {
            return  $status  =  0;
        }
    }

    public function getRequestTnaUnplanned($bagian, $member, $departemen)
    {
        if ($bagian == 'BOD') {
            $this->select()->where('user.dic', $member)->where('user.departemen', $departemen)->where('tna.status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', null)->where('status_approval_3', null)->where('kelompok_training', 'unplanned');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            $this->join('approval', 'approval.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select()->where('user.divisi', $member)->where('user.departemen', $departemen)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $this->select()->where('user.departemen', $member)->where('user.departemen', $departemen)->where('tna.status', 'accept')->where('kelompok_training', 'unplanned');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
            return $this->get()->getResultArray();
        } else {
            return  $status  =  array();
        }
    }

    // public function getRequestTnaUnplanned($bagian, $member)
    // {
    //     if ($bagian == 'BOD') {
    //         $this->select()->where('dic', $member)->where('status', 'accept')->where('status_approval_0', 'accept')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3', null)->where('kelompok_training', 'unplanned');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna');
    //         return $this->get()->getResultArray();
    //     } elseif ($bagian == 'KADIV') {
    //         $this->select()->where('divisi', $member)->where('status', 'accept')->where('kelompok_training', 'unplanned');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', 'accept')->where('status_approval_1', null);
    //         return $this->get()->getResultArray();
    //     } elseif ($bagian == 'KADEPT') {
    //         $this->select()->where('departemen', $member)->where('status', 'accept')->where('kelompok_training', 'unplanned');
    //         $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_0', null);
    //         return $this->get()->getResultArray();
    //     } else {
    //         return  $status  =  array();
    //     }
    // }


    public function getKadivAcceptDistinct($date, $year)
    {
        $this->select('user.departemen')->where("Month(mulai_training)", $date)->where('YEAR(mulai_training)', $year)->where('kelompok_training', 'unplanned')->distinct();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian', 'left');
        return $this->get()->getResultArray();
    }

    public function getKadivAccept($date, $year, $departemen)
    {
        $this->select('tna.*,user.*,approval.*')->where('month(mulai_training)', $date)->where('YEAR(mulai_training)', $year)->where('user.departemen', $departemen)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian', 'left');
        return $this->get()->getResultArray();
    }

    // public function getIdTna()
    // {
    //     $this->select();
    //     return $this->get()->getLastRow();
    // }

    // public function getDetailReject($id)
    // {
    //     $this->select('tna.*,approval.*,user.bagian')->where('tna.id_tna', $id);
    //     $this->join('approval', 'approval.id_tna = tna.id_tna');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    // }

    // public function getDateTraining()
    // {
    //     $this->select('tna.rencana_training,tna.training,approval.status_approval_1,approval.status_approval_2,approval.status_approval_3')->distinct();
    //     $this->join('approval', 'approval.id_tna = tna.id_tna');
    //     return $this->get()->getResult();
    // }


        // work
//     public function getUnplannedMonthly($date)
//     {
//         $sql = $this->query("select	MONTH(tna.mulai_training) as 'Planing Training',
// 		count(tna.training) as 'Jumlah Training',
// 		count(case when approval.status_approval_1 = 'accept' then 1 else null end) as 'Admin Approval',
// 		count(case when approval.status_approval_2 = 'accept' then 1 else null end) as 'BOD Approval' ,
// 		sum(case when approval.status_approval_2='reject' then 1 when approval.status_approval_3='reject' then 1 else 0 end) as 'Reject'
// from	tna  join	approval on approval.id_tna = tna.id_tna 
// where	tna.kelompok_training = 'unplanned' and YEAR(mulai_training) = $date and status_approval_0 ='accept'
// group by MONTH(tna.mulai_training)")->getResultArray();

//         return $sql;
//     }

    //TRIAL
    public function getUnplannedMonthly()
    {
        $now = date('Y');
        $next_year = date('Y')+1;
        $sql = $this->query(
            "SELECT
        MONTH(tna.mulai_training) AS 'Planing Training',
        YEAR(tna.mulai_training) AS 'Year Training',
        COUNT(tna.training) AS 'Jumlah Training',
        COUNT(CASE WHEN approval.status_approval_1 = 'accept' THEN 1 ELSE NULL END) AS 'Admin Approval',
        COUNT(CASE WHEN approval.status_approval_2 = 'accept' THEN 1 ELSE NULL END) AS 'BOD Approval',
        SUM(CASE WHEN approval.status_approval_2 = 'reject' THEN 1
            WHEN approval.status_approval_3 = 'reject' THEN 1 ELSE 0 END) AS 'Reject'
        FROM
            tna
        JOIN
            approval ON approval.id_tna = tna.id_tna
        WHERE
            tna.kelompok_training = 'unplanned'
            AND status_approval_0 = 'accept'
            AND status_approval_1 = 'accept'
            AND status_approval_2 = 'accept'
            AND YEAR(mulai_training) BETWEEN $now AND $next_year
        GROUP BY
            YEAR(tna.mulai_training), MONTH(tna.mulai_training)"
            )->getResultArray();

            return $sql;
    }

    public function getDataTrainingMonthly($date, $year)
    {
        $this->select()->where("Month(mulai_training)", $date)->where("YEAR(mulai_training)", $year)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept')->where('status_approval_3',NULL);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    // public function getTrainingDitolak()
    // {
    //     $this->select();
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'reject')->orWhere('status_approval_2', 'reject')->orWhere('status_approval_3', 'reject');
    //     return $this->get()->getResultArray();
    // }

    // public function getAtmp()
    // {
    //     $this->select('tna.*,approval.*,user.bagian,user.npk');
    //     $this->join('approval', 'approval.id_tna = tna.id_tna');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('status_approval_3', 'accept');
    //     return $this->get()->getResultArray();
    // }

    public function getMemberSchedule($bagian, $member)
    {
        if ($bagian == 'BOD') {
            $this->select('tna.*,approval.*,user.bagian,user.departemen')->where('user.dic', $member)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADIV') {
            $this->select('tna.*,approval.*,user.bagian,user.departemen')->where('user.divisi', $member)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT');
            return $this->get()->getResultArray();
        } elseif ($bagian == 'KADEPT') {
            $jabatan = ['EXPERT', 'STAFF 4UP', 'KASIE'];
            $this->select('tna.*,approval.*,user.bagian,user.departemen')->where('user.departemen', $member)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $jabatan);
            return $this->get()->getResultArray();
        } else {
            $this->select('tna.*,approval.*,user.bagian,user.departemen')->where('user.seksi', $member)->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
            return $this->get()->getResultArray();
        }
    }

    public function getPersonalSchedule($id, $npk, $bagian)
    {
        $this->select('tna.*,approval.*,user.*')
        ->groupStart()->where('tna.npk', $npk)->where('tna.bagian',$bagian)->where('kelompok_training', 'unplanned')->groupEnd();
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian',$bagian);
        return $this->get()->getResultArray();
    }

    // public function getDataHome()
    // {
    //     $this->select('tna.rencana_training,tna.kategori_training')->distinct();
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    // }

    // public function getDataJadwalHome($date)
    // {
    //     $this->select('training as Training,COUNT(training) as Pendaftar')->where('rencana_training', $date);
    //     $this->groupBy('training');
    //     return $this->get()->getResultArray();
    // }

    // public function getJadwalHomeVer($training, $date)
    // {
    //     $this->select('kategori_training,rencana_training')->where('rencana_training', $date)->where('training', $training);
    //     return $this->get()->getResultArray();
    // }


    public function getSchedule()
    {
        $this->select('tna.*,approval.*')->where('tna.kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->get()->getResultArray();
    }

    public function countSchedule()
    {
        $this->select('COUNT (*) as total')->where('tna.kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', null);
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        return $this->countAllResults();
    }

    public function getEvaluasiReaksi($npk,$bagian)
    {
        $this->select('tna.*,approval.*,user.*,evaluasi_reaksi.*,training.material_path')->where('tna.npk', $npk)->where('tna.bagian',$bagian)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->get()->getResultArray();
    }

    public function countEvaluasiReaksi($npk,$bagian)
    {
        $this->select('COUNT(*) as total')->where('tna.npk', $npk)->where('tna.bagian', $bagian)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->where('status_training', 1);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        $this->join('training', 'training.id_training = tna.id_training');
        return $this->countAllResults();
    }

    // public function getDataForEvaluation($id)
    // {
    //     $this->select('tna.*,approval.*,user.bagian,user.id_user,user.npk')->where('tna.id_tna', $id);
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    // }

    // public function getDataHistory($id)
    // {
    //     $this->select()->where('tna.id_tna', $id);
    //     $this->join('history', 'history.id_tna = tna.id_tna');
    //     return $this->get()->getResultArray();
    // }

    // public function getDataHistory($id)
    // {
    //     $this->select('tna.*,approval.*,user.bagian,user.id_user,user.npk')->where('tna.id_tna', $id);
    //     $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
    //     $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
    //     return $this->get()->getResultArray();
    // }


    public function getDetailEvaluasiReaksi($id,$npk,$bagian)
    {
        $this->select('tna.*,user.bagian,user.id_user,user.npk,user.departemen,evaluasi_reaksi.*')->where(['tna.id_tna' => $id]);
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', $bagian);
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }


    public function getDataEfektivitas($id)
    {
        $user = $this->user->getAllUser($id);
        $status = [0];

        if ($user['bagian'] == "BOD") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADIV')->where('user.dic', $user['dic']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian'] == "KADIV") {
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.bagian', 'KADEPT')->where('user.divisi', $user['divisi']);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KADEPT") {
            $bagian = ['KASIE', 'STAFF 4UP', 'EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->whereIn('user.bagian', $bagian)->where('user.departemen', $user['departemen'])->where('user.level', 'USER');
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } elseif ($user['bagian']  == "KASIE" || $user['bagian']  == "STAFF 4UP") {
            $bagian = ['STAFF 4UP', 'KASIE', 'EXPERT'];
            $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('kelompok_training', 'unplanned');
            $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept')->whereNotIn('status_training', $status);
            $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left')->where('user.seksi', $user['seksi'])->where('user.level', 'USER')->whereNotIn('user.bagian', $bagian);
            $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
            return $this->get()->getResultArray();
        } else {
            return 'E';
        }

        // $this->select('tna.*,user.bagian,approval.*,user.id_user,user.npk,evaluasi_efektivitas.status_efektivitas')->where('kelompok_training', 'unplanned');
        // $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        // $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        // $this->join('evaluasi_efektivitas', 'evaluasi_efektivitas.id_tna = tna.id_tna');
        // return $this->get()->getResultArray();
    }
    public function getHistoryUnplanned($bagian,$npk)
    {
        $this->select()->where('tna.bagian', $bagian)->where('tna.npk',$npk)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_3', 'accept');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', '1');
        return $this->get()->getResult();
    }

    public function getUnplannedTerdaftar($bagian,$npk)
    {
        $this->select()->where('tna.bagian', $bagian)->where('tna.npk', $npk)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna');
        $this->join('evaluasi_reaksi', 'evaluasi_reaksi.id_tna = tna.id_tna')->where('status_evaluasi', null);
        return $this->get()->getResult();
    }

    public function getUnplannedRequest($id_user)
    {
        $status = 'save';
        $query = "SELECT COUNT(*) as row_count
                FROM tna
                WHERE id_user = ? AND status = ? AND kelompok_training = 'unplanned'";
    
        $result = $this->db->query($query, [$id_user, $status])->getRow();
        
        if ($result) {
            return $result->row_count;
        }
    
        return 0;
    }

    public function getUnplannedSubmitted($id_user, $npk, $bagian)
    {
        $statusArray = ['accept', 'wait'];
        $query = "SELECT COUNT(*) as row_count
            FROM tna
            WHERE (id_user = ? AND npk = ? AND status IN (" . implode(',', array_fill(0, count($statusArray), '?')) . ")
            AND kelompok_training = 'unplanned')
            OR (bagian = ? AND npk = ? AND status IN (" . implode(',', array_fill(0, count($statusArray), '?')) . ")
            AND kelompok_training = 'unplanned')";
        $result = $this->db->query($query, array_merge([$id_user, $npk], $statusArray, [$bagian, $npk], $statusArray))->getRow();
        
        if ($result) {
            return $result->row_count;
        }

        return 0;
    }

    public function getKadivAcceptForExport($date,$year)
    {
        $this->select('tna.*,user.*,approval.*,accommodation.*')->where('month(mulai_training)', $date)->where('YEAR(mulai_training)', $year)->where('kelompok_training', 'unplanned');
        $this->join('approval', 'approval.id_tna = tna.id_tna')->where('status_approval_1', 'accept')->where('status_approval_2', 'accept');
        $this->join('user', 'user.npk = tna.npk AND user.bagian = tna.bagian','left');
        $this->join('accommodation', 'tna.id_tna = accommodation.id_tna', 'left');
        // $this->join('accommodation','accommodation.id_tna = tna.id_tna');
        return $this->get()->getResultArray();
    }
}