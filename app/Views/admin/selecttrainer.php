<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <div class="card-header">
        <h3 class="card-title">Trainer For Training Monthly</h3>
    </div>
    <!-- /.card-header -->
    <?php if(empty($month)) : ?>
    <?php else : ?>
        <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Month <?= date('Y')+1 ?></th>
                </tr>
            </thead>
            <?php foreach ($month as $Month) : ?>
                <?php if($Month['tahun'] == date('Y')+1) : ?>
                <tbody>
                    <tr>
                        <td><a href="<?= base_url() ?>detail_trainer/<?= $Month['bulan'] ?>/<?= $Month['tahun'] ?>"><?=  date("F", mktime(0, 0, 0, $Month['bulan'], 10)),' ', $Month['tahun'] ?></a>
                        <?php 
                            $lists = $tna->getTnaForTrainer($Month['bulan'], $Month['tahun']);
                            $filteredLists = array_filter($lists, function($item) {
                                return $item['instruktur_1'] === null;
                            });

                            if(!empty($filteredLists)){
                                echo "<span class='badge badge-pill badge-primary'>!</span>";
                            }    
                        ?>
                    </td>
                    </tr>
                </tbody>
            <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Month <?= date('Y') ?></th>
                </tr>
            </thead>
            <?php foreach ($month as $Month) : ?>
                <?php if($Month['tahun'] == date('Y')) : ?>
                <tbody>
                    <tr>
                        <td><a href="<?= base_url() ?>detail_trainer/<?= $Month['bulan'] ?>/<?= $Month['tahun'] ?>"><?=  date("F", mktime(0, 0, 0, $Month['bulan'], 10)),' ', $Month['tahun'] ?></a>
                        <?php 
                            $lists = $tna->getTnaForTrainer($Month['bulan'], $Month['tahun']);
                            $filteredLists = array_filter($lists, function($item) {
                                return $item['instruktur_1'] === null;
                            });

                            if(!empty($filteredLists)){
                                echo "<span class='badge badge-pill badge-primary'>!</span>";
                            }    
                        ?>    
                    </td>
                    </tr>
                </tbody>
            <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
</div>
<?= $this->endSection() ?>