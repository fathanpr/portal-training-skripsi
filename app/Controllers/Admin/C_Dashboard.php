<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\M_Budget;
use App\Models\M_Contact;
use App\Models\M_ListTraining;
use App\Models\M_Tna;
use App\Models\UserModel;
use Kint\Parser\ToStringPlugin;
use PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel\Month;

class C_Dashboard extends BaseController
{
    public M_ListTraining $training;

    private M_Tna $tna;

    private M_Budget $budget;

    private UserModel $user;
    public function __construct()
    {
        $this->training = new M_ListTraining();
        $this->tna = new M_Tna();
        $this->budget = new M_Budget();
        $this->user = new UserModel();
    }

    public function index($year)
    {

        $Years = $this->tna->getYear();

        $data = [
            'tittle' => 'Dashboard',
            'month' => $this->TrainingDahboardMonth(),
            'department' => $this->user->DistinctDepartemen(),
            'dept' => $this,
            'years' => $Years,
            'yearss' => $year,
            'man_hour_total' => $this->tna->getTotalManHour($year)
        ];
        
        //dd($data);
        return view('admin/dashboard', $data);
    }

    public function dashboard_bod($year)
    {

        $Years = $this->tna->getYear();
        $data = [
            'tittle' => 'Dashboard',
            'month' => $this->TrainingDahboardMonth(),
            'department' => $this->user->DistinctDepartemenForBOD(),
            'dept' => $this,
            'years' => $Years,
            'yearss' => $year,
            'man_hour_total' => $this->tna->getTotalManHour($year)
        ];
        
        //dd($data);
        return view('admin/dashboard', $data);
    }

    public function dashboard_divisi($year)
    {

        $Years = $this->tna->getYear();
        $data = [
            'tittle' => 'Dashboard',
            'month' => $this->TrainingDahboardMonth(),
            'department' => $this->user->DistinctDepartemenByDivisi(session()->get('divisi')),
            'dept' => $this,
            'years' => $Years,
            'yearss' => $year,
            'man_hour_total' => $this->tna->getTotalManHour($year)
        ];
        
        //dd($data);
        return view('user/dashboard_divisi', $data);
    }

    public function dashboard_department($year){

        $bagian = session()->get('bagian');
        if ($bagian == 'KADEPT') {
            $Years = $this->tna->getYear();
            $data = [
                'tittle' => 'Dashboard',
                'month' => $this->TrainingDahboardMonth(),
                'department' => $this->user->DistinctDepartemen(),
                'seksi' => $this->user->DistinctSeksiByDepartemen(session()->get('departemen')),
                'dept' => $this,
                'years' => $Years,
                'yearss' => $year,
                'man_hour_total' => $this->tna->getTotalManHour($year)
            ];
            return view('user/dashboard_department', $data);
        } else {
            return redirect()->to(base_url('home_user'));
        }
        
        //dd($data);
    }

    public function DashboardYear()
    {
        $year = $this->request->getPost('year');

        $data = [];

        $budget = $this->budget->BudgetDashboard($year);
        $dashboard_column_all = $this->TrainingDashboardCountColumn($year);
        $dashboard_line_all = $this->TrainingDashboardCountLine($year);
        $category = $this->CategoryDashboard($year);
        $Jenis_training = $this->JenisTrainingDashboard($year);
        $need_approval = $this->DashboardNeedApproval($year);
        $approved = $this->DashboardApproved($year);
        $rejected = $this->DashboardRejected($year);
        $CountImplemented = $this->tna->CountTrainingImplemented($year);
        $PercentImplemented = $this->percentTrainingImplemented($year);
        $CountNotImplemented = $this->tna->CountTrainingNotImplemented($year);
        $PercentNotImplemented = $this->PercentTrainingNOtImplemented($year);
        $allTraining = $this->tna->CountAllTraining($year);
        $man_hour_total = $this->tna->getTotalManHour($year);
        $man_hour_departemen = $this->tna->getManHourDepartemen($year);
        $count_user_perdepartment = $this->user->getCountPerDepartment();

        $man_hour_final = [];
        foreach ($count_user_perdepartment as $cupd) {
            $found = false;
            foreach ($man_hour_departemen as $mht) {
                if ($cupd['nama_departemen'] == $mht['nama_departemen']) {
                    $man_hour_final[] = array_merge($cupd, $mht);
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $cupd['total_man_hour'] = 0;
                $man_hour_final[] = $cupd;
            }
        }

        array_push($data, $budget);
        array_push($data, $dashboard_column_all);
        array_push($data, $dashboard_line_all);
        array_push($data, $category);
        array_push($data, $Jenis_training);
        array_push($data, $need_approval);
        array_push($data, $approved);
        array_push($data, $rejected);
        array_push($data, $CountImplemented);
        array_push($data, $PercentImplemented);
        array_push($data, $CountNotImplemented);
        array_push($data, $PercentNotImplemented);
        array_push($data, $allTraining);
        array_push($data, $man_hour_total);
        array_push($data, $man_hour_final);


        //$sess =  session()->set('year', rand(1, 10));

        echo json_encode($data);
    }

    public function DashboardYearDivisi()
    {
        $year = $this->request->getPost('year');
        $department = session()->get('departemen');
        $divisi = session()->get('divisi');

        $data = [];

        $budget = $this->budget->BudgetDashboardDivisi($year,$divisi);
        $dashboard_column_all = $this->TrainingDashboardCountColumnDivisi($year,$divisi);
        $dashboard_line_all = $this->TrainingDashboardCountLineDivisi($year,$divisi);
        $category = $this->CategoryDashboard($year);
        $Jenis_training = $this->JenisTrainingDashboard($year);

        $need_approval = $this->DashboardNeedApprovalDivisi($year,$divisi);
        $approved = $this->DashboardApprovedDivisi($year,$divisi);
        $rejected = $this->DashboardRejectedDivisi($year,$divisi);
        $CountImplemented = $this->tna->CountTrainingImplementedDivisi($year,$divisi);
        $PercentImplemented = $this->percentTrainingImplementedDivisi($year,$divisi);
        $CountNotImplemented = $this->tna->CountTrainingNotImplementedDivisi($year,$divisi);
        $PercentNotImplemented = $this->PercentTrainingNOtImplementedDivisi($year,$divisi);
        $allTraining = $this->tna->CountAllTrainingDivisi($year,$divisi);
        $man_hour_total = $this->tna->getTotalManHourDivisi($year,$divisi);
        $man_hour_divisi = $this->tna->getManHourDivisi($year,$divisi);
        $count_user_perdivisi = $this->user->getCountPerDivisi($divisi);

        $man_hour_final = [];
        foreach ($count_user_perdivisi as $cupd) {
            $found = false;
            foreach ($man_hour_divisi as $mht) {
                if ($cupd['nama_departemen'] == $mht['nama_departemen']) {
                    $man_hour_final[] = array_merge($cupd, $mht);
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $cupd['total_man_hour'] = 0;
                $man_hour_final[] = $cupd;
            }
        }

        array_push($data, $budget);
        array_push($data, $dashboard_column_all);
        array_push($data, $dashboard_line_all);
        array_push($data, $category);
        array_push($data, $Jenis_training);
        array_push($data, $need_approval);
        array_push($data, $approved);
        array_push($data, $rejected);
        array_push($data, $CountImplemented);
        array_push($data, $PercentImplemented);
        array_push($data, $CountNotImplemented);
        array_push($data, $PercentNotImplemented);
        array_push($data, $allTraining);
        array_push($data, $man_hour_total);
        array_push($data, $man_hour_final);


        //$sess =  session()->set('year', rand(1, 10));

        echo json_encode($data);
    }

    public function DashboardYearDepartment()
    {
        $department = session()->get('departemen');
        $divisi = session()->get('divisi');
        $year = $this->request->getPost('year');
        $seksi = $this->user->DistinctSeksiByDepartemen($department);

        $data = [];

        $budget = $this->budget->BudgetDashboardDepartment($year,$department);
        $dashboard_column_all = $this->training_dashboard_count_column_department($year,$department);
        $dashboard_line_all = $this->training_dashboard_count_line_department($year, $department);


        // var_dump($this->tna->getDashboardTrainingRejectDepartment($year,$department)).die();
        $category = $this->CategoryDashboardDepartment($year, $department);
        $Jenis_training = $this->JenisTrainingDashboardDepartment($year,$department);
        $need_approval = $this->DashboardNeedApprovalDepartment($year, $department);
        $approved = $this->DashboardApprovedDepartment($year, $department);
        $rejected = $this->DashboardRejectedDepartment($year, $department);

        //Training
        $CountImplemented = $this->tna->CountTrainingImplementedDepartment($year,$department);
        $PercentImplemented = $this->percentTrainingImplementedDepartment($year,$department);
        $CountNotImplemented = $this->tna->CountTrainingNotImplementedDepartment($year,$department);
        $PercentNotImplemented = $this->PercentTrainingNOtImplementedDepartment($year,$department);
        $allTraining = $this->tna->CountAllTrainingDepartment($year,$department);
        //End Training

        $man_hour_total = $this->tna->getTotalManHourDepartment($year,$department);
        $man_hour_section = $this->tna->getManHourSection($year,$department);
        $count_user_persection = $this->user->getCountPerSection($department);

        $man_hour_final = [];
        foreach ($count_user_persection as $cups) {
            $found = false;
            foreach ($man_hour_section as $mht) {
                if ($cups['nama_seksi'] == $mht['nama_seksi']) {
                    $man_hour_final[] = array_merge($cups, $mht);
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $cups['total_man_hour'] = 0;
                $man_hour_final[] = $cups;
            }
        }

        array_push($data, $budget);
        array_push($data, $dashboard_column_all);
        array_push($data, $dashboard_line_all);
        array_push($data, $category);
        array_push($data, $Jenis_training);
        array_push($data, $need_approval);
        array_push($data, $approved);
        array_push($data, $rejected);

        //Grafik Training
        array_push($data, $CountImplemented);
        array_push($data, $PercentImplemented);
        array_push($data, $CountNotImplemented);
        array_push($data, $PercentNotImplemented);
        array_push($data, $allTraining);

        //Grafik Man Hour
        array_push($data, $man_hour_total);
        array_push($data, $man_hour_final);


        //$sess =  session()->set('year', rand(1, 10));

        echo json_encode($data);
    }

    public function TrainingDahboardMonth()
    {
        $training = $this->tna->DashboardTraining(date('Y'));

        // dd($training);
        if (empty($training)) {
            return  '';
        } else {
            foreach ($training as $Training) {
                $Month[] = date("F", mktime(0, 0, 0, $Training['Bulan'], 10));
            }
            return $Month;
        }
    }


    //Dashboard Column For all Training
    public function TrainingDashboardCountColumn($year)
    {
        $training = $this->tna->DashboardTraining($year);
        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }

            return array_values($total);
        }
    }

    public function TrainingDashboardCountColumnDivisi($year,$divisi)
    {
        $training = $this->tna->DashboardTrainingDivisi($year,$divisi);
        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }

            return array_values($total);
        }
    }

    public function TrainingDashboardCountColumnSeksi($seksi, $year)
    {
        $training = $this->tna->DashboardTrainingSeksi($year,$seksi);
        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }

            return array_values($total);
        }
    }

    public function training_dashboard_count_column_department($year , $department)
    {
        $training = $this->tna->dashboard_training_department($year, $department);
        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }

            return array_values($total);
        }
    }

    //Dashboard Column For all Training
    public function TrainingDashboardCountLine($year)
    {
        $training = $this->tna->DashboardTrainingLine($year);

        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function TrainingDashboardCountLineDivisi($year,$divisi)
    {
        $training = $this->tna->DashboardTrainingLineDivisi($year,$divisi);

        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function training_dashboard_count_line_department($year,$department)
    {
        $training = $this->tna->dashboard_training_line_department($year,$department);

        if (empty($training)) {
            return 0;
        } else {
            $total = [1 => 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function TrainingDashboardMonthDepartment($department)
    {
        $training = $this->tna->DashboardTrainingDepartment(date('Y'), $department);
        if (empty($training)) {
            return  '';
        } else {
            foreach ($training as $Training) {
                $Month[] = date("F", mktime(0, 0, 0, $Training['Bulan'], 10));
            }
            return $Month;
        }
    }



    public function JenisTrainingDashboard($year)
    {
        $jenis = $this->tna->getJenisTraining($year);
        // dd($jenis);
        $JenisTrainings = [];

        foreach ($jenis as $Jenis) {
            $count = $this->tna->CountJenisTraining($Jenis->jenis_training, $year);
            // dd($count);
            if ($Jenis->jenis_training == 'Strategic Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#FDD623",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Technical Competency Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#B93F94",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Cultural Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#6754A3",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Leadership Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#028DD0",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Soft Competency Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#F8A926",
                    'sliced' => true
                ];
            } else {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#E5144B",
                    'sliced' => true
                ];
            }

            array_push($JenisTrainings, $JenisTraining);
        }
        return $JenisTrainings;
    }

    public function JenisTrainingDashboardDepartment($year, $department)
    {
        $jenis = $this->tna->getJenisTrainingDepartment($year, $department);
        // dd($jenis);
        $JenisTrainings = [];

        foreach ($jenis as $Jenis) {
            $count = $this->tna->CountJenisTrainingDepartment($Jenis->jenis_training, $year, $department);
            // dd($count);
            if ($Jenis->jenis_training == 'Strategic Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#FDD623",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Technical Competency Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#B93F94",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Cultural Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#6754A3",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Leadership Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#028DD0",
                    'sliced' => true
                ];
            } elseif ($Jenis->jenis_training == 'Soft Competency Training') {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#F8A926",
                    'sliced' => true
                ];
            } else {
                $JenisTraining = [
                    'name' => $Jenis->jenis_training,
                    'y' => $count[0]->jenis_training,
                    'color' => "#E5144B",
                    'sliced' => true
                ];
            }

            array_push($JenisTrainings, $JenisTraining);
        }
        return $JenisTrainings;
    }

    public function CategoryDashboard($year)
    {
        $categories = $this->tna->getCategory($year);

        $category = [];
        foreach ($categories as $Categories) {
            $count = $this->tna->CountCategory($Categories->kategori_training, $year);
            if ($Categories->kategori_training == 'Internal') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#E1154A",
                    'sliced' => true

                ];
            } elseif ($Categories->kategori_training == 'External') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#5DBA48",
                    'sliced' => true
                ];
            } elseif ($Categories->kategori_training == 'Inhouse') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#FDA71E",
                    'sliced' => true
                ];
            } else {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#47BEBA",
                    'sliced' => true
                ];
            }

            array_push($category, $categori);
        }

        //dd($categori);
        return $category;
    }

    public function CategoryDashboardDepartment($year, $department)
    {
        $categories = $this->tna->getCategory($year);

        $category = [];
        foreach ($categories as $Categories) {
            $count = $this->tna->CountCategoryDepartment($Categories->kategori_training, $year,$department);
            if ($Categories->kategori_training == 'Internal') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#E1154A",
                    'sliced' => true

                ];
            } elseif ($Categories->kategori_training == 'External') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#5DBA48",
                    'sliced' => true
                ];
            } elseif ($Categories->kategori_training == 'Inhouse') {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#FDA71E",
                    'sliced' => true
                ];
            } else {
                $categori = [
                    'name' => $Categories->kategori_training,
                    'y' => $count[0]->kategori_training,
                    'color' => "#47BEBA",
                    'sliced' => true
                ];
            }

            array_push($category, $categori);
        }

        //dd($categori);
        return $category;
    }


    public function DashboardRejected($year)
    {
        $rejected = $this->tna->getDashboardTrainingReject($year);
        $total = $this->tna->getCountTrainingByYear($year);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }

        if (empty($rejected)) {
            return 0;
        } else {
            $number = $rejected[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardRejectedDepartment($year,$department)
    {
        $rejected = $this->tna->getDashboardTrainingRejectDepartment($year,$department);
        $total = $this->tna->getCountTrainingByYearDepartment($year,$department);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }

        if ($rejected[0]['id_tna'] == 0 || empty([$rejected])) {
            return 0;
        } else {
            $number = $rejected[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardRejectedDivisi($year,$divisi)
    {
        $rejected = $this->tna->getDashboardTrainingRejectDivisi($year,$divisi);
        $total = $this->tna->getCountTrainingByYearDivisi($year,$divisi);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }

        if ($rejected[0]['id_tna'] == 0 || empty([$rejected])) {
            return 0;
        } else {
            $number = $rejected[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }


    public function DashboardApproved($year)
    {
        $approved  = $this->tna->getDashboardTrainingApproved($year);
        // dd($approved);
        $total = $this->tna->getCountTrainingByYear($year);

        $totals = $total[0]['id_tna'];

        if($totals == 0){
            $totals = 1;
        }

        if (empty($approved)) {
            return 0;
        } else {
            $number = $approved[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardApprovedDepartment($year, $department)
    {
        $approved  = $this->tna->getDashboardTrainingApprovedDepartment($year,$department);
        $total = $this->tna->getCountTrainingByYearDepartment($year,$department);
        $totals = $total[0]['id_tna'];

        if($totals == 0){
            $totals = 1;
        }

        if ($approved[0]['id_tna'] == 0 || empty([$approved])) {
            return 0;
        } else {
            $number = $approved[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardApprovedDivisi($year, $divisi)
    {
        $approved  = $this->tna->getDashboardTrainingApprovedDivisi($year,$divisi);
        $total = $this->tna->getCountTrainingByYearDivisi($year,$divisi);

        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }

        if ($approved[0]['id_tna'] == 0 || empty([$approved])) {
            return 0;
        } else {
            $number = $approved[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardNeedApproval($year)
    {
        $need = $this->tna->getDashboardTrainingNeedApproval($year);
        $total = $this->tna->getCountTrainingByYear($year);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }
        // dd($need);
        if ($need[0]['id_tna'] == 0 || empty([$need])) {
            return 0;
        } else {
            $number = $need[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardNeedApprovalDepartment($year,$department)
    {
        $need = $this->tna->getDashboardTrainingNeedApprovalDepartment($year,$department);
        $total = $this->tna->getCountTrainingByYearDepartment($year,$department);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }
        // dd($need);
        if ($need[0]['id_tna'] == 0 || empty([$need])) {
            return 0;
        } else {
            $number = $need[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }

    public function DashboardNeedApprovalDivisi($year,$divisi)
    {
        $need = $this->tna->getDashboardTrainingNeedApprovalDivisi($year,$divisi);
        $total = $this->tna->getCountTrainingByYearDivisi($year,$divisi);
        $totals = $total[0]['id_tna'];
        if($totals == 0){
            $totals = 1;
        }
        // dd($need);
        if ($need[0]['id_tna'] == 0 || empty([$need])) {
            return 0;
        } else {
            $number = $need[0]['id_tna'] / $totals * 100;
            return round($number, 1);
        }
    }


    public function TrainingDashboardCountColumnDepartment($department, $year)
    {

        $training = $this->tna->DashboardTrainingDepartment($year, $department);
        $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
        if (empty($training)) {
            return  0;
        } else {

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function ManHourTrainingDashboardCountColumnDepartment($department, $year)
    {
        $man_hour = $this->tna->DashboardManHourDepartment($year, $department);
        $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
        if (empty($man_hour)) {
            return  0;
        } else {
            foreach ($man_hour as $ManHour) {
                $total[$ManHour['Bulan']] = $ManHour['total_man_hour'];
            }
            return array_values($total);
        }
    }

    public function ManHourTrainingDashboardCountColumnSeksi($seksi, $year)
    {
        $man_hour = $this->tna->DashboardManHourSeksi($year, $seksi);
        $total = [1 => null, null, null, null, null, null, null, null, null, null, null, null];
        if (empty($man_hour)) {
            return  0;
        } else {
            foreach ($man_hour as $ManHour) {
                $total[$ManHour['Bulan']] = $ManHour['total_man_hour'];
            }
            return array_values($total);
        }
    }

    public function TrainingDashboardCountLineDepartment($department, $year)
    {
        $training = $this->tna->DashboardTrainingLineDepartment($year, $department);
        $total = [1 => 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        if (empty($training)) {
            return  0;
        } else {

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function TrainingDashboardCountLineSeksi($seksi, $year)
    {
        $training = $this->tna->DashboardTrainingLineSeksi($year, $seksi);
        $total = [1 => 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        if (empty($training)) {
            return  0;
        } else {

            foreach ($training as $Training) {
                $total[$Training['Bulan']] = $Training['total'];
            }
            return array_values($total);
        }
    }

    public function percentTrainingImplemented($year)
    {
        $implemented  = $this->tna->CountTrainingImplemented($year);
        $allTraining = $this->tna->CountAllTraining($year);
        //  dd($allTraining);
        if ($implemented[0]['training'] != 0) {
            $count = $implemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }

    public function percentTrainingImplementedDivisi($year,$divisi)
    {
        $implemented  = $this->tna->CountTrainingImplementedDivisi($year,$divisi);
        $allTraining = $this->tna->CountAllTrainingDivisi($year,$divisi);
        //  dd($allTraining);
        if ($implemented[0]['training'] != 0) {
            $count = $implemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }

    public function percentTrainingImplementedDepartment($year,$department)
    {
        $implemented  = $this->tna->CountTrainingImplementedDepartment($year,$department);
        $allTraining = $this->tna->CountAllTrainingDepartment($year,$department);
        //  dd($allTraining);
        if ($implemented[0]['training'] != 0) {
            $count = $implemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }


    public function PercentTrainingNOtImplemented($year)
    {
        $NotImplemented = $this->tna->CountTrainingNotImplemented($year);

        $allTraining = $this->tna->CountAllTraining($year);

        if ($NotImplemented[0]['training'] != 0) {
            $count = $NotImplemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }

    public function PercentTrainingNOtImplementedDepartment($year,$department)
    {
        $NotImplemented = $this->tna->CountTrainingNotImplementedDepartment($year,$department);
        $allTraining = $this->tna->CountAllTrainingDepartment($year,$department);
        if ($NotImplemented[0]['training'] != 0) {
            $count = $NotImplemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }

    public function PercentTrainingNOtImplementedDivisi($year,$divisi)
    {
        $NotImplemented = $this->tna->CountTrainingNotImplementedDivisi($year,$divisi);
        $allTraining = $this->tna->CountAllTrainingDivisi($year,$divisi);
        if ($NotImplemented[0]['training'] != 0) {
            $count = $NotImplemented[0]['training'] / $allTraining[0]['training'] * 100;
            return $count;
        } else {
            return 0;
        }
    }

    public function testChartDepartment()
    {
        $department = $this->user->DistinctDepartemen();

        //  foreach ($department as $Department) {
        $month = $this->TrainingDashboardMonthDepartment('HRD');
        // $column = $this->TrainingDahboardCountColumnDepartment($Department['departemen']);
        // $line = $this->TrainingDahboardCountLineDepartment($Department['departemen']);
        d($month);
        // d($line);
        // }
    }
}
