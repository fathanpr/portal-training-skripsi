<?php

namespace App\Models;

use CodeIgniter\Model;

class M_ListTraining extends Model
{

    protected $table      = 'training';
    protected $primaryKey = 'id_training';
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['judul_training', 'jenis_training', 'deskripsi', 'vendor', 'biaya', 'material_path'];


    function M_test()
    {
        $data = $this->get();
        return $data;
    }


    public function getCategory()
    {
        $this->select('jenis_training,deskripsi')->distinct();
        return $this->get()->getResult();
    }


    public  function getList($id)
    {
        $this->select('training.*,categories.category')->where('jenis_training', $id);
        $this->join('categories', 'categories.id_categories = training.jenis_training');
        return $this->get()->getResult();
    }


    public function getAll()
    {
        $data = $this->select('*')->orderBy('judul_training', 'ASC')->get();
        return $data->getResultArray();
    }

    public function getTrainingByTna($judul_training){
        $this->select()->where('judul_training',$judul_training);
        return $this->get()->getResultArray();
    }


    public function getTna($judul_training)
    {
        $this->select()->where('judul_training', $judul_training);
        return $this->get()->getResult();
    }

    public function getIdTraining($id = false)
    {
        $this->where(['id_training' => $id]);
        $this->join('categories', 'categories.id_categories=training.jenis_training');
        return   $this->first();
    }

    // public function getDataIdTraining($training)
    // {
    //     $this->select('id_training')->like('judul_training', urlDecode($training));
    //     $this->get()->getResultArray();
    // }


    public function getJenisTrainining()
    {
        $this->select('jenis_training')->distinct();
        return $this->get()->getResult();
    }

    public function getJudulById($id){
        return $this->select('judul_training')->where('id_training', $id)->first();
    }

    public function CountJenisTraining($training)
    {
        return $this->selectCount('jenis_training')->where('jenis_training', $training)->first();
    }
}