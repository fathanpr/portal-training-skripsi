<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>
<div class="card m-1">
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <div class="row">
            <div class="col ml-3">
                <h2>Delete Data Training</h2>
                <p>Note : Hanya bisa menghapus data yang belum dimulai (belum Done pada menu Schedule)</p>
                <table class="table table-hover" id="tnaAtmp">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>NPK</th>
                            <th>Department</th>
                            <th>Training Category</th>
                            <th>Training Title</th>
                            <th>Vendor</th>
                            <th>Training Type</th>
                            <th>Training Category</th>
                            <th>Training Method</th>
                            <th>Training Goals</th>
                            <th>Request Training</th>
                            <th>Training Start</th>
                            <th>Training Finished</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <?php foreach ($tna as $data) : ?>
                        <tr id="rowTna<?= $data['id_tna'] ?>">
                        <td><?= $data['nama'] ?></td>
                        <td><?= $data['npk'] ?></td>
                        <td><?php $departemen = $user->getDepartemenByNpk($data['npk']); echo $departemen ? $departemen->departemen : '-'; ?></td>
                        <td>
                            <?php if($data['kelompok_training'] == 'amdi_aop') : ?>
                            <span class="badge badge-info">AMDI AOP</span>
                            <?php elseif($data['kelompok_training'] == 'cross_budget') :  ?>
                            <span class="badge badge-success">CROSS BUDGET</span>
                            <?php elseif($data['kelompok_training'] == 'training') : ?>
                            <span class="badge badge-primary">TRAINING</span>
                            <?php else : ?>
                            <span class="badge badge-warning">UNPLANNED</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $data['training'] ?></td>
                        <td><?= $data['vendor'] ?></td>
                        <td><?= $data['jenis_training'] ?></td>
                        <td><?= $data['kategori_training'] ?></td>
                        <td><?= $data['metode_training'] ?></td>
                        <td><?= $data['tujuan_training'] ?></td>
                        <td><?= $data['request_training'] ?></td>
                        <td><?= $data['mulai_training'] ?></td>
                        <td><?= $data['rencana_training'] ?></td>
                        <td>
                            <button class="btn btn-danger" onclick="deleteTraining('<?= $data['id_tna'] ?>')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>

<script>
    function deleteTraining(idTna) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?= base_url('delete_data_training') ?>',
                    type: 'POST',
                    data: { id_tna: idTna },
                    success: function (response) {
                        $('#rowTna'+idTna).remove();
                        Swal.fire(
                            'Deleted!',
                            'Your training has been deleted.',
                            'success'
                        );
                    },
                    error: function () {
                        Swal.fire(
                            'Error!',
                            'Error during the AJAX request.',
                            'error'
                        );
                    }
                });
            }
        });
    }
</script>
<?= $this->endSection() ?>