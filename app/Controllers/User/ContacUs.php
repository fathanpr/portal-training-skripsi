<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\M_Contact;
use App\Models\M_EvaluasiReaksi;
use App\Models\M_Tna;
use App\Models\UserModel;
use App\Models\M_Categories;
use App\Models\M_Astra;
use App\Models\M_Technical; 
use App\Models\M_TechnicalB;
use App\Models\M_Expert;
use App\Models\M_Company;
use App\Models\M_Soft;
use App\Models\M_Silabus;


class ContacUs extends BaseController
{

    private M_Contact $contact;
    private M_Categories $categories;
    private UserModel $user;
    private M_Astra $astra;
    private M_Technical $technical; 
    private M_TechnicalB $technicalb;
    private M_Expert $expert;
    private M_Company $company;
    private M_Soft $soft;
    private M_Silabus $silabus;


    public function __construct()
    {
        $this->contact = new M_Contact();
        $this->categories = new M_Categories();
        $this->user = new UserModel();
        $this->astra = new M_Astra();
        $this->technical = new M_Technical();  
        $this->technicalb = new M_TechnicalB();
        $this->expert = new M_Expert();
        $this->company = new M_Company();
        $this->soft = new M_Soft(); 
        $this->silabus = new M_Silabus();
    }

    public function index()
    {

        $data = [
            'tittle' => 'Contac Us',
        ];
        return view('user/contacususer', $data);
    }

    public function requestSylabus(){
        $jenis_training = $this->categories->getJenisTraining();
        $departemen = $this->user->DistinctDepartemen();
        $id_user = session()->get('id');

        $data_request = $this->silabus->getDataById($id_user);

        $departemen = array_filter($departemen, function($item) { return $item['departemen'] != 'AMDI AOP'; });
        $departemen = array_map(function($item) { return $item['departemen']; }, $departemen);

        $data = [
            'tittle' => 'Request Sylabus',
            'jenis_training' => $jenis_training,
            'departemen' => $departemen,
            'data_request' => $data_request
        ];

        return view('user/requestsylabus', $data);
    }
    
    public function getKompetensiAbyDept($departemen){
        $astra = [];
        $data_astra = $this->astra->getDataAstra();
        foreach($data_astra as $data){
            $AstraComp = [
                'id' => $data['id_astra'],
                'category' => "ALC - " . $data['astra'],
                'competency' => $data['astra'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Astra'
            ];
            array_push($astra, $AstraComp);
        }

        //TECHNICAL A
        $technical = [];
        $data_technical = $this->technical->getDataTechnical();
        foreach($data_technical as $data){
            $TechnicalComp = [
                'id' => $data['id_technical'],
                'category' => "Technical Comp - " . $data['technical'],
                'competency' => $data['technical'],
                'departemen' => $data['departemen'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'TechnicalA'
            ];
            array_push($technical, $TechnicalComp);
        }

        $expert = [];
        $data_expert = $this->expert->getDataExpert();
        foreach($data_expert as $data){
            $ExpertComp = [
                'id' => $data['id_expert'],
                'category' => "Exp - " . $data['expert'],
                'competency' => $data['expert'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Expert'
            ];
            array_push($expert, $ExpertComp);
        }
        
        $filteredTechnical = array_filter($technical, function($item) use ($departemen) {
            return $item['departemen'] == $departemen;
        });

        $result = [];
        if(!empty($filteredTechnical)){
            $result = array_merge($astra,$expert, $filteredTechnical);
        }
        echo json_encode($result);
    }

    public function getKompetensiBbyDept($departemen){
        $technicalb = [];
        $data_technicalb = $this->technicalb->getDataTechnical();
        foreach($data_technicalb as $data){
            $TechnicalbComp = [
                'id' => $data['id_technicalB'],
                'category' => "TechB - " . $data['technicalB'],
                'competency' => $data['technicalB'],
                'department' => $data['department'],
                'keterangan' => 'TechnicalB'
            ];
            array_push($technicalb, $TechnicalbComp);
        }

        $filteredTechnicalB = array_filter($technicalb, function($item) use ($departemen) {
            return $item['department'] == $departemen;
        });

        //COMPANY
        $company = [];
        $data_company = $this->company->getDataCompany();
        foreach($data_company as $data){
            $CompanyComp = [
                'id' => $data['id_company'],
                'category' => "Comp - " . $data['company'],
                'competency' => $data['company'],
                'proficiency' => $data['proficiency'],
                'divisi' => $data['divisi'],
                'keterangan' => 'Company'
            ];
            array_push($company, $CompanyComp);
        }
        $divisi_distinct = $this->user->getDepartmentDivision($departemen);
        $divisi = $divisi_distinct[0]['divisi'];
        
        $filteredCompany = array_filter($company, function($item) use ($divisi) {
            return $item['divisi'] == $divisi;
        });

        //SOFT
        $soft = [];
        $data_soft = $this->soft->getDataSoft();
        foreach($data_soft as $data){
            $SoftComp = [
                'id' => $data['id_soft'],
                'category' => "Soft - " . $data['soft'],
                'competency' => $data['soft'],
                'proficiency' => $data['proficiency'],
                'keterangan' => 'Soft'
            ];
            array_push($soft, $SoftComp);
        }

        if(!empty($filteredCompany) && !empty($filteredTechnicalB)){
            $result = array_merge($filteredTechnicalB,$filteredCompany,$soft);
            echo json_encode($result);
        }
    }

    public function sendContact()
    {
        $nama = $this->request->getVar('nama');
        $email = $this->request->getVar('email');
        $pesan = $this->request->getVar('pesan');

        $data = [
            'nama' => $nama,
            'email' => $email,
            'pesan' => $pesan
        ];
        $this->contact->save($data);
        session()->setFlashdata('success', 'Pesan Berhasil Dikirim');
        return redirect()->to('/contac_us');
    }

    public function submitForm(){
        $id_user = session()->get('id');
        $departemen = session()->get('departemen');
        $divisi = session()->get('divisi');
        $judul_training = $this->request->getPost('judul_training');
        $jenis_training = $this->request->getPost('jenis_training');
        $pelaksanaan_training = $this->request->getPost('pelaksanaan_training');
        $sasaran_peserta = $this->request->getPost('sasaran_peserta');
        $sasaran_departemen = $this->request->getPost('sasaran_departemen');
        $pilihan_kompetensi = $this->request->getPost('pilihan_kompetensi');
        $tujuan_training = $this->request->getPost('tujuan_training');
        $deskripsi_materi = $this->request->getPost('deskripsi_materi');
        $metode_training = $this->request->getPost('metode_training[]');
        $durasi_training = $this->request->getPost('durasi_training');
        $kelengkapan = $this->request->getPost('kelengkapan[]');
        $metode_evaluasi = $this->request->getPost('metode_evaluasi[]');
        $notes = $this->request->getPost('notes');

        $data = [
            'id_user' => $id_user,
            'departemen' => $departemen,
            'divisi' => $divisi,
            'tanggal_request' => date('Y-m-d'),
            'judul' => $judul_training,
            'jenis' => $jenis_training,
            'pelaksanaan' => $pelaksanaan_training,
            'sasaran_peserta' => $sasaran_peserta,
            'sasaran_departemen' => $sasaran_departemen,
            'kompetensi' => $pilihan_kompetensi,
            'tujuan' => $tujuan_training,
            'deskripsi' => $deskripsi_materi,
            'metode' => implode(',', $metode_training), 
            'durasi' => $durasi_training,
            'kelengkapan' => implode(',', $kelengkapan), 
            'metode_evaluasi' => implode(',', $metode_evaluasi), 
            'notes' => $notes,  
        ];
        $this->silabus->save($data);
    }
}