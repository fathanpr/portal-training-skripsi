<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>
<?php if(!empty($budget)) : ?>
    <?php foreach($budget as $budgets) : ?>
    <?php if(!empty($data_cross_budget)) : ?>
        <div class="card overflow-auto m-1">
            <div class="card-header">
                <h3 class="card-title"><?= $tittle ?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 overflow-auto">
                <table class="table table-striped overflow-auto">
                    <tr>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Training</th>
                        <th>Training Type</th>
                        <th>Training Category</th>
                        <th>Training Method</th>
                        <th>Training Start</th>
                        <th>Training Finished</th>
                        <th>Training Goals</th>
                        <th>Notes</th>
                        <th>Budget Estimation</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="kadiv-verify">
                    <?php $i = 0;  $sum = 0;?>
                    <?php foreach ($data_cross_budget as $statuses) : ?>
                    <?php if($statuses['id_budget'] == $budgets['id_budget']) : ?>
                    <tr>
                        <td><?= $statuses['nama'] ?></td>
                        <?php $departemen = $user->getUserByNama($statuses['nama']); ?>
                        <td><?=  $departemen['departemen'] ?></td>
                            <td><?= $statuses['training'] ?></td>
                            <td><?= $statuses['jenis_training'] ?></td>
                            <td><?= $statuses['kategori_training'] ?></td>
                            <td><?= $statuses['metode_training'] ?></td>
                            <td><?= $statuses['mulai_training'] ?></td>
                            <td><?= $statuses['rencana_training'] ?></td>
                            <td><?= $statuses['tujuan_training'] ?></td>
                            <td><?= $statuses['notes'] ?></td>
                            <td><?= "Rp " . number_format($statuses['biaya_actual'], 0, ',', '.') ?></td>
                            <td>
                                <?php if(session()->get('bagian') == 'BOD') : ?>
                                    <a onclick="AcceptBod(<?= $i ?>)" id="accept-bod<?= $i ?>" href="javascript:;"
                                    class="btn btn-success m-1" style="width:100px;color:white;"
                                    data-accept-kadiv="<?= $statuses['id_tna'] ?>"><i class=" fa fa-fw fa-check"></i>Accept</a>
                                <input type="hidden" id="accept-bod-input<?= $i ?>" value="<?= $statuses['id_tna'] ?>">
                                <a onclick="verify_bod(<?= $i ?>)" id="reject-bod<?= $i ?>" href="javascript:;"
                                    class="bod-verify btn btn-danger m-1" style="width:100px;"><i
                                        class="fa fa-fw fa-close"></i>Reject</a>
                                <input type="hidden" id="reject-bod-input<?= $i ?>" value="<?= $statuses['id_tna'] ?>">
                                <?php else : ?>
                                    <a onclick="AcceptKadiv(<?= $i ?>)" id="accept-kadiv<?= $i ?>" href="javascript:;"
                                    class="btn btn-success m-1"
                                    data-biaya-actual="<?= $statuses['biaya_actual'] ?>" 
                                    data-accept-kadiv="<?= $statuses['id_tna'] ?>"><i class=" fa fa-fw fa-check"></i>Accept</a>
                                <input type="hidden" id="accept-kadiv-input<?= $i ?>" value="<?= $statuses['id_tna'] ?>">
                                <a id="reject-kadiv<?= $i ?>" href="javascript:;"
                                    class="kadiv-verify btn btn-danger m-1"
                                    data-reject-kadiv="<?= $statuses['id_tna']  ?>" onclick="Kadiv_verify(<?= $i ?>)"><i
                                        class="fa fa-fw fa-close"></i>Reject</a>
                                <input type="hidden" id="reject-kadiv-input<?= $i ?>" value="<?= $statuses['id_tna'] ?>">
                                <?php endif; ?>
                            </td>
                        </tr>
                        <div class=" modal fade" id="rejectKadiv<?= $i ?>" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Rejected Reason</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="d-flex flex-column">
                                            <label for="alasan">Reason</label>
                                            <textarea id="alasan<?= $i ?>" class="mt-1" name="alasan<?= $i ?>"
                                                required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a id="kadiv-reject<?= $i ?>" href="javascript:;" class="btn btn-danger"
                                        onclick="reject_kadiv(<?= $i ?>) "><i
                                                class=" fa fa-fw fa-close"></i>Reject</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $sum += $statuses['biaya_actual']; $i++; ?>
                        <?php endif; ?>
                        <?php endforeach; ?>
                        <?php else : ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php if(!empty($budgets)) : ?>
                <div class="m-3 d-flex justify-content-around">
                <div><strong>Year : </strong>
                    <?= $budgets['year'] ?>
                </div>
                <div><strong>Alocated Budget : </strong>
                    <?= "Rp " . number_format($budgets['alocated_budget'], 0, ',', '.') ?>
                </div>
                <div><strong>Available
                        Budget : </strong><?= "Rp " . number_format($budgets['available_budget'], 0, ',', '.') ?></div>
                <div><strong>Used Budget : </strong><?= "Rp " . number_format($budgets['used_budget'], 0, ',', '.') ?></div>
                <div><strong>Temporary Calculation:
                    </strong><?= "Rp " . number_format($budgets['temporary_calculation'], 0, ',', '.') ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
<script>

</script>
<?php endif; ?>
<?= $this->endSection() ?>