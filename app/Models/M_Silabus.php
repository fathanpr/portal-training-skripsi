<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Silabus extends Model
{
    protected $table      = 'request_silabus';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_silabus';
    protected $allowedFields = ['id_user', 'npk', 'departemen', 'divisi','tanggal_request','judul','jenis','pelaksanaan','sasaran_peserta','sasaran_departemen','kompetensi','tujuan','deskripsi','metode','durasi','kelengkapan','metode_evaluasi','notes','status_baca'];


    public function getAllSilabusRequest(){
        $this->select('*');
        return $this->get()->getResultArray();
    }

    public function countSylabusUnread(){
        $this->select('COUNT(*) as count')->where('status_baca IS NULL');
        
        return $this->countAllResults();
    }

    public function countAllSylabus(){
        $this->select('COUNT(*) as count');
        
        return $this->countAllResults();
    }

    public function getDataById($id_user){
        $this->select()->where('id_user', $id_user);
        return $this->get()->getResultArray();
    }
}