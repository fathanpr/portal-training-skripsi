<?= $this->extend('/template/templateuser') ?>

<?= $this->section('content') ?>


<style>
select {
    border: 1px solid #fff;
    background-color: transparent;
    -moz-appearance: none;
}
</style>

<div class="card m-1">
    <center class="m-4">
        <h4>Dashboard <?= session()->get('departemen') ?> <select id="year" onchange="ChangeYear()">
                <?php foreach ($years as  $Year) : ?>
                <option value="<?= $Year['Years'] ?>" <?= ($yearss == $Year['Years'])  ? 'selected' : '' ?>>
                    <?= $Year['Years'] ?></option>
                <?php endforeach; ?>
            </select>
            <form method="get" name="form" action="">
                <input type="hidden" placeholder="Enter Data" name="years" id="years">
            </form>
        </h4>
    </center>
</div>

<div class="m-1 d-flex flex-row">
<div class="card ml-1" style="width:40%;">
        <div class="d-flex align-content-center">
            <div style="width:100%;">
                <center>
                    <h5>Total Man Hour</h5>
                    <input readonly type="text" class="form-control" style="width:70%; text-align:center;" id="man_hour_total">
                    <h6 id="man_hour_persen"></h6>
                </center>
            </div>
        </div>
        <div class="card m-2" style="background-color:#F0F8FF;height:100%">
            <center>
                <h5>Man Hour</h5>
                <div id="man_hour_departemen"></div>
            </center>

        </div>
    </div>

    <div class="" style="width:100%;height:100%">
        <div class="card">
            <center>
                <h5>Status Approval</h5>
            </center>
            <div class="card accent-blued-flex flex-row justify-content-between">
                <div id="need_approval" style="width:30%;height:100%;color:blue;"></div>
                <div id="approval" style="width:30%;height:100%;color:blue;"></div>
                <div id="rejected" style="width:30%;height:100%;color:blue;"></div>
            </div>
        </div>
        <div class="card" id="training" style="color:blue;"></div>
    </div>

    <div class="card ml-1" style="width:40%;">
        <div class="d-flex align-content-center">
            <div style="width:100%;">
                <center>
                    <h5>Total Budget (RP)</h5>
                    <input readonly type="text" class="form-control" style="width:70%;" id="alocated_budget">
                    <h5>Used Budget (RP)</h5>
                    <input readonly type="text" class="form-control" style="width:70%;" id="used_budget">
                    <h6 id="global_persen"></h6>
                </center>
            </div>
        </div>
        <div class="card m-2" style="background-color:#F0F8FF;height:100%">
            <center>
                <h5>Available Budget</h5>
                <div id="department_budget"></div>
            </center>

        </div>
    </div>
</div>
<div class="card m-1" style="width:99%;height:400px;">

    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="carousel-inner">
            <?php $i = 0;
            foreach ($seksi as $section) :

                $active = ($i == 0) ? 'active' : '';
            ?>
            <div class="carousel-item <?= $active ?> d-flex">
                <div class="m-1 flex-fill w-100" id="<?= $section['seksi'] ?>" style="width:100%;height:100%;"></div>
                <div class="m-1 flex-fill w-100" id="man_hour_<?= $section['seksi'] ?>" style="width:100%;height:100%;"></div>
            </div>
            <?php $i++;
            endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" style="pointer-events: none;bottom: 85%;">
            <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color:black; pointer-events: auto;"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" style="pointer-events: none;bottom: 85%;">
            <span class="carousel-control-next-icon" aria-hidden="true" style="background-color:black; pointer-events: auto;"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<script>
$(document).ready(function() {
    //ChangeYear()


    $.ajax({
        type: 'post',
        url: "<?= base_url() ?>dashboard_year_department",
        async: true,
        dataType: "json",
        data: {
            year: <?= $yearss ?>,
        },
        success: function(data) {

            console.log(data)

            //Budget
            let Budgets = data[0];
            let ManHourTotal = data[13] + ' Hours';
            let MoDepartment = data[14];
            console.log(MoDepartment)
            // console.log(ManHourTotal)
            let Alocated = 0,
                Used = 0,
                Available = 0;
            let department_budget = ``;
            let department_man_hour = ``;
            $('#man_hour_total').val(ManHourTotal);

            Object.entries(Budgets).forEach(([key, value]) => {
                Alocated += parseInt(value.alocated_budget);
                Used += parseInt(value.used_budget);
                Available += parseInt(value.available_budget);
                let department_percent = value.available_budget / value.alocated_budget *
                    100;
                department_budget += `<h6>${value.department}</h6>
                    <div class="mb-1" style="display:flex;justify-content:center;">
                        <input readonly type="text" class="form-control" style="width:50%;height:30px" value="${RupiahFormat(value.available_budget)}">
                        <input readonly type="text" class="form-control" style="width:30%;height:30px" value="${department_percent.toFixed(1)}%">
                    </div>`;
            })

            Object.entries(MoDepartment).forEach(([key, value]) => {
                department_man_hour += `<h6>${value.nama_seksi}</h6>
                    <div class="mb-1" style="display:flex;justify-content:center;">
                        <input readonly type="text" class="form-control" style="width:45%;height:30px" value="${value.total_man_hour} Hours">
                        <input readonly type="text" class="form-control" style="width:45%;height:30px" value="${value.jumlah} Employees">
                    </div>`;
            })

            let persent = Used / Alocated * 100;

            Alocated = RupiahFormat(Alocated);

            Available = RupiahFormat(Available);

            Used = RupiahFormat(Used);
            console.log(department_budget)

            $('#alocated_budget').val(Alocated);
            $('#used_budget').val(Used);
            $('#global_persen').text(persent.toFixed(1) + '%');
            $('#department_budget').html(department_budget)
            $('#man_hour_departemen').html(department_man_hour)


            //Training All

            //training chart
            Highcharts.chart('training', {
                title: {
                    text: '<div class="d-flex justify-content-between" style="width:100%;height:100%"><div><h6 style="font-size:15px">Training Implemented  &emsp; &emsp;</h6></div><div><h6 style="font-size:15px">&emsp; &emsp;Not Implement Yet</h6></div></div>',

                },
                subtitle: {
                    text: `<div class="d-flex justify-content-between" style="width:100%;height:100%"><div><h6 style="font-size:15px">${data[8][0].training}  &emsp; &emsp;&emsp;&emsp;  &emsp;  &emsp;&emsp;<span style="color: green;">${data[9].toFixed(1)}  %</span> &emsp;&emsp; &emsp; &emsp;</h6></div><div><h6 style="font-size:15px">${data[10][0].training}&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <span style="color: red;">${data[11].toFixed(1)}%</span></h6></div></div>`
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                        'Sep',
                        'Oct', 'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Training'
                    }
                },
                tooltip: {
                    valueSuffix: ' Training'
                },
                series: [{
                    type: 'column',
                    name: 'Training Target',
                    data: data[1],
                    color: '#0092D5'

                }, {
                    type: 'line',
                    name: 'Training Implemented',
                    data: data[2],
                    color: '#F6164A'

                }]
            });


            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'need_approval',
                    type: 'pie'
                },
                title: {
                    verticalAlign: 'middle',
                    floating: true,
                    text: `<span style="font-size: 30px"> ${data[5]} % </span><br><span style="font-size:15px">Need<br>Approval</span>`,
                },
                plotOptions: {
                    pie: {
                        innerSize: '100%'
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false
                            },
                            inactive: {
                                opacity: 1
                            }
                        }
                    }
                },
                series: [{
                    // borderWidth: 0,
                    name: 'Need Approval',
                    data: [{
                            name: 'Need Approval',
                            y: data[5],
                            color: '#0079C2'

                        },
                        {
                            name: 'Approve And Reject',
                            y: 100 - data[5],
                            color: "#DDF4E4",
                        }
                    ],
                    size: '100%',
                    innerSize: '75%',
                    showInLegend: false,
                    dataLabels: {
                        enabled: false
                    }
                }, {
                    size: '65%',
                    innerSize: '95%',
                    dataLabels: {
                        enabled: false
                    },
                    data: [{
                        // y: 30
                    }, {
                        y: 20,
                        color: 'rgba(0,0,0,0)'
                    }]
                }],
                credits: {
                    enabled: false
                }
            });

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'approval',
                    type: 'pie'
                },
                title: {
                    verticalAlign: 'middle',
                    floating: true,
                    text: `<span style="font-size: 30px"> ${data[6]} % </span><br><span style="font-size:15px">Approved</span>`,
                },
                plotOptions: {
                    pie: {
                        innerSize: '100%'
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false
                            },
                            inactive: {
                                opacity: 1
                            }
                        }
                    }
                },
                series: [{
                        borderWidth: 0,
                        name: 'Approved',
                        data: [{
                                name: 'Approved',
                                y: data[6],
                                color: '#6CBD45'

                            },
                            {
                                name: "Need Approval And Reject",
                                y: 100 - data[6],
                                color: "#DDF4E4",
                            }
                        ],
                        size: '100%',
                        innerSize: '75%',
                        showInLegend: false,
                        dataLabels: {
                            enabled: false
                        }
                    },
                    {
                        size: '65%',
                        innerSize: '95%',
                        dataLabels: {
                            enabled: false
                        },
                        data: [{

                            // y: 30
                        }, {
                            y: 20,
                            color: 'rgba(0,0,0,0)'
                        }]
                    }
                ],
                credits: {
                    enabled: false
                }
            });

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'rejected',
                    type: 'pie'
                },
                title: {
                    verticalAlign: 'middle',
                    floating: true,
                    text: `<span style="font-size: 30px"> ${data[7]}% </span><br><span style="font-size:15px">Rejected</span>`,
                },
                plotOptions: {
                    pie: {
                        innerSize: '100%'
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false
                            },
                            inactive: {
                                opacity: 1
                            }
                        }
                    }
                },
                series: [{
                    borderWidth: 0,
                    name: 'Rejected',
                    data: [{
                            name: "Rejected",
                            y: data[7],
                            color: '#E5144B'
                        },
                        {
                            name: "Approved And Need Approval",
                            y: 100 - data[7],
                            color: "#DDF4E4",
                        }
                    ],
                    size: '100%',
                    innerSize: '75%',
                    showInLegend: false,
                    dataLabels: {
                        enabled: false
                    }
                }, {
                    size: '65%',
                    innerSize: '95%',
                    dataLabels: {
                        enabled: false
                    },
                    data: [{
                        // y: 30
                    }, {
                        y: 20,
                        color: 'rgba(0,0,0,0)'
                    }]
                }],
                credits: {
                    enabled: false
                }
            });


            <?php foreach ($seksi as $section) : ?>
            Highcharts.chart('<?= $section['seksi'] ?>', {
                title: {
                    text: '<?= $section['seksi'] ?>',
                    align: 'center'
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Training'
                    }
                },
                tooltip: {
                    valueSuffix: 'Training'
                },
                series: [{
                    type: 'column',
                    name: 'Training Target',
                    data: <?= json_encode($dept->TrainingDashboardCountColumnSeksi($section['seksi'], $yearss)) ?>,
                    color: '#0092D5'
                }, {
                    type: 'spline',
                    name: 'Training Implemented',
                    data: 0,
                    color: '#F6164A'

                }]
            });

            Highcharts.chart('man_hour_<?= $section['seksi'] ?>', {
                title: {
                    text: '<?= $section['seksi'] ?>',
                    align: 'center'
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Hour'
                    }
                },
                tooltip: {
                    valueSuffix: ' Hours'
                },
                series: [{
                    type: 'column',
                    name: 'Man Hour',
                    data: <?= json_encode($dept->ManHourTrainingDashboardCountColumnSeksi($section['seksi'], $yearss)) ?>,
                    color: '#02c927'
                }]
            });
            <?php endforeach; ?>





        }
    });
});

function ChangeYear() {
    let year = $('#year').val();
    // $('#years').val(year);

    window.location.replace('<?= base_url() ?>dashboard_department/' + year)


}

function RupiahFormat(number) {
    return Result = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR"
    }).format(number);
}
</script>


<?= $this->endSection() ?>