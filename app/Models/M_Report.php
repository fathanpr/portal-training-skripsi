<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Report extends Model
{
    protected $table      = 'report';
    protected $primaryKey = 'id_report';
    // protected $useAutoIncrement = true;
    protected $allowedFields = ['id_training', 'id_trainer', 'id_handler_report', 'date', 'clock', 'score'];

    public function GetCheckDataReport($id_training, $id_trainer, $date)
    {
        $this->select('id_training', $id_training)->where('id_trainer', $id_trainer)->where('date', $date);
        return  $this->get()->getResultArray();
    }

    public function getDataByTrainingAndDate($id_training, $date)
    {
        $this->select()->where('id_training', $id_training)->where('date', $date);
        return $this->get()->getResultArray();
    }
    public function getTrainerByHandler($id_handler_report)
    {
        $this->select()->where('id_handler_report', $id_handler_report);
        return $this->get()->getResultArray();
    }

    public function getDataByTrainerAndDate($id_trainer, $id_handler)
    {
        $this->select('report.*')->where('id_trainer', $id_trainer)->where('report.id_handler_report', $id_handler);
        $this->join('report_handler', 'report_handler.id_handler_report=report.id_handler_report');
        return $this->get()->getResultArray();
    }


    public function getReportTrainer($id)
    {
        $this->select('training.judul_training,report_handler.date,clock,score,id_trainer')->where('id_trainer', $id);
        $this->join('training', 'training.id_training=report.id_training');
        $this->join('report_handler', 'report_handler.id_handler_report=report.id_handler_report');
        return $this->get()->getResultArray();
    }


    public function getReportList($id)
    {
        $sql = $this->query("select training.judul_training,sum(clock) as clock from report join training on training.id_training=report.id_training where id_trainer = $id group by training.judul_training");

        return $sql->getResultArray();
    }

    public function getReportByHandler($id_handler)
    {
        $this->select()->where('id_handler_report', $id_handler);
        return $this->get()->getResultArray();
    }

    public function getTrainerForPushScore($id_handler, $id_trainer)
    {
        return $this->select()->where('id_handler_report', $id_handler)->where('id_trainer', $id_trainer)->first();
    }
}