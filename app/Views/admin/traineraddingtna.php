<?= $this->extend('/template/template') ?>

<?= $this->section('content') ?>

<div class="card m-1 overflow-auto">
    <div class="card-header">
        <h3 class="card-title"><?= $tittle ?></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body overflow-auto">
        <table class="table table-bordered overflow-auto" id="trainer_select">
            <thead>
                <tr>
                    <th>Training</th>
                    <th>Date Start</th>
                    <th>Date Finish</th>
                    <th>Instruktur 1</th>
                    <th>Instruktur 2</th>
                    <th>Instruktur 3</th>
                    <th>Instruktur 4</th>
                    <th>Instruktur 5</th>
                    <th>Duration (Hour)</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $loop = 0;
                foreach ($training as $Training) :

                    $Trainers = $trainer->getTrainerByIdTraining($Training['id_training']);
                    $hasInstruktur1 = false;
                    foreach ($Trainers as $Trainer) {
                        if ($Trainer['id_trainer'] == $Training['instruktur_1']) {
                            $hasInstruktur1 = true;
                            break;
                        }
                    }
                    //dd($Trainers);
                ?>
                <tr>
                    <td>
                        <input class="form-control" value="<?= $Training['training'] ?>" id="training<?= $loop ?>"
                            name="training<?= $loop ?>" style="width:150px" readonly>
                        <input type="hidden" class="form-control" value="<?= $Training['id_training'] ?>"
                            id="id_training<?= $loop ?>" name="id_training<?= $loop ?>">
                        <input type="hidden" class="form-control" value="<?= $Training['id_handler_report'] ?>"
                            id="handler<?= $loop ?>" name="handler<?= $loop ?>">
                    </td>
                    <td>
                    <input type="date" class="form-control" value="<?= $Training['mulai_training'] ?>"
                            id="date_start<?= $loop ?>" name="date_start<?= $loop ?>" <?= $hasInstruktur1 ? 'readonly' : '' ?>>
                        <input type="hidden" class="form-control" value="<?= $Training['mulai_training'] ?>"
                            id="date_start_old<?= $loop ?>" name="date_start_old<?= $loop ?>">
                    </td>
                    <td>
                    <input type="date" class="form-control" value="<?= $Training['rencana_training'] ?>"
                            id="date_finish<?= $loop ?>" name="date_finish<?= $loop ?>" <?= $hasInstruktur1 ? 'readonly' : '' ?>>
                        <input type="hidden" class="form-control" value="<?= $Training['rencana_training'] ?>"
                            id="date_finish_old<?= $loop ?>" name="date_finish_old<?= $loop ?>">
                    </td>
                    <td>
                        <div class=" form-group">
                        <select class="form-control" id="trainer-instruktur1<?= $loop ?>" style="width:150px" <?= $hasInstruktur1 ? 'disabled' : '' ?>>
                                <option value="NULL">Choose</option>
                                <?php foreach ($Trainers as $Trainer) : ?>
                                <option value="<?= $Trainer['id_trainer'] ?>"
                                    <?= ($Trainer['id_trainer'] == $Training['instruktur_1']) ? 'selected' : '' ?>>
                                    <?= $Trainer['nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <select class="form-control" id="trainer-instruktur2<?= $loop ?>" style="width:150px" <?= $hasInstruktur1 ? 'disabled' : '' ?>>
                                <option value="NULL">Choose</option>
                                <?php foreach ($Trainers as $Trainer) : ?>
                                <option value="<?= $Trainer['id_trainer'] ?>"
                                    <?= ($Trainer['id_trainer'] == $Training['instruktur_2']) ? 'selected' : '' ?>>
                                    <?= $Trainer['nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select class="form-control" id="trainer-instruktur3<?= $loop ?>" style="width:150px" <?= $hasInstruktur1 ? 'disabled' : '' ?>>
                                <option value="NULL">Choose</option>
                                <?php foreach ($Trainers as $Trainer) : ?>
                                <option value="<?= $Trainer['id_trainer'] ?>"
                                    <?= ($Trainer['id_trainer'] == $Training['instruktur_3']) ? 'selected' : '' ?>>
                                    <?= $Trainer['nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select class="form-control" id="trainer-instruktur4<?= $loop ?>" style="width:150px" <?= $hasInstruktur1 ? 'disabled' : '' ?>>
                                <option value="NULL">Choose</option>
                                <?php foreach ($Trainers as $Trainer) : ?>
                                <option value="<?= $Trainer['id_trainer'] ?>"
                                    <?= ($Trainer['id_trainer'] == $Training['instruktur_4']) ? 'selected' : '' ?>>
                                    <?= $Trainer['nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select class="form-control" id="trainer-instruktur5<?= $loop ?>" style="width:150px" <?= $hasInstruktur1 ? 'disabled' : '' ?>>
                                <option value="NULL">Choose</option>
                                <?php foreach ($Trainers as $Trainer) : ?>
                                <option value="<?= $Trainer['id_trainer'] ?>"
                                    <?= ($Trainer['id_trainer'] == $Training['instruktur_5']) ? 'selected' : '' ?>>
                                    <?= $Trainer['nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </td>
                    <td><input class="form-control" value="<?= $Training['duration'] ?>" id="duration<?= $loop ?>" <?= $hasInstruktur1 ? 'disabled' : '' ?>></td>
                    <td>
                        <div class="d-flex">
                        <?php if (!$hasInstruktur1) : ?>
                            <button class="btn btn-success btn-sm" onclick="SaveTrainer(<?= $loop ?>)">
                                <i class="fa-solid fa-floppy-disk"></i>
                            </button>
                        <?php endif; ?>
                            <!-- <button class="btn btn-success btn-sm" onclick="SaveTrainer(<?= $loop ?>)"><i
                                    class="fa-solid fa-floppy-disk"></i></button> -->

                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-sm ml-2" onclick="Report(<?= $loop ?>)">
                                <i class="fa-solid fa-file"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php $loop++;
                endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="Reports" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Report Training</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div id="Report-content"></div>
                    <figure id="score-content">
                        <div id="score-training"></div>
                        <h4>Isi Program</h6>
                            <div id="score-program-kesesuaian"></div>
                            <div id="score-tampilan-materi"></div>
                            <div id="score-program-training"></div>
                            <div id="score-kesesuaian-metode"></div>
                            <div id="score-penambahan-keterampilan"></div>
                            <div id="score-kelayakan"></div>
                            <div id="score-kelayakan-akomodasi"></div>
                            <?php for ($loop = 1; $loop <= 5; $loop++) : ?>
                            <h4 id="instruktur<?= $loop ?>">Instruktur <?= $loop ?></h4>
                            <div id="score-pengetahuan-instruktur<?= $loop ?>"></div>
                            <div id="score-kemampuan-instruktur<?= $loop ?>"></div>
                            <div id="score-kemampuan-melibatkan-instruktur<?= $loop ?>"></div>
                            <div id="score-kemampuan-menanggapi-instruktur<?= $loop ?>"></div>
                            <div id="score-kemampuan-mengendalikan-instruktur<?= $loop ?>"></div>
                            <?php endfor; ?>
                            <div id="harapan-peserta"></div>
                            <div id="peningkatan-instruktur"></div>
                            <h4>Masukan Dan Saran</h6>
                                <div id="wawasan"></div>
                                <div id="score-skill"></div>
                                <div id="score-rekomendasi"></div>
                                <div id="kebutuhan"></div>

                    </figure>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
function SaveTrainer(loop) {
    let id_training = $('#id_training' + loop).val()
    let date_start = $('#date_start' + loop).val()
    let date_finish = $('#date_finish' + loop).val()
    let date_start_old = $('#date_start_old' + loop).val()
    let date_finish_old = $('#date_finish_old' + loop).val()
    let instruktur1 = $('#trainer-instruktur1' + loop).val()
    let instruktur2 = $('#trainer-instruktur2' + loop).val()
    let instruktur3 = $('#trainer-instruktur3' + loop).val()
    let instruktur4 = $('#trainer-instruktur4' + loop).val()
    let instruktur5 = $('#trainer-instruktur5' + loop).val()
    let duration = $('#duration' + loop).val()
    let handler = $('#handler' + loop).val()

     
    if (!date_start || !date_finish || (instruktur1 === "NULL") || !duration) {
        Swal.fire({
            icon: 'error',
            title: 'Data Tidak Boleh Kosong',
            text: 'Pastikan "date start," "date finish," dan "instruktur 1" terisi sebelum melanjutkan.',
        });
        return;
    } else if (instruktur1 === instruktur2) {
        Swal.fire({
            icon: 'error',
            title: 'Instruktur Tidak Boleh Sama!',
            text: 'Silahkan pilih kembali instruktur dengan nama yang berbeda.',
        });
    } else if(isNaN(duration) || duration <= 0){
        Swal.fire({
            icon: 'error',
            title: 'Duration Salah',
            text: 'Isi duration hanya dengan angka dan harus lebih dari 0',
        });
    }  else if (instruktur2 !== "NULL" && parseInt(instruktur2) !== (parseInt(instruktur1) + 1)) {
        return;
    Swal.fire({
        icon: 'error',
        title: 'Instruktur Harus Diisi Berurutan!',
        text: 'Pastikan instruktur diisi secara berurutan dari 1 hingga 5.',
    });
    } else if (instruktur3 !== "NULL" && parseInt(instruktur3) !== (parseInt(instruktur2) + 1)) {
        Swal.fire({
            icon: 'error',
            title: 'Instruktur Harus Diisi Berurutan!',
            text: 'Pastikan instruktur diisi secara berurutan dari 1 hingga 5.',
        });
    } else if (instruktur4 !== "NULL" && parseInt(instruktur4) !== (parseInt(instruktur3) + 1)) {
        Swal.fire({
            icon: 'error',
            title: 'Instruktur Harus Diisi Berurutan!',
            text: 'Pastikan instruktur diisi secara berurutan dari 1 hingga 5.',
        });
    } else if (instruktur5 !== "NULL" && parseInt(instruktur5) !== (parseInt(instruktur4) + 1)) {
        Swal.fire({
            icon: 'error',
            title: 'Instruktur Harus Diisi Berurutan!',
            text: 'Pastikan instruktur diisi secara berurutan dari 1 hingga 5.',
        });
    } else{
        Swal.fire({
            icon: 'success',
            title: 'Trainer berhasil ditentukan',
            text: '',
        });
    $.ajax({
        type: 'post',
        url: "<?= base_url() ?>integrate_trainer",
        async: true,
        dataType: "json",
        data: {
            id_training: id_training,
            date_start: date_start,
            date_finish: date_finish,
            date_start_old: date_start_old,
            date_finish_old: date_finish_old,
            instruktur1: instruktur1,
            instruktur2: instruktur2,
            instruktur3: instruktur3,
            instruktur4: instruktur4,
            instruktur5: instruktur5,
            duration: duration,
            handler: handler
        },
        beforeSend: function() {
            let timerInterval
            Swal.fire({
                title: 'Please Wait',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        },
        success: function(data) {

            //console.info(data)

            if (data == true) {
                Swal.fire({
                    icon: 'error',
                    title: "The trainer's name cannot be the same",
                    text: 'Something went wrong!',
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: "Your Trainer's has been saved",
                    showConfirmButton: false,
                    timer: 1500
                })
                window.location.reload()
            }
        }
    });
}

}

function Report(loop) {
    let id_training = $('#id_training' + loop).val()
    let date_start = $('#date_start' + loop).val()
    $.ajax({
        type: 'post',
        url: "<?= base_url() ?>report_training",
        async: true,
        dataType: "json",
        data: {
            id_training: id_training,
            date_start: date_start,
        },
        success: function(data) {
            jQuery.noConflict()
            $('#Report-content').html('')
            $('#Report-content').append(data[0])

            Highcharts.chart('score-training', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'Score Pelatihan',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[1][0]],
                        ['Puas', data[1][1]],
                        ['Tidak Puas', data[1][2]],
                        ['Sangat Tidak Puas', data[1][3]],
                    ]
                }]
            });

            Highcharts.chart('score-program-kesesuaian', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '1. Kesesuaian isi program terhadap sasaran training',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[2][0]],
                        ['Puas', data[2][1]],
                        ['Tidak Puas', data[2][2]],
                        ['Sangat Tidak Puas', data[2][3]],
                    ]
                }]
            });
            Highcharts.chart('score-tampilan-materi', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '2. Tampilan materi training',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[3][0]],
                        ['Puas', data[3][1]],
                        ['Tidak Puas', data[3][2]],
                        ['Sangat Tidak Puas', data[3][3]],
                    ]
                }]
            });

            Highcharts.chart('score-program-training', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '3. Isi program training',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[4][0]],
                        ['Puas', data[4][1]],
                        ['Tidak Puas', data[4][2]],
                        ['Sangat Tidak Puas', data[4][3]],
                    ]
                }]
            });

            Highcharts.chart('score-kesesuaian-metode', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '4. Kesesuaian metode training dengan topik yang dibahas (penggunaan contoh/latihan/diskusi/studi kasus untuk pemahaman)',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[5][0]],
                        ['Puas', data[5][1]],
                        ['Tidak Puas', data[5][2]],
                        ['Sangat Tidak Puas', data[5][3]],
                    ]
                }]
            });

            Highcharts.chart('score-penambahan-keterampilan', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '5. Penambahan keterampilan/pengetahuan dari program training yang diajarkan',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[6][0]],
                        ['Puas', data[6][1]],
                        ['Tidak Puas', data[6][2]],
                        ['Sangat Tidak Puas', data[6][3]],
                    ]
                }]
            });

            Highcharts.chart('score-kelayakan', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '6. Kelayakan penyajian materi yang diberikan (Audio/visual/audiovisual/peralatan lain)',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[7][0]],
                        ['Puas', data[7][1]],
                        ['Tidak Puas', data[7][2]],
                        ['Sangat Tidak Puas', data[7][3]],
                    ]
                }]
            });



            Highcharts.chart('score-kelayakan-akomodasi', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '7. Kelayakan akomodasi/konsumsi/fasilitas training yang diberikan',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[8][0]],
                        ['Puas', data[8][1]],
                        ['Tidak Puas', data[8][2]],
                        ['Sangat Tidak Puas', data[8][3]],
                    ]
                }]
            });

            $('#harapan-peserta').html(data[9])

            $('#peningkatan-instruktur').html(data[10])
            $('#wawasan').html(data[11])

            Highcharts.chart('score-skill', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'Seberapa banyak anda mendapatkan pengetahuan/skill baru dari training ini ?',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['100', data[12][0]],
                        ['75', data[12][1]],
                        ['50', data[12][2]],
                        ['25', data[12][3]],
                    ]
                }]
            });


            Highcharts.chart('score-rekomendasi', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'Apakah anda akan merekomendasikan training ini kepada rekan kerja yang lain ?',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Ya', data[13][0]],
                        ['Tidak', data[13][1]],
                    ]
                }]
            });

            $('#kebutuhan').html(data[14]);

            Highcharts.chart('score-pengetahuan-instruktur1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '1. Pengetahuan/ pemahaman instruktur terhadap materi pelatihan',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[15][0]],
                        ['Puas', data[15][1]],
                        ['Tidak Puas', data[15][2]],
                        ['Sangat Tidak Puas', data[15][3]],
                    ]
                }]
            });

            // // batas

            Highcharts.chart('score-kemampuan-instruktur1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '2. Kemampuan dalam menjelaskan materi pelatihan',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[16][0]],
                        ['Puas', data[16][1]],
                        ['Tidak Puas', data[16][2]],
                        ['Sangat Tidak Puas', data[16][3]],
                    ]
                }]
            });

            Highcharts.chart('score-kemampuan-melibatkan-instruktur1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '3. Kemampuan melibatkan partisipasi peserta dalam proses belajar',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[17][0]],
                        ['Puas', data[17][1]],
                        ['Tidak Puas', data[17][2]],
                        ['Sangat Tidak Puas', data[17][3]],
                    ]
                }]
            });


            Highcharts.chart('score-kemampuan-menanggapi-instruktur1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '4. Kemampuan menanggapi permasalahan dan pertanyaan peserta',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[18][0]],
                        ['Puas', data[18][1]],
                        ['Tidak Puas', data[18][2]],
                        ['Sangat Tidak Puas', data[18][3]],
                    ]
                }]
            });
            Highcharts.chart('score-kemampuan-mengendalikan-instruktur1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: '5. Kemampuan mengendalikan penggunaan waktu',
                    align: 'left'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Share',
                    data: [
                        ['Sangat Puas', data[19][0]],
                        ['Puas', data[19][1]],
                        ['Tidak Puas', data[19][2]],
                        ['Sangat Tidak Puas', data[19][3]],
                    ]
                }]
            });


            //instruktur 2
            if (data[20][0] == 0 && data[20][1] == 0 && data[20][2] == 0 && data[20][3] == 0) {
                $('#instruktur2').remove()
                $('#score-pengetahuan-instruktur2').remove()
                $('#score-kemampuan-instruktur2').remove()
                $('#score-kemampuan-melibatkan-instruktur2').remove()
                $('#score-kemampuan-menanggapi-instruktur2').remove()
                $('#score-kemampuan-mengendalikan-instruktur2').remove()
            } else {
                Highcharts.chart('score-pengetahuan-instruktur2', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '1. Pengetahuan/ pemahaman instruktur terhadap materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[20][0]],
                            ['Puas', data[20][1]],
                            ['Tidak Puas', data[20][2]],
                            ['Sangat Tidak Puas', data[20][3]],
                        ]
                    }]
                });

                // // batas

                Highcharts.chart('score-kemampuan-instruktur2', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '2. Kemampuan dalam menjelaskan materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[21][0]],
                            ['Puas', data[21][1]],
                            ['Tidak Puas', data[21][2]],
                            ['Sangat Tidak Puas', data[21][3]],
                        ]
                    }]
                });

                Highcharts.chart('score-kemampuan-melibatkan-instruktur2', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '3. Kemampuan melibatkan partisipasi peserta dalam proses belajar',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[22][0]],
                            ['Puas', data[22][1]],
                            ['Tidak Puas', data[22][2]],
                            ['Sangat Tidak Puas', data[22][3]],
                        ]
                    }]
                });


                Highcharts.chart('score-kemampuan-menanggapi-instruktur2', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '4. Kemampuan menanggapi permasalahan dan pertanyaan peserta',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[23][0]],
                            ['Puas', data[23][1]],
                            ['Tidak Puas', data[23][2]],
                            ['Sangat Tidak Puas', data[23][3]],
                        ]
                    }]
                });
                Highcharts.chart('score-kemampuan-mengendalikan-instruktur2', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '5. Kemampuan mengendalikan penggunaan waktu',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[24][0]],
                            ['Puas', data[24][1]],
                            ['Tidak Puas', data[24][2]],
                            ['Sangat Tidak Puas', data[24][3]],
                        ]
                    }]
                });
            }


            //instruktur 3
            if (data[25][0] == 0 && data[25][1] == 0 && data[25][2] == 0 && data[25][3] == 0) {
                $('#instruktur3').remove()
                $('#score-pengetahuan-instruktur3').remove()
                $('#score-kemampuan-instruktur3').remove()
                $('#score-kemampuan-melibatkan-instruktur3').remove()
                $('#score-kemampuan-menanggapi-instruktur3').remove()
                $('#score-kemampuan-mengendalikan-instruktur3').remove()
            } else {
                Highcharts.chart('score-pengetahuan-instruktur3', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '1. Pengetahuan/ pemahaman instruktur terhadap materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[25][0]],
                            ['Puas', data[25][1]],
                            ['Tidak Puas', data[25][2]],
                            ['Sangat Tidak Puas', data[25][3]],
                        ]
                    }]
                });

                // // batas

                Highcharts.chart('score-kemampuan-instruktur3', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '2. Kemampuan dalam menjelaskan materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[26][0]],
                            ['Puas', data[26][1]],
                            ['Tidak Puas', data[26][2]],
                            ['Sangat Tidak Puas', data[26][3]],
                        ]
                    }]
                });

                Highcharts.chart('score-kemampuan-melibatkan-instruktur3', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '3. Kemampuan melibatkan partisipasi peserta dalam proses belajar',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[27][0]],
                            ['Puas', data[27][1]],
                            ['Tidak Puas', data[27][2]],
                            ['Sangat Tidak Puas', data[27][3]],
                        ]
                    }]
                });


                Highcharts.chart('score-kemampuan-menanggapi-instruktur3', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '4. Kemampuan menanggapi permasalahan dan pertanyaan peserta',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[28][0]],
                            ['Puas', data[28][1]],
                            ['Tidak Puas', data[28][2]],
                            ['Sangat Tidak Puas', data[28][3]],
                        ]
                    }]
                });
                Highcharts.chart('score-kemampuan-mengendalikan-instruktur3', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '5. Kemampuan mengendalikan penggunaan waktu',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[29][0]],
                            ['Puas', data[29][1]],
                            ['Tidak Puas', data[29][2]],
                            ['Sangat Tidak Puas', data[29][3]],
                        ]
                    }]
                });
            }

            //instruktur 4
            if (data[30][0] == 0 && data[30][1] == 0 && data[30][2] == 0 && data[30][3] == 0) {
                $('#instruktur4').remove()
                $('#score-pengetahuan-instruktur4').remove()
                $('#score-kemampuan-instruktur4').remove()
                $('#score-kemampuan-melibatkan-instruktur4').remove()
                $('#score-kemampuan-menanggapi-instruktur4').remove()
                $('#score-kemampuan-mengendalikan-instruktur4').remove()
            } else {
                Highcharts.chart('score-pengetahuan-instruktur4', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '1. Pengetahuan/ pemahaman instruktur terhadap materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[30][0]],
                            ['Puas', data[30][1]],
                            ['Tidak Puas', data[30][2]],
                            ['Sangat Tidak Puas', data[30][3]],
                        ]
                    }]
                });

                // // batas

                Highcharts.chart('score-kemampuan-instruktur4', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '2. Kemampuan dalam menjelaskan materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[31][0]],
                            ['Puas', data[31][1]],
                            ['Tidak Puas', data[31][2]],
                            ['Sangat Tidak Puas', data[31][3]],
                        ]
                    }]
                });

                Highcharts.chart('score-kemampuan-melibatkan-instruktur4', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '3. Kemampuan melibatkan partisipasi peserta dalam proses belajar',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[32][0]],
                            ['Puas', data[32][1]],
                            ['Tidak Puas', data[32][2]],
                            ['Sangat Tidak Puas', data[32][3]],
                        ]
                    }]
                });


                Highcharts.chart('score-kemampuan-menanggapi-instruktur4', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '4. Kemampuan menanggapi permasalahan dan pertanyaan peserta',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[33][0]],
                            ['Puas', data[33][1]],
                            ['Tidak Puas', data[33][2]],
                            ['Sangat Tidak Puas', data[33][3]],
                        ]
                    }]
                });
                Highcharts.chart('score-kemampuan-mengendalikan-instruktur4', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '5. Kemampuan mengendalikan penggunaan waktu',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[34][0]],
                            ['Puas', data[34][1]],
                            ['Tidak Puas', data[34][2]],
                            ['Sangat Tidak Puas', data[34][3]],
                        ]
                    }]
                });
            }

            //instruktur 5
            if (data[35][0] == 0 && data[35][1] == 0 && data[35][2] == 0 && data[35][3] == 0) {
                $('#instruktur5').remove()
                $('#score-pengetahuan-instruktur5').remove()
                $('#score-kemampuan-instruktur5').remove()
                $('#score-kemampuan-melibatkan-instruktur5').remove()
                $('#score-kemampuan-menanggapi-instruktur5').remove()
                $('#score-kemampuan-mengendalikan-instruktur5').remove()
            } else {
                Highcharts.chart('score-pengetahuan-instruktur5', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '1. Pengetahuan/ pemahaman instruktur terhadap materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[35][0]],
                            ['Puas', data[35][1]],
                            ['Tidak Puas', data[35][2]],
                            ['Sangat Tidak Puas', data[35][3]],
                        ]
                    }]
                });

                // // batas

                Highcharts.chart('score-kemampuan-instruktur5', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '2. Kemampuan dalam menjelaskan materi pelatihan',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[36][0]],
                            ['Puas', data[36][1]],
                            ['Tidak Puas', data[36][2]],
                            ['Sangat Tidak Puas', data[36][3]],
                        ]
                    }]
                });

                Highcharts.chart('score-kemampuan-melibatkan-instruktur5', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '3. Kemampuan melibatkan partisipasi peserta dalam proses belajar',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[37][0]],
                            ['Puas', data[37][1]],
                            ['Tidak Puas', data[37][2]],
                            ['Sangat Tidak Puas', data[37][3]],
                        ]
                    }]
                });


                Highcharts.chart('score-kemampuan-menanggapi-instruktur5', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '4. Kemampuan menanggapi permasalahan dan pertanyaan peserta',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[38][0]],
                            ['Puas', data[38][1]],
                            ['Tidak Puas', data[38][2]],
                            ['Sangat Tidak Puas', data[38][3]],
                        ]
                    }]
                });
                Highcharts.chart('score-kemampuan-mengendalikan-instruktur5', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '5. Kemampuan mengendalikan penggunaan waktu',
                        align: 'left'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Share',
                        data: [
                            ['Sangat Puas', data[39][0]],
                            ['Puas', data[39][1]],
                            ['Tidak Puas', data[39][2]],
                            ['Sangat Tidak Puas', data[39][3]],
                        ]
                    }]
                });
            }

            $('#Reports').modal('show')


            console.info(data)

        }

        //  console.info(data)

    });
}

$(document).ready( function () {
    $('#trainer_select').DataTable();
} );
</script>
<?= $this->endSection() ?>