<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();

$routes->get('/test', 'Test::test1');
$routes->get('/edu', 'Admin\C_User::addIdEducation');

//login
$routes->get('/', 'Login::index', ['filter' => 'NAuth']);
$routes->get('/pilih_akun/(:num)', 'Login::pilih_akun/$1', ['filter' => 'Auth']);
$routes->get('/change_password', 'Login::ganti_password', ['filter' => 'NAuth']);
$routes->post('/submit_ganti_password', 'Login::submit_change_password', ['filter' => 'NAuth']);
$routes->post('/verify', 'Login::validation', ['filter' => 'NAuth']);
$routes->get('/logout', 'Login::logout', ['filter' => 'Auth']);


//dashboard
// $routes->get('/dashboard/(:num)', 'Admin\C_Dashboard::index/$1', ['filter' => 'Auth']);
$routes->get('/dashboard/(:num)', 'Admin\C_Dashboard::index/$1', ['filter' => 'Admin']);
$routes->get('/dashboard_bod/(:num)', 'Admin\C_Dashboard::dashboard_bod/$1', ['filter' => 'Bod']);

$routes->get('/dashboard_department/(:num)', 'Admin\C_Dashboard::dashboard_department/$1', ['filter' => 'Auth']);
$routes->get('/dashboard_divisi/(:num)', 'Admin\C_Dashboard::dashboard_divisi/$1', ['filter' => 'Auth']);
$routes->post('/dashboard_year', 'Admin\C_Dashboard::DashboardYear', ['filter' => 'Auth']);
$routes->post('/dashboard_year_department', 'Admin\C_Dashboard::DashboardYearDepartment', ['filter' => 'Auth']);
$routes->post('/dashboard_year_divisi', 'Admin\C_Dashboard::DashboardYearDivisi', ['filter' => 'Auth']);
$routes->post('/report_training', 'Admin\C_Trainer::ReportTraining', ['filter' => 'Auth']);


//Home
$routes->get('/home', 'Home::index', ['filter' => 'Admin']);
$routes->post('/search', 'Home::search', ['filter' => 'Auth']);


//competency
$routes->get('/list_astra', 'Admin\C_Competency::astra', ['filter' => 'Admin']);
$routes->post('/edit_competency_astra', 'Admin\C_Competency::EditAstra', ['filter' => 'Auth']);
$routes->delete('/delete/astra/(:num)', 'Admin\C_Competency::DeleteAstra/$1', ['filter' => 'Auth']);


$routes->get('/list_technicalA', 'Admin\C_Competency::technicalA', ['filter' => 'Admin']);
$routes->get('/list_technicalB', 'Admin\C_Competency::technicalB', ['filter' => 'Admin']);
$routes->post('/save_technical', 'Admin\C_Competency::SaveTechnical', ['filter' => 'Admin']);
$routes->delete('/delete/technical/(:num)', 'Admin\C_Competency::DeleteTechnical/$1', ['filter' => 'Admin']);
$routes->get('/technical_departemen/(:any)/(:any)', 'Admin\C_Competency::DetailTechnical/$1/$2', ['filter' => 'Admin']);
$routes->post('/input_technical_file', 'Admin\C_CompetencyTechnical::InputExcel', ['filter' => 'Admin']);
$routes->get('/list_expert', 'Admin\C_Competency::Expert', ['filter' => 'Admin']);
$routes->post('/expert_file', 'Admin\C_CompetencyExpert::InputExcel', ['filter' => 'Admin']);
$routes->post('/detail_expert_checked', 'Admin\C_CompetencyExpert::CheckedExpert', ['filter' => 'Admin']);
$routes->post('/edit_competency_expert', 'Admin\C_Competency::EditExpert', ['filter' => 'Admin']);
$routes->delete('/delete/expert/(:num)', 'Admin\C_Competency::DeleteExpert/$1', ['filter' => 'Admin']);

//soft Competency
$routes->get('/list_soft', 'Admin\C_CompetencySoft::index', ['filter' => 'Admin']);
$routes->post('/soft_file', 'Admin\C_CompetencySoft::InputExcel', ['filter' => 'Admin']);
$routes->post('/edit_competency_soft', 'Admin\C_CompetencySoft::EditSoft', ['filter' => 'Admin']);
$routes->delete('/delete/soft/(:num)', 'Admin\C_CompetencySoft::Delete/$1', ['filter' => 'Admin']);

//Company Competency
$routes->get('/list_company', 'Admin\C_CompetencyCompany::index', ['filter' => 'Admin']);
$routes->post('/input_company_file', 'Admin\C_CompetencyCompany::InputExcel', ['filter' => 'Admin']);
$routes->get('/company_division/(:any)', 'Admin\C_CompetencyCompany::DetailCompany/$1', ['filter' => 'Admin']);
$routes->post('/save_company', 'Admin\C_CompetencyCompany::EditCompany', ['filter' => 'Admin']);
$routes->delete('/delete/company/(:num)', 'Admin\C_CompetencyCompany::Delete/$1', ['filter' => 'Admin']);

//Technical B Competency
$routes->post('/multiple_input_technicalB', 'Admin\C_CompetencyTechnicalB::InputExcel', ['filter' => 'Admin']);
$routes->get('/technical_departemen/(:any)', 'Admin\C_CompetencyTechnicalB::DetailTechnical/$1', ['filter' => 'Admin']);
$routes->post('/save_single_technicalB', 'Admin\C_CompetencyTechnicalB::SaveSingleTechnical', ['filter' => 'Admin']);
$routes->delete('/delete_technicalB/(:any)/(:any)', 'Admin\C_CompetencyTechnicalB::delete/$1/$2', ['filter' => 'Admin']);
$routes->post('/jabatan_user', 'Admin\C_Competency::getJabatan', ['filter' => 'Admin']);
$routes->post('/edit_technicalB', 'Admin\C_CompetencyTechnicalB::EditTechnicalB', ['filter' => 'Admin']);

//Copmpetency User
$routes->post('/competency_user', 'Admin\C_User::EditCompetencyUser', ['filter' => 'Admin']);
$routes->post('/change_competency', 'Admin\C_User::ChangeCompetency', ['filter' => 'Admin']);

//categories


//list training
$routes->get('/list_training', 'Admin\C_ListTraining::index', ['filter' => 'Admin']);
$routes->get('/non_training', 'Admin\C_ListTraining::nonTraining', ['filter' => 'Admin']);
$routes->get('/detail/(:any)', 'Admin\C_ListTraining::detail/$1', ['filter' => 'Admin']);
$routes->post('/addCategory', 'Admin\C_ListTraining::addCategory', ['filter' => 'Admin']);
$routes->post('/import', 'Admin\C_ListTraining::import', ['filter' => 'Admin']);
$routes->post('/import2', 'Admin\C_ListTraining::import2', ['filter' => 'Admin']);
$routes->delete('/delete/(:num)', 'Admin\C_ListTraining::delete/$1', ['filter' => 'Admin']);
$routes->get('/update/(:num)', 'Admin\C_ListTraining::update/$1', ['filter' => 'Admin']);
$routes->post('/edit/(:num)', 'Admin\C_ListTraining::edit/$1', ['filter' => 'Admin']);
$routes->post('/add_category', 'Admin\C_ListTraining::singleAddCategory', ['filter' => 'Admin']);
$routes->post('/save_training', 'Admin\C_ListTraining::saveSingleTraining', ['filter' => 'Admin']);
$routes->post('/delete_all', 'Admin\C_ListTraining::deleteAllTraining', ['filter' => 'Admin']);
$routes->post('/delete_training', 'Admin\C_ListTraining::deleteTraining', ['filter' => 'Admin']);
$routes->post('/edit_training', 'Admin\C_ListTraining::editTraining', ['filter' => 'Admin']);

$routes->post('/material_upload', 'Admin\C_ListTraining::MaterialUpload', ['filter' => 'Admin']);



//User Admin
$routes->get('/user', 'Admin\C_User::index', ['filter' => 'Admin']);
$routes->get('/sm_astra', 'Admin\C_User::AstraSkillMap', ['filter' => 'Admin']);
$routes->get('/sm_expert', 'Admin\C_User::ExpertSkillMap', ['filter' => 'Admin']);
$routes->get('/sm_soft', 'Admin\C_User::SoftSkillMap', ['filter' => 'Admin']);
$routes->get('/sm_technical/(:any)', 'Admin\C_User::TechnicalSkillMap/$1', ['filter' => 'Admin']);
$routes->get('/sm_technicalB/(:any)', 'Admin\C_User::TechnicalBSkillMap/$1', ['filter' => 'Admin']);
$routes->get('/sm_company/(:any)', 'Admin\C_User::CompanySkillMap/$1', ['filter' => 'Admin']);
$routes->post('/addUser', 'Admin\C_User::addUser', ['filter' => 'Admin']);
$routes->post('/divisibydic', 'Admin\C_User::getDivisiByDic', ['filter' => 'Admin']);
$routes->post('/departemenbydivisi', 'Admin\C_User::getDepartemenByDivisi', ['filter' => 'Admin']);
$routes->post('/seksibydepartemen', 'Admin\C_User::getSeksiByDepartemen', ['filter' => 'Admin']);
$routes->post('/bagianbyseksi', 'Admin\C_User::getBagianBySeksi', ['filter' => 'Admin']);
$routes->delete('/delete/user/(:num)', 'Admin\C_User::delete/$1', ['filter' => 'Admin']);
$routes->post('/update_user', 'Admin\C_User::update', ['filter' => 'Admin']);
// $routes->post('/edit_user/(:num)', 'Admin\C_User::edit/$1', ['filter' => 'Auth']);
$routes->post('/save_user', 'Admin\C_User::singleUser', ['filter' => 'Admin']);
$routes->post('/add_education', 'Admin\C_User::AddEducation', ['filter' => 'Admin']);
$routes->post('/add_career', 'Admin\C_User::AddCareer', ['filter' => 'Admin']);
$routes->post('/get_education', 'Admin\C_User::getEducation', ['filter' => 'Admin']);
$routes->post('/edit_user', 'Admin\C_User::EditUser', ['filter' => 'Admin']);
$routes->get('/edit_user', 'Admin\C_User::EditUser', ['filter' => 'Admin']);
$routes->post('/delete_education', 'Admin\C_User::deleteEducation', ['filter' => 'Admin']);
$routes->post('/delete_career', 'Admin\C_User::deleteCareer', ['filter' => 'Admin']);

//Evaluasi Admin
$routes->get('/evaluasi_reaksi_admin', 'User\Evaluasi::EvaluasiReaksiAdmin', ['filter' => 'Admin']);
$routes->post('/detail_evaluasi_reaksi_admin', 'User\Evaluasi::DetailEvaluasiReaksiAdmin', ['filter' => 'Admin']);
$routes->get('/evaluasi_efektivitas_admin', 'User\Evaluasi::EvaluasiEfektivitasAdmin', ['filter' => 'Admin']);
$routes->post('/detail_evaluasi_efektivitas_admin', 'User\Evaluasi::DetailEvaluasiEfektivitasAdmin', ['filter' => 'Admin']);


//Form TNA ADMIN
$routes->get('/tna', 'Admin\C_Tna::index', ['filter' => 'Auth']);
$routes->get('/training_monthly', 'Admin\C_Tna::trainingMonthly', ['filter' => 'Auth']);
$routes->get('/export_training_monthly/(:num)', 'Admin\C_Tna::exportTrainingMonthly/$1', ['filter' => 'Auth']);
$routes->get('/export_training_monthly_other/(:num)/(:num)', 'Admin\C_Tna::exportTrainingMonthlyOther/$1/$2', ['filter' => 'Auth']);
$routes->get('/export_training_monthly_unplanned/(:num)/(:num)', 'Admin\C_TnaUnplanned::exportTrainingMonthly/$1/$2', ['filter' => 'Auth']);
$routes->get('/other_monthly', 'Admin\C_Tna::otherMonthly', ['filter' => 'Auth']);
$routes->post('/accept_admin', 'Admin\C_Tna::accept', ['filter' => 'Auth']);
$routes->post('/change_training', 'Admin\C_Tna::change', ['filter' => 'Auth']);
$routes->post('/delete_training_Reject', 'Admin\C_Tna::DeleteTrainingReject', ['filter' => 'Auth']);


// $routes->get('/accept_admin/(:num)', 'Admin\C_Tna::accept/$1');
$routes->post('/reject_admin', 'Admin\C_Tna::reject', ['filter' => 'Admin']);
$routes->get('/kadiv_status', 'Admin\C_Tna::kadivStatus', ['filter' => 'Admin']);
$routes->get('/kadiv_status/(:num)', 'Admin\C_Tna::kadivStatusFilter/$1', ['filter' => 'Admin']);
$routes->get('/kadept_status', 'Admin\C_Tna::kadeptStatus', ['filter' => 'Admin']);
$routes->get('/kadept_status/(:num)', 'Admin\C_Tna::kadeptFilter/$1', ['filter' => 'Admin']);
$routes->get('/bod_status', 'Admin\C_Tna::bodStatus', ['filter' => 'Admin']);
$routes->get('/bod_status/(:num)', 'Admin\C_Tna::bodStatusFilter/$1', ['filter' => 'Admin']);
$routes->get('/kadiv_accept/(:any)', 'Admin\C_Tna::kadivAccept/$1', ['filter' => 'Admin']);
$routes->get('/kadiv_accept_other/(:any)/(:any)', 'Admin\C_Tna::kadivAcceptOther/$1/$2', ['filter' => 'Admin']);
$routes->post('/accept_adminfixed', 'Admin\C_Tna::acceptAdmin', ['filter' => 'Auth']);
$routes->post('/view_attachment', 'Admin\C_Tna::ViewAttachment', ['filter' => 'Admin']);
$routes->post('/reject_adminfixed', 'Admin\C_Tna::rejectAdmin', ['filter' => 'Auth']);
$routes->post('/reject_admin_first', 'Admin\C_Tna::rejectAdminFirst', ['filter' => 'Auth']);
$routes->post('/detail_tna', 'Admin\C_Tna::detailTna', ['filter' => 'Auth']);
$routes->post('/detail_reject', 'Admin\C_Tna::detailReject', ['filter' => 'Auth']);
$routes->get('/detail_reject', 'Admin\C_Tna::detailReject', ['filter' => 'Auth']);
$routes->get('/training_ditolak', 'Admin\C_Tna::TrainingDitolak', ['filter' => 'Auth']);
$routes->get('/atmp_report', 'Admin\C_Tna::AtmpReport', ['filter' => 'Admin']);
$routes->get('/download_atmp_report', 'Admin\C_Tna::DownloadAtmpReport', ['filter' => 'Auth']);
$routes->get('/download_atmp_submit', 'Admin\C_Tna::DownloadAtmpSubmit', ['filter' => 'Auth']);
$routes->get('/download_trainer_report', 'Admin\C_Tna::DownloadTrainerReport', ['filter' => 'Auth']);
$routes->get('/atmp_submit', 'Admin\C_Tna::AtmpSubmit', ['filter' => 'Auth']);
$routes->get('/trainer_report', 'Admin\C_Tna::TrainerReport', ['filter' => 'Admin']);
$routes->post('/trainer_detail_score', 'Admin\C_Tna::TrainerDetailScore', ['filter' => 'Auth']);
$routes->get('/training_not_implemented', 'Admin\C_Tna::TrainingNotImplemented', ['filter' => 'Auth']);
$routes->get('/form_unplanned_admin', 'Admin\C_Tna::FormUnplannedAdmin', ['filter' => 'Admin']);
$routes->get('/form_cross_budget', 'Admin\C_Tna::FormCrossBudget', ['filter' => 'Admin']);
$routes->get('/form_amdi_aop', 'Admin\C_Tna::FormAmdiAop', ['filter' => 'Admin']);
$routes->get('/get_user_data', 'Admin\C_Tna::getUserBySearch', ['filter' => 'Auth']);
$routes->post('/get_competency_by_id_user', 'Admin\C_Tna::getCompetencyByIdUser', ['filter' => 'Auth']);
$routes->get('/get_competency_data', 'Admin\C_Tna::getCompetencyBySearch', ['filter' => 'Auth']);
$routes->post('/save_form_unplanned_admin', 'Admin\C_Tna::UnplannedAdminFormSave', ['filter' => 'Auth']);
$routes->post('/save_form_cross_budget', 'Admin\C_Tna::CrossBudgetFormSave', ['filter' => 'Auth']);
$routes->post('/save_form_amdi_aop', 'Admin\C_Tna::AmdiAopFormSave', ['filter' => 'Auth']);

//schedule training
$routes->get('/schedule_training', 'Admin\C_Schedule::index', ['filter' => 'Admin']);
$routes->get('/schedule_other_training', 'Admin\C_Schedule::otherSchedule', ['filter' => 'Admin']);
// $routes->get('/schedule_action/(:num)', 'Admin\C_Schedule::askForEvaluation/$1', ['filter' => 'Auth']);
$routes->post('/schedule_action/(:num)', 'Admin\C_Schedule::askForEvaluation/$1', ['filter' => 'Auth']);
$routes->post('/schedule_not_implemented', 'Admin\C_Schedule::NotImplemented', ['filter' => 'Auth']);

//history
$routes->get('/history', 'Admin\C_History::index', ['filter' => 'Auth']);
$routes->post('/detail_history', 'Admin\C_History::DetailHistory', ['filter' => 'Auth']);
$routes->post('/sertifikat_upload', 'Admin\C_History::SertifikatUpload', ['filter' => 'Admin']);
$routes->post('/upload_history', 'Admin\C_History::UploadHistory', ['filter' => 'Admin']);
$routes->post('/edit_data_history', 'Admin\C_History::GetDataEditHistory', ['filter' => 'Admin']);


//contac admin
$routes->get('/massage_user', 'Admin\C_Contact::index', ['filter' => 'Auth']);
$routes->post('/delete_contact', 'Admin\C_Contact::delete', ['filter' => 'Auth']);


//Unplanned Training
$routes->get('/tna_unplanned', 'Admin\C_TnaUnplanned::index', ['filter' => 'Admin']);
$routes->get('/status_unplanned', 'Admin\C_TnaUnplanned::kadivStatusUnplanned', ['filter' => 'Admin']);
$routes->get('/status_unplanned/(:num)', 'Admin\C_TnaUnplanned::kadivStatusUnplannedFilter/$1', ['filter' => 'Admin']);
$routes->get('/bod_status_unplanned', 'Admin\C_TnaUnplanned::bodStatusUnplanned', ['filter' => 'Admin']);
$routes->get('/bod_status_unplanned/(:num)', 'Admin\C_TnaUnplanned::bodStatusUnplannedFilter/$1', ['filter' => 'Admin']);
$routes->get('/unplanned_monthly', 'Admin\C_TnaUnplanned::unplannedMonthly', ['filter' => 'Admin']);
$routes->get('/kadiv_accept_unplanned/(:any)/(:any)', 'Admin\C_TnaUnplanned::kadivAccept/$1/$2', ['filter' => 'Admin']);

//unplanned Schedule admin
$routes->get('/schedule_unplanned', 'Admin\C_Schedule::unplannedSchedule', ['filter' => 'Admin']);
$routes->get('/schedule_action_unplanned/(:num)', 'Admin\C_Schedule::askForEvaluationUnplanned/$1', ['filter' => 'Admin']);


//unplanned History Admin
$routes->get('/history_unplanned', 'Admin\C_HistoryUnplanned::index', ['filter' => 'Auth']);
$routes->post('/detail_historyunplan_admin', 'Admin\C_HistoryUnplanned::DetailHistory', ['filter' => 'Auth']);
$routes->post('/sertifikat_upload_unplanned', 'Admin\C_HistoryUnplanned::SertifikatUpload', ['filter' => 'Auth']);


//Competency Astra
$routes->post('/astra_file', 'Admin\C_CompetencyAstra::InputExcel', ['filter' => 'Auth']);
$routes->post('/detail_competency_checked', 'Admin\C_CompetencyAstra::CheckedAstra', ['filter' => 'Auth']);


//Budget
$routes->get('/budget', 'Admin\C_Budget::index', ['filter' => 'Admin']);
$routes->post('/save_budget', 'Admin\C_Budget::SaveBudget', ['filter' => 'Admin']);

//Database Department
$routes->get('/database_department', 'Admin\C_Department::index', ['filter' => 'Admin']);
$routes->post('/change_structure', 'Admin\C_Department::ChangeStructure', ['filter' => 'Auth']);
$routes->post('/new_department', 'Admin\C_Department::NewDepartment', ['filter' => 'Auth']);
$routes->post('/update_department', 'Admin\C_Department::UpdateNameDepartment', ['filter' => 'Auth']);


//Trainer 

$routes->get('/trainer', 'Admin\C_Trainer::index', ['filter' => 'Admin']);
$routes->post('/trainer_type', 'Admin\C_Trainer::Type', ['filter' => 'Auth']);
$routes->post('/single_save_trainer', 'Admin\C_Trainer::SingleSave', ['filter' => 'Auth']);
$routes->post('/trainer_material_adding', 'Admin\C_Trainer::Material', ['filter' => 'Auth']);
$routes->post('/data_trainer_profile', 'Admin\C_Trainer::getDataTrainerProfile', ['filter' => 'Auth']);
$routes->post('/adding_training', 'Admin\C_Trainer::AddingTraining', ['filter' => 'Auth']);
$routes->post('/delete_material', 'Admin\C_Trainer::DeleteMaterial', ['filter' => 'Auth']);
$routes->post('/multiple_trainer', 'Admin\C_Trainer::SaveMultipleTrainer', ['filter' => 'Auth']);
$routes->get('/Trainer_Selected', 'Admin\C_Trainer::TrainerSelect', ['filter' => 'Auth']);
$routes->get('/detail_trainer/(:num)/(:num)', 'Admin\C_Trainer::DetailTrainerForTna/$1/$2', ['filter' => 'Auth']);
$routes->post('/integrate_trainer', 'Admin\C_Trainer::IntegrateTrainer', ['filter' => 'Auth']);
$routes->post('/change_since', 'Admin\C_Trainer::ChangeSince', ['filter' => 'Auth']);
$routes->post('/delete_trainer', 'Admin\C_Trainer::DeleteTrainer', ['filter' => 'Auth']);


//USER

//profile
$routes->get('/user_profile', 'User\User::index', ['filter' => 'Auth']);
$routes->post('/change_profile', 'User\User::UpdateProfile', ['filter' => 'Auth']);
$routes->post('/competency_profile', 'User\User::CompetencyProfile', ['filter' => 'Auth']);


//Home User
$routes->get('/home_user', 'User\Home::index', ['filter' => 'Auth']);
$routes->post('/data_home', 'User\Home::DataHome', ['filter' => 'Auth']);
$routes->get('/data_home', 'User\Home::DataHome', ['filter' => 'Auth']);
$routes->post('/jadwal', 'User\Home::JadwalHome', ['filter' => 'Auth']);
$routes->post('/get_registrans/(:any)/(:any)/(:any)', 'User\Home::getRegistrans/$1/$2/$3', ['filter' => 'Auth']);

$routes->post('/jadwal/(:any)', 'User\Home::JadwalHome/$1', ['filter' => 'Auth']);
$routes->post('/modal_member', 'User\Home::MemberModal', ['filter' => 'Auth']);

//Competency User
$routes->get('/member_competency', 'User\Competency::index', ['filter' => 'Auth']);
$routes->post('/detail_competency', 'User\Competency::MemberProfile', ['filter' => 'Auth']);


//Training List
$routes->get('/list_training_user', 'User\ListTraining::index', ['filter' => 'Auth']);
$routes->get('/detail_user/(:any)', 'User\ListTraining::detail/$1', ['filter' => 'Auth']);
$routes->get('/non_training_user', 'User\ListTraining::nonTrainingUser', ['filter' => 'Auth']);

//TNA USER
$routes->get('/data_member', 'User\FormTna::index', ['filter' => 'Auth']);
$routes->post('/form_tna', 'User\FormTna::TnaUser', ['filter' => 'Auth']);
$routes->post('/multi_tna', 'User\FormTna::MultiTna', ['filter' => 'Auth']);
$routes->post('/multi_unplanned', 'User\FormTna::MultiUnplanned', ['filter' => 'Auth']);
$routes->post('/training_submitted', 'User\FormTna::trainingSubmitted', ['filter' => 'Auth']);
$routes->get('/form_tna', 'User\FormTna::TnaUser', ['filter' => 'Auth']);
$routes->get('/User/FormTna', 'User\FormTna::AjaxTna', ['filter' => 'Auth']);
$routes->post('/User/FormTna', 'User\FormTna::AjaxTna', ['filter' => 'Auth']);
$routes->post('/save_form', 'User\FormTna::TnaForm', ['filter' => 'Auth']);
$routes->post('/save_form_multi', 'User\FormTna::TnaFormMulti', ['filter' => 'Auth']);
$routes->post('/tna/send', 'User\FormTna::TnaSend', ['filter' => 'Auth']);
$routes->get('/status_tna', 'User\FormTna::status', ['filter' => 'Auth']);
$routes->get('/status_tna/(:num)', 'User\FormTna::statusFilter/$1', ['filter' => 'Auth']);
$routes->get('/status_tna_personal', 'User\FormTna::statusPersonal', ['filter' => 'Auth']);
$routes->get('/status_tna_personal/(:num)', 'User\FormTna::statusPersonalFilter/$1', ['filter' => 'Auth']);
$routes->get('/status_unplanned_personal', 'User\FormTna::statusPersonalUnplanned', ['filter' => 'Auth']);
$routes->get('/request_tna', 'User\FormTna::requestTna', ['filter' => ['Auth','Role']]);
$routes->post('/accept_kadiv', 'User\FormTna::acceptKadiv', ['filter' => 'Auth']);
$routes->post('/cektraining', 'User\FormTna::cekTraining', ['filter' => 'Auth']);
$routes->post('/reject_kadiv', 'User\FormTna::rejectKadiv', ['filter' => 'Auth']);
$routes->post('/accept_bod', 'User\FormTna::acceptBod', ['filter' => 'Auth']);
$routes->post('/reject_bod', 'User\FormTna::rejectBod', ['filter' => 'Auth']);
$routes->post('/delete_training_user', 'User\FormTna::DeleteTrainingUser', ['filter' => 'Auth']);

//Cross Budget
$routes->get('/request_cross_budget', 'User\FormTna::CrossBudget', ['filter' => ['Auth','Role']]);

//Our Schedule User
$routes->get('/member_schedule', 'User\OurSchedule::member', ['filter' => 'Auth']);
$routes->get('/personal_schedule', 'User\OurSchedule::personal', ['filter' => 'Auth']);


//Evaluasi
$routes->get('/evaluasi_reaksi', 'User\Evaluasi::index', ['filter' => 'Auth']);
$routes->get('/evaluasi_reaksi_other', 'User\Evaluasi::other', ['filter' => 'Auth']);
$routes->get('/form_evaluasi/(:num)', 'User\Evaluasi::EvaluasiForm/$1', ['filter' => 'Auth']);
$routes->post('/send_evaluasi_reaksi', 'User\Evaluasi::SendEvaluasiReaksi', ['filter' => 'Auth']);
$routes->get('/form_evaluasi_selesai/(:num)', 'User\Evaluasi::DetailEvaluasiReaksi/$1', ['filter' => 'Auth']);
$routes->post('/data_evaluasi', 'User\Evaluasi::DataEvaluasi', ['filter' => 'Auth']);
$routes->get('/evaluasi_reaksi_member', 'User\Evaluasi::EvaluasiMember', ['filter' => 'Auth']);
$routes->get('/evaluasi_reaksi_member_other', 'User\Evaluasi::EvaluasiMemberOther', ['filter' => 'Auth']);
$routes->post('/detail_evaluasi_member', 'User\Evaluasi::detailEvaluasiMember', ['filter' => 'Auth']);
$routes->post('/detail_evaluasi_member_other', 'User\Evaluasi::detailEvaluasiMemberOther', ['filter' => 'Auth']);
$routes->get('/evaluasi_efektifitas', 'User\EvaluasiEfektifitas::index', ['filter' => 'Auth']);
$routes->get('/evaluasi_efektifitas_other', 'User\EvaluasiEfektifitas::other', ['filter' => 'Auth']);
$routes->get('/form_efektivitas/(:num)', 'User\EvaluasiEfektifitas::formEvaluasi/$1', ['filter' => 'Auth']);
$routes->get('/form_efektivitas_other/(:num)', 'User\EvaluasiEfektifitas::formEvaluasiOther/$1', ['filter' => 'Auth']);
$routes->post('/save_efektivitas', 'User\EvaluasiEfektifitas::saveEfektivitas', ['filter' => 'Auth']);
$routes->get('/detail_efektivitas/(:num)', 'User\EvaluasiEfektifitas::DetailEfektivitas/$1', ['filter' => 'Auth']);
$routes->post('/data_evaluasiEfektivitas', 'User\EvaluasiEfektifitas::DataEvaluasiEfektivitas', ['filter' => 'Auth']);

$routes->get('/send_email', 'User\EvaluasiEfektifitas::sendEmail', ['filter' => 'Auth']);
$routes->cli('/send_email', 'User\EvaluasiEfektifitas::sendEmail');
$routes->get('/evaluasi_efektifitas_personal', 'User\EvaluasiEfektifitas::PersonalEvaluasi');
$routes->get('/evaluasi_efektifitas_other_personal', 'User\EvaluasiEfektifitas::OtherPersonalEvaluasi');
$routes->get('/evaluasi_efektifitas_unplanned_personal', 'User\EvaluasiEfektivitasUnplanned::PersonalEvaluasiUnplanned');

//history
$routes->get('/personal_history', 'User\History::index', ['filter' => 'Auth']);
$routes->get('/member_history', 'User\History::memberHistory', ['filter' => 'Auth']);
$routes->get('/download_sertifikat/(:any)', 'User\History::download/$1', ['filter' => 'Auth']);
$routes->post('/detail_history_member', 'User\History::detailHistoryMember', ['filter' => 'Auth']);

//unplanned trainining
$routes->get('/data_member_unplanned', 'User\UnplannedTraining::index', ['filter' => 'Auth']);
$routes->post('/form_unplanned', 'User\UnplannedTraining::TnaUserUnplanned', ['filter' => 'Auth']);
$routes->post('/send_unplanned', 'User\FormTna::TnaSend', ['filter' => 'Auth']);
$routes->get('/request_unplanned', 'User\UnplannedTraining::requestUnplanned', ['filter' => ['Auth','Role']]);
$routes->get('/status_tna_unplanned', 'User\UnplannedTraining::status', ['filter' => 'Auth']);
$routes->post('/unplanned', 'User\UnplannedTraining::Unplanned', ['filter' => 'Auth']);
$routes->post('/unplanned_save', 'User\UnplannedTraining::UnplannedSave', ['filter' => 'Auth']);
$routes->post('/data_training', 'User\UnplannedTraining::DataTraining', ['filter' => 'Auth']);

//unplanned schedule
$routes->get('/personal_schedule_unplanned', 'User\OurSchedule::personal', ['filter' => 'Auth']);
$routes->get('/member_schedule_unplanned', 'User\OurSchedule::member', ['filter' => 'Auth']);

//Unplanned Evaluation
$routes->get('/evaluasi_reaksi_unplanned', 'User\EvaluasiUnplanned::index', ['filter' => 'Auth']);
$routes->get('/form_evaluasi_unplanned/(:num)', 'User\EvaluasiUnplanned::EvaluasiForm/$1', ['filter' => 'Auth']);
$routes->get('/form_unplanned_selesai/(:num)', 'User\EvaluasiUnplanned::DetailEvaluasiReaksi/$1', ['filter' => 'Auth']);
$routes->get('/evaluasi_efektifitas_unplanned', 'User\EvaluasiEfektivitasUnplanned::index', ['filter' => 'Auth']);
$routes->get('/form_efektivitas_unplanned/(:num)', 'User\EvaluasiEfektivitasUnplanned::formEvaluasi/$1', ['filter' => 'Auth']);
$routes->post('/save_efektivitas_unplanned', 'User\EvaluasiEfektivitasUnplanned::saveEfektivitas', ['filter' => 'Auth']);
$routes->get('/detail_efektivitas_unplanned/(:num)', 'User\EvaluasiEfektivitasUnplanned::DetailEfektivitas/$1', ['filter' => 'Auth']);
$routes->post('/data_evaluasiEfektivitas', 'User\EvaluasiEfektifitas::DataEvaluasiEfektivitas', ['filter' => 'Auth']);
$routes->get('/evaluasi_reaksi_member_unplanned', 'User\EvaluasiUnplanned::EvaluasiMember', ['filter' => 'Auth']);
$routes->post('/send_evaluasi_reaksi_unplanned', 'User\EvaluasiUnplanned::SendEvaluasiReaksi', ['filter' => 'Auth']);
$routes->post('/detail_evaluasi_member_unplanned', 'User\EvaluasiUnplanned::detailEvaluasiMember', ['filter' => 'Auth']);

//unplanned history
$routes->post('/detail_history_unplanned', 'User\HistoryUnplanned::detailHistoryMember', ['filter' => 'Auth']);

//contac Us
$routes->get('/contac_us', 'User\ContacUs::index', ['filter' => 'Auth']);
$routes->post('/send_massage', 'User\ContacUs::sendContact', ['filter' => 'Auth']);

//Request New Sylabus
$routes->get('/request_sylabus', 'User\ContacUs::requestSylabus', ['filter' => ['Auth','Role']]);
$routes->get('/get_competency_a/(:any)', 'User\ContacUs::getKompetensiAbyDept/$1', ['filter' => 'Auth']);
$routes->get('/get_competency_b/(:any)', 'User\ContacUs::getKompetensiBbyDept/$1', ['filter' => 'Auth']);
$routes->post('/submit_request_sylabus', 'User\ContacUs::submitForm', ['filter' => 'Auth']);
$routes->get('/detail_request_sylabus', 'Admin\C_Contact::getSilabus', ['filter' => 'Auth']);
$routes->post('/baca_silabus', 'Admin\C_Contact::bacaSilabus', ['filter' => 'Auth']);

$routes->get('/data_tna', 'Admin\C_Tna::DataTraining', ['filter' => ['Auth','Admin']]);
$routes->post('/delete_data_training', 'Admin\C_Tna::DeleteDataTraining', ['filter' => ['Auth','Admin']]);