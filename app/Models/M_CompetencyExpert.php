<?php

namespace App\Models;

use CodeIgniter\Model;

class M_CompetencyExpert extends Model
{
    protected $table      = 'competency_profile_expert';
    // protected $useAutoIncrement = true;
    protected $primaryKey = 'id_competency_expert';
    protected $allowedFields = ['id_user', 'npk', 'id_expert', 'score_expert','updated_at'];


    public function getIdExpert($id)
    {
        return $this->where(['id_user' => $id])->first();
    }

    public function getProfileExpertCompetency($id_user, $npk)
    {
        $this->select('expert_competency.expert,expert_competency.proficiency,competency_profile_expert.id_competency_expert,competency_profile_expert.score_expert,competency_profile_expert.updated_at')->where('competency_profile_expert.id_user', $id_user)->orWhere('competency_profile_expert.npk', $npk);
        $this->join('expert_competency', 'expert_competency.id_expert = competency_profile_expert.id_expert');
        return $this->get()->getResultArray();
    }

    public function getExpertByIdCompetency($id)
    {

        $this->select('expert_competency.expert,expert_competency.proficiency,competency_profile_expert.id_competency_expert,competency_profile_expert.score_expert')->where('competency_profile_expert.id_competency_expert', $id);
        $this->join('expert_competency', 'expert_competency.id_expert = competency_profile_expert.id_expert');
        return $this->get()->getResultArray();
    }

    public function getExpertIdCompetency($id_user,$npk)
    {
        $this->select('competency_profile_expert.id_expert')->where('competency_profile_expert.id_user', $id_user)->orWhere('competency_profile_expert.npk', $npk);
        $this->join('expert_competency', 'expert_competency.id_expert = competency_profile_expert.id_expert');
        return $this->get()->getResultArray();
    }

    public function getExpertById($id_expert,$npk)
    {
        $this->select()->where('id_expert', $id_expert)->where('npk',$npk);
        return $this->get()->getResultArray();
    }

    public function getExpertCompetencySkillMap(){
        $this->select('expert,npk,score_expert,proficiency,updated_at')
            ->join('expert_competency', 'expert_competency.id_expert = competency_profile_expert.id_expert')
            // ->where('competency_profile_expert.score_expert > 0')
            // ->where('competency_profile_expert.updated_at IS NOT NULL')
            ->orderBy('competency_profile_expert.npk', 'ASC');
        return $this->get()->getResultArray();
    }
}