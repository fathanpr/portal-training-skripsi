<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/png" href="<?= base_url('favicon3.ico') ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="<?= base_url() ?>login/fonts/icomoon/style.css">

        <link rel="stylesheet" href="<?= base_url() ?>login/css/owl.carousel.min.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>login/css/bootstrap.min.css">

        <!-- Style -->
        <link rel="stylesheet" href="<?= base_url() ?>login/css/style.css">
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>

        <title>Ganti Password</title>
    </head>

    <body>


        <div class="d-lg-flex half">
            <div class="contents order-1 order-md-2">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7">
                            <h3><strong>Change Password</strong></h3>
                            <p class="mb-4"></p>
                            <form action="<?= base_url() ?>submit_ganti_password" method="post">
                                <div class="form-group first">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" placeholder="Your Username" id="username"
                                        name="username" required>
                                </div>
                                <div class="form-group last mb-3">
                                    <label for="password">Last Password</label>
                                    <input type="password" class="form-control" placeholder="Your Password"
                                        id="password" name="password" required>
                                </div>
                                <div class="form-group last mb-3">
                                    <label for="new_password">New Password</label>
                                    <input type="password" class="form-control" placeholder="Your New Password"
                                        id="new_password" name="new_password" required>
                                        <span id="passwordErrors" style="color: red;"></span>
                                </div>
                                <div class="form-group last mb-3">
                                    <label for="verifiy_password">Enter New Password Again</label>
                                    <input type="password" class="form-control" placeholder="Your New Password Again"
                                        id="verifiy_password" name="verifiy_password" required>
                                    <span id="passwordError" style="color: red;"></span>
                                </div>

                                <input type="submit" value="Confirm" id="confirmButton" class="btn btn-block btn-primary">
                                <p class="text-center mt-3">Already have account ? <a href="<?= base_url() ?>">Login</a></p>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg order-2 order-md-1" style="background-image: url('<?= base_url() ?>login/images/CBI.JPG');"></div>

        </div>
        <script src="<?= base_url() ?>login/js/jquery-3.3.1.min.js"></script>
        <script src="<?= base_url() ?>login/js/popper.min.js"></script>
        <script src="<?= base_url() ?>login/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>login/js/main.js"></script>
        <script src="<?= base_url() ?>sweet/sweetalert2.all.min.js"></script>

        <script>
            document.getElementById('new_password').oninput = checkPasswords;
            document.getElementById('verifiy_password').oninput = checkPasswords;
            document.getElementById('confirmButton').disabled = true;

            function checkPasswords() {
                var newPassword = document.getElementById('new_password').value;
                var verifyPassword = document.getElementById('verifiy_password').value;
                var specialCharRegex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

                if (newPassword !== verifyPassword || newPassword === '' || verifyPassword === '') {
                    document.getElementById('new_password').style.borderColor = 'red';
                    document.getElementById('verifiy_password').style.borderColor = 'red';
                    document.getElementById('passwordError').textContent = 'Password doesn\'t match';
                    document.getElementById('confirmButton').disabled = true;
                } else if (newPassword.length < 5 || verifyPassword.length < 5) {
                    document.getElementById('new_password').style.borderColor = 'red';
                    document.getElementById('verifiy_password').style.borderColor = 'red';
                    document.getElementById('passwordErrors').textContent = 'Password must be at least 5 characters';
                    document.getElementById('confirmButton').disabled = true;
                } else if (specialCharRegex.test(newPassword) || specialCharRegex.test(verifyPassword)) {
                    document.getElementById('new_password').style.borderColor = 'red';
                    document.getElementById('verifiy_password').style.borderColor = 'red';
                    document.getElementById('passwordErrors').textContent = 'Password should not contain special characters';
                    document.getElementById('confirmButton').disabled = true;
                } else {
                    document.getElementById('new_password').style.borderColor = '';
                    document.getElementById('verifiy_password').style.borderColor = '';
                    document.getElementById('passwordError').textContent = '';
                    document.getElementById('passwordErrors').textContent = '';
                    document.getElementById('confirmButton').disabled = false;
                }

                if(newPassword == '' || verifyPassword == ''){
                    document.getElementById('confirmButton').disabled = true;
                    document.getElementById('passwordError').textContent = '';
                    document.getElementById('passwordErrors').textContent = '';
                }
            }

            $(document).ready(function() {
                $('form').on('submit', function(e) {
                    e.preventDefault();

                    $.ajax({
                        url: '<?= base_url() ?>submit_ganti_password',
                        type: 'post',
                        data: $(this).serialize(),
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status === 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: data.message,
                                }).then((result) => {
                                    window.location.href = '<?= base_url() ?>';
                                });
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error!',
                                    text: data.message,
                                })
                            }
                        },
                        error: function() {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error!',
                                text: "Failed to send request.",
                            })
                        }
                    });
                });
            });

        </script>

    </body>

</html>